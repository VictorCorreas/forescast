/********************************************
 * Author: Javier Lopez Andradas
 * Description: Class to controll the refresh of the Forecast Items and the view of forecast
 * 
 * 
 * <date>       <version>       <description>
 * ?????        1.0             Initial
 * 04/09/2018   2.0             Added without sharing, no Role hierachy shared
 * *****************************************/
public without sharing class FO_ActualizaForecast {
    /*****************************************
     * Author:  JAvier Lopez Andradas
     * Company: gCTIO
     * Description: Helper class to refresh all FOI data in a given Forecast 
     * 
     * <date>		<version>		<Description>
     * 12/04/2018	1.1				avoid the FOi of Opps in F6
     * 28/05/2018	2.0				Added GAM flow
     * 18/06/2018	2.1				When has a value and its modified modied the Forecast
     * 21/06/2018	2.2				JLA_Cambio NBullPointer
     * 09/08/2018	3.0				Changes cause FO_VE_ project
     * 15/10/2018   4.0             JLA_Changes casuse MNCs
     * 22/10/2018   4.1             JLA_Check if nulls the value
     * ***************************************/
    @AuraEnabled
    public static String actualizaFO(String idFore){
        System.debug('JLA___Start_actualizaFO-> '+idFore);
        //JLA 4.0 Also PArent Period for MNC!!
        FO_Forecast__c fore = [Select Id,FO_Estado__c,FO2_Monocurrency__c,FO2_Api__c,CurrencyIsoCode,FO_Periodo__r.BI_FVI_Activo__c,FO_Periodo__r.BI_FVI_Fecha_Inicio__c,FO_Periodo__r.BI_FVI_Fecha_Fin__c,
                                FO_Periodo__r.FO_Periodo_Padre__r.BI_FVI_Fecha_Inicio__c,FO_Periodo__r.FO_Periodo_Padre__r.BI_FVI_Fecha_Fin__c,FO_Nodo__c,RecordType.DeveloperName,
                                FO_Commit_Pipeline__c ,FO_Upside_Pipeline__c from FO_Forecast__c where Id=:idFore];
        System.debug('JLA___Fore '+fore);
        if(fore.FO_Estado__c.equals('Enviado') || fore.FO_Periodo__r.BI_FVI_Activo__c==false || fore.FO_Periodo__r.BI_FVI_Fecha_Fin__c< Date.today()){
            return 'OK';
        }
        
        List<CurrencyType> lst_cr = [Select IsoCode,ConversionRate from CurrencyType where isActive=true];
     	Map<String,double> mp_curr = new Map<String,double>();
    	for(CurrencyType curr:lst_cr)
        	mp_curr.put(curr.IsoCode,curr.ConversionRate);
        System.debug('No esta en Enviado');
        if(fore.RecordType.DeveloperName.equals('FO_Ejecutivo') || fore.RecordType.DEveloperName.equals('FO_MNC_Ejecutivo')){
            //Es de tipo semanal
            List<FO_Forecast_Item__c> lst_FOI = [select currencyIsoCode, Id,FO_Opportunity__c,FO_StageName__c,FO_NAV__c,FO2_NBAV_Budget__c,FO_Tipo_Forecast__c from FO_Forecast_Item__c where FO_Forecast_Padre__c=:idFore ];
            Map<String,FO_Forecast_Item__c> mp_FOI = new Map<String,FO_Forecast_Item__c>();
            for(FO_Forecast_Item__c foi:lst_FOI){
                mp_FOI.put(foi.FO_Opportunity__c,foi);
            }
            System.debug('Hemos sacado los FOI');
            List<BI_FVI_Nodo_Cliente__c> lst_ncl = [Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c=:fore.FO_Nodo__c ];
            List<String> lst_acc = new List<String>();
            for(BI_FVI_Nodo_Cliente__c ncl : lst_ncl)
                lst_acc.add(ncl.BI_FVI_IdCliente__c);
            System.debug('lista cuentas: '+lst_acc);
            //JLA_v5
            Date dtt_ini=null;
            Date dtt_fin = null;
            if(fore.RecordType.DeveloperName.equals('FO_MNC_Ejecutivo')){
                dtt_ini=fore.FO_Periodo__r.FO_Periodo_Padre__r.BI_FVI_Fecha_Inicio__c;
                dtt_fin=fore.FO_Periodo__r.FO_Periodo_Padre__r.BI_FVI_Fecha_Fin__c;
            }else{
                dtt_ini=fore.FO_Periodo__r.BI_FVI_Fecha_Inicio__c;
                dtt_fin=fore.FO_Periodo__r.BI_FVI_Fecha_Fin__c;
            }
            System.debug('Inicio: '+dtt_ini);
            System.debug('Fin: '+dtt_fin);
            Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
            List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
            List<String> val_def = new List<String>();
            for(Schema.PicklistEntry entry :values){
                if(entry.getValue().contains('Closed Lost')==true || entry.getValue().contains('Cancelled | Suspended') || entry.getValue().contains('F1') ==true ||  entry.getValue().contains('F6') ==true)
                    val_def.add(entry.getValue());
            }
            System.debug('StagesNames: '+val_def);
            System.debug('Hemos sacado los clientes');
            //JLA extracted BI_O4_Committed__c && BI_O4_Upside__c
            List<Opportunity> lst_Opp = [select currencyIsoCode,Id,StageName,CloseDate,FO_No_Valido_Forecast__c,BI_Net_annual_value_NAV__c,BI_O4_TGS_NBAV_Budget__c,
                                        BI_O4_Committed__c,BI_O4_Upside__c
                                        from Opportunity where 
                                        CloseDate>=:dtt_ini AND CloseDate<=:dtt_fin
                                        AND AccountId IN:lst_acc AND ( NOT StageName IN :val_def )AND REcordType.DeveloperName='BI_Ciclo_completo'];
            System.debug('lista Ops'+lst_Opp);
            System.debug('mp_foi'+mp_foi);
            Boolean modFore=false;
            List<FO_Forecast_Item__c> lst_upd = new List<FO_Forecast_Item__c>();
            List<FO_Forecast_Item__c> lst_ins = new List<FO_Forecast_Item__c>();
            List<FO_Forecast_Item__c> lst_del = new List<FO_Forecast_Item__c>();
            for(Opportunity opp : lst_Opp){
                //JLA_07/06/2018 -> camio para evitar contar las oportunidades ganadas no validas para el forecast
            	if(opp.FO_No_Valido_Forecast__c==true)
                	continue;
                FO_Forecast_Item__c foi = mp_foi.get(opp.Id);
                System.debug(foi);
                if(foi!=null){
                    System.debug('HEmos llegado dentro los FOI');
                    if(foi.FO_NAV__c!=opp.BI_Net_annual_value_NAV__c || foi.FO_StageName__c!=opp.StageName.subString(0,2) || foi.FO2_NBAV_Budget__c!=opp.BI_O4_TGS_NBAV_Budget__c){
                        Double valueFoi;
                        Double valueOpp;
                        boolean isChanged=false;
                        if(foi.FO_NAV__c!=opp.BI_Net_annual_value_NAV__c && (fore.FO2_Api__c==null || fore.FO2_Api__c.equals('BI_Net_annual_value_NAV__c') && foi.FO_Tipo_Forecast__c!=null)){
                            valueFoi=foi.FO_NAV__c;
                            valueOpp= opp.BI_Net_annual_value_NAV__c;
                            isChanged=true;
                        }else if(fore.FO2_Api__c!=null && foi.FO2_NBAV_Budget__c!=opp.BI_O4_TGS_NBAV_Budget__c &&  fore.FO2_Api__c.equals('BI_O4_TGS_NBAV_Budget__c') && foi.FO_Tipo_Forecast__c!=null){
                            valueFoi=foi.FO2_NBAV_Budget__c;
                            valueOpp= opp.BI_O4_TGS_NBAV_Budget__c;
                            isChanged=true;
                        }    
                        if(isChanged){
                           if(fore.FO2_Monocurrency__c==false){
                                valueFoi=valueFoi*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(foi.CurrencyIsoCode);
                                valueOpp=valueOpp*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(opp.CurrencyIsoCode);
                            }
                            //JLA_Cambio NBullPointer
                            if(foi.FO_Tipo_Forecast__c!=null){
                                if(foi.FO_Tipo_Forecast__c.equals('Upside')){
                                    fore.FO_Upside_Pipeline__c-=valueFoi;
                                    fore.FO_Upside_Pipeline__c+=valueOpp;
                                } else{
                                    fore.FO_Commit_Pipeline__c-=valueFoi;
                                    fore.FO_Commit_Pipeline__c+=valueOpp;                                
                                }
                            modFore=true;
                            } 
                        }
                        foi.FO_NAV__c=opp.BI_Net_annual_value_NAV__c;
                        foi.FO_StageName__c=opp.StageName.subString(0,2);
                        foi.FO2_NBAV_Budget__c=opp.BI_O4_TGS_NBAV_Budget__c;
                        lst_upd.add(foi);
                    }
                    mp_foi.remove(opp.Id);
                }else{
                    foi = new FO_Forecast_Item__c();
                    foi.FO_StageName__c=opp.StageName.subString(0,2);
                    foi.FO_CloseDate__c=opp.CloseDate;
                    foi.FO_NAV__c=opp.BI_Net_annual_value_NAV__c;
                    foi.FO_Opportunity__c=opp.Id;
                    foi.FO_Forecast_Padre__c=idFore;
                    foi.CurrencyIsoCode=opp.currencyIsoCode;
                    foi.FO2_NBAV_Budget__c=opp.BI_O4_TGS_NBAV_Budget__c;
                    if(fore.RecordType.DeveloperName.equals('FO_MNC_Ejecutivo')){
                        Double value;
                        if(fore.FO2_Api__c!=null && fore.FO2_Api__c.equals('BI_O4_TGS_NBAV_Budget__c')){
                            value =opp.BI_O4_TGS_NBAV_Budget__c;
                        }else{
                            value=opp.BI_Net_annual_value_NAV__c;
                        }
                        if(fore.FO2_Monocurrency__c==false){
                            value=value*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(opp.currencyIsoCode);
                        }
                        if(opp.BI_O4_Committed__c){
                            foi.FO_Tipo_Forecast__c='Commit';
                            //Calculamos los valores de lo que tenemos que poner como commit
                            if(value!=null)
                                fore.FO_Commit_Pipeline__c+=value;
                            modFore=true;

                        }else if(opp.BI_O4_Upside__c){
                            foi.FO_Tipo_Forecast__c='Upside';
                            if(value!=null)
                                fore.FO_Upside_Pipeline__c+=value;
                            modFore=true;
                        }    
                    }
                    
                    lst_ins.add(foi);
                    
                }
            }   
            if(modFore==true)
                update(fore);
            System.debug('lst_in'+lst_ins);
            if(lst_upd.isEmpty()==false){
                try{
                    update(lst_upd);
                }catch(Exception e){
                    return  Label.FO_Error_Update_FOI_New+e.getStackTraceString();
                }
           
            }
            if(lst_ins.isEmpty()==false){
                try{
                    insert(lst_ins);
                }catch(Exception e){
                    return Label.FO_New_Opps_FOI_refresh+e.getMessage();
                }
            }
            if(mp_foi.values().isEmpty()==false){
                for(String ops : mp_FOI.keySet()){
                    lst_del.add(mp_FOI.get(ops));
                }
                try{
                  delete(lst_del);
                }catch(Exception e){
                    return Label.FO_Deleted_Opps_FOI_refresh+e.getStackTraceString();
                }
            }
            
            return 'OK';
        }else{
            if(fore.RecordType.DeveloperName.equals('FO_VE_Overlay')){
                return 'OK';
            }
            //Es manager hya que buscar los Forecast hijos
            Map<Id,FO_Forecast__c> lst_sons = new Map<Id,FO_Forecast__c>([select Id,FO_Estado__c,FO_Forecast_Padre__c,FO_Commit_Pipeline__c,FO_Upside_Pipeline__c,CurrencyIsoCode  from FO_Forecast__c where FO_Nodo__r.BI_FVI_NodoPadre__c=:fore.FO_Nodo__c AND FO_Periodo__c=:fore.FO_Periodo__c]);
            List<FO_Forecast_Futuro__c> lst_futFo =  [Select Id,FO_Forecast_Padre__c,FO_Ajuste_Forecast_Manager__c,FO_Total_Forecast_Ajustado__c,FO_Periodo__c,CurrencyIsoCode FROM FO_Forecast_Futuro__c where (FO_Forecast_Padre__r.FO_Nodo__r.BI_FVI_NodoPadre__c=:fore.FO_Nodo__c  AND FO_Forecast_Padre__r.FO_Periodo__c = :fore.FO_Periodo__c) OR FO_Forecast_Padre__c=:fore.Id];
            Map<String,Map<String,FO_Forecast_Futuro__c>> mp_futFo = new Map<String,Map<String,FO_Forecast_Futuro__c>>();
            for(FO_Forecast_Futuro__c foFut:lst_futFo){
                Map<String,FO_Forecast_Futuro__c> mp_aux = mp_futFo.get(foFut.FO_Forecast_Padre__c);
                if(mp_aux==null)
                    mp_aux = new Map<String,FO_Forecast_Futuro__c> ();
                mp_aux.put(foFut.FO_Periodo__c,foFut);
                mp_futFo.put(foFut.FO_Forecast_Padre__c,mp_aux);
            }
            List<FO_Forecast__c> lst_upd = new List<FO_Forecast__c>();
            List<FO_Forecast_Futuro__c> lst_creFut = new List<FO_Forecast_Futuro__c>();
            Map<Id,FO_Forecast_Futuro__c> mp_updFut = new Map<Id,FO_Forecast_Futuro__c>();
            if(lst_sons.keySet().size()!=0){
                for(String key : lst_sons.keySet()){
                    FO_Forecast__c act = lst_sons.get(key);
                    
                    if(act.FO_Forecast_Padre__c != fore.Id){
                        
                        if(act.FO_Estado__c.equals('Enviado')){
                            
                            fore.FO_Commit_Pipeline__c +=act.FO_Commit_Pipeline__c*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(act.CurrencyIsoCode) ;
                            fore.FO_Upside_Pipeline__c+=act.FO_Upside_Pipeline__c*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(act.CurrencyIsoCode);
                            //No hace falta actualizar un padre porque no deberia tener otro padre != al actula a no ser que
                            //este sea nulo
                            Map<String,FO_Forecast_Futuro__c> mp_act = mp_futFo.get(act.Id);
                            Map<String,FO_Forecast_Futuro__c> mp_fore = mp_futFo.get(fore.Id);
                            for(String per : mp_act.keySet()){
                                FO_Forecast_Futuro__c futAct = mp_act.get(per);
                                FO_Forecast_Futuro__c futFore = mp_fore.get(per);
                              /* JLA code not neeeded if(futFore==null){
                                    futFore = new FO_Forecast_Futuro__c();
                                    futFore.FO_Ajuste_Forecast_Manager__c=futAct.FO_Ajuste_Forecast_Manager__c;
                                    futFore.FO_Total_Forecast_Ajustado__c=futAct.FO_Total_Forecast_Ajustado__c;
                                    futFore.FO_Periodo__c=per;
                                    futFore.FO_Forecast_Padre__c=fore.Id;
                                    mp_fore.put(per, futFore);
                                    lst_creFut.add(futFore);
                                }else{*/
                                    futFore.FO_Ajuste_Forecast_Manager__c+=futAct.FO_Ajuste_Forecast_Manager__c*mp_curr.get(futFore.CurrencyIsoCode)/mp_curr.get(futAct.CurrencyIsoCode);
                                    futFore.FO_Total_Forecast_Ajustado__c+=futAct.FO_Total_Forecast_Ajustado__c*mp_curr.get(futFore.CurrencyIsoCode)/mp_curr.get(futAct.CurrencyIsoCode);
                                    mp_updFut.put(futFore.Id,futFore);
                                //}
                            }
                        }
                        act.FO_Forecast_Padre__c = fore.Id;
                        lst_upd.add(act);
                    }
                    
                }
                lst_upd.add(fore);
                
                System.debug('JLA mp actualizar: '+lst_upd);
                if(lst_upd.isEmpty()==false){
                    update(lst_upd);
                }
                System.debug('JLA crea los futuros que no esten creados');
                if(lst_creFut.isEmpty()==false){
                   	insert (lst_creFut);    
                }
                if(mp_updFut.values().isEmpty()==false){
                    update(mp_updFut.values());
                }
                //JLA_FO_VE_Project    
                if(fore.RecordType.DeveloperName.equals('FO_VE_Overlay_Manager')){
                    System.debug('JLA__ES_Overlay Manager');
                	List<FO_Snapshot_Pipeline__c> lst_pipeSon = [Select Id,FO_Period__c,FO_Stage__c,FO_Value__c,FO_Forecast_Padre__c from FO_Snapshot_Pipeline__c where FO_Forecast_Padre__c in :lst_sons.keySet() OR FO_Forecast_Padre__c = :fore.Id ];
                    List<FO_Snapshot_Actuals__c> lst_actSon = [Select Id,FO_Periodo__c,FO_Forecast_Padre__c,FO_Value__c from FO_Snapshot_Actuals__c where FO_FOrecast_Padre__c in :lst_sons.keySet() OR FO_Forecast_Padre__c = :fore.Id];
                    System.debug('JLA__Pipeline->'+lst_pipeSOn);
                    System.debug('JLA__Actuals->'+lst_actSon);
                    if(lst_pipeSon.isEmpty()==false){
                        //Empezamos ajugar
                        //primero tenemos que ordenar esto por mapas para sumar lo que haya que sumar
                        //Map{FORECAST,{PEriodo,{Stage,NAme}}}
                        Map<String,Map<String,Map<String,FO_Snapshot_Pipeline__c>>> mp_sons = new Map<String,Map<String,Map<String,FO_Snapshot_Pipeline__c>>>();
                        for(FO_Snapshot_Pipeline__c snp : lst_pipeSon){
                            Map<String,Map<String,FO_Snapshot_Pipeline__c>> mp_aux = mp_sons.get(snp.FO_Forecast_Padre__c);
                            if(mp_aux==null)
                                mp_aux = new Map<String,Map<String,FO_Snapshot_Pipeline__c>>();
                            Map<String,FO_Snapshot_Pipeline__c> aux = mp_aux.get(snp.FO_Period__C);
                            if(aux==null)
                                aux = new Map<String,FO_Snapshot_Pipeline__c>();
                            aux.put(snp.FO_Stage__c,snp);
                            mp_aux.put(snp.FO_Period__c,aux);
                            mp_sons.put(snp.FO_Forecast_Padre__c,mp_aux);
                        }
                        System.debug('JLA__mp_sons->'+mp_sons);
                        //SUMA de todos los valores de los Forecast hijos {PERIODO,{STAGE,VALOR}}
                        Map<String,Map<String,Decimal>> mp_Totsons = new Map<String,Map<String,Decimal>>();
                        for(String idForeSon : lst_sons.keySet()){
                            Map<String,Map<String,FO_Snapshot_Pipeline__c>> mp_aux = mp_sons.get(idForeSon);
                            if(mp_aux!=null){
                                for(String per :mp_aux.keySet()){
                                    //{STAGE;VALOR}
                                    Map<String,Decimal> mp_SUM = mp_Totsons.get(per);
                                    Map<String,FO_Snapshot_Pipeline__c> mp_alone= mp_aux.get(per);
                                    if(mp_SUM==null){
                                        mp_SUM=new Map<String,Decimal>();
                                    }
                                    for(String stage:mp_alone.keySet()){
                                        if(mp_SUM.get(stage)==null){
                                            mp_SUM.put(stage,mp_alone.get(stage).FO_Value__c);
                                        }else{
                                            mp_SUM.put(stage,mp_SUM.get(stage)+mp_alone.get(stage).FO_Value__c);
                                        }
                                    }
                                    mp_Totsons.put(per,mp_SUM);
                                }  
                            }
                        }
                        //Ya tenemos el resultado de sumar todos los pipes
                        List<FO_Snapshot_Pipeline__c> lst_snpIns = new List<FO_Snapshot_Pipeline__c> ();
                        List<FO_Snapshot_Pipeline__c> lst_snpUpd = new List<FO_Snapshot_Pipeline__c> ();
                        List<FO_Snapshot_Pipeline__c> lst_snpDel = new List<FO_Snapshot_Pipeline__c> ();
                        if(mp_sons.get(fore.Id)==null){
                            //hay que crear todos los Snapshots
                            for(String per : mp_Totsons.keySet()){
                                for(String stage: mp_Totsons.get(per).keySet()){
                                    FO_Snapshot_Pipeline__c snap = new FO_Snapshot_Pipeline__c();
                                    snap.FO_Value__c=mp_Totsons.get(per).get(stage);
                                    snap.FO_Stage__c=stage;
                                    snap.FO_Period__c=per;
                                    snap.FO_Forecast_Padre__c=fore.Id;
                                    lst_snpIns.add(snap);
                                }
                            }
                        }else{
                            Map<String,Map<String,FO_Snapshot_Pipeline__c>> mp_foreSnap = mp_sons.get(fore.Id);
                            for(String per : mp_foreSnap.keySet()){
                                if(mp_Totsons.get(per)==null){
                                    //Hay que elimnar todos estos del de forecast                     
                                    lst_snpDel.addAll(mp_foreSnap.get(per).values());
                                }else{
                                    Map<String,Decimal> mp_StageSons = mp_Totsons.get(per);
                                    Map<String,FO_Snapshot_Pipeline__c> mp_stageFore = mp_foreSnap.get(per);
                                    for(String stage : mp_stageFore.keySet()){
                                        FO_Snapshot_Pipeline__c snap = mp_stageFore.get(stage);
                                        if(mp_StageSons.get(stage)==null)
                                            lst_snpDel.add(snap);
                                        else{
                                            if(snap.FO_Value__c!=mp_StageSons.get(stage)){
                                                snap.FO_Value__c=mp_StageSons.get(stage);
                                                lst_snpUpd.add(snap);
                                            }
                                        }   
                                    }
                                    for(String stage : mp_StageSons.keySet()){
                                        if(mp_stageFore.get(stage)==null){
                                            FO_Snapshot_Pipeline__c snap = new FO_Snapshot_Pipeline__c();
                                    		snap.FO_Value__c=mp_StageSons.get(stage);
                                    		snap.FO_Stage__c=stage;
                                    		snap.FO_Period__c=per;
                                            snap.FO_Forecast_Padre__c=fore.Id;
                                    		lst_snpIns.add(snap);
                                        }
                                    }
                                }
                            }
                        }
                        if(lst_snpIns.isEmpty()==false)
                            insert(lst_snpIns);
                        if(lst_snpUpd.isEmpty()==false)
                            update(lst_snpUpd);
                        if(lst_snpDel.isEmpty()==false)
                            delete(lst_snpDel);
                    }
                    if(lst_actSon.isEmpty()==false){
                        //AL igual que el otro tenemos que ordenar esta basura
                        //{FORECAST,{PERIDO,SNAP}}
                        Map<String,Decimal> mp_sons = new Map<String,Decimal>();
                        Map<String,FO_Snapshot_Actuals__c> mp_SnpFore = new Map<String,FO_Snapshot_Actuals__c>();
                        for(FO_Snapshot_Actuals__c snp : lst_actSon){
                            if(snp.FO_Forecast_Padre__c.equals(fore.Id)){
                                mp_SnpFore.put(snp.FO_Periodo__c,snp);
                            }else{
                                if(mp_sons.get(snp.FO_Periodo__c)==null){
                                    mp_sons.put(snp.FO_Periodo__c,snp.FO_Value__c);
                                }else{
                                    mp_sons.put(snp.FO_Periodo__c,mp_sons.get(snp.FO_Periodo__c)+snp.FO_Value__c);
                                }
                            }
                        }
                        List<FO_Snapshot_Actuals__c> lst_snpIns = new List<FO_Snapshot_Actuals__c>();
                        List<FO_Snapshot_Actuals__c> lst_snpUpd = new List<FO_Snapshot_Actuals__c>();
                        List<FO_Snapshot_Actuals__c> lst_snpDel = new List<FO_Snapshot_Actuals__c>();
                        if(mp_sons.values().isEmpty()==true){
                            lst_snpDel.addAll(mp_SnpFore.values());
                        }else{
                            if(mp_SnpFore.values().isEmpty()==false){
                            	for(String per : mp_SnpFore.keySet()){
                                	if(mp_sons.get(per)==null)
                                        lst_snpDel.add(mp_SnpFore.get(per));
                                    else{
                                        FO_Snapshot_Actuals__c act = mp_SnpFore.get(per);
                                        if(act.FO_Value__c!=mp_sons.get(per)){
                                            act.FO_Value__c=mp_sons.get(per);
                                            lst_snpUpd.add(act);
                                        }
                                    }
                            	}    
                            }
                            for(String per :mp_sons.keySet()){
                                if(mp_SnpFore.get(per)==null){
                                    FO_Snapshot_Actuals__c act = new FO_Snapshot_Actuals__c();
                                    act.FO_Periodo__c=per;
                                    act.FO_Value__c=mp_sons.get(per);
                                    act.FO_Forecast_Padre__c=fore.Id;
                                    lst_snpIns.add(act);
                                }
                            }
                            
                        }
                        if(lst_snpIns.isEmpty()==false){
                            insert(lst_snpIns);
                        }
                        if(lst_snpUpd.isEmpty()==false){
                            update(lst_snpUpd);
                        }
                        if(lst_snpDel.isEmpty()==false){
                            delete(lst_snpDel);
                        }
                    }
            	}
            }
            
            
            return 'OK';
        }
        
    }
	 
}