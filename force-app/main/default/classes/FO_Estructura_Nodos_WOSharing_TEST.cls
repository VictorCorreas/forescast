/*******************************************
 * Author: Javier Lopez Andradas
 * Company:gCTIO
 * Description: Coverage the class with the same name
 * 
 * 
 * ********************************************/
@isTest
public class FO_Estructura_Nodos_WOSharing_TEST {
    @isTest
    public static void test1(){
        TGS_User_Org__c usr_org = new TGS_User_Org__c();
        usr_org.TGS_Is_BI_EN__c=true;
        usr_org.SetupOwnerId=UserInfo.getUserId();
        insert usr_org;
        String IdRT = [Select Id from RecordType where DeveloperName = 'BI_FVI_NCS' limit 1].Id;
        BI_FVI_NOdos__c nd1 = new BI_FVI_Nodos__c();
        nd1.RecordTypeId=IdRT;
        nd1.BI_FVI_Pais__c='Spain';
        Database.SaveResult sv = Database.insert(nd1);
        BI_FVI_Nodos__c nd2 = new BI_FVI_Nodos__C();
        nd2.RecordTypeId=IdRT;
        nd2.BI_FVI_Pais__c= 'Spain';
        nd2.BI_FVI_NodoPadre__c=sv.getId();
        insert(nd2);
        BI_FVI_Nodos__c nd3 = new BI_FVI_Nodos__C();
        nd3.RecordTypeId=IdRT;
        nd3.BI_FVI_Pais__c= 'Spain';
        Database.SaveResult svr3 = Database.insert(nd3);
        nd1.Id=sv.getId();
        nd1.BI_FVI_NodoPadre__c=svr3.getId();
        update(nd1);
    }
}