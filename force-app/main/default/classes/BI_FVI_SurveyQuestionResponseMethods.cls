/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   Controller class to Trigger SurveyTaker__c  
    Test Class:    BI_FVI_SurveyTakerMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    01/06/2016           	Geraldine P. Montes     Initial Version
   	15/03/2018              Humberto Nunes          Se agrego el Tipo de Registro 
    -------------------------------------------------------------------------------*/ 

	public with sharing class BI_FVI_SurveyQuestionResponseMethods {

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_Registro_Datos_Desempeno__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

	public static void CreateRegistroDesemp(List<SurveyQuestionResponse__c> lstNewRecords)
	{
		set<Id> setIdSQR = new set<Id>();
		for(SurveyQuestionResponse__c objNewRecords : lstNewRecords)
		{
			if(objNewRecords != null)
			{
				setIdSQR.add(objNewRecords.Id); 
			}
		}

		list<SurveyQuestionResponse__c> lstSQR = [select Response__c, SurveyTaker__c, SurveyTaker__r.BI_Cliente__c,CreatedDate from SurveyQuestionResponse__c where Survey_Question__r.BI_FVI_CSI_Question__c = true and Id In :setIdSQR];
		
		List<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new List<BI_Registro_Datos_Desempeno__c>();

		for(SurveyQuestionResponse__c objSQR : lstSQR)
		{
			if(objSQR != null) 
			{
				BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
				Datetime dtConvertDT = objSQR.CreatedDate;	
				objRegDatosDesemp.BI_FVI_Fecha__c = date.newinstance(dtConvertDT.year(), dtConvertDT.month(), dtConvertDT.day());
				objRegDatosDesemp.BI_FVI_Id_Cliente__c = objSQR.SurveyTaker__r.BI_Cliente__c;
				objRegDatosDesemp.BI_Tipo__c = 'ISC';
				objRegDatosDesemp.BI_FVI_Valor__c = decimal.valueof(objSQR.Response__c);
				objRegDatosDesemp.BI_FVI_IdObjeto__c = objSQR.SurveyTaker__c;
				objRegDatosDesemp.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
				lstRegDatosDesemp.add(objRegDatosDesemp);

			}
		}
		try {

			insert lstRegDatosDesemp;
			system.debug('lstRegDatosDesempNew----->>' +lstRegDatosDesemp);
			
		} catch(Exception e) {
			System.debug(e.getMessage());
		}
	}
}