/*******************************************
 * Author: Javier Lopez Andradas
 * Company: gCTIO
 * Description:  Class to manage the custom component for Node estructure
 * 
 * 
 * 
 * <date>		<version>		<description>		<author>
 * 12/06/2018	1.0				Intial version		Javier Lopez Andradas
 * ******************************************/
public with sharing class BI_FVI_Estr_ActivaNodo	 {
	/**********************************************************************
	 * Author: Javier Lopez Andradas
	 * Company:  gCTIO
	 * Description: Method that manage the closed opps for a given user
	 * 
	 * <date>		<version>		<descripion>	<author>
	 * 12/06/2018	1.0				Initial version	Javier Lopez Andradas     
	 * *******************************************************************/
	@AuraEnabled
	public static void changeClosedOpps(String idNode,String ownerId,String query,String mode,String nombreNodo){
		if(mode.equals('Account-')==true){
			BI_FVI_Nodos__c nd = new BI_FVI_Nodos__c();
			nd.Id=idNode;
			nd.BI_FVI_Activo__c=true;
			nd.BI_Estruct_Estado_sincronizacion_cuentas__c='ENVIADO';
			update(nd);
			List<BI_FVI_Nodo_Cliente__c> lst_vdn = [Select Id,BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__C where BI_FVI_IdNOdo__c=:idNode AND BI_FVI_Exclusivo__c=null];
			if(lst_vdn.isEmpty()==false){
				for(BI_FVI_Nodo_Cliente__c vdn :lst_vdn){
					vdn.BI_FVI_Exclusivo__c=vdn.BI_FVI_IdCliente__c;
				}    
				update(lst_vdn);
			}
			
			
		}else if(mode.equals('Opportunity-')){
			BI_FVI_Nodos__c nd = new BI_FVI_Nodos__c();
			nd.Id=idNode;
			nd.BI_Estruct_Estado_sincronizacion_opp__c='ENVIADO';
			update(nd);
		}else if(mode.equals('Case-')){
			BI_FVI_Nodos__c nd = new BI_FVI_Nodos__c();
			nd.Id=idNode;
			nd.BI_Estruct_Estado_sincronizacion_casos__c='ENVIADO';
			update(nd);
		}
		BI_FVI_Estr_BatchClass btch = new BI_FVI_Estr_BatchClass(query,mode,ownerId,idNode,nombreNodo,null);
		Database.executeBatch(btch, 10);
	}
	
	/**********************************************************************
	 * Author: Javier Lopez Andradas
	 * Company:  gCTIO
	 * Description: Method that manage the closed opps for a given user
	 * 
	 * <date>		<version>		<descripion>	<author>
	 * 12/06/2018	1.0				Initial version	Javier Lopez Andradas
	 * *******************************************************************/
	@AuraEnabled
	public static List<String> retrieveActualOwners(String idNode){
		List<Account> lst_acc = [Select OwnerId from Account where Id in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c=:idNode)];
		List<String> lst_owners = new List<String> ();
		for(Account acc : lst_acc){
			lst_owners.add(acc.OwnerId);
		}
		return lst_owners;
	}
	/********************************************************************
	 * Author: Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: Activate/Deactivate a node all OpportunityTeamMembers of a node
	 * 
	 * 
	 * <date>		<version>		<description>
	 * 14/06/2018	1.0				initial
	 * 10/07/2018	N/A				DEPRECATED
	 * ******************************************************************/
/*    @AuraEnabled
	public static String activateNode(String idNode,boolean active,String mode,String ownerId){
		BI_FVI_Nodos__c node = new BI_FVI_Nodos__c(Id=idNode,BI_FVI_activo__c=active);
		try{
			update(node);
		}catch(Exception e){
			return 'ERROR: '+e.getStackTraceString();
		}
		if(active==true){
			BI_FVI_Estr_BatchClass btch = new BI_FVI_Estr_BatchClass('Select Id from Account where Id in (Select BI_FVI_IdCliente__C from BI_FBI_Nodo_Cliente__c where BI_FVI_IdNodo__c =\''+idNode+'\')',mode,ownerId,idNode);
			Database.executeBatch(btch);    
		}
		
		return 'OK';
	}*/
	/********************************************************************
	 * Author: Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: Delete all OpportunityTeamMembers of a node
	 * 
	 * 
	 * <date>		<version>		<description>
	 * 14/06/2018	1.0				initial
	 * 10/07/2018	N/A				DEPRECATED
	 * ******************************************************************/
   /* @AuraEnabled
	public static String deleteOppTeam(String idNode){
		  //Cambiada necesitamos al menos 2 queries  List<OpportunityTeamMember> lst_oppTeam = [Select Id from OpportunityTeamMember where opportunity.AccountId in (select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c =:idNode)];
		List<Opportunity> lst_opp = [Select Id from Opportunity where AccountId in (select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c =:idNode)];
		if(lst_opp.isEmpty()==false){
			List<OpportunityTeamMember> lst_oppteam = [Select Id from OpportunityTeamMember where OpportunityId in :lst_opp];
			try{
				delete(lst_oppTeam);
			}catch(Exception e){
				return 'ERROR: '+e.getStackTraceString();
			}    
		}
		return 'OK';
	}*/
	/********************************************************************
	 * Author: Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: Delete all AccountTeamMember of a node
	 * 
	 * 
	 * <date>		<version>		<description>
	 * 14/06/2018	1.0				initial
	 * 07/10/2018	N/A				DEPRECATED
	 * ******************************************************************/
	/*@AuraEnabled
	public static String deleteAccTeam(String idNode){      
		List<AccountTeamMember> lst_accte = [Select Id from AccountTeamMember where AccountId in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c =:idNode)];
		if(lst_accte.isEmpty()==false){
			try{
				delete(lst_accte);
			}catch(Exception e){
				return 'ERROR: '+e.getStackTraceString();
			}
				 
		}
		return 'OK';
	}*/
	/*********************************************
	 * Author : JAvier Lopez Andradas
	 * Comapny: gCTIO
	 * Description: Retrieves the values of the ppicklist of OppsAccess in AccountTeamMember
	 * 
	 * <date>			<version>		<description>
	 * 20/06/2018		1.0				Initial
	 * 
	 * *********************************************/
	@AuraEnabled
	public static List<PickValues> getOppAcc (){
		List<PickValues> lst_pick = new List<PickValues>();
		Schema.DescribeFieldResult lst_ops = AccountShare.OpportunityAccessLevel.getDescribe();
		List<Schema.PicklistEntry>  access = lst_ops.getPicklistValues();
		for(Schema.PicklistEntry valorActual : access){
			lst_pick.add(new PickValues(valorActual.getLabel(),valorActual.getValue()));
		}
		return lst_pick;
	}
	 /*********************************************
	 * Author : JAvier Lopez Andradas
	 * Comapny: gCTIO
	 * Description: Retrieves the values of the ppicklist of ContactAccess in AccountTeamMember
	 * 
	 * <date>			<version>		<description>
	 * 20/06/2018		1.0				Initial
	 * 10/07/2018		N/A			DEPRECATED
	 * *********************************************/
  /*  @AuraEnabled
	public static List<PickValues> getContactAcc (){
		List<PickValues> lst_pick = new List<PickValues>();
		Schema.DescribeFieldResult lst_ops = AccountShare.ContactAccessLevel.getDescribe();
		List<Schema.PicklistEntry>  access = lst_ops.getPicklistValues();
		for(Schema.PicklistEntry valorActual : access){
			lst_pick.add(new PickValues(valorActual.getLabel(),valorActual.getValue()));
		}
		return lst_pick;
	}*/
	/*********************************************
	 * Author : JAvier Lopez Andradas
	 * Comapny: gCTIO
	 * Description: Retrieves the values of the ppicklist of CaseAccess in AccountTeamMember
	 * 
	 * <date>			<version>		<description>
	 * 20/06/2018		1.0				Initial
	 * 
	 * *********************************************/
	@AuraEnabled
	public static List<PickValues> getCasAcc (){
		List<PickValues> lst_pick = new List<PickValues>();
		Schema.DescribeFieldResult lst_ops = AccountShare.CaseAccessLevel.getDescribe();
		List<Schema.PicklistEntry>  access = lst_ops.getPicklistValues();
		for(Schema.PicklistEntry valorActual : access){
			lst_pick.add(new PickValues(valorActual.getLabel(),valorActual.getValue()));
		}
		return lst_pick;
	}
	/*********************************************
	 * Author : JAvier Lopez Andradas
	 * Comapny: gCTIO
	 * Description: Retrieves the values of the ppicklist of AccounAccess in AccountTeamMember
	 * 
	 * <date>			<version>		<description>
	 * 20/06/2018		1.0				Initial
	 * 
	 * *********************************************/
	@AuraEnabled
	public static List<PickValues> getAccAcc (){
		List<PickValues> lst_pick = new List<PickValues>();
		Schema.DescribeFieldResult lst_ops = AccountShare.AccountAccessLevel.getDescribe();
		List<Schema.PicklistEntry>  access = lst_ops.getPicklistValues();
		for(Schema.PicklistEntry valorActual : access){
			lst_pick.add(new PickValues(valorActual.getLabel(),valorActual.getValue()));
		}
		return lst_pick;
	}
	/*********************************************
	 * Author : JAvier Lopez Andradas
	 * Comapny: gCTIO
	 * Description: Retrieves the values of the ppicklist of Role in AccountTeamMember
	 * 
	 * <date>			<version>		<description>
	 * 20/06/2018		1.0				Initial
	 * 
	 * *********************************************/
	@AuraEnabled
	public static List<PickValues> getRoles (){
		List<PickValues> lst_pick = new List<PickValues>();
		lst_pick.add(new PickValues(null,null)); // Agregar valor nulo - eHelp 04683887
		Schema.DescribeFieldResult lst_ops = AccountTeamMember.TeamMemberRole.getDescribe();
		List<Schema.PicklistEntry>  access = lst_ops.getPicklistValues();
		for(Schema.PicklistEntry valorActual : access){
			lst_pick.add(new PickValues(valorActual.getLabel(),valorActual.getValue()));
		}
		return lst_pick;
	}
	/**********************************
	 * Author :Javier Lopez
	 * Comapny: gCTIO
	 * Description: Auxiuliar class to retrieve the values of picklist
	 * 
	 * 
	 * 
	 * **********************************/
	public class PickValues{
		@AuraEnabled public String value;
		@AuraEnabled public String label;
		PickValues(String label,String value){
			this.value=value;
			this.label=label;
		}
	}
	/**************************************
	 * Author: Javier Lopez
	 * Company: gCTIO
	 * Description: Activate the node of type NCS
	 * 
	 * <date>		<version>		<description>
	 * 21/06/2018	1.0				Initial
	 * 10/07/2018	1.1				All value for AccountAccess is not allowed for update or create
	 * 30/07/2018	1.2				Changes for update all AccountShare related to the Account Team created
	 * 13/11/2018	1.3				JLA-changes for avoid to update more than the accountShare configurated
	 * 19/11/2018	2.0				JLA-to Batch
	 * ****************************************/
	@AuraEnabled
	public static String activateNode(String Idnodo,String ownerId,String AccOpp,String AccCase,String AccAcc,String Role){
		System.debug('JLa_START_ActivateNode');
		BI_FVI_Nodos__c nd = new BI_FVI_Nodos__C();
		nd.Id=Idnodo;
		nd.BI_FVI_Activo__c=true;
		try{
			update(nd);
		}catch(Exception e){
			return e.getStackTraceString();
		}
		String query = 'Select BI_FVI_IdCliente__c from BI_FVI_Nodo_cliente__c where BI_FVI_IdNodo__c = \''+Idnodo+'\'';
		Map<String,String> mp_params = new Map<String,String>();
		mp_params.put('Role',Role);
		mp_params.put('AccOpp',AccOpp);
		mp_params.put('AccCase',AccCase);
		mp_params.put('AccAcc',AccAcc);
		BI_FVI_Estr_BatchClass btch = new BI_FVI_Estr_BatchClass(query,'AccountTeam',ownerId,Idnodo,Idnodo,mp_params);
		Database.executeBatch(btch, 10);
		/* JLA_to batch to avoid row limits 
		List<BI_FVI_Nodo_Cliente__C> lst_vdn = [select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c =:Idnodo];
		System.debug('ActivateNode: clientes'+lst_vdn);
		if(lst_vdn.isEmpty()==false){
			List<AccountTeamMember> lst_acctm = new List<AccountTeamMember>();
			//List<AccountShare> lst_accSh = new List<AccountShare>();
			List<String> lst_acc = new List<String>();
			for(BI_FVI_Nodo_Cliente__c ndv : lst_vdn){
				AccountTeamMember act = new AccountTeamMember();
				act.AccountId=ndv.BI_FVI_IdCliente__c;
				act.UserId=ownerId;
				act.TeamMemberRole=Role;
				lst_acctm.add(act);
				lst_acc.add(ndv.BI_FVI_IdCliente__c);
				
			}
			 System.debug('ActivateNode: Acctm'+lst_acctm);
			
			try{
				insert(lst_acctm);
			}catch(Exception e){
				System.debug(e);
				database.rollback(sp);
				return 'ERROR: insertando el equipo de cliente: '+e.getStackTraceString();
			}
			//JLA_V1_2 :Changed over AccSh
			List<AccountShare> lst_accSh =[Select Id,AccountId from AccountShare where userorgroupid=:OwnerId AND AccountId in :lst_acc AND LastModifiedDate=TODAY AND LastModifiedByid=:UserInfo.getUserId() AND RowCause ='Team' ];
			System.debug('JLA_Numero de AccountShare: '+lst_accSh.size());
			Map<String,AccountShare> mp_acc = new Map<String,AccountShare>();
			for(AccountShare accSh :lst_accSh){
				mp_acc.put(accSh.AccountId,accSh);
			}
			for(String acc :mp_acc.keySet()){
				AccountShare accSh = mp_acc.get(acc);
				accSh.OpportunityAccessLevel=AccOpp;
				if(AccAcc.equals('All')==false)
					accSh.AccountAccessLevel=AccAcc;
				else
					accSh.AccountAccessLevel='Read';
				accSh.CaseAccessLevel=AccCase;
				//accSh.ContactAccessLevel=AccCont;
			}
			
			System.debug('ActivateNode: AccSh'+mp_acc.values());
			try{
				 update(mp_acc.values());
			}catch(Exception e){
				System.debug(e);
				database.rollback(sp);
				return 'ERROR: insertando la colaboración en las cuentas: '+e.getStackTraceString();
			}
		}
		*/
		return 'OK';
	}
	/**************************************
	 * Author: Javier Lopez
	 * Company: gCTIO
	 * Description: Activate the node of type NCS
	 * 
	 * <date>		<version>		<description>                                                                           <author>
	 * 21/06/2018	1.0				Initial                                                                                 Javier Lopez
	 * 05/07/2018	1.1				Added the function and deleted AccountShare it will be deleted with de AccTeam          Javier Lopez
	 * 04/04/2019   1.2				Seteo en null el campo estado de los vdn (ncp y ncs) y el campo nodo del cliente (ncp)  Pablo Belzunce
	 * ****************************************/
	@AuraEnabled
	public static String deactivateNode(String Idnodo,String ownerId,String recordTypeId,String funcion){
		BI_FVI_Nodos__c nd = new BI_FVI_Nodos__C();
		nd.Id=Idnodo;
		nd.BI_FVI_Activo__c=false;
        //Comentado try catch ya que al entrar al catch se devuelve en el callback un success y no está bien manejado del lado del cliente
		//try{
			update(nd);
		//}catch(Exception e){
		//	return e.getStackTraceString();
		//}
		
		List<BI_FVI_Nodo_Cliente__c> lst_vdn = [Select Id, BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c = :Idnodo];
		//Seteo el estado del vdn en null para ncp y ncs
		System.debug(lst_vdn);
    	for(BI_FVI_Nodo_Cliente__c vdn : lst_vdn) {
    	    vdn.BI_Estruct_Estado__c = null;
    	}
    	update lst_vdn;

		String nameRT=Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
		if(nameRT.equals('BI_FVI_NCS')==false){
		    //Lista de clientes para setear en null el campo Nodo del cliente (solo para ncp)
		    List<String> lst_accId = new List<String>();

			for(BI_FVI_Nodo_Cliente__c vdn :lst_vdn){
				vdn.BI_FVI_Exclusivo__c=null;
    	        lst_accId.add(vdn.BI_FVI_IdCliente__c);
			}
			//update(lst_vdn);
			
			//Seteo en null el campo Nodo del cliente
			
			List<Account> lst_acc = [Select Id, BI_FVI_Nodo__c From Account Where Id in : lst_accId];
        	for(Account acc : lst_acc) acc.BI_FVI_Nodo__c = null;
        	update lst_acc;
			
            update(lst_vdn);
            
			return 'OK';
		}
		List<AccountTeamMember> lst_acctm  = [Select Id from AccountTeamMember where AccountId in  (Select BI_FVI_IdCliente__C from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c=:Idnodo) ANd UserId=:ownerId AND TeamMemberRole =:funcion];
		if(lst_acctm.isEmpty()==false){
			try{
				delete(lst_acctm);
			}catch(Exception e){
				return 'ERROR: eliminando el usuario del equipo de cuentas '+e.getStackTraceString();
			}
		}
	 /*JLA_V1.1   List<AccountShare> lst_accSh = [Select Id from AccountShare where AccountId in  (Select BI_FVI_Idcliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_Idnodo__c =:Idnodo) AND userorgroupid=:ownerId ];
		if(lst_accSh.isEmpty()==false){
			try{
				delete(lst_accSh);
			}catch(Exception e){
				return 'ERROR: eliminando el usuario de la colaboracion de cuentas '+e.getStackTraceString();
			}
		}
*/            
		
		return 'OK';
	}
	/***************************************
	 * Author: Javier Lopez Andradas
	 * Company :gCTIO
	 * Description: Retrieves BI_FVI_Nodo_Cliente__C from the account vinculated to a node NCP active
	 * 
	 * <date>		<version>		<description>
	 * 28/06/2018	1.0				Initial
	 * 16/08/2018	1.1				Added isEmpty in cases of no Accounts vinculated to this node
	 * ****************************************/
	@AuraEnabled
	public static List<BI_FVI_Nodo_cliente__c> getVDN(String idNodo){
		//Necesaria dos queries sobre el mismo objeto ya que SF no te permite hacer una "inner query" sobre le mismo objeto duh!
		List<BI_FVI_Nodo_Cliente__c> lst_vdn = [Select BI_FVI_IdCliente__C from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c=:idNodo];
		List<String> lst_acc = new List<String>();
		if(lst_vdn.isEmpty()==true)
			return null;
		for(BI_FVI_Nodo_Cliente__c vdn :lst_vdn){
			lst_acc.add(vdn.BI_FVI_IdCliente__c);
		}
		return[Select Id,BI_FVI_IdCliente__c,BI_FVI_IdCliente__r.Name,BI_FVI_idNodo__c,BI_FVI_idNodo__r.Name from BI_FVI_Nodo_Cliente__c where BI_FVI_IdCliente__c in :lst_acc AND ((BI_FVI_IdNodo__r.BI_FVI_Activo__c=true AND BI_FVI_IdNodo__r.RecordType.DeveloperNAme='BI_FVI_NCP') OR (BI_FVI_IdNodo__r.RecordType.DeveloperNAme IN ('BI_FVI_NCO','BI_FVI_NCG')) )];
	}
	/*******************************************
	@Author : JAvier Lopez Andradas
	@Company: gCTIO
	@Description: Check if the user has the right to ADM the node

	<date>			<version>		<description>
	30/08/2018		1.0				Initial description
	**********************************************/
	@AuraEnabled
	public static boolean chechADM(){
		List<PermissionSetAssignment> lst = [Select Id from PermissionSetAssignment where PermissionSet.Name='BI_FVI_ADM_Estruct_Nodos' AND AssigneeID =:UserInfo.getUserId()];
		if(lst.isEmpty()==true)
			return false;
		return true;

	}
    
    /*******************************************
	@Author : Federico Peña
	@Company: Certa Consulting
	@Description: Metodo para llamar a la nueva activacion de nodos y unificar el proceso con el codigo de activacion masiva

	<date>			<version>		<description>
	07/06/2019		1.0				Version Inicial
	17/07/2019      1.1             Se corrige la aridad al llamado del método activarNodo
	**********************************************/
	@AuraEnabled
	public static void activacionNueva(String idNodo){
		
        BI_Estruct_ActivacionDeNodosController.activarNodo(idNodo, false);

	}
}