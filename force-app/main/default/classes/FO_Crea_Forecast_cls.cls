/************************************************
 * Author: Javier Lopez Andradas
 * Company: gCTIO
 * Description:  
 * 
 * **********************************************/
public without sharing class FO_Crea_Forecast_cls {
	/******************************************************
	 * Author: Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: retrieves the actual period for doing the forecast
	 * 
	 * 
	 * ******************************************************/
	@AuraEnabled
	public static BI_Periodo__c getPer(){
		BI_Periodo__c per = [Select Id,NAme from BI_Periodo__c where FO_Type__c='Semana' AND BI_FVI_Fecha_Inicio__c<=:Date.today() AND BI_FVI_Fecha_Fin__c>=:DAte.today() limit 1];
		System.debug(per);
		return per;
	}
	/***************************************************
	 * Author; Javier Lopez Andradas
	 * Company:gCTIO
	 * Description: retrieves all the Nodes that the actual user is the owner
	 * 
	 * <date>		<version>		<description>
	 * ??????		1.0				Intial
	 * 06/08/2018	2.0				Now search in the Nodos Share record for collaboration
	 * 07/08/2018	2.1				Changed roll-back and Parent Owner can make the Forecast of their subordinates
	 * 05/11/2018	2.2				Cannot make Forecast of inactive nodes
	 * *************************************/
	@AuraEnabled
	public static List<BI_FVI_Nodos__c> getNodes(){
		return [Select Id, Name,OwnerId from BI_FVI_Nodos__c where (OwnerId =:UserInfo.getUserId() OR BI_FVI_NodoPadre__r.OwnerId = :UserInfo.getUserId() OR BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.OwnerId = :UserInfo.getUserId())
		AND ((RecordType.DeveloperName ='BI_FVI_NCP' AND FO_Clasificacion__c!='Multinationals')  OR (RecordType.DeveloperName='BI_FVI_NCS' AND BI_Estruct_Subtipo_de_Nodo__c IN ('Comercial','Venta Especialista'))) AND BI_FVI_Activo__c=true];
	}
	
}