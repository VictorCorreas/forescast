@isTest
private class BI_FVI_NodosObjetivosMethods_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García
    Company:       NEAborda
    Description:   Método para cargar datos de prueba

    History: 

    <Date>                  <Author>                <Change Description>
    10/05/2016              Antonio Masferrer       Initial Version  
    29/12/2016				Humberto Nunes			se agrega BI_FVI_IdObjeto__c = '098765432123buh789'; para que no salte la nueva condicion en la regla de validacion.    
	18/02/2018				Humberto Nunes			Se Agrega variable MAP_NAME_RT
   	15/03/2018              Humberto Nunes          Se agrego el Tipo de Registro 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static List<BI_FVI_Nodos__c> lst_nodos = new List<BI_FVI_Nodos__c>();
	
	static List<BI_Objetivo_Comercial__c> lst_oc = new List<BI_Objetivo_Comercial__c>();
	
	static List<BI_Periodo__c> lst_per = new List<BI_Periodo__c>();

	static List<BI_Registro_Datos_Desempeno__c> lst_rdd = new List<BI_Registro_Datos_Desempeno__c>();

	static List<Account> lst_acc = new List<Account>();

	static List<BI_FVI_Nodos_Objetivos__c> lst_no = new List<BI_FVI_Nodos_Objetivos__c>();

	static Set<Id> set_id_no = new Set<Id>();

	static List<BI_FVI_Nodo_Cliente__c> lst_nc = new List<BI_FVI_Nodo_Cliente__c>();

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_FVI_Nodos__c' OR  sObjectType = 'BI_FVI_Nodos_Objetivos__c' OR  sObjectType = 'Account' OR  sObjectType = 'BI_Objetivo_Comercial__c' OR  sObjectType = 'BI_Registro_Datos_Desempeno__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

	@isTest static void dataLoad() {
		
		BI_FVI_Nodos__c nodoP1 = new BI_FVI_Nodos__c(
			Name = 'Nodo Padre 1',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoP1);

		BI_FVI_Nodos__c nodoP2 = new BI_FVI_Nodos__c(
			Name = 'Nodo Padre 2',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoP2);


		BI_FVI_Nodos__c nodoP3 = new BI_FVI_Nodos__c(
			Name = 'Nodo Padre 3',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoP3);

		BI_FVI_Nodos__c nodoP4 = new BI_FVI_Nodos__c(
			Name = 'Nodo Padre 4',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoP4);

		BI_FVI_Nodos__c nodoP5 = new BI_FVI_Nodos__c(
			Name = 'Nodo Padre 5',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoP5);

		
		BI_FVI_Nodos__c nodoC = new BI_FVI_Nodos__c(
			Name = 'Nodo Comercial',
			BI_FVI_Activo__c = true,
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCP'),
			BI_FVI_Pais__c = Label.BI_Peru
		);
		lst_nodos.add(nodoC);

		insert lst_nodos;
		
		lst_nodos[1].BI_FVI_NodoPadre__c = lst_nodos[0].Id;
		lst_nodos[2].BI_FVI_NodoPadre__c = lst_nodos[1].Id;
		lst_nodos[3].BI_FVI_NodoPadre__c = lst_nodos[2].Id;
		lst_nodos[4].BI_FVI_NodoPadre__c = lst_nodos[3].Id;
		lst_nodos[5].BI_FVI_NodoPadre__c = lst_nodos[4].Id;

		update lst_nodos;

		

		Account acc1 = new Account(
			Name = 'Cliente 1 XXX Test',
			BI_Country__c = Label.BI_Peru,
			BI_Tipo_de_identificador_fiscal__c = 'RUC',
			BI_No_Identificador_fiscal__c = '12345678910',
			BI_Segment__c = 'test',
			BI_Subsegment_Regional__c = 'test',
			BI_Territory__c = 'test',
			BI_FVI_Nodo__c = lst_nodos[5].Id
		);
		lst_acc.add(acc1);

		Account acc2 = new Account(
			Name = 'Cliente 2 XXX Test',
			BI_Country__c = Label.BI_Peru,
			BI_Tipo_de_identificador_fiscal__c = 'RUC',
			BI_No_Identificador_fiscal__c = '11245678910',
			BI_Segment__c = 'test',
			BI_Subsegment_Regional__c = 'test',
			BI_Territory__c = 'test',
			BI_FVI_Nodo__c = lst_nodos[5].Id
		);
		lst_acc.add(acc2);

		insert lst_acc;

		

		BI_Objetivo_Comercial__c objetivo1 = new BI_Objetivo_Comercial__c(
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos'),
			BI_FVI_Activo__c = true,
			BI_Tipo__c = 'Visitas'
		);
		lst_oc.add(objetivo1);

		BI_Objetivo_Comercial__c objetivo2 = new BI_Objetivo_Comercial__c(
			RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos'),
			BI_FVI_Activo__c = true,
			BI_Tipo__c = 'Actos Comerciales'
		);
		lst_oc.add(objetivo2);

		insert lst_oc;


		BI_Registro_Datos_Desempeno__c rdd1 = new BI_Registro_Datos_Desempeno__c(
			BI_Tipo__c = 'Visitas',
			BI_FVI_Fecha__c = Date.today(),
			BI_FVI_Id_Cliente__c = lst_acc[0].Id,
			BI_FVI_Valor__c = 4,
			BI_FVI_IdObjeto__c = '098765432123buh789'
		);
		lst_rdd.add(rdd1);

		BI_Registro_Datos_Desempeno__c rdd2 = new BI_Registro_Datos_Desempeno__c(
			BI_Tipo__c = 'Actos Comerciales',
			BI_FVI_Fecha__c = Date.today(),
			BI_FVI_Id_Cliente__c = lst_acc[1].Id,
			BI_FVI_Valor__c = 10,
			BI_FVI_IdObjeto__c = '098765432123buh789'
		);
		lst_rdd.add(rdd2);

		insert lst_rdd;


		BI_Periodo__c periodo1 = new BI_Periodo__c(
			BI_FVI_Activo__c = true,
			BI_FVI_Fecha_Inicio__c = Date.today() - 13, 
			BI_FVI_Fecha_Fin__c = Date.today() + 5
		);
		lst_per.add(periodo1);

		BI_Periodo__c periodo2 = new BI_Periodo__c(
			BI_FVI_Activo__c = true,
			BI_FVI_Fecha_Inicio__c = Date.today() - 60, 
			BI_FVI_Fecha_Fin__c = Date.today() +15
		);
		lst_per.add(periodo2);

		insert lst_per;
		

		BI_FVI_Nodo_Cliente__c nodoCliente1 = new BI_FVI_Nodo_Cliente__c(
			BI_FVI_IdCliente__c = lst_acc[0].Id,
			BI_FVI_IdNodo__c = lst_nodos[5].Id,
            BI_Estruct_Saltar_Validacion_Nodo_Activo__c = true
		);
		lst_nc.add(nodoCliente1);

		BI_FVI_Nodo_Cliente__c nodoCliente2 = new BI_FVI_Nodo_Cliente__c(
			BI_FVI_IdCliente__c = lst_acc[1].Id,
			BI_FVI_IdNodo__c = lst_nodos[5].Id,
            BI_Estruct_Saltar_Validacion_Nodo_Activo__c = true
		);
		lst_nc.add(nodoCliente2);

		insert lst_nc;

		
		BI_FVI_Nodos_Objetivos__c nodo_objetivo1 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[5].Id,
			BI_FVI_IdPeriodo__c = lst_per[0].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15
		);	
		lst_no.add(nodo_objetivo1);

		BI_FVI_Nodos_Objetivos__c nodo_objetivo2 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[5].Id,
			BI_FVI_IdPeriodo__c = lst_per[0].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[1].Id,
			BI_FVI_Objetivo__c = 15
		);
		lst_no.add(nodo_objetivo2);

		BI_FVI_Nodos_Objetivos__c nodo_objetivo3 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[4].Id,
			BI_FVI_IdPeriodo__c = lst_per[0].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15
		);	
		lst_no.add(nodo_objetivo3);

		BI_FVI_Nodos_Objetivos__c nodo_objetivo4 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[3].Id,
			BI_FVI_IdPeriodo__c = lst_per[0].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15
		);	
		lst_no.add(nodo_objetivo4);

		BI_FVI_Nodos_Objetivos__c nodo_objetivo5 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[2].Id,
			BI_FVI_IdPeriodo__c = lst_per[1].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15
		);	
		lst_no.add(nodo_objetivo5);

		BI_FVI_Nodos_Objetivos__c nodo_objetivo6 = new BI_FVI_Nodos_Objetivos__c(
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'),
			BI_FVI_Id_Nodo__c = lst_nodos[1].Id,
			BI_FVI_IdPeriodo__c = lst_per[1].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15
		);	

        // ************** ASOCIADO A NODOS *****************
        BI_Objetivo_Comercial__c objOComercial2 = new BI_Objetivo_Comercial__c();
        objOComercial2.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos');
        objOComercial2.BI_Tipo__c  = 'Visitas';
        objOComercial2.BI_FVI_Unidad_Medida__c = 'Cantidad';
        objOComercial2.BI_FVI_Descripcion__c = 'Visitas';
        objOComercial2.BI_FVI_Activo__c = true;
        insert objOComercial2;

        // ASOCIADO A NODO1... 
        BI_Registro_Datos_Desempeno__c objNodosDesNew4 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew4.BI_FVI_Fecha__c = Date.today()+1;
        objNodosDesNew4.BI_FVI_Id_Nodo__c = lst_nodos[5].Id;
        objNodosDesNew4.BI_Tipo__c = 'Visitas';
        objNodosDesNew4.BI_FVI_Valor__c = 5;
        objNodosDesNew4.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew4.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
        insert objNodosDesNew4;

        // OBJETIVO EN NODO
        BI_FVI_Nodos_Objetivos__c objNObjetivo2 = new BI_FVI_Nodos_Objetivos__c();
        objNObjetivo2.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos');
        objNObjetivo2.BI_FVI_Id_Nodo__c = lst_nodos[5].Id;
        objNObjetivo2.BI_FVI_IdPeriodo__c = lst_per[0].Id;
        objNObjetivo2.BI_FVI_Objetivo_Comercial__c = objOComercial2.id;
        objNObjetivo2.BI_FVI_Objetivo__c = 500;
        lst_no.add(objNObjetivo2);
        // ************** ASOCIADO A NODOS *****************

		// ************** ASOCIADO A NODOS JERARQUICOS *****************

        BI_FVI_Nodos__c objNodoJerarquico = new BI_FVI_Nodos__c();
        objNodoJerarquico.name = 'NodoJERARQUICO';
        objNodoJerarquico.BI_FVI_Pais__c = Label.BI_Argentina;
        objNodoJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_NCJ');
        objNodoJerarquico.BI_FVI_Activo__c = true;
        insert objNodoJerarquico;

        BI_Objetivo_Comercial__c objOComercialJerarquico = new BI_Objetivo_Comercial__c();
        objOComercialJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Objetivos_Jerarquicos');
        objOComercialJerarquico.BI_Tipo__c  = 'Oportunidad';
        objOComercialJerarquico.BI_OBJ_Campo__c = 'Amount';
        objOComercialJerarquico.BI_FVI_Unidad_Medida__c = 'Moneda';
        objOComercialJerarquico.BI_FVI_Descripcion__c = 'FCV';
        objOComercialJerarquico.BI_FVI_Activo__c = true;
        insert objOComercialJerarquico;

        BI_FVI_Nodos_Objetivos__c objNObjetivoJerarquico = new BI_FVI_Nodos_Objetivos__c();
        objNObjetivoJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Jerarquicos');
        objNObjetivoJerarquico.BI_FVI_Id_Nodo__c = objNodoJerarquico.id;
        objNObjetivoJerarquico.BI_FVI_IdPeriodo__c = lst_per[1].Id;
        objNObjetivoJerarquico.BI_FVI_Objetivo_Comercial__c = objOComercialJerarquico.id;
        objNObjetivoJerarquico.BI_FVI_Objetivo__c = 500;
		// ************** ASOCIADO A NODOS JERARQUICOS *****************

        lst_no.add(objNObjetivoJerarquico);

		insert lst_no;	
		
		for(BI_FVI_Nodos_Objetivos__c no : lst_no){
			set_id_no.add(no.Id);
		}
		
	}


	@isTest static void generaNodo_Objetivo_Desempeno_TEST() {
		
		BI_TestUtils.throw_exception = false;

		User user = [select Id from User where Id = :UserInfo.getUserId()];
		
		system.runAs(user){

			dataLoad();
			
			system.debug('···> NODOS: ' + lst_nodos);
			system.debug('···> CLIENTES: ' + lst_acc);
			system.debug('···> OBJETIVOS: ' + lst_oc);
			system.debug('···> PERIODO: ' + lst_per);
			system.debug('···> DESEMPEÑOS: ' + lst_rdd);
			system.debug('···> NODOS OBJETIVOS: ' + lst_no);

			List<BI_FVI_Nodo_Objetivo_Desempeno__c> lst_nod = [SELECT Id,
																	  BI_FVI_Nodo_Objetivo__c,
																	  BI_FVI_Registro_Datos_Desempeno__c,
																	  BI_FVI_Valor__c 
															   FROM BI_FVI_Nodo_Objetivo_Desempeno__c
															   WHERE BI_FVI_Nodo_Objetivo__c IN: set_id_no];

		    system.debug('····> LISTA NODOS OBJETIVOS DESEMPEÑOS: ' + lst_nod);

/*		    system.assertEquals(lst_nod[0].BI_FVI_Nodo_Objetivo__c , lst_no[0].Id);
		    system.assertEquals(lst_nod[0].BI_FVI_Registro_Datos_Desempeno__c , lst_rdd[0].Id);
		    system.assertEquals(lst_nod[0].BI_FVI_Valor__c , lst_rdd[0].BI_FVI_Valor__c);
*/		}

		BI_TestUtils.throw_exception = true;
	}
	@isTest static void exception_TEST() {
		BI_TestUtils.throw_exception = true;

		User user = [select Id from User where Id = :UserInfo.getUserId()];
		
		system.runAs(user){

			dataLoad();
			
		}
	}
}