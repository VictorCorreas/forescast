/***************************************
 * Author: Javier López Andradas
 * Description: Helper for Edit Page
 * Company:gCTIO
 * 
 * 
 * 
 * 
 * ***************************************/
public without sharing class FO_ModificaFOIController {
    @AuraEnabled
    /***********************************
     * 
     * 
     * Description: Return the values of picklist Tipe, depende on class pickList
     * 
     * **********************************/
    public static List<pickList> getTipo(){
        List<pickList> lst = new List<pickList>();
        Schema.DescribeFieldResult stageNameField = FO_Forecast_Item__c.FO_Tipo_Forecast__c.getDescribe();
        List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        for(Schema.PicklistEntry entry :values){
            lst.add(new pickList(entry.getValue(),entry.getLabel()));
        }
        return lst;
    }
    @AuraEnabled
    public static List<pickList> getEstado(){
        List<pickList> lst = new List<pickList>();
        Schema.DescribeFieldResult stageNameField = FO_Forecast__c.FO_Estado__c.getDescribe();
        List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        for(Schema.PicklistEntry entry :values){
            if(entry.getValue().equals('Borrador'))
            	lst.add(new pickList(entry.getValue(),entry.getLabel()));
        }
        return lst;
    }
   public class pickList{
        @AuraEnabled public String value;
        @AuraEnabled public String label;
      public pickList(String value,String label){
            this.value=value;
            this.label=label;
        }
    }
    @AuraEnabled 
    public static String updateFOI(String recId,String CloseDate,String tipo,Boolean selDate,Boolean selType){
        FO_Forecast_Item__c aux = [Select FO_Estado_Padre__c,FO_Forecast_Padre__r.FO_Periodo__r.BI_FVI_Activo__c  from FO_Forecast_Item__c where Id=:recId];
        if(aux.FO_Estado_Padre__c.equals('Enviado') ){
            return Label.FO_Err_Mod_Submited;
        }
        IF(aux.FO_Forecast_Padre__r.FO_Periodo__r.BI_FVI_Activo__c==false){
            return Label.FO_Modify_NOA_Period;
        }
        FO_Forecast_Item__c foi = new FO_Forecast_Item__c();
        foi.Id=recId;
        String[] split = CloseDate.split('-');
        if(selDate==true){
            Date dtt = Date.newInstance(Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));
        	foi.FO_CloseDate__c=dtt;
        }
       if(selType==true)
        	foi.FO_Tipo_Forecast__c=tipo;
        try{
            update(foi);
        }catch(Exception e){
            
            if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            	String[] splitter = e.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                return Label.FO_Err_Upd_FOI+splitter[1].split('\n')[0];
            }
            return Label.FO_Err_Upd_FOI+e.getStackTraceString()+', '+e.getMessage();
        }
        return 'Ok';
    }
    
    @AuraEnabled 
    public static String updateFO(String recId,String tipo){
        FO_Forecast__c aux = [Select FO_Forecast_Padre__r.FO_Estado__c,FO_Periodo__r.BI_FVI_Activo__c from FO_Forecast__c where Id=:recId];
        if(aux.FO_Forecast_Padre__r.FO_Estado__c.equals('Enviado')){
            return Label.FO_Err_Ret_Submit;
        }
        if(aux.FO_Periodo__r.BI_FVI_Activo__c==false){
             return Label.FO_Modify_NOA_Period;
        }
        FO_Forecast__c fo = new FO_Forecast__c();
        fo.Id=recId;
        fo.FO_Estado__c=tipo;
        try{
            update(fo);
        }catch(Exception e){
            return Label.FO_Err_Upd_FOI_Unex+e.getStackTraceString();
        }
        return 'Ok';
    }
    
    @AuraEnabled
    public static String getParent(String recId){
        return[Select FO_Forecast_Padre__c from FO_Forecast__c where Id=:recId].FO_Forecast_Padre__c;
    }
    /****************************************
     * Author. Javier Lopez
     * Company: gCTIO
     * Description : retrieves the field
     * 
     * <date>     	<version>   <description>
     * ??????       1.0         Initial
     * 15/10/2018 	1.1			Added OwnersId of the Opp and the Forecast to know who can modify the CloseDate
     * *****************************************/
    @AuraEnabled
    public static FO_Forecast_Item__c getClose(String recId){
        return[Select FO_CloseDate__c,FO_Tipo_Forecast__c,FO_Forecast_Padre__r.OwnerId,FO_Opportunity__r.OwnerId,FO_Forecast_Padre__r.RecordType.DEveloperName from FO_Forecast_Item__c where Id=:recId];
    }
  	/*****************************************
  	 * Author: Javier Lopez Andradas
  	 * Company: gCTIO
  	 * Description: Retrieves all both states of the Forecast
  	 * 
  	 * <date>		<version>		<description>
  	 * 19/04/2018	1.0				Initial version
  	 * 23/04/2018	1.1				Avoid null pointer exception
  	 * **************************************/
    @AuraEnabled
    public static String getState(String recId){
        FO_Forecast__c fore = [select FO_Estado__c,FO_Forecast_Padre__r.FO_Estado__c,FO_Forecast_Padre__c from FO_Forecast__c where Id=:recId];
        if(fore.FO_Estado__c.equals('Enviado'))
            return 'Enviado';
        if(fore.FO_Forecast_Padre__c!=null && fore.FO_Forecast_Padre__r.FO_Estado__c.equals('Enviado'))
            return 'Enviado';
        return 'Borrador';
        
        
    }
    
}