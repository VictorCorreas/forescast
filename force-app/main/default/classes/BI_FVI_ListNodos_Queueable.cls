public class BI_FVI_ListNodos_Queueable implements Queueable{
	    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Adrián Caro             
    Company:       Accenture
    Description:   class Queueable to update listnodos optimization

    History: 

    <Date>                  <Author>                <Change Description>   
    06/02/2017              Adrián Caro             Initial Version
    04/04/2017				Humberto Nunes          Se quito el -1 de la expresion "if(lstupt.size()-1 > Index)" ya que siempre estaba dejando el ultimo elemento sin procesar. 
    11/05/2017				Cristina Rodriguez		Solve queries With No Where Or Limit Clause
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
	List <BI_FVI_Nodo_Cliente__c> lstupt = new List <BI_FVI_Nodo_Cliente__c>();
	public Integer Index = 0;
	public String StrErrors = '';
	Set <String> setpaises = new Set <String>();

	public BI_FVI_ListNodos_Queueable(List <BI_FVI_Nodo_Cliente__c> lst_nc_Upd, Integer i, Set <String> set_paises){
		this.lstupt = lst_nc_Upd;
		this.Index = i;
		this.setpaises = set_paises;

	}

	public void execute(QueueableContext context) 
	{
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		//START CRM 10/05/2017 Solve queries With No Where Or Limit Clause
		//List <BI_FVI_MailConfiguration__mdt> lstMail = [Select Label, Correo__c from BI_FVI_MailConfiguration__mdt];
		List <String> lstAddress = new List <String>();
		try{
			//throw new BI_Exception('test');
			System.debug('pasa try');
			update lstupt[Index];			
			Index ++;
			if(lstupt.size() > Index)
			{
				System.enqueueJob(new BI_FVI_ListNodos_Queueable(lstupt,Index, setpaises));
			}
			else
			{
				for(BI_FVI_MailConfiguration__mdt mailConfig : [Select Label, Correo__c from BI_FVI_MailConfiguration__mdt WHERE Label IN : setpaises])
				{
					if(StrErrors!= '')
					{
						lstAddress.add(mailConfig.Correo__c);
						mail.setToAddresses(lstAddress);
						mail.setSubject('Error List');
						mail.setPlainTextBody('Update Error List' + StrErrors);
						Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
					}					
				}
			}
		}
		catch (Exception exc)
		{
			System.debug('pasa catch');
			StrErrors += exc.getMessage();			
		}
		//END CRM  10/05/2017 Solve queries With No Where Or Limit Clause
	}
}