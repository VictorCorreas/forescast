global without sharing class BI_FVI_SingleExecution 
{
	private static Map<String,Boolean> singletonMap;

	global static Boolean hasAlreadyExecuted(String ClassNameOrExecutionName)
	{  
	    if(singletonMap != null) 
	    {
	        Boolean alreadyExecuted = singletonMap.get(ClassNameOrExecutionName);
	        if(alreadyExecuted != null) 
	        {
	            return alreadyExecuted;
	        }           
	    }
	    //By default return false
	    return false;
	}

	global static void setAlreadyExecuted(String ClassNameOrExecutionName) 
	{
	    if(singletonMap == null) 
	    {
	        singletonMap = new Map<String,Boolean>();
	    }
	    singletonMap.put(ClassNameOrExecutionName,true);
	}
}