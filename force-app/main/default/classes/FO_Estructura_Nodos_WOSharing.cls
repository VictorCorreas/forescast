/************************************************
 * Author: Javier Lopez Andradas
 * COmpany: gCTIO
 * DEscription: Auxiliar Class to seek in all node hierachy in a way to search the deep ok a node
 *              It need to be withousharing, have to see parent records
 *              Only seek for NCS Nodes in current country
 * 
 * <date>       <version>       <description>
 * 05/06/2018   1.0             Initial
 * **********************************************/
public  class FO_Estructura_Nodos_WOSharing {
    /**********************************************
     * Author: Javier Lopez Andradas
     * COmpany: gCTIO
     * DEscription: Auxiliar Class to seek in all node hierachy in a way to search the deep ok a node
     *              It need to be withousharing, have to see parent records
     *              Only seek for NCS Nodes in current country it will be executed in pre
     * 
     * <date>       <version>       <description>
     * 05/06/2018   1.0             Initial
     * 13/07/2018	1.1				Problemas al insertar
     * **********************************************/
    public static void seekHierarchy(List<BI_FVI_Nodos__c> news,List<BI_FVI_Nodos__c> olds){
        System.debug('JLA_Start_seekHierarchy: '+news);
        System.debug('JLA_Start_seekHierarchy_OLDS: '+olds);
        List<BI_FVI_Nodos__c> nd_upd = new List<BI_FVI_Nodos__c>();
        //Es nuevo
        //Vemos los rT
         //Sacamos los RT
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.BI_FVI_Nodos__c;
        Map<String,String> mp_rt = new Map<String,String>();
        for(Schema.RecordTypeInfo rt : d.getRecordTypeInfos()){
            mp_rt.put(rt.getRecordTypeId(),rt.getDeveloperName());
        }
        System.debug('RT_	'+mp_rt);
        if(olds==null || olds.isEmpty()==true){
            System.debug('Dentro: ');
            for(Integer i = 0;i<news.size();i++){
                BI_FVI_Nodos__c nds = news.get(i);
                if(mp_rt.get(nds.RecordTypeId).equals('BI_FVI_NCP') || mp_rt.get(nds.RecordTypeId).equals('BI_FVI_NCS')){
                	System.debug('El fucking padre es nulo?: '+news.get(i).BI_FVI_NodoPadre__c);
	                if(news.get(i).BI_FVI_NodoPadre__c!=null){
    	                nd_upd.add(news.get(i));
        	        }    
                }
                
                
            }
        }else{
            for(Integer i = 0 ;i<news.size();i++){
                if(news.get(i).BI_FVI_NodoPadre__c!=olds.get(i).BI_FVI_NodoPadre__c){
                    nd_upd.add(news.get(i));
                }
                    
            }
        }
        System.debug('nd_upd: '+nd_upd);
        
        if(nd_upd.isEmpty()==false){
            List<BI_FVI_Nodos__c> lst_def = new List<BI_FVI_NOdos__c>();
            Map<Id,BI_FVI_Nodos__c> lst_updChild = new Map<Id,BI_FVI_NOdos__c>();
            Map<Id,BI_FVI_Nodos__c> mp_nd = new Map<Id,BI_FVI_Nodos__c>([Select Id,BI_FVI_NodoPadre__c,FO_Nivel_Padre__c,FO_Cabecera_de_Jerarquia__c from BI_FVI_Nodos__c where REcordType.DeveloperName in ('BI_FVI_NCS','BI_FVI_NCP' )]);
            //JLA {Id_Nodo_Padre,[Nodos_hijos]}
            Map<Id,List<BI_FVI_Nodos__c>> mp_ndPa = new Map<Id,List<BI_FVI_Nodos__c>>();
            for(BI_FVI_Nodos__c nd : mp_nd.values()){
                List<BI_FVI_Nodos__c> lst_aux = mp_ndPa.get(nd.BI_FVI_NodoPadre__c);
                if(lst_aux==null)
                    lst_aux = new List<BI_FVI_Nodos__c>();
                lst_aux.add(nd);
                mp_ndPa.put(nd.BI_FVI_NodoPadre__c,lst_aux);
            }
            System.debug('Mapa de los nodos por pais: '+mp_nd);
            for(INteger i = 0 ;i<nd_upd.size();i++){
                BI_FVI_Nodos__c nd = nd_upd.get(i);
                Decimal jera=0;
                if(nd.BI_FVI_NodoPadre__c!=null && nd.FO_Cabecera_de_Jerarquia__c==false){
                	jera = nd.FO_Nivel_Padre__c+1;
        	        nd.FO_Jerarquia__c='VP-'+jera;
                    nd.FO_Nivel_Auxiliar__c=jera;
    	            lst_def.add(nd);   
                }else{
                    nd.FO_Nivel_Auxiliar__c=jera;
                    nd.FO_Jerarquia__c='VP';
    	            lst_def.add(nd); 
                }
                System.debug('Miramos los hijos');
                //Es un insert no va a tenber hijos
                if(nd_upd.get(i).Id!=null)
            		FO_Estructura_Nodos_WOSharing.recursiveChilds(mp_ndPa,jera+1,nd_upd.get(i).Id,lst_updChild);        
            }
            
            System.debug('Lsita a atualizar: '+lst_def);
            System.debug('Lista de hijos: '+lst_updChild);
            if(lst_updChild.keySet().isEmpty()==false){
                update(lst_updChild.values());
                
            }
            	

        } 
        
        System.debug('JLA_Ends_seekHierarchy');
    }
    /********************************************
     * Author:Javier Lopez Andradas
     * Company: gCTIO
     * DEscription:  seek the depp of the node in hierarchy
     * 
     * <date>			<version>			<description>
     * 07/06/2018		1.0					Initial
     * 
     * 
     * ******************************************/
   public static void recursiveChilds(Map<Id,List<BI_FVI_Nodos__c>> mp_ndPa,Decimal deep,Id Idnodo,Map<Id,BI_FVI_Nodos__c> result){
        List<BI_FVI_Nodos__c> lst_nodes = mp_ndPa.get(IdNodo);
       System.debug('Jerarquia: '+deep);
       System.debug('IdNodo: '+Idnodo);
        if(lst_nodes==null){
            return;
        }else{
            for(BI_FVI_Nodos__c nd : lst_nodes){
                if(nd.FO_Cabecera_de_Jerarquia__c==true){
                    nd.FO_Jerarquia__c='VP';
                    deep=0;
                }else{
                    nd.FO_Jerarquia__c='VP-'+deep;
                }                    
                recursiveChilds(mp_ndPa,deep+1,nd.Id,result);
                
                result.put(nd.Id,nd);
            }
        }
        
        return;
    }
    
}