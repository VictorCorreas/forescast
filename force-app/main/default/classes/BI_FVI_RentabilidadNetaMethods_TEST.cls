@isTest 
private class BI_FVI_RentabilidadNetaMethods_TEST
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       Accenture
    Description:   Test class to manage the coverage code for BI_FVI_RentabilidadNetaMethods 
    
    History: 
    <Date>                  <Author>                <Change Description>
    06/10/2017              Humberto Nunes          Initial Version
    07/11/2017				Alfonso Alvarez			Adaptación del código a la eliminación el campo de relacion entre RentabilidadNeta y Cuenta
    15/03/2018				Jesus Arcones			Añadir campo RecordType en la inserción de Objetivos para cumplir con el filtro del campo Id del Nodo
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static List<Account> lst_acc = new List<Account>();
    static List<BI_FVI_Nodos__c> lst_nodos = new List<BI_FVI_Nodos__c>();
    static List<BI_FVI_Nodo_Cliente__c> lst_nc = new List<BI_FVI_Nodo_Cliente__c>();
    static List<BI_Periodo__c> lst_per = new List<BI_Periodo__c>();
    static List<BI_Objetivo_Comercial__c> lst_oc = new List<BI_Objetivo_Comercial__c>();
    static List<BI_FVI_Nodos_Objetivos__c> lst_no = new List<BI_FVI_Nodos_Objetivos__c>();
    static List<BI_FVI_Rentabilidad_Neta__c> lst_rn = new List<BI_FVI_Rentabilidad_Neta__c>();

    @isTest static void dataLoad() 
    {
		// Recordtypes necesarios para objetos de nodos - 15/03/2018 JAG
        Map<String,RecordType> MAP_NAME_RT = new Map<String,RecordType>();
		for(RecordType rt :[SELECT Id,DeveloperName  FROM RecordType WHERE DeveloperName IN 
                            ('BI_FVI_NCP','BI_FVI_Objetivos_Nodos','BI_OBJ_Nodo_Objetivos_Nodos')]){
			MAP_NAME_RT.put(rt.DeveloperName, rt);
		}
        
        // CREAR UNA CUENTA TEST
        Account acc1 = new Account  (
                                        Name = 'Cliente 1 XXX Test',
                                        BI_Country__c = Label.BI_Peru,
                                        BI_Tipo_de_identificador_fiscal__c = 'RUC',
                                        BI_No_Identificador_fiscal__c = '12345678910'
                                    );
        lst_acc.add(acc1);
        insert lst_acc;

        // CREAR UN NODO
        BI_FVI_Nodos__c nodoC = new BI_FVI_Nodos__c (
                                                        Name = 'Nodo Comercial',
                                                        BI_FVI_Activo__c = true,
                                                        RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCP').Id, // 15/03/2018 JAG
                                                        BI_FVI_Pais__c = Label.BI_Peru
                                                    );
        lst_nodos.add(nodoC);
        insert lst_nodos;

        // CREA VINCULO ENTRE CLIENTE Y NODO
        BI_FVI_Nodo_Cliente__c nodoCliente1 = new BI_FVI_Nodo_Cliente__c(
                                                                            BI_FVI_IdCliente__c = lst_acc[0].Id,
                                                                            BI_FVI_IdNodo__c = lst_nodos[0].Id,
            																BI_Estruct_Saltar_Validacion_Nodo_Activo__c = true
                                                                        );
        lst_nc.add(nodoCliente1);
        insert lst_nc;

        // CREAR PERIODO
        BI_Periodo__c periodo1 = new BI_Periodo__c  (
                                                        BI_FVI_Activo__c = true,
                                                        BI_FVI_Fecha_Inicio__c = Date.today() - 13, 
                                                        BI_FVI_Fecha_Fin__c = Date.today() + 5
                                                    );
        lst_per.add(periodo1);
        insert lst_per;

        // CREAR OBJETIVO 
        BI_Objetivo_Comercial__c objetivo1 = new BI_Objetivo_Comercial__c   (
                                                                                BI_FVI_Activo__c = true,
                                                                                BI_Tipo__c = 'Rentabilidad Neta',
																				RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos').Id // 15/03/2018 JAG
                                                                            );
        lst_oc.add(objetivo1);
        insert lst_oc;

        BI_FVI_Nodos_Objetivos__c nodo_objetivo1 = new BI_FVI_Nodos_Objetivos__c(
			BI_FVI_Id_Nodo__c = lst_nodos[0].Id,
			BI_FVI_IdPeriodo__c = lst_per[0].Id,
			BI_FVI_Objetivo_Comercial__c = lst_oc[0].Id,
			BI_FVI_Objetivo__c = 15000,
			RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos').Id // 15/03/2018 JAG
        );
        lst_no.add(nodo_objetivo1);
        insert lst_no;

        // CREAR RENTABILIDAD NETA PARA DISPARAR EL TRIGGER QUE GENERA LOS NODOS OBJETIVOS DESEMPEÑOS

        BI_FVI_Rentabilidad_Neta__c rentabilidad_neta1 = new BI_FVI_Rentabilidad_Neta__c(
                                                                                            BI_FVI_Fecha_Registro__c = Date.today(),
             																				// 07-11-2017 Inicio AJAE
                                                                                            //BI_FVI_Cuenta__c = lst_acc[0].Id,
                                                                                            //// 07-11-2017 FIN AJAE
                                                                                            BI_FVI_Nodo__c = lst_nodos[0].Id,
                                                                                            BI_FVI_Cantidad_Altas_Movil__c = 10,
                                                                                            BI_FVI_Cantidad_Bajas_Movil__c = 5,
                                                                                            BI_FVI_Cantidad_Altas_Fija__c = 8,
                                                                                            BI_FVI_Cantidad_Bajas_Fija__c = 10,
                                                                                            BI_FVI_Cargo_FijoMes_Altas_Movil__c = 1500.5,
                                                                                            BI_FVI_Cargo_FijoMes_Bajas_Movil__c = 875.25,
                                                                                            BI_FVI_Cargo_FijoMes_Altas_Fija__c = 975.5,
                                                                                            BI_FVI_Cargo_FijoMes_Bajas_Fija__c = 1000
                                                                                        );
        lst_rn.add(rentabilidad_neta1);
        insert lst_rn;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_FVI_RentabilidadNetaMethods.generaNodo_Objetivo_Desempeno ()
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    06/10/2017              Humberto Nunes          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createRDD_Test() 
    {
        BI_TestUtils.throw_exception = false;

        User user = [select Id from User where Id = :UserInfo.getUserId()];
        
        system.runAs(user){

            dataLoad();
            
            system.debug('···> NODOS: ' + lst_nodos);
            system.debug('···> CLIENTES: ' + lst_acc);
            system.debug('···> NODOS CLIENTES: ' + lst_nc);

            list <Account> lstAcc = [SELECT Id, Name, BI_FVI_Nodo__c From Account Where Id=: lst_acc[0].Id];
            system.debug('···> NODO EN EL CLIENTE: ' + lstAcc);

            system.debug('···> PERIODO: ' + lst_per);
            system.debug('···> OBJETIVOS: ' + lst_oc);
            system.debug('···> NODOS OBJETIVOS: ' + lst_no);
            system.debug('···> RENTABILIDAD NETA: ' + lst_rn);

            List<BI_FVI_Nodo_Objetivo_Desempeno__c> lst_nod = [SELECT Id,
                                                                      BI_FVI_Nodo_Objetivo__c,
                                                                      BI_FVI_Registro_Datos_Desempeno__c,
                                                                      BI_FVI_Valor__c 
                                                               FROM BI_FVI_Nodo_Objetivo_Desempeno__c
                                                               WHERE BI_FVI_Nodo_Objetivo__c =: lst_no[0].Id];

            system.debug('····> LISTA NODOS OBJETIVOS DESEMPEÑOS: ' + lst_nod);          
         }

        BI_TestUtils.throw_exception = true;    
    }


    @isTest static void exception_TEST() 
    {
        BI_TestUtils.throw_exception = true;

        User user = [select Id from User where Id = :UserInfo.getUserId()];
        
        system.runAs(user)
        {
            dataLoad();           
        }
    }
}