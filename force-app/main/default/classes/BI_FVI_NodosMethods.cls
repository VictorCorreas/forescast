global with sharing class BI_FVI_NodosMethods {
       /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes        
    Company:       NEAborda
    Description:   Proceso para llenar los NAV con las cuentas encontradas en determinado NAG 

    History: 

    <Date>                  <Author>                <Change Description>
    09/06/2016              Humberto Nunes          Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void NAGtoNAV(List<BI_FVI_Nodos__c> tnew , List<BI_FVI_Nodos__c> told)
    {            
        Map<Id, Id> mapa_NAG_NAV = new Map<Id,Id>();
        List <BI_FVI_Nodos__c> NUpd = new List<BI_FVI_Nodos__c>();
        
        for(Integer i = 0 ; i < tnew.size() ; i++)
        {
            // SE OBTIENE LOS NODOS EN LOS QUE HUBO CAMBIO DEL CAMPO "ASIGNAR A" Y NO SEA NULL
            if (tnew[i].BI_FVI_Asignar_a__c != null && tnew[i].BI_FVI_Asignar_a__c != told[i].BI_FVI_Asignar_a__c)
            {                
                mapa_NAG_NAV.put(tnew[i].Id, tnew[i].BI_FVI_Asignar_a__c);
                NUpd.add(new BI_FVI_Nodos__c (Id=tnew[i].Id, BI_FVI_Asignar_a__c = null));
            }   
        }  
        
        if (mapa_NAG_NAV.size()>0)
        {
            // SE OBTIENEN TODOS LOS REGISTROS DE NODO-CLIENTE DE LOS NODOS ORIGEN
            List<BI_FVI_Nodo_Cliente__c> list_NC = [SELECT Id, BI_FVI_IdCliente__c, BI_FVI_IdNodo__c FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdNodo__c IN: mapa_NAG_NAV.keySet() ];
            
            // SE DETERMINA POR CADA NODO DESTINO CUAL ES EL ESPACIO DISPONIBLE DE CUENTAS PARA AGREGAR
            List<BI_FVI_Nodos__c> list_NodosDestino = [SELECT Id, BI_FVI_NCuentas__c, BI_FVI_Pais__c FROM BI_FVI_Nodos__c WHERE Id IN: mapa_NAG_NAV.values() ];
                
            // PARAMETROS POR PAIS
            List<NE__Lov__c> lst_lovs = [SELECT Name, NE__Value1__c,Country__c FROM NE__Lov__c WHERE NE__Type__c = 'NODOS' AND Name ='N_CLIENTES_VOLATIL_TEMPORAL_ADQ'];
                   
            List<BI_FVI_Nodo_Cliente__c> list_NC_UPD = new List<BI_FVI_Nodo_Cliente__c>();

            for(Id NodoOrigen : mapa_NAG_NAV.keySet())
            {
                for (BI_FVI_Nodos__c NodoDestino : list_NodosDestino)
                {
                    
                    if (NodoDestino.Id == mapa_NAG_NAV.get(NodoOrigen))
                    {
                        for (NE__Lov__c Lov: lst_lovs)
                        {
                            if (Lov.Country__c == NodoDestino.BI_FVI_Pais__c)
                            {                            
                                integer Cont = Integer.valueOf(Lov.NE__Value1__c) - Integer.valueOf(NodoDestino.BI_FVI_NCuentas__c);
                                for (BI_FVI_Nodo_Cliente__c NC : list_NC)
                                {
                                    If (Cont > 0 && NC.BI_FVI_IdNodo__c == NodoOrigen)
                                    {
                                        list_NC_UPD.add(new BI_FVI_Nodo_Cliente__c(Id=NC.Id, BI_FVI_IdNodo__c = mapa_NAG_NAV.get(NodoOrigen)));
                                        Cont--;
                                    }
                                }
                            }
                        }
                    }
                }
            } 
            update list_NC_UPD;
            update NUpd;
        }
    }

    /*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   class for sharing object nodos
    Test Class:    BI_FVI_NodosShare_TEST
    
    History:
     
    <Date>                  <Author>                    <Change Description>
    14/06/2016              Geraldine P. Montes         Initial Version
    05/06/2016              Antonio Masferrer           Add try catch
    14/07/2016              Humberto Nunes              Validar que solo funcione para nodos NAG    
    12/05/2017              Cristina Rodriguez          Solve Async Future Method Inside Loops. Changed BI_FVI_NodosShare.createNodosShare
    -------------------------------------------------------------------------------*/ 
    //START CRM 12/05/2017 Async Future Method Inside Loops
    public static void NodoShare(list<BI_FVI_Nodos__c>lstNewRecords)
    {   
        try{
            String strPais;
            String strGrupo;
            set<String> setIdGrupo = new set<String>();
            set<String> setPaises = new set<String>();

            RecordType NodoRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_NAG'];

            for(BI_FVI_Nodos__c objNewRecords : lstNewRecords)
            {
                if (objNewRecords.RecordTypeId == NodoRecordType.Id)
                {               
                    setPaises.add(objNewRecords.BI_FVI_Pais__c);
                }
            }

            if (!setPaises.isEmpty())
            {                       
                        system.debug('setPaises ' + setPaises);
                        //system.debug('Id Grupo ' + objGroup.Id);
                        BI_FVI_NodosShare.createNodosShare(setPaises);                    
            }
           
        }
        catch(Exception e)
        {
            BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.NodoShare', 'BI_FVI', e, 'Trigger');
        }

    }
    /*******OLD
    public static void NodoShare(list<BI_FVI_Nodos__c>lstNewRecords)
    {   
        try{
            String strPais;
            String strGrupo;
            set<String> setIdGrupo = new set<String>();
            map<string, string> mapDevPais = new map<string, string>();

            RecordType NodoRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_NAG'];

            for(BI_FVI_Nodos__c objNewRecords : lstNewRecords)
            {
                if (objNewRecords.RecordTypeId == NodoRecordType.Id)
                {
                    strPais = objNewRecords.BI_FVI_Pais__c.toUpperCase().substring(0, 3);
                    strGrupo = 'BI_FVI_'+strPais+'_DIST_ADM_BO';
                    mapDevPais.put(strGrupo,objNewRecords.BI_FVI_Pais__c);
                }
            }

            if (!mapDevPais.isEmpty())
            {
                list<Group> lstGroup = [SELECT Id,Name,DeveloperName FROM Group WHERE DeveloperName IN :mapDevPais.keySet()];
                if(lstGroup != null)
                {
                    for(Group objGroup : lstGroup)
                    {            
                        system.debug('mapaaaa ' + mapDevPais.values());
                        system.debug('Id Grupo ' + objGroup.Id);
                        BI_FVI_NodosShare.createNodosShare(objGroup.Id, mapDevPais.get(objGroup.DeveloperName));
                    }
                }
            }
           
        }
        catch(Exception e)
        {
            BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.NodoShare', 'BI_FVI', e, 'Trigger');
        }

    }
    */
    //END CRM 12/05/2017 Async Future Method Inside Loops
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Sevilla Durango
    Company:        NEAborda
    Description:    Method used to colaborate a new NAB Node with a group 

    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History:     
    
    <Date>                  <Author>                <Change Description>
    16/06/2016              Alvaro Sevilla             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void ColaborarNodosNAB(list<BI_FVI_Nodos__c> lstNodosNew, list<BI_FVI_Nodos__c> lstNodosOld){

        String pais;
        String grupo;
        RecordType NodoRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_NAB'];
        list<BI_FVI_Nodos__Share> lstNodoShareNAB = new list<BI_FVI_Nodos__Share>();
        list<BI_FVI_Nodos__Share> lstBorrarNodoShare = new list<BI_FVI_Nodos__Share>();
        set<String> stDevGrupo = new set<String>();
        set<String> stIdNodos = new set<String>();
        map<String,String> mpDevGrupo = new map<String,String>();
        map<String,BI_FVI_Nodos__Share> mpIdNodos = new map<String,BI_FVI_Nodos__Share>();

        try{

            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            for(BI_FVI_Nodos__c NewRecords : lstNodosNew){

                if(NewRecords.BI_FVI_Pais__c != null){

                    pais = NewRecords.BI_FVI_Pais__c.toUpperCase().substring(0, 3);
                    grupo = 'BI_FVI_'+pais+'_TEL_BO_DC';

                    if(!stDevGrupo.contains(grupo)){
                        stDevGrupo.add(grupo);                   
                    }

                }
                           
            }

            if(lstNodosOld != null && lstNodosOld.size() > 0){

                for(BI_FVI_Nodos__c OldRecords : lstNodosOld){

                    if(OldRecords.BI_FVI_Pais__c != null){

                        pais = OldRecords.BI_FVI_Pais__c.toUpperCase().substring(0, 3);
                        grupo = 'BI_FVI_'+pais+'_TEL_BO_DC';
                        stIdNodos.add(OldRecords.Id);

                        if(!stDevGrupo.contains(grupo)){
                            stDevGrupo.add(grupo);
                        }

                    }
                           
                }
               
            }
          
            for(Group objGroup : [SELECT Id,Name,DeveloperName FROM Group WHERE DeveloperName IN :stDevGrupo]){   

                mpDevGrupo.put(objGroup.DeveloperName,objGroup.Id);

            }  

            for(BI_FVI_Nodos__Share elemNodoSh : [SELECT Id,ParentId,UserOrGroupId FROM BI_FVI_Nodos__Share WHERE ParentId IN :stIdNodos]){

                mpIdNodos.put(elemNodoSh.ParentId,elemNodoSh);
            }

            for(Integer i=0; i < lstNodosNew.size(); i++){

                if(lstNodosNew[i].RecordTypeId == NodoRecordType.Id){

                    pais = lstNodosNew[i].BI_FVI_Pais__c.toUpperCase().substring(0, 3);
                    grupo = 'BI_FVI_'+pais+'_TEL_BO_DC';

                    if(mpDevGrupo.containsKey(grupo)){

                        BI_FVI_Nodos__Share objNodoShare = new BI_FVI_Nodos__Share();
                        objNodoShare.UserOrGroupId  = mpDevGrupo.get(grupo);
                        objNodoShare.ParentId = lstNodosNew[i].Id;
                        objNodoShare.AccessLevel = 'Edit';

                        lstNodoShareNAB.add(objNodoShare);

                    }

                    if(lstNodosOld != null && lstNodosOld.size() > 0){

                        if(lstNodosOld[i].BI_FVI_Pais__c != lstNodosNew[i].BI_FVI_Pais__c){
                        
                                lstBorrarNodoShare.add(mpIdNodos.get(lstNodosOld[i].Id));                    

                        }
                    }
                }
                
            }
                        
            if(lstNodoShareNAB != null && lstNodoShareNAB.size() > 0){
                
                insert lstNodoShareNAB;
            }

            if(lstBorrarNodoShare != null && lstBorrarNodoShare.size() > 0) {

                delete lstBorrarNodoShare;            
            }

        }catch(Exception exc){

             BI_LogHelper.generate_BILog('BI_UserMethods.ColaborarNodosNAB', 'BI_FVI', exc, 'Trigger');
        }

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García             
    Company:       NEAborda
    Description:   Método para reasignar a los clientes en nodos de barbecho el primero de cada mes en funcion de las visitas 

    History: 

    <Date>                  <Author>                <Change Description>
    09/06/2016              Humberto Nunes          Initial Version     
    06/02/2017              Adrián Caro             call enqueueJob
    04/04/2017              Humberto Nunes          Se incluyo el País como parametro en la Funcion para cuando sea llamada desde el boton, solo ejecute el
                                                    El proceso para el pais del nodo del barbecho desde el cual se pulsa. 
                                                    Se controla que existe el pais en el mapa "map_country_lov". 
    16/05/2017              Guillermo Muñoz         Se realiza la actualización de los nodos mediante la clase BI_LoadAllKindQueable
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    webservice static void asignaNodosBarbechoSchedule_Countries(String Pais)
    {
        system.debug('ENTRO POR ACA.... asignaNodosBarbechoSchedule_Countries'+Pais);
        try{
            
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
            
            List<BI_FVI_Nodos__c> lst_NAB;
            List<BI_FVI_Nodo_Cliente__c> lst_nc;
            List<BI_FVI_Nodo_Cliente__c> lst_nc_Upd  = new List<BI_FVI_Nodo_Cliente__c>();

            if(Pais != null && Pais != '')
            {
                lst_NAB = [SELECT Id, Name, BI_FVI_Pais__c FROM BI_FVI_Nodos__c WHERE RecordType.DeveloperName = 'BI_FVI_NAB' AND BI_FVI_Pais__c =: Pais];
                lst_nc =  [SELECT   Id, 
                                    BI_FVI_IdCliente__c, 
                                    BI_FVI_IdCliente__r.BI_FVI_NDiasUltimaVisita__c,
                                    BI_FVI_IdCliente__r.BI_FVI_NDiasIngresoNAT__c,
                                    BI_FVI_IdNodo__c, 
                                    BI_FVI_IdNodo__r.BI_FVI_Pais__c,
                                    BI_FVI_IdNodo__r.RecordType.DeveloperName 
                            FROM    BI_FVI_Nodo_Cliente__c 
                            WHERE   (BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAV' OR BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAT') AND BI_FVI_IdNodo__r.BI_FVI_Pais__c =: Pais];
            }
            else 
            {
                lst_NAB = [SELECT Id, Name, BI_FVI_Pais__c FROM BI_FVI_Nodos__c WHERE RecordType.DeveloperName = 'BI_FVI_NAB'];
                lst_nc =  [SELECT   Id, 
                                    BI_FVI_IdCliente__c, 
                                    BI_FVI_IdCliente__r.BI_FVI_NDiasUltimaVisita__c,
                                    BI_FVI_IdCliente__r.BI_FVI_NDiasIngresoNAT__c,
                                    BI_FVI_IdNodo__c, 
                                    BI_FVI_IdNodo__r.BI_FVI_Pais__c,
                                    BI_FVI_IdNodo__r.RecordType.DeveloperName 
                            FROM    BI_FVI_Nodo_Cliente__c 
                            WHERE   BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAV' OR BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAT'];
            }
                
            system.debug('lst_NAB:' + lst_NAB); 
            system.debug('lst_nc:' + lst_nc); 

            Map<String, Map<Integer, Id>> map_country_nb = new Map<String, Map<Integer, Id>>();
            Map<Integer, Id> OrderNodes;
            for(BI_FVI_Nodos__c nodoB : lst_NAB)
            {
                Integer Order = 0;

                if (nodoB.Name.indexOf('_ENE') != -1) Order = 1;
                if (nodoB.Name.indexOf('_FEB') != -1) Order = 2;
                if (nodoB.Name.indexOf('_MAR') != -1) Order = 3;
                if (nodoB.Name.indexOf('_ABR') != -1) Order = 4;
                if (nodoB.Name.indexOf('_MAY') != -1) Order = 5;
                if (nodoB.Name.indexOf('_JUN') != -1) Order = 6;
                if (nodoB.Name.indexOf('_JUL') != -1) Order = 7;
                if (nodoB.Name.indexOf('_AGO') != -1) Order = 8;
                if (nodoB.Name.indexOf('_SEP') != -1) Order = 9;
                if (nodoB.Name.indexOf('_OCT') != -1) Order = 10;
                if (nodoB.Name.indexOf('_NOV') != -1) Order = 11;
                if (nodoB.Name.indexOf('_DIC') != -1) Order = 12;

                if(!map_country_nb.containsKey(nodoB.BI_FVI_Pais__c))  map_country_nb.put(nodoB.BI_FVI_Pais__c, new Map<Integer, Id>());
                OrderNodes = map_country_nb.get(nodoB.BI_FVI_Pais__c);
                OrderNodes.put(Order,nodoB.Id);
            }

            system.debug(map_country_nb);
           
            List<NE__Lov__c> lst_lovs = [SELECT Name, NE__Value1__c,NE__Value2__c,NE__Value3__c,Country__c FROM NE__Lov__c WHERE NE__Type__c = 'NODOS' AND Name LIKE 'N_D%'];
            Map<String, List<String>> map_country_lov = new Map<String, List<String>>();
            for(NE__Lov__c lov : lst_lovs)
            {
                List<String> lst_st = new List<String>();
                lst_st.add(lov.NE__Value1__c);
                lst_st.add(lov.NE__Value2__c);
                lst_st.add(lov.NE__Value3__c);

                if(!map_country_lov.containsKey(lov.Country__c))
                {
                    map_country_lov.put(lov.Country__c, lst_st);
                }
                
            }

            system.debug(map_country_lov);
           
            Set<String> set_paises = new Set<String>();

            Date fecha_actual = Date.today();
            Integer mes_actual = fecha_actual.month();                   

            for (BI_FVI_Nodo_Cliente__c nc: lst_nc)
            {
                if (map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)!=null)
                {
                    Integer salto = Integer.valueOf(map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[1]);
                    Integer mes_salto = mes_actual + salto;
                    if(mes_salto > 12 ) mes_salto -= 12;

                    system.debug('mes_actual:' + mes_actual + ' salto:' + salto + ' mes_salto: ' + mes_salto +  ' [0]: ' + map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[0] +  ' [1]: ' + map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[1] +  ' [2]: ' + map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[2]);
                    system.debug('nc.BI_FVI_IdNodo__r.RecordType.DeveloperName:' + nc.BI_FVI_IdNodo__r.RecordType.DeveloperName); 
                    system.debug('Integer.valueOf(map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[2]):' + Integer.valueOf(map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[2]) + ' NDINAT:' + nc.BI_FVI_IdCliente__r.BI_FVI_NDiasIngresoNAT__c) ; 

                    if ( (nc.BI_FVI_IdNodo__r.RecordType.DeveloperName == 'BI_FVI_NAV') || (nc.BI_FVI_IdNodo__r.RecordType.DeveloperName == 'BI_FVI_NAT' && nc.BI_FVI_IdCliente__r.BI_FVI_NDiasIngresoNAT__c > Integer.valueOf(map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[2])) )
                    {   
                        system.debug('ENTRO POR ACA....');

                        if(nc.BI_FVI_IdCliente__r.BI_FVI_NDiasUltimaVisita__c > Integer.valueOf(map_country_lov.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c)[0]) || nc.BI_FVI_IdCliente__r.BI_FVI_NDiasUltimaVisita__c == null)
                            nc.BI_FVI_IdNodo__c = map_country_nb.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c).get(mes_actual);
                        else
                            nc.BI_FVI_IdNodo__c = map_country_nb.get(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c).get(mes_salto);     

                        lst_nc_Upd.add(nc);                 
                    }
                    set_paises.add(String.valueOf(nc.BI_FVI_IdNodo__r.BI_FVI_Pais__c));
                }                
            }
            system.debug('ENTRO POR ACA.... lst_nc_Upd: ' + lst_nc_Upd);
            //update lst_nc_Upd;
            //Integer i = 0;
            //String [] FVI_PAIS = lst_nc_Upd.get(BI_FVI_IdNodo__r.BI_FVI_Pais__c);
            //System.enqueueJob(new BI_FVI_ListNodos_Queueable(lst_nc_Upd,i,set_paises));
            if(!lst_nc_Upd.isEmpty()){

                System.enqueueJob(new BI_LoadAllKindQueable(lst_nc_Upd, 50, 'UPDATE', 'Movimiento Nodos ' + System.now()));
            }
        }
        catch(Exception exc)
        {
            BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.asignaNodosBarbechoSchedule_Countries', 'BI_FVI', Exc, 'Trigger');
        }
    }  


      /*-------------------------------------------------------------------------------
    Author:        Humberto Nunes De Sousa
    Company:       New Energy Aborda
    Description:   class for sharing object nodos
    Test Class:    BI_FVI_NodosShare_TEST
    
    History:
     
    <Date>                  <Author>                    <Change Description>
    14/06/2016              Humberto Nunes              Initial Version
    18/04/2018				Javier Lopez				JLA_comprobacion de que el cambio sea sobre un Nodo activo
    -------------------------------------------------------------------------------*/ 
    public static void changeOwnerAccounts(List<BI_FVI_Nodos__c> tnew , List<BI_FVI_Nodos__c> told)
    {   
        try
        {
            set<Id> setIdNodos = new set<Id>();
            for (Integer i = 0 ; i < tnew.size() ; i++)
                if (tnew[i].OwnerId != told[i].OwnerId)
                    setIdNodos.add(tnew[i].Id);

            if (!setIdNodos.isEmpty())
            {
                List<Account> accountOwners = [SELECT id, OwnerId, BI_FVI_Nodo__r.OwnerId,BI_FVI_Nodo__r.BI_FVI_Activo__c from Account where BI_FVI_Nodo__c in :setIdNodos];
                for (Account A: accountOwners)
                	if(A.BI_FVI_Nodo__r.BI_FVI_Activo__c==true)
                    	A.OwnerId = A.BI_FVI_Nodo__r.OwnerId;

                update accountOwners;                           
            }           
        }
        catch(Exception e)
        {
            BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.changeOwnerAccounts', 'BI_FVI', e, 'Trigger');
        }

    }
      /*-------------------------------------------------------------------------------
    Author:        Javier Lopez
    Company:       gCTIO
    Description:   class for update OwnerShip od the Account related to a node OR the AccountTeamMembers when it passed to new
    Test Class:    
    
    History:
     
    <Date>                  <Author>                    <Change Description>
    2018/04/18              Javier Lopez                Initial Version
    2018/04/24				Javier Lopez				Changed for add AccountTeamMember methods
	2018/06/21				Javier Lopez				Changed for NCS it will made by lightning controller
	2018/60/21				Javier Lopez				Deprecated always in lightning
    -------------------------------------------------------------------------------*/ 
    public static void changeOwnerActivateNode(List<BI_FVI_Nodos__c> news,List<BI_FVI_Nodos__c> olds){
    	/*System.debug('JLA: Nodos changeOwnerActivateNode START');
        List<String> lst_nodes = new List<String>();
        Map<Id,Schema.RecordTypeInfo> mp_rt = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosById();
        
        for(Integer i = 0;i<news.size();i++){
            //JLA_ comprobamos que ha sido modificado el campo activo, si no hace nada
            if(news[i].BI_FVI_Activo__c!=olds[i].BI_FVI_Activo__c && news[i].BI_FVI_Activo__c==true && mp_rt.get(news[i].RecordTypeId).getDeveloperName().equals('BI_FVI_NCP')){
                lst_nodes.add(news[i].Id);
            }
        }
        System.debug('JLA: Nodos changeOwnerActivateNode lista nodos'+lst_nodes);
        if(lst_nodes.isEmpty()==false){
            //Map<ID,BI_FVI_Nodos__c> mp_nodes = new MAp<Id,BI_FVI_Nodos__c>([Select Id,OwnerId from BI_FVI_Nodos__c where Id in :lst_nodes]);
            //Al haberse activado el nodo hemos de nuscar todos los clientes ligados al mismo para cambiar la propiedad del mismo
            List<BI_FVI_Nodo_Cliente__c> lst_vnc = new List<BI_FVI_Nodo_Cliente__c>(); 
            Map<Id,List<BI_FVI_Nodo_Cliente__c>> mp_ncs = new Map<Id,List<BI_FVI_Nodo_Cliente__c>>();
            List<BI_FVI_Nodo_Cliente__c> lst_nodesClie =[Select BI_FVI_IdCliente__c,BI_FVI_IdNodo__c,BI_FVI_IdNodo__r.OwnerId,BI_FVI_IdNodo__r.RecordType.DeveloperName from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c in :lst_nodes];
            for(BI_FVI_Nodo_Cliente__c ndc : lst_nodesClie){
            	System.debug('JLA: developer NAme rt->'+ndc.BI_FVI_IdNodo__r.RecordType.DeveloperName);
            	/*if(ndc.BI_FVI_IdNodo__r.RecordType.DeveloperName.equals('BI_FVI_NCS')){
            		List<BI_FVI_Nodo_Cliente__c> aux = mp_ncs.get(ndc.BI_FVI_IdNodo__c);
            		if(aux==null)
            			aux=new List<BI_FVI_Nodo_Cliente__c>();
            		aux.add(ndc);
            		mp_ncs.put(ndc.BI_FVI_IdNodo__c,aux);
            	}else{
            		lst_vnc.add(ndc);
            	//}

            }

            System.debug('JLA: Nodos changeOwnerActivateNode lista nodos clientes Para cambiar Owner'+lst_vnc);
            System.debug('JLA: Nodos changeOwnerActivateNode lista nodos clientes Para cambiar Añadir al equipo de cuentas'+mp_ncs);
            List<Account> lst_updAcc = new List<Account>();
            //En caso de no tener no hay que hacer nada
            if(lst_vnc.isEmpty()==false){
                for(BI_FVI_Nodo_Cliente__c vnc : lst_vnc){
                    Account acc = new Account(Id=vnc.BI_FVI_IdCliente__c, OwnerId=vnc.BI_FVI_IdNodo__r.OwnerId,BI_FVI_Nodo__c=vnc.BI_FVI_IdNodo__c);
                    lst_updAcc.add(acc);
                }
                System.debug('JLA: Nodos changeOwnerActivateNode lista cuentas a actualizar: '+lst_updAcc);
                Map<Integer,BI_bypass__c> mp_by = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),false,true,false,false);                
                DataBase.SaveResult [] arr_svr = DataBase.update(lst_updAcc);
                BI_MigrationHelper.disableBypass(mp_by);
                for(DataBase.SaveResult svr :arr_svr){
                    if(svr.isSuccess()==false){
                    	String errString ='';
                    	for(DataBase.Error err : svr.getErrors()){
                    		errString+=err.getStatusCode()+': '+err.getMessage();
                    	}
                    	BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.changeOwnerAccounts,'+errString+'//Id de la cuenta '+svr.getId(), 'BI_FVI', null, 'Trigger');
                    	for(BI_FVI_Nodos__c node: news){
                    		node.addError('Error inseperado actualizando la cuenta del nodo: '+errString);
                    	}
                    }
                }
            }
          /*JLA_Deprecated
           *   if(mp_ncs.isEmpty()==false){
            	List<BI_FVI_Nodos__c> lst_nodes_ncs = [Select Id,OwnerId,BI_FVI_Funcion__c,BI_FVI_Acceso_oportunidades__c,BI_FVI_Acceso_clientes__c,BI_FVI_Acceso_casos__c from BI_FVI_Nodos__c where Id in :mp_ncs.keySet() AND BI_FVI_Asignacion_atomatica__c=true];
            	if(lst_nodes_ncs.isEmpty()==false){
            		List<AccountTeamMember> lst_acctm = new List<AccountTeamMember>();
            		List<AccountShare> lst_accSh = new List<AccountShare>();
            		for(BI_FVI_Nodos__c node : lst_nodes_ncs){
            			List<BI_FVI_Nodo_Cliente__c> lst_ndc = mp_ncs.get(node.Id);
            			for(BI_FVI_Nodo_Cliente__c ndc : lst_ndc){
            				AccountTeamMember acctm = new AccountTeamMember();
            				acctm.AccountId = ndc.BI_FVI_IdCliente__c;
            				acctm.UserID = node.OwnerID;
            				acctm.TeamMemberRole = node.BI_FVI_Funcion__c;
            				lst_acctm.add(acctm);
            				AccountShare accSh = new AccountShare();
            				accSh.AccountId=ndc.BI_FVI_IdCliente__c;
							accSh.userorgroupid =node.OwnerId;
							accSh.OpportunityAccessLevel=node.BI_FVI_Acceso_oportunidades__c;
							accSh.AccountAccessLevel=node.BI_FVI_Acceso_clientes__c;
							accSh.CaseAccessLevel=node.BI_FVI_Acceso_casos__c;
							lst_accSh.add(accSh);
            			}
            		}
            		try{
                       
            			insert lst_acctm;
                        
            		}catch(Exception e){
            			BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.changeOwnerAccounts, asignacion al equipo de CUENTAS PHASE 1', 'BI_FVI', e, 'Trigger');
            			for(BI_FVI_Nodos__c node : lst_nodes_ncs){
            				node.addError('Error inseperado añadiendo el propietario del nodo al equipo de cuentas: '+e.getMessage());
            			}
                        
            			return;
            		}
            		try{
            			insert lst_accSh;
            		}catch(Exception e){
            			BI_LogHelper.generate_BILog('BI_FVI_NodosMethods.changeOwnerAccounts, asignacion al equipo de CUENTAS PHASE 2', 'BI_FVI', e, 'Trigger');
            			for(BI_FVI_Nodos__c node : news){
            				node.addError('Error inseperado añadiendo el propietario del nodo al equipo de cuentas: '+e.getMessage());
            			}
            			return;
            		}

            	}
            }
            
        }
        System.debug('JLA: Nodos changeOwnerActivateNode END');*/
    }
    
          /*-------------------------------------------------------------------------------
    Author:        Daniel Sánchez	
    Company:       everis
    Description:   Method to create Plan de Cartera child record to Nodo
    Test Class:    BI_FVI_NodosMethods_TEST
    
    History:
     
    <Date>                  <Author>                    <Change Description>
    2018/09/10              Daniel Sánchez              Initial Version

    -------------------------------------------------------------------------------*/ 
    public static void createCartera(List<BI_FVI_Nodos__c> news,List<BI_FVI_Nodos__c> olds){
        Id ncpIdRT = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Comercial Principal').getRecordTypeId();
        Id ncsIdRT = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Comercial Superior').getRecordTypeId();
        List<E2PC_Cartera__c> carts = new List<E2PC_Cartera__c>();
        for (Integer i = 0 ; i < news.size() ; i++){
            if (news[i].RecordTypeId == ncpIdRT || news[i].RecordTypeId == ncsIdRT){
                E2PC_Cartera__c cart = new E2PC_Cartera__c();
                Integer currentYear = System.Today().year();
                cart.Name = 'PCA_' + news[i].Name;
                cart.E2PC_Nodo__c = news[i].Id;
                cart.CurrencyIsoCode = news[i].CurrencyIsoCode;
                carts.add(cart);
            }
        }
        insert carts;
    }
    
    
    /*-------------------------------------------------------------------------------
Autor:        Federico Peña
Compañía:     Certa
Descripción:  Método para no dejar cambiar el propietario ni tipo de nodo si está activo o si alguna vez lo estuvo

Historial:

<Date>                  <Author>                    <Change Description>
20/02/2019              Federico Peña                Versión Inicial   
10/07/2019				Pablo Belzunce				 Quitada validación que impedía cambiar el tipo del nodo si éste está o había estado activo
17/07/2019				Pablo Belzunce				 Ahora se permite cambiar el owner de nodos FVI que están o estuvieron activos.
-------------------------------------------------------------------------------*/ 
    
    public static void validacionesNodoActivo(List<BI_FVI_Nodos__c> news, Map<Id, BI_FVI_Nodos__c> oldMap){
        
        //Cargo una lista con todos los ids de nodos.
        Set<Id> nodosIds = new Set<Id>();
        for (BI_FVI_Nodos__c nodo : news){
            nodosIds.add(nodo.Id);
        }
        
        //Query para traer todos los historiales de modificacion del checkbox activo.
        List<BI_FVI_Nodos__History> listaHistorial = new List<BI_FVI_Nodos__History>();
		listaHistorial = [SELECT ParentId, Field, NewValue,OldValue FROM BI_FVI_Nodos__History WHERE ParentId in :nodosIds AND Field = 'BI_FVI_Activo__c'];
        
        for(BI_FVI_Nodos__c nodo: news){
            
            Boolean estaOEstuvoActivo = false;
            
            //Valido si el nodo actualmente esta activo
            if(nodo.BI_FVI_Activo__c == true){
                estaOEstuvoActivo = true;
            }
            
            if(listaHistorial.size() > 0){
                //Para cada registro del historial, me fijo si pertenece al nodo en cuestion y si alguna vez estuvo el checkbox en true.
                for(BI_FVI_Nodos__History historial: listaHistorial){
                    if(historial.ParentId == nodo.id && (historial.NewValue == true || historial.OldValue == true) ){
                        estaOEstuvoActivo = true; 
                    }
                }
            }
            
            //Me fijo si estoy cambiando el propietario del nodo y si esta o estuvo activo alguna vez.
            //Agregado: también me fijo que el nodo no sea FVI
            if(nodo.OwnerId != oldMap.get(nodo.Id).OwnerId && estaOEstuvoActivo && nodo.BI_FVI_Overlay__c != 'Principal FVI' 
               && nodo.BI_FVI_Overlay__c != 'Superior FVI' && nodo.RecordType.DeveloperName != 'BI_FVI_NAT' && nodo.RecordType.DeveloperName == 'BI_FVI_NAV'){
                nodo.addError('No se puede cambiar el propietario de un nodo activo o que haya estado activo.'); 
            }
            
            //Me fijo si estoy cambiando el tipo del nodo y si esta o estuvo activo aguna vez.
            /*Validación retirada
            if(nodo.BI_FVI_Overlay__c != oldMap.get(nodo.Id).BI_FVI_Overlay__c && estaOEstuvoActivo){
                nodo.addError('No se puede cambiar el tipo de un nodo activo o que haya estado activo.'); 
            }
			*/
            
        } 
        
    }
}