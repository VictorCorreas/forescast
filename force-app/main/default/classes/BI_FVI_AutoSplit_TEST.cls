/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       Aborda
        Description:   Class to assure BI_FVI_AutoSplit class coverage
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
      27/06/2016                      Jorge Galindo             Initial version
	03/08/2016	          Jorge Galindo		      added new mandatory BI_Licitacion__c, StageName, BI_Fecha_de_vigencia__c to Opportunity, moved opportunity creation 
      20/09/2017                  Angel F. Santaliestra       Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class BI_FVI_AutoSplit_TEST {

     static List<Account> lst_acc = new List<Account>();
     static List<Opportunity> lst_opp =  new List<Opportunity>();
     static List<NE__OrderItem__c> lst_orderItemRoot =  new List<NE__OrderItem__c>();
    
    
     @isTest private static void dataLoad(){
        User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        system.runAs(me){

            Account acc1 = new Account(
                        Name = 'XXX Zytrust 1',
                        BI_Tipo_de_identificador_fiscal__c = 'DNI',
                        BI_No_Identificador_fiscal__c = '20518569391',
                        BI_Country__c= Label.BI_Peru,
                        BillingStreet = 'calle xxx',
                        BillingCity = 'ciudad xxx',
                        BillingState = 'provincia xxx',
                        BillingPostalCode = '00000',
                        BillingCountry = Label.BI_Peru,
                        BI_Segment__c = 'test',
                        BI_Subsegment_Regional__c = 'test',
                        BI_Territory__c = 'test',
                        BI_FVI_Tipo_Planta__c = ''
            );
                  lst_acc.add(acc1);

            
            
                  insert lst_acc;
                  
                  List<Contact> lst_cont = new List<Contact>();
                  for(Integer i = 0; i < 1; i++){
                        Contact con = new Contact(
                              LastName = 'test'+ String.valueOf(i),
                              AccountId = lst_acc[i].Id,
                              Email = 'test'+ String.valueOf(i)+'@gmail.com',
                              HasOptedOutOfEmail = false,
                              BI_Activo__c = true,
                              BI_Tipo_de_documento__c = 'NIF',
                              BI_Country__c = Label.BI_Peru,
                              BI_Numero_de_documento__c = '0000000' + i
                        );
                        lst_cont.add(con);
                  }

                  insert lst_cont;


            

          

            NE__Catalog_Header__c catalogheaderobj = new NE__Catalog_Header__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test FVI',
                    NE__Active__c = true,
                    NE__Enable_Global_Cart__c = false,
                    NE__Name__c = 'Catalog FVI',
                    NE__Source_Catalog_Header_Id__c = 'a1R11000000H0tc',
                    NE__Start_Date__c = system.today()-1
                  ); 
                  insert catalogheaderobj;
            
                  NE__Catalog__c catalogobj = new NE__Catalog__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test FVI',
                    NE__Active__c = true,
                    NE__Catalog_Header__c = catalogheaderobj.Id,
                    NE__Enable_for_oppty__c = false,
                    NE__Engine_Code__c = 'a1U110000002812',
                    NE__Source_Catalog_Id__c = 'a1U110000002812',
                    NE__Version__c = 1.0,
                    NE__Visible_web__c = false
                  ); 
                  insert catalogobj;

                  NE__Catalog_Category__c catalogCategory = new NE__Catalog_Category__c(
                        Name = 'Movil',
                        NE__CatalogId__c = catalogobj.Id
                  );

                  List<NE__Product__c> lst_prod = new List<NE__Product__c>();
                  NE__Product__c prod1 = new NE__Product__c(
                        Name = 'Plan vuela 1',
                        Payback_Family__c = 'Planes'
                  );
                  NE__Product__c prod2 = new NE__Product__c(
                        Name = 'Plan vuela 2',
                        Payback_Family__c = 'Planes'
                  );
                  NE__Product__c prod3 = new NE__Product__c(
                        Name = 'Iphone 5',
                        Payback_Family__c = 'Terminales'
                  );
                  NE__Product__c prod4 = new NE__Product__c(
                        Name = 'Iphone 6',
                        Payback_Family__c = 'Terminales'
                  );
            
            	  NE__Product__c prod5 = new NE__Product__c(
                        Name = 'Oferta Complex'
                  );
            
            
                  lst_prod.add(prod1);
                  lst_prod.add(prod2);
                  lst_prod.add(prod3);
                  lst_prod.add(prod4);
            	  lst_prod.add(prod5);

                  insert lst_prod;

                  List<NE__DocumentationCharacterist__c> lst_doc = new List<NE__DocumentationCharacterist__c>();
                  
                  for(NE__Product__c prod : lst_prod){
                        
                        NE__DocumentationCharacterist__c doc1 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: Todo Destino (M-M/M-O/M-F/M-LDI*) *incluye USA y Cánada',
                              NE__Product__c = prod.Id,
                              NE__Value__c = '01'
                        );
                        lst_doc.add(doc1);

                        NE__DocumentationCharacterist__c doc2 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: 6 DUOs',
                              NE__Product__c = prod.Id,
                              NE__Value__c = '11'
                        );
                        lst_doc.add(doc2);

                        NE__DocumentationCharacterist__c doc3 = new NE__DocumentationCharacterist__c(
                              Name = 'M-LDI (Menos USA y Canada)',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '02'
                        );
                        lst_doc.add(doc3);

                        NE__DocumentationCharacterist__c doc4 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: RPM',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc4);

                        NE__DocumentationCharacterist__c doc5 = new NE__DocumentationCharacterist__c(
                              Name = 'SMS',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc5);

                        NE__DocumentationCharacterist__c doc6 = new NE__DocumentationCharacterist__c(
                              Name = 'MMS',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc6);

                        NE__DocumentationCharacterist__c doc7 = new NE__DocumentationCharacterist__c(
                              Name = 'SMS Internacional',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc7);


                        NE__DocumentationCharacterist__c doc8 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: LDI (USA y Canada)',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc8);

                        NE__DocumentationCharacterist__c doc9 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: LDI América',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc9);

                        NE__DocumentationCharacterist__c doc10 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: LDI Europa',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc10);

                        NE__DocumentationCharacterist__c doc11 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: LDI Africa / Oceania',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc11);

                        NE__DocumentationCharacterist__c doc12 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: MO',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc12);


                        NE__DocumentationCharacterist__c doc13 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: MF',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc13);

                        NE__DocumentationCharacterist__c doc14 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: MM',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc14);

                        NE__DocumentationCharacterist__c doc15 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: SMS Internacional',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc15);

                         NE__DocumentationCharacterist__c doc16 = new NE__DocumentationCharacterist__c(
                              Name = 'Tarifas Adicionales: WAP',
                              NE__Product__c = prod.Id,          
                              NE__Value__c = '10'
                        );
                        lst_doc.add(doc16);
                  }

                  insert lst_doc;
                    
                  List<NE__Catalog_Item__c> lst_ci = new List<NE__Catalog_Item__c>();
            
            
            	  // Plan vuela 1
                  NE__Catalog_Item__c catalogItem1 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[0].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );
            
                  // Plan vuela 2
                  NE__Catalog_Item__c catalogItem2 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[1].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );
            
            	  // Iphone 5
                  NE__Catalog_Item__c catalogItem3 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[2].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );
            
                  // Iphone 6
                  NE__Catalog_Item__c catalogItem4 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[3].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );

                  lst_ci.add(catalogItem1);
                  lst_ci.add(catalogItem2);
                  lst_ci.add(catalogItem3);
                  lst_ci.add(catalogItem4);

                  insert lst_ci;
            
            //JGL 03/08/2016
            RecordType rt_cc= [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Ciclo_completo'];
            lst_opp = new List<Opportunity>();
            Opportunity oppCaso1 = new Opportunity(
                Name = 'Test 1',
                BI_Country__c = 'Peru',
                AccountId = lst_acc[0].Id,
                RecordTypeId = rt_cc.Id,
                BI_Opportunity_Type__c = 'Movil',
                StageName = 'F2 - Negotiation',
                BI_Duracion_del_contrato_Meses__c = 14,
                BI_Plazo_estimado_de_provision_dias__c = 22,
                CloseDate = Date.today() + 15,
                BI_Probabilidad_de_exito__c = '95',
                CurrencyIsoCode = 'MXN',
                BI_Requiere_contrato__c = false,
                BI_FVI_Contacto__c = lst_cont[0].Id,
                BI_Licitacion__c = 'No',
                BI_Fecha_de_vigencia__c = Date.today() + 15,
                BI_FVI_Payback__c = 0
            );
            lst_opp.add(oppCaso1);
            
            Test.startTest();
            
            insert lst_opp;

			Test.stopTest();
            
            	List<NE__Order__c> lst_order = new List<NE__Order__c>();
                  NE__Order__c order1 = new NE__Order__c(
                        NE__OptyId__c = oppCaso1.Id,
                        NE__CatalogId__c = catalogobj.Id,
                        Payback__c = 0,
                        NE__PayBack__c = 0
                  );
                
                  lst_order.add(order1);
           
                  insert lst_order;
            
		
                  //CASE 1: Móvil with Plan Vuela 1 and Iphone 5
                  // First we have to create the root order Item, and then , to create the childs 
                  NE__OrderItem__c orderItem1 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'MNP Port In',
                        NE__Qty__c = 3,
                        NE__BaseOneTimeFee__c =  25,
                        NE__ProdId__c = lst_prod[4].id,
                        BI_FVI_Lineas_de_Portabilidad__c = '1111,2222,3333',
                        NE__Catalog__c = catalogobj.Id
                  );
                  lst_orderItemRoot.add(orderItem1);

                
            
                 insert lst_orderItemRoot;
            
            	
            
            	  //03/08/2016 JGL added OI attribute
 				  List<NE__Order_Item_Attribute__c> oIAttributesForRoots  = new  List<NE__Order_Item_Attribute__c>();
                      
                  NE__Order_Item_Attribute__c oiAtt  = new NE__Order_Item_Attribute__c(
                      										Name = 'Permanencia',
                                                            CurrencyIsoCode = 'EUR',
                                                            NE__Order_Item__c =  lst_orderItemRoot[0].id,
                                                            NE__Value__c = '12 meses'            
                                                            );
            
            	  oIAttributesForRoots.add(oiAtt);
            
            	  insert oIAttributesForRoots;
            
            	  List<NE__OrderItem__c> lst_orderItem = new List<NE__OrderItem__c>();
             
             	  NE__OrderItem__c orderItem2 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'MNP Port In',
                        NE__Qty__c = 1,
                        NE__BaseOneTimeFee__c =  25,
                        NE__ProdId__c = lst_prod[1].Id,
                        NE__CatalogItem__c = catalogItem2.Id,
                        NE__Parent_Order_Item__c = lst_orderItemRoot[0].id,
                        NE__Root_Order_Item__c = lst_orderItemRoot[0].id,
                        NE__Catalog__c = catalogobj.Id
                  );
            	
                  lst_orderItem.add(orderItem2);
            
                  NE__OrderItem__c orderItem3 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'MNP Port In',
                        NE__Qty__c = 1,
                        NE__BaseOneTimeFee__c =  25,
                        NE__CatalogItem__c = catalogItem3.Id,
                        NE__Parent_Order_Item__c = lst_orderItemRoot[0].id,
                        NE__Root_Order_Item__c = lst_orderItemRoot[0].id,
                        NE__Catalog__c = catalogobj.Id
                  );
                  lst_orderItem.add(orderItem3);
            
                 
            	
                  insert lst_orderItem;

        }
    }
    
     @isTest static void autoSplit_TEST(){
   			
         	System.debug('BEGINNIG autoSplit_TEST');
         
            User user = [select Id from User where Id = :UserInfo.getUserId() LIMIT 1];
            system.runAs(user){
                  dataLoad();
            }
         	
         	List<NE__OrderItem__c> oItems = [SELECT id,Name,NE__Catalog__c,NE__CatalogItem__c,NE__Root_Order_Item__c,NE__Qty__c,BI_FVI_Lineas_de_Portabilidad__c,NE__ProdId__c FROM NE__OrderItem__c];
         
         	System.debug('OITEMS LIST :' + JSON.serialize(oItems));
                
         	Map<Id,Opportunity> optyMap = new Map<Id,Opportunity>([SELECT id,StageName FROM Opportunity]);
         
         	System.debug('optys size : ' + optyMap.values().size());
              
         	
         
         	//BI_FVI_AutoSplitMethods.autoSplit(optyMap,null);
       
         
            System.debug('ENDING autoSplit_TEST');
        
         

    }
    
    
    
}