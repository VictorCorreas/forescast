/******************************************************
 * Author: Javier Lopez
 * Company: gCTIO
 * Description: class without sharing for LAM modified FO of GAM user, dont have visibilities
 * Date: 29/05/2018
 * 
 * ****************************************************/

public without sharing class FO_ForecastMethodsAllView {
/**************************************************
	 * Author: Javier Lopez Andradas
	 * Company: gCTIO
	 * Description:  Changes the values of Forecast GAM if the values is changed
	 * 
	 * <date>		<version>			<description>
	 * 24/05/2018	1.0					initial version
	 * 31/04/2018	1.1					Avoid null pointer exception when the FO_Tipo_Forecast__c==null
	 * 06/06/2018	1.2					Avoid null pointer exception when economics values ==null
	 * ***********************************************/
    
    public static void actualizaFOGAM(List<FO_Forecast__c> news,List<FO_Forecast__c> olds){
     /*   System.debug('JLA_Start_actualizaGAM');
        System.debug('JLA_news: '+news);
        System.debug('JLA_olds: '+olds);
        List<FO_Forecast__c> lst_fo = new List<FO_Forecast__C>();
        List<String> lst_foEnvia = new List<String>();
        List<String> lst_foBorra = new List<String>();
        for(Integer i = 0;i<news.size();i++){
            if(news.get(i).FO_Estado__c!=olds.get(i).FO_Estado__c){
                if(news.get(i).FO_Estado__c.equals('Enviado')){
                    lst_foEnvia.add(news.get(i).Id);
                }else{
                    lst_foBorra.add(news.get(i).Id);
                }
            }
        }
        System.debug('JLA_foEnvia: '+lst_foEnvia);
        System.debug('JLA_foBorra: '+lst_foBorra);
        if(lst_foEnvia.isEmpty() && lst_foBorra.isEmpty())
            return;
        //Pendiente de definicion de si en enviado se pueden modiciar los forecast
        List<FO2_Forecast_Item_LAM__c> lst_foiLAM = [Select FO2_Forecast_Item__r.CurrencyISoCode,FO2_Forecast_Item__r.FO_Tipo_Forecast__c,FO2_Forecast_Item__r.FO_NAV__c,FO2_Forecast_Item__r.FO2_NBAV_Budget__c,FO2_Forecast__r.CurrencyIsoCode,FO2_Forecast__r.FO2_API__c,FO2_Forecast__r.FO2_Monocurrency__c,FO2_Forecast__c,FO2_Forecast__r.FO_Commit_Pipeline__c,FO2_Forecast__r.FO_Upside_Pipeline__c,FO2_Forecast_Item__r.FO_Forecast_Padre__c from FO2_Forecast_Item_LAM__c where FO2_Forecast_Item__c in (Select Id from FO_Forecast_Item__c where FO_Forecast_Padre__c in :lst_foEnvia OR FO_Forecast_Padre__c in :lst_foBorra) AND FO2_Forecast__r.FO_Estado__c='Borrador'];
        System.debug('lst_foiLAM: '+lst_foiLAM);
       	List<CurrencyType> lst_curr = [Select IsoCode,ConversionRate from CurrencyType];
        Map<String,Double> mp_curr = new Map<String,Double>();
        for(CurrencyType curr:lst_curr)
            mp_curr.put(curr.IsoCode,curr.ConversionRate);
        Map<String,list<FO2_Forecast_Item_LAM__c>> mp_foiFOOri  = new Map<String,List<FO2_Forecast_Item_LAM__c>>();
        for(FO2_Forecast_Item_LAM__c foiLAM : lst_foiLAM){
            List<FO2_Forecast_Item_LAM__c> aux = mp_foiFOOri.get(foiLAM.FO2_Forecast_Item__r.FO_Forecast_Padre__c);
            if(aux==null)
                aux = new List<FO2_Forecast_Item_LAM__c>();
            aux.add(foiLAM);
            mp_foiFOOri.put(foiLAM.FO2_Forecast_Item__r.FO_Forecast_Padre__c,aux);
        }
        Map<Id,FO_Forecast__c> mp_insert = new Map<Id,FO_FOrecast__c>();
        for(String idFo :lst_foEnvia){
            
            List<FO2_FOrecast_Item_LAM__c> lst_foi = mp_foiFOOri.get(idFo);
            System.debug('Entro en Envia'+lst_foi);
            if(lst_foi!=null){
                for(FO2_Forecast_Item_LAM__c foi :lst_foi){
                    FO_Forecast__c aux = mp_insert.get(foi.FO2_Forecast__c);
                    if(foi.FO2_Forecast_Item__r.FO_Tipo_Forecast__c==null)
                        continue;
                    if(aux==null){
                        aux = new FO_Forecast__c();
                        aux.Id = foi.FO2_Forecast__c;
                        aux.FO_Commit_Pipeline__c = foi.FO2_Forecast__r.FO_Commit_Pipeline__c;
                        aux.FO_Upside_Pipeline__c = foi.FO2_Forecast__r.FO_Upside_Pipeline__c;
                    }
                    double value = 0;
                    if(foi.FO2_Forecast__r.FO2_Api__c.equals('BI_Net_Annual_Value__c')){
                        value = foi.FO2_Forecast_Item__r.FO_NAV__c;
                    }else{
                        if(foi.FO2_Forecast_Item__r.FO2_NBAV_Budget__c!=null)
                        	value = foi.FO2_Forecast_Item__r.FO2_NBAV_Budget__c;
                    }
                    //JLA_20180606 avoid null-pointer
                    if(value==null)
                        value=0;
                    if(foi.FO2_Forecast__r.FO2_Monocurrency__C==false){
                        value=(value*mp_curr.get(foi.FO2_Forecast__r.CurrencyIsoCode))/mp_curr.get(foi.FO2_Forecast_Item__r.CurrencyIsoCode);
                    }
                    
                    if(foi.FO2_Forecast_Item__r.FO_Tipo_Forecast__c.equals('Commit')){
                        aux.FO_Commit_Pipeline__c+=value;
                    }else{
                        aux.FO_Upside_Pipeline__c+=value;
                    }
                    mp_insert.put(aux.Id,aux);
                }
            }
        }
        for(String idFo :lst_foBorra){
            List<FO2_FOrecast_Item_LAM__c> lst_foi = mp_foiFOOri.get(idFo);
            System.debug('Entro en Borrador: '+lst_foi);
            if(lst_foi!=null){
                for(FO2_Forecast_Item_LAM__c foi :lst_foi){
                    if(foi.FO2_Forecast_Item__r.FO_Tipo_Forecast__c==null)
                        continue;
                    FO_Forecast__c aux = mp_insert.get(foi.FO2_Forecast__c);
                    if(aux==null){
                        aux = new FO_Forecast__c();
                        aux.Id = foi.FO2_Forecast__c;
                        aux.FO_Commit_Pipeline__c = foi.FO2_Forecast__r.FO_Commit_Pipeline__c;
                        aux.FO_Upside_Pipeline__c = foi.FO2_Forecast__r.FO_Upside_Pipeline__c;
                    }
                    double value = 0;
                    if(foi.FO2_Forecast__r.FO2_Api__c.equals('BI_Net_Annual_Value__c')){
                        value = foi.FO2_Forecast_Item__r.FO_NAV__c;
                    }else{
                        value = foi.FO2_Forecast_Item__r.FO2_NBAV_Budget__c;
                    }
                    //JLA_20180606 avoid null-pointer
                    if(value==null)
                        value=0;
                    if(foi.FO2_Forecast__r.FO2_Monocurrency__C==false){
                        value=(value*mp_curr.get(foi.FO2_Forecast__r.CurrencyIsoCode))/mp_curr.get(foi.FO2_Forecast_Item__r.CurrencyIsoCode);
                    }
                    if(foi.FO2_Forecast_Item__r.FO_Tipo_Forecast__c.equals('Commit')){
                        aux.FO_Commit_Pipeline__c-=value;
                    }else{
                        aux.FO_Upside_Pipeline__c-=value;
                    }
                    mp_insert.put(aux.Id,aux);
                }
            }
        }
        if(mp_insert.isEmpty()==false)
            update(mp_insert.values());
        System.debug('JLA_FIN_actualizaGAM');*/
    }
}