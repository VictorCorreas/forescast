@isTest
private class BI_FVI_SurveyQuestionMethods_TEST 
{	
	public static void createData() 
	{
		Survey__C Encuesta = new Survey__C (BI_FVI_Type__c = 'CSI', BI_FVI_Country__c = 'Peru', Submit_Response__c='XXX');
		insert Encuesta;
        
		String st = '1\r\n'; st += '2\r\n'; st += '3';
		Survey_Question__c  Question1= new Survey_Question__c(Survey__c=Encuesta.id, BI_FVI_CSI_Question__c=true, Question__c='Pregunta 1', Choices__c=st, Type__c='Single Select--Vertical', OrderNumber__c = 1);
		Survey_Question__c  Question2= new Survey_Question__c(Survey__c=Encuesta.id, BI_FVI_CSI_Question__c=true, Question__c='Pregunta 2', Choices__c=st, Type__c='Single Select--Vertical', OrderNumber__c = 2);
		Survey_Question__c  Question3= new Survey_Question__c(Survey__c=Encuesta.id, BI_FVI_CSI_Question__c=true, Question__c='Pregunta 3', Choices__c=st, Type__c='Single Select--Vertical', OrderNumber__c = 3);
		List <Survey_Question__c> SQ  = new List<Survey_Question__c>();
		SQ.add(Question1);
		SQ.add(Question2);
		SQ.add(Question3);
		insert SQ;
	}

	@isTest static void test_General() 
	{
		BI_TestUtils.throw_exception = false;
		BI_FVI_SurveyQuestionMethods_TEST.createData();
		BI_TestUtils.throw_exception = true;
		BI_FVI_SurveyQuestionMethods_TEST.createData();
	}
}