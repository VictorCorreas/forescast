/*************************************
 * @Author: Javier Lopez Andradas
 * @Company: gCTIO
 * @Description: Controller of auras FO_VE_ForecastSubordinad AND FO_VE_RelatedForeSub_ctr
 * 
 * <date>			<version>		<description>
 * 07/09/2018		1.0				Initial	
 * ********************************/
public without sharing class FO_RelatedForeSub_ctr {
    /*************************************
     * @Author: Javier Lopez Andradas
 	 * @Company: gCTIO
 	 * @Description: REturn the value of the MOnth Forecast
 	 * @params: IdFore-> Id FO_Forecast__c parent of the Fof
 	 * @Params : per-> ID of bI_Periodo__c of the Fof
 	 * 
 	 * <date>			<version>		<description>
 	 * 07/09/2018		1.0				Initial	
     * ************************************/
	@auraEnabled
    public static FO_Forecast_Futuro__c getFof(String IdFore,String per){
        return [Select currencyIsoCode,FO_Forecast_Final__c from FO_Forecast_Futuro__c where FO_Forecast_Padre__c =:idFore AND FO_Periodo__c =:per limit 1];
    } 
    /*************************************
     * @Author: Javier Lopez Andradas
 	 * @Company: gCTIO
 	 * @Description: returns to the Subordinated Forecast
 	 * @params: IdFore-> Id FO_Forecast__c 
 	 * 
 	 * 
 	 * <date>			<version>		<description>
 	 * 07/09/2018		1.0				Initial	
     * ************************************/
    @AuraEnabled
    public static String devolverForecast(String IdFore){
        FO_Forecast__c fore  = new FO_FOrecast__c();
        fore.Id=idFore;
        fore.FO_Estado__c='Borrador';
        try{
            update(fore);
        }catch(Exception e){
            return e.getStackTraceString();
        }
        return 'OK';
    }
    /*************************************
     * @Author: Javier Lopez Andradas
 	 * @Company: gCTIO
 	 * @Description: returns all th subordinated Forecast 
 	 * @params: IdFore-> Id FO_Forecast__c 
 	 * 
 	 * 
 	 * <date>			<version>		<description>
 	 * 07/09/2018		1.0				Initial	
     * ************************************/
    @AuraEnabled
    public static List<FO_FOrecast__c> extractSub(String idFore){
        return [select Id,FO_Periodo__r.FO_Periodo_Padre__c,FO_Estado__c,FO_Nodo__r.Name,FO_Nodo__c,Name from FO_Forecast__c where FO_Forecast_Padre__c=:idFore];
    }
}