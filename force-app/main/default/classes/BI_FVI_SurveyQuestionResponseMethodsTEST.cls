/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   Test Class of the class BI_FVI_SurveyQuestionResponseMethods
    Test Class:    BI_FVI_SurveyTakerMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    01/06/2016           Geraldine P. Montes         Initial Version
    31/05/2017           Cristina Rodriguez          Commented of_Time_Question_was_Responded_to__c(SurveyQuestionResponse__c) & BI_ECU_Nombre_cliente__c (Case)
    20/09/2017               Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    -------------------------------------------------------------------------------*/ 

@isTest
private class BI_FVI_SurveyQuestionResponseMethodsTEST {
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_Registro_Datos_Desempeno__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

	Public static List<Account> lstAcc = new List<Account>();
	Public static Account objAcc = new Account();
	Public static List<BI_Registro_Datos_Desempeno__c> lstRDD = new List<BI_Registro_Datos_Desempeno__c>();
	Public static BI_Registro_Datos_Desempeno__c objRDD = new BI_Registro_Datos_Desempeno__c();
	Public static List<SurveyQuestionResponse__c> lstSQR = new List<SurveyQuestionResponse__c>();
	Public static SurveyQuestionResponse__c objSQR = new SurveyQuestionResponse__c();
	Public static Case objCase = new Case();
	Public static Survey__c objSurvey = new Survey__c();
	Public static SurveyTaker__c objST = new SurveyTaker__c();
	Public static Survey_Question__c objSQ = new Survey_Question__c();
	
	Public static void CreateData() {

		
	    objAcc.Name = 'CLIENTE_TEST';
	    objAcc.BI_Country__c = Label.BI_Peru;
	    objAcc.BI_No_Identificador_fiscal__c = '20431271525';
	    objAcc.CurrencyIsoCode = 'MXN';
	    objAcc.BI_FVI_Tipo_Planta__c = 'No Cliente';
		objAcc.BI_Segment__c                         = 'test';
		objAcc.BI_Subsegment_Regional__c             = 'test';
		objAcc.BI_Territory__c                       = 'test';
		lstAcc.add(objAcc);
		insert lstAcc;		
					
		objRDD.BI_FVI_Fecha__c = system.today();
		objRDD.BI_FVI_Id_Cliente__c = lstAcc[0].Id;
		objRDD.BI_Tipo__c = 'ISC';
		objRDD.BI_FVI_Valor__c = 4;
		objRDD.BI_FVI_IdObjeto__c = '098765432123buh789';
		objRDD.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
		lstRDD.add(objRDD);
		insert lstRDD;

		
//		objCase.BI_ECU_Nombre_cliente__c = 'AccPrueba';
		objCase.TGS_Pais__c = Label.BI_Peru;
		objCase.BI_Department__c = 'FVI';
		objCase.BI_Type__c = 'Contrato';
		objCase.Subject = 'Rellenar encuesta de satisfaccion al cliente';
		objCase.Status = 'Nuevo';
		objCase.Priority = 'Media';
		objCase.Origin = 'Contracts';
		insert objCase;

		
		objSurvey.Name = 'Survey ISC';
		objSurvey.BI_FVI_Country__c = Label.BI_Peru;
		objSurvey.BI_FVI_Type__c = 'CSI';
		objSurvey.Submit_Response__c = 'XXDD';
		insert objSurvey;

		
		objST.Case__c = objCase.id;
		objST.Survey__c = objSurvey.id;
		objST.BI_Cliente__c = lstAcc[0].id;
		insert objST;

		
		objSQ.Name = 'Edad';
		objSQ.Type__c = 'Single Select--Vertical';
		objSQ.OrderNumber__c = 2;
		objSQ.Question__c = 'Color Favorito';
		objSQ.Choices__c = '1';
		objSQ.BI_FVI_CSI_Question__c = true;
		objSQ.Survey__c = objSurvey.id;
		insert objSQ;


		objSQR.Response__c = '1';
		objSQR.Survey_Question__c = objSQ.Id;
		objSQR.SurveyTaker__c = objST.Id;
//		objSQR.of_Time_Question_was_Responded_to__c = 1;
		lstSQR.add(objSQR);

		
	}
	
	public static testMethod void BI_FVI_SurveyQuestionResponseMethods_TEST() 
    {
    	User objUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

		System.runAs(objUser)
		{
	        Test.startTest();
	            CreateData();
	            insert lstSQR;
	            BI_FVI_SurveyQuestionResponseMethods.CreateRegistroDesemp(lstSQR); 
	        Test.stopTest();
	    }
    }
	
}