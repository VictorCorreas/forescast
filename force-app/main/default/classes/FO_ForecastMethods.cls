public class FO_ForecastMethods {
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    final static Map<Id,String> MAP_Id_RT = new Map<Id,String>();
    static {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('FO_Forecast__c','Opportunity')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
            MAP_Id_RT.put(rt.Id, rt.DeveloperName);
        }
    }
    
    /***************************************************************
     * Author:Javier Lopez
     * Description: Create all Future Forecasts for a created Forecast
     * IN:list of new Forecast
     * Execute in post_create
     * 
     * <date>       <version>       <description>
     * ??????       1.0             Intial
     * 16/10/2018   1.1             Added condition for MNCs  
     * *************************************************************/
    public static void createForecastFutur(List<FO_Forecast__c> news){
        
       System.debug('FO_ForecastMethods.createForecastFutur() BEGIN ->'+news);
       // ERROR: Debería mirar en qué año se ejecuta para no traer todos los forecast
       Map<Id,BI_Periodo__c> mp_peri = new Map<Id,BI_Periodo__c>([Select Id,FO_Periodo_Padre__c,FO_Type__c,BI_FVI_Fecha_Inicio__c,BI_FVI_Fecha_Fin__c,FO_Secuencia__c from BI_Periodo__c where FO_Type__c!=null]);  
       Map<Id,FO_Forecast__c> mp_toEval = new Map<Id,FO_FOrecast__c>();
       List<String> lst_Nodos = new List<String>();
       List<String> lst_Periodos = new List<String>();
        //Para estudiar la jerarquia de los Forecast
        Map<String,Map<Decimal,String>> mp_periPadre = new Map<String,Map<Decimal,String>>();
        for(BI_Periodo__c per : mp_peri.values()){
            if(per.FO_Periodo_Padre__c!=null){
            	Map<Decimal,String> lst_IdSon = mp_periPadre.get(per.FO_Periodo_Padre__c);
                if(lst_IdSon==null){
                    lst_IdSon = new Map<Decimal,String>();
                }     
                lst_IdSon.put(per.FO_Secuencia__c,per.Id);
                mp_periPadre.put(per.FO_Periodo_Padre__c,lst_IdSon);

            }
        }
        List<Id> mp_year = new List<Id>();
        //{Id_PeriododActual,Id_PEriodoAnt}
        Map<String,String> mp_periAnt = new Map<String,String>();
        for(FO_Forecast__c fo: news){
            if(mp_peri.get(fo.FO_Periodo__c)!=null && mp_peri.get(fo.FO_Periodo__c).FO_Type__c.equals('Semana') ){
                mp_toEval.put(fo.Id,fo);
                lst_Nodos.add(fo.FO_Nodo__c);
                BI_Periodo__c actPer = mp_peri.get(fo.FO_Periodo__c);
                BI_Periodo__c actMes = mp_peri.get(actPer.FO_Periodo_Padre__c);
                if(actPer.FO_Secuencia__c==1){
                    //Ha habido un cambio de Mes, tenemos que ver si hay un cambio de Q, si lo hay no hay que traerse lo antiguo de ese Q
                    if(actMes.FO_Secuencia__c==1){
                        //cambio de Q no tengo que traerme sus Forecast Futuros para ponerlos por pantalla
						//Me teng uqe traer los de los cuarter anteriores ...
                        if(mp_peri.get(actMes.FO_Periodo_Padre__c).FO_Secuencia__c==1){
                            mp_year.add(fo.Id);
                            continue;
                        }else{
                            //Tengo que irme hasta el año a buscar
                            BI_Periodo__c perTri = mp_peri.get(actMes.FO_Periodo_Padre__c);
                            String antTri = mp_periPadre.get(perTri.FO_Periodo_Padre__c).get(perTri.FO_Secuencia__c-1);
                            String mes = mp_PeriPadre.get(antTri).get(mp_PeriPadre.get(antTri).keySet().size());
                            String sem = mp_PeriPAdre.get(mes).get(mp_PeriPadre.get(mes).keySet().size());
                            mp_periAnt.put(actPer.Id, sem);
                        }
                    }else{
                        String IdperAnt = mp_periPadre.get(mp_periPadre.get(actMes.FO_Periodo_Padre__c).get(actMes.FO_Secuencia__c-1)).get(mp_periPadre.get(mp_periPadre.get(actMes.FO_Periodo_Padre__c).get(actMes.FO_Secuencia__c-1)).keySet().size());
                        mp_periAnt.put(actPer.Id,IdperAnt);
                    }
                }else{
                    mp_periAnt.put(actPer.Id,mp_periPadre.get(actMes.Id).get(actPer.FO_Secuencia__c-1));
                }
                //JLA: si se pide se hará la copia de los Forecast futuros del anteroroforecast
              //  String quarter = mp_peri.get(mp_peri.get(actPer.FO_Periodo_Padre__c).FO_Periodo_Padre__c).Id;
                //for(String perMes : mp_periPadre.get(quarter).values()){
                  //  lst_Periodos.add(mp_periPadre.get(perMes).get(mp_periPadre.get(perMes).keySet().size()));
                //}
                
       	 	}else{
            	fo.addError(Label.FO_Only_week_Per);
        	}  
    	}
      	//Ahora vamos a generar la copia del Forecast anterior, si es necesario
      	
      	List<FO_Forecast_Futuro__c> lst_futur = new List<FO_Forecast_Futuro__c>();
        if(lst_Periodos.isEmpty()==false){
            lst_futur = [select Id,FO_Periodo__c,FO_Forecast_Padre__r.FO_Periodo__c,FO_Total_Forecast_Ajustado__c,FO_Forecast_Padre__r.FO_Nodo__c from FO_Forecast_Futuro__c where  FO_Forecast_Padre__r.FO_Nodo__c in :lst_Nodos AND FO_Forecast_Padre__r.FO_Periodo__c in :lst_Periodos];
        }
        //{Id_Nodo+BI_Periodo_Forecast,{Id_Periodo_Forecast_Futuro,Total_Forecast_Ajustad}}
            Map<String,Map<String,Double>> mp_futuros = new Map<String,Map<String,Double>>();
      /*  for(FO_Forecast_Futuro__c foF:lst_futur){
            Map<String,Double> mp_rec = mp_futuros.get(foF.FO_Forecast_Padre__r.FO_Nodo__c+''+foF.FO_Forecast_Padre__r.FO_Periodo__c);
            if(mp_rec==null)
                mp_rec =new MAp<String,Double>();
            mp_rec.put(foF.FO_Periodo__c,foF.FO_Total_Forecast_Ajustado__c);
            mp_futuros.put(foF.FO_Forecast_Padre__r.FO_Nodo__c+''+foF.FO_Forecast_Padre__r.FO_Periodo__c,mp_rec);
            
        }*/
		List<FO_Forecast_Futuro__c> lst_Futuro = new List<FO_Forecast_Futuro__c>();
        for(String idFO : mp_toEval.keySet()){
            FO_Forecast__c fo = mp_toEval.get(idFO);
            //JLA_FO_MNC
            if(mp_year.contains(idFo)==true || mp_periAnt.get(fo.FO_Periodo__c)==null || mp_futuros.get(fo.FO_Nodo__c+''+mp_periAnt.get(fo.FO_Periodo__c))==null || fo.RecordTypeId.equals(MAP_NAME_RT.get('FO_Manager')) || fo.RecordTypeId.equals(MAP_NAME_RT.get('FO_MNC_Manager')) || fo.RecordTypeId.equals(MAP_NAME_RT.get('FO_MNC_Ejecutivo'))  ){
                //Hay que crear todo los de Forecast Futuros a 0
                Set<String> lst_IdPer = new Set<String>();
                BI_Periodo__c per = mp_peri.get(fo.FO_Periodo__c);
               	String Id_mes = per.FO_Periodo_Padre__c;
                Map<Decimal,String> mp_Semanas = mp_periPadre.get(Id_mes);
                for(Decimal j = per.FO_Secuencia__c+1;INteger.valueOf(j)<=mp_Semanas.keySet().size();j++){
                    if(mp_Semanas.get(j)!=null)
                    	lst_IdPer.add(mp_semanas.get(j));
                }
                System.debug('JLA_mp_periPadre'+mp_periPadre);
                BI_Periodo__c pa = mp_peri.get(Id_mes);
                System.debug('JLA_IdMes: '+Id_mes);
                System.debug('pa: '+pa);
                while(pa!=null){
                    System.debug('HIjos de pa: '+mp_periPadre.get(pa.Id));
                    if(mp_periPadre.get(pa.Id)!=null){
                        for(Decimal perEv : mp_periPadre.get(pa.Id).keySet()){

                            if(per.BI_FVI_Fecha_Fin__c<=mp_peri.get(mp_periPadre.get(pa.Id).get(perEv)).BI_FVI_Fecha_Fin__c){
                                lst_IdPer.add(mp_periPadre.get(pa.Id).get(perEv));
                            }
                        }
                    }
                    lst_IdPer.add(pa.Id);
                    pa =mp_peri.get(pa.FO_Periodo_Padre__c);
                    System.debug('Nuevo pa: '+pa);
                }
                System.debug('JLA_lst_IdPEr: '+lst_IdPer);
                for(String perAux : lst_IdPEr){
                    FO_Forecast_Futuro__c futur = new FO_Forecast_Futuro__c();
                    futur.FO_Forecast_Padre__c=fo.Id;
                    futur.FO_Total_Forecast_Ajustado__c=0;
                    futur.FO_Periodo__c=perAux;
                    lst_Futuro.add(futur);
                }
            }else{
             /*   //Tengo que recoger los forecast futuros anteriores
                Map<String,Double> mp_ant = mp_futuros.get(fo.FO_Nodo__c+''+mp_periAnt.get(fo.FO_Periodo__c));
               	//vemos los valores a pintar
               	BI_Periodo__c act = mp_peri.get(fo.FO_Periodo__c);
                Set<String> st_per = new Set<String>();
                while(act!=null){
                    if(act.FO_Periodo_Padre__c!=null){
                        for(String id_perAux : mp_periPadre.get(act.FO_Periodo_Padre__c).values()){
                            if(act.BI_FVI_Fecha_Fin__c<mp_peri.get(id_perAux).BI_FVI_Fecha_Fin__c){
                                st_per.add(id_perAux);
                            }
                        }
                    }
                    st_per.add(act.Id);
                	act = mp_peri.get(act.FO_Periodo_Padre__c);
                }
                for(String per : st_per){
                    FO_Forecast_Futuro__c futur = new FO_Forecast_Futuro__c();
                    futur.FO_Forecast_Padre__c=fo.Id;
                    if(mp_ant.get(per)!=null){
                    	futur.FO_Total_Forecast_Ajustado__c=mp_ant.get(per);    
                    }else{
                        futur.FO_Total_Forecast_Ajustado__c=0;    
                    }
                    futur.FO_Periodo__c=per;
                    lst_Futuro.add(futur);
                }*/
            }
        }
        insert(lst_Futuro);
        System.debug('FO_ForecastMethods.createForecastFutur() END'+ news);

    }//public static void 
    /******************************************************************
     * Author:JAvier Lopez
     * DEscription:Create all the FOI related to the FO
     * IN:List of all FO
     * Execute in post_create
     * <date>		<version>		<description>
     * 04/06/2018	1.1				Añadida query, no trae los volores
     * 22/05/2018	2.0				Added new economic field from the Opp and operation for NCG nodes
     * 22/05/2018	2.1				Create FOI LAM in case of GAM Node
     * 06/06/2018	2.2				JLA-Added condition in NBAV==null -> sum=0
     * 08/006/2018	2.3				Roll back	
     * 31/07/2018	2.4				JLA-Added filter condition  F6
     * 31/07/2018	3.0				JLA-Changed version for FO_VE CRQ
     * 15/10/2018 4.0       JLA-Changed for MNC Forecast also try to retrieve all opps in the current MONTH
     * ****************************************************************/
    public static void creaForecastItems(List<FO_Forecast__c> news){
        /* deprecated it bwill created inthe page when it is refrsehd
        System.debug('FO_ForecastMethods.createForecastItems() BEGIN'+news);
        List<String> lst_idsPer = new List<String>();
        List<String> lst_idsNodo = new List<String>();
        list<FO_Forecast__c> lst_toEval = new List<FO_Forecast__c>();
        //JLA_V3_0
        List<String> lst_ids = new List<String>();
        for(FO_Forecast__c fo : news){
            System.debug('JLA__TEST: tipo de registro '+MAP_Id_RT.get(fo.RecordTypeId)+' -> '+fo.RecordTypeId);
            if(fo.RecordTypeid!=null && MAP_Id_RT.get(fo.RecordTypeId).contains('Overlay')==false && MAP_Id_RT.get(fo.RecordTypeId).contains('Manager')==false){
                lst_ids.add(fo.Id);
            }
        }
        System.debug('Lista de Ids: '+lst_ids);
        if(lst_ids.isEmpty()==true)
            return;
        //Map<Id,RecordType> mp_rt = new Map<Id,RecordType>([Select Id,DeveloperName from RecordType where SObjectType IN ('FO_Forecast__C','Opportunity')]);
        //JLA_20180406_S_1 :Cambaido no se trae los valores
        List<FO_Forecast__c> list_foAux = [Select Id,RecordTypeId,FO_Periodo__c,FO_Nodo__c,FO2_API__c,FO2_Monocurrency__c from FO_Forecast__c where Id in :lst_ids];
       
        for(FO_Forecast__c fo:list_foAux){
            System.debug('Id_Rt: '+fo.RecordTypeId);
            if(MAP_Id_RT.get(fo.RecordTypeId).equals('FO_Ejecutivo')){
             	lst_idsPer.add(fo.FO_Periodo__c);
                lst_idsNodo.add(fo.FO_Nodo__c);
                lst_toEval.add(fo);
            }
        //JLA_20180406_E_1 :Cambaido no se trae los valores   	 
        }
        if(lst_idsPer.isEmpty()==false){
          //JLA_v4.0 Sacamos tambien el Periodo PAdre por si fuese MNC
        	Map<Id,BI_Periodo__c> lst_per =new Map<Id,BI_Periodo__c>([Select Id,BI_FVI_Fecha_Inicio__c,BI_FVI_Fecha_Fin__c,FO_Periodo_Padre__r.BI_FVI_Fecha_Inicio__c,FO_Periodo_Padre__r.BI_FVI_Fecha_Fin__c from BI_Periodo__c where Id in:lst_idsPer]);
            List<BI_FVI_Nodo_Cliente__c> lst_Nodo = [select BI_FVI_IdCliente__c,BI_FVI_IdNodo__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c in:lst_idsNodo];
            
            List<String> lst_acc = new List<String>();
            for(BI_FVI_Nodo_Cliente__c nc : lst_Nodo){
                lst_acc.add(nc.BI_FVI_IdCLiente__c);
            }
            //JLA_v4.0 Estblecemos las nuevas fechas
            Date fechIni=null,fechFin=null;
            if(MAP_Id_RT.get(fo.RecordTypeId).contains('MNC')){
            	for(BI_Periodo__c per : lst_per.values()){
                	if(fechIni==null || fechIni>per.FO_Periodo_Padre__r.BI_FVI_Fecha_Inicio__c){
                    	fechIni=per.FO_Periodo_Padre__r.BI_FVI_Fecha_Inicio__c;
                	}
                	if(fechFin==null || fechFin<per.FO_Periodo_Padre__r.BI_FVI_Fecha_Fin__c){
                    	fechFin=per.FO_Periodo_Padre__r.BI_FVI_Fecha_Fin__c;
                  	}
                }
              }else{
                for(BI_Periodo__c per : lst_per.values()){
                	if(fechIni==null || fechIni>per.BI_FVI_Fecha_Inicio__c){
                    	fechIni=per.BI_FVI_Fecha_Inicio__c;
                	}
                	if(fechFin==null || fechFin<per.BI_FVI_Fecha_Fin__c){
                    	fechFin=per.BI_FVI_Fecha_Fin__c;
                  	}
                }    
              }
            
            Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
        	List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        	List<String> val_def = new List<String>();
        	for(Schema.PicklistEntry entry :values){
                //JLA_V2_4
            	if(entry.getValue().contains('Closed Lost')==true || entry.getValue().contains('Cancelled | Suspended') || entry.getValue().contains('F1') || entry.getValue().contains('F6') )
                	val_def.add(entry.getValue());
        	}
            String IdOpp=MAP_NAME_RT.get('BI_Ciclo_completo');
            //Buscamos por id para usar tablas idexada
            System.debug('Query sobre oppss: RT= '+idOpp+' Closedate mayor-menor :'+fechIni+'-'+fechFin+': Account normales '+lst_acc+' Stege not in : '+val_def);           
            List<Opportunity> lst_opp = [Select currencyIsoCode,Id,StageName,BI_Net_annual_value_NAV__c,CloseDate,BI_O4_TGS_NBAV_Budget__c,AccountId,BI_O4_Committed__c,BI_O4_Upside__c from Opportunity where RecordTypeId =:idOpp AND CloseDate>=:fechIni AND CloseDate<=:fechFin AND (AccountId in:lst_acc) AND (NOT StageName in :val_def)];
           	System.debug('JLA_Lista de opps: '+lst_opp);
            List<FO_Forecast_Item__c> lst_foi =  new list<FO_Forecast_Item__c>();                  
            for(FO_Forecast__c fo : lst_toEval){
                BI_Periodo__c per = lst_per.get(fo.FO_Periodo__c);
                for(Opportunity opp : lst_opp){
                    if(opp.CloseDate>=per.BI_FVI_Fecha_Inicio__c && opp.CloseDate<=per.BI_FVI_Fecha_Fin__c){
                        FO_Forecast_Item__c foi = new FO_Forecast_Item__c();
                        foi.FO_Forecast_Padre__c=fo.Id;
                        foi.FO_StageName__c=opp.StageName.subString(0,2);
                        foi.FO_NAV__c=opp.BI_Net_annual_value_NAV__c;
                        foi.FO_CloseDate__c=opp.CloseDate;
                        foi.FO2_NBAV_Budget__c=opp.BI_O4_TGS_NBAV_Budget__c;
                        //foi.FO_Commit__c=false;
                        //foi.FO_Upside__c=false;
                        foi.FO_Opportunity__c=opp.Id;
                        foi.CurrencyIsoCode=opp.currencyIsoCode;
                        lst_foi.add(foi);
                    }
                }
            }
            	insert(lst_foi);    
            }
        System.debug('FO_ForecastMethods.createForecastItems() END'+news);
        */
    } // public static void creaForecastItems(List<FO_Forecast__c> news){
    /*****************************************************************
     * Author: Javier Lopez
     * Description: Changes the rT of FO if ots has Subordinated Nodes,add hierchaqu
     * EXCECUTE POST
     * <date>		<version>		<Author>		<description>
     * ??????		1.2				JLA				fill FO_Numero_Nodos__c
     * 22/03/2018	1.3				JLA				fill with user Currency teh forecast
     * 21/05/2018	2.0	(Deprecated)			JLA				Changed the way to eval this:
     * 													Node.RT.NCP=FO.RT.Ejecutivo
     * 													Node.RT.NCS=FO.RT.Manager
     * 													Node.RT.NCG=FO.RT.GAM Semanal
     * 												If the check of monocurrency is selected in the obj change the currency of the Forecast	
     * 08/06/2018	3.0				JLA				Deprecated version 2.0, added the record of the objetive of a node
     * 31/07/2018	4.0				JLA				Added new RT for Overlay users
     * 09/08/2018	4.1				JLA				Changed in Overlay Manager, we have to know if any of the inferior nodes are Overlay if true the node forecast is overlay
     * 14/09/2018	4.2				JLA				Fix test if is null the value of BI_OBJ_Campo__c
     * 15/10/2018	5.0				JLA 			Changed if is overlay and has Multinational is multinational
     * 17/10/2018   5.1             JLA             Fixes with a node that have an NCP below and its multinationals
     * 05/11/2018   5.2             JLA             Only Active nodes
     * ****************************************************************/
    public static void changeRT(List<FO_Forecast__c> news){
        System.debug('FO_ForecastMethods.changeRT() BEGIN'+news);
        List<FO_Forecast__c> lst_fo = [Select Id,FO_Nodo__c,FO_Periodo__c,FO_Nodo__r.BI_FVI_Overlay__c,FO_Nodo__r.FO_Clasificacion__c from FO_Forecast__c where Id in:news];
        
        List<String> lst_nodes = new List<String>();
        List<String> lst_per = new List<String>();
        for(FO_Forecast__c fo: lst_fo){
            lst_nodes.add(fo.FO_Nodo__c);
            lst_per.add(fo.FO_Periodo__c);
           
        }
        list<BI_FVI_Nodos_Objetivos__c> lst_obj = [Select Id,BI_FVI_Objetivo_Comercial__r.FO2_Monocurrency__c,BI_FVI_Objetivo_Comercial__r.BI_OBJ_Campo__c, BI_FVI_Id_Nodo__c,BI_FVI_IdPeriodo__c from BI_FVI_Nodos_Objetivos__c where BI_FVI_Id_Nodo__c in :lst_nodes AND BI_FVI_IdPeriodo__c in:lst_per  AND BI_FVI_Objetivo_Comercial__r.BI_Tipo__c='Cartera']; 
        Map<Id,Map<Id,BI_FVI_Nodos_Objetivos__c>> mp_objs = new Map<Id,Map<Id,BI_FVI_Nodos_Objetivos__c>>();
        for(BI_FVI_Nodos_Objetivos__c obj : lst_obj){
            Map<Id,BI_FVI_Nodos_Objetivos__c> aux = mp_objs.get(obj.BI_FVI_Id_Nodo__c);
            if(aux==null)
                aux = new Map<Id,BI_FVI_Nodos_Objetivos__c>();
            aux.put(obj.BI_FVI_IdPeriodo__c,obj);
            mp_objs.put(obj.BI_FVI_Id_Nodo__c,aux);
        }
        //JLA_v5.0
        Map<Id,BI_FVI_Nodos__c> mp_nodes = new Map<Id,BI_FVI_Nodos__c>([Select Id,BI_FVI_NodoPadre__c,BI_FVI_OverLay__c,FO_Clasificacion__c from BI_FVI_Nodos__c where RecordType.DeveloperName IN ('BI_FVI_NCS','BI_FVI_NCP') AND BI_FVI_Activo__c=true]);
        List<FO_Forecast__c> lst_fore = new List<FO_Forecast__c>();
        Set<String> lst_padres = new Set<String>();
        Map<String,List<BI_FVI_Nodos__c>> mp_count_nodes=new Map<String,List<BI_FVI_Nodos__c>>();
        for(BI_FVI_Nodos__c nd:mp_nodes.values()){
            List<BI_FVI_Nodos__c> aux = mp_count_nodes.get(nd.BI_FVI_NodoPadre__c);
            if(aux==null)
                aux = new List<BI_FVI_Nodos__c>();
            aux.add(nd);
            mp_count_nodes.put(nd.BI_FVI_NodoPadre__c,aux);
        }
                
        for(FO_Forecast__c fo :lst_fo){
            System.debug('JAVLO@@@:Id'+fo.Id);
            System.debug('JAVLO@@@:Nodo'+fo.FO_Nodo__c);
            System.debug('JAVLO@@@:Per'+fo.FO_Periodo__c);
            if(mp_count_nodes.get(fo.FO_Nodo__c)!=null){
                //Los overlay no son estos si no que son los inferiores
                boolean isOverlay=false;
                boolean isMNC =false;
                List<BI_FVI_Nodos__c> lst_ndSon = mp_count_nodes.get(fo.FO_Nodo__c);
                for(BI_FVI_Nodos__c nd : lst_ndSon){
                    if(checkOverlay(mp_count_nodes,nd)==true){
                        isOverlay=true;
                        if(nd.FO_Clasificacion__c!=null && nd.FO_Clasificacion__c.equals('Multinationals'))
                        	isMNC=true;
                        break;
                    }
                        
                }
                
                if(isOverlay==true){
                    //JLA_V4_0
                    //JLA_V5.0
                    if(isMNC){
                    	fo.RecordTypeId=MAP_NAME_RT.get('FO_MNC_Manager');		
                    }else{
                    	fo.RecordTypeId=MAP_NAME_RT.get('FO_VE_Overlay_Manager');		
                    }
                    
                }else{
                    //JLA_Fixes
                    if(mp_nodes.get(fo.FO_Nodo__c).FO_Clasificacion__c!=null && mp_nodes.get(fo.FO_Nodo__c).FO_Clasificacion__c.equals('Multinationals')){
                        fo.RecordTypeId=MAP_NAME_RT.get('FO_MNC_Ejecutivo');
                    }else{
                        fo.RecordTypeId=MAP_NAME_RT.get('FO_Manager');            
                    }
                	
                }
                
                fo.FO_Numero_Nodos__c=mp_count_nodes.get(fo.FO_Nodo__c).size();
            }else{
                //JLA_V4_0
                if(fo.FO_Nodo__r.BI_FVI_Overlay__c !=null && fo.FO_Nodo__r.BI_FVI_Overlay__c.equals('Overlay')){
                	if(fo.FO_Nodo__r.FO_Clasificacion__c!= null && fo.FO_Nodo__r.FO_Clasificacion__c.equals('Multinationals')){
                		fo.RecordTypeId=MAP_NAME_RT.get('FO_MNC_Ejecutivo');
                	}else{
                		fo.RecordTypeId=MAP_NAME_RT.get('FO_VE_Overlay');
                	}
                    
                }else{
                	fo.RecordTypeId=MAP_NAME_RT.get('FO_Ejecutivo');    
                }
                
            }
            Map<Id,BI_FVI_Nodos_Objetivos__c> aux = mp_objs.get(fo.FO_Nodo__c);
            if(aux!=null){
                BI_FVI_Nodos_Objetivos__c obj = aux.get(fo.FO_Periodo__c);
                if(obj!=null){
                    fo.FO2_Monocurrency__c=obj.BI_FVI_Objetivo_Comercial__r.FO2_Monocurrency__c;
                    if(obj.BI_FVI_Objetivo_Comercial__r.BI_OBJ_Campo__c!=null && obj.BI_FVI_Objetivo_Comercial__r.BI_OBJ_Campo__c.equals('BI_O4_TGS_NBAV_Budget__c')){
                        fo.FO2_API__c='BI_O4_TGS_NBAV_Budget__c';
                    }else{
                        fo.Fo2_API__c='BI_Net_annual_value_NAV__c';
                    }
                }
            }
            System.debug('@@@JLA_'+fo.RecordTypeId);
        }
        update(lst_fo);
        System.debug('FO_ForecastMethods.changeRT() END'+news);
        
    }// public static void changeRT(List<FO_Forecast__c> news){
    /*****************************************************
     * Author: JAvier Lopez
     * Description: Validate unity of FOrecast
     * 
     * 
     * 
     * ******************************************************/
    public static void validateForecast(List<FO_Forecast__c> news){
        list<String> lst_per = new List<String>();
        list<String> lst_node =new List<String>();
        for(FO_Forecast__c fo:news){
            lst_per.add(fo.FO_Periodo__c);
            lst_node.add(fo.FO_Nodo__c);
        }
        list<FO_Forecast__c> lst_Fore = [Select Id,FO_Periodo__c,FO_Nodo__c from FO_Forecast__c where FO_Nodo__c in:lst_node AND FO_Periodo__c in:lst_per];
        list<String> lst_union = new List<String>();
        for(FO_Forecast__c fo : lst_Fore){
            lst_union.add(fo.FO_Nodo__c+''+fo.FO_Periodo__c);
        }
        for(FO_Forecast__c fo :news){
            if(lst_union.contains(fo.FO_Nodo__c+''+fo.FO_Periodo__c)){
                fo.addError(Label.FO_Duplicated_error);
            }
        }
    } 
    /***************************************************************
     * Author:Alfonso Alvarez
     * Description: Update Forecast Commit and/or Forecast Upside in parent Forecast related
     * IN:news: Lista de cambios nuevos, old: foto de BBDD antes de realizar el cambio
     * 
     * <date>		<version>		<description>						<author>
     * ------		1.0				Initial								AJAE
     * 2018/03/19	1.2				Added FO_Futuro in the check		JLA
     * 2018/03/22	1.3				Added conversion of currency		JLA
     * *************************************************************/
    public static void updateForecastCommitUpside(List<FO_Forecast__c> news, List<FO_Forecast__c> olds){ 

     List<Id> lst_fo = new List<Id>();
     Map<Id,FO_Forecast__c> mp_olds;
     Map<Id,FO_Forecast__c> mp_foi_actualizar;  
     Map<Id,double> mp_fo_commit=new Map<Id,double>(); // Mapa que almacena para cada Forecast el valor a sumar o restar acumulado de commit
     Map<Id,double> mp_fo_upside=new Map<Id,double>(); // Mapa que almacena para cada Forecast el valor a sumar o restar acumulado de upside
     //JLA_01_Start   
	 List<FO_Forecast__c> lst_foreFutRest = new List<FO_Forecast__c>();//Lista de Id de los forecast que hay que resta al Forecast futuro
	 List<FO_Forecast__c> lst_foreFutSum = new List<FO_Forecast__c>();//Lista de Id de los Forecast Futuros que hay que sumar al forecast futuro
     List<String> lst_ForeFutParent = new List<String>();//Lista de los Forecast padres para sacar sus ajustes   
     List<CurrencyType> lst_cr = [Select IsoCode,ConversionRate from CurrencyType where isActive=true];
     Map<String,double> mp_curr = new Map<String,double>();
     List<String> lst_idFo = new List<String>();
     for(CurrencyType curr:lst_cr)
         mp_curr.put(curr.IsoCode,curr.ConversionRate);
     for(FO_Forecast__c foi_old : olds){
         lst_idFo.add(foi_old.FO_Forecast_Padre__c);
     }
         Map<Id,FO_Forecast__c> mp_foCurr = new Map<Id,FO_Forecast__c>([Select Id,CurrencyIsoCode from FO_Forecast__c where Id in :lst_idFo]);
     //JLA_01_END
     for(Integer i=0;i<news.size();i++){
       FO_Forecast__c fo_new=(FO_Forecast__c)news.get(i);
       FO_Forecast__c fo_old=(FO_Forecast__c)olds.get(i);
       if( fo_new.FO_Estado__c != fo_old.FO_Estado__c && fo_new.FO_Forecast_Padre__c!=null){ // Comprobamos que se ha modificado el campo seleccionado en el Forecast
			String currFO = mp_foCurr.get(fo_new.FO_Forecast_Padre__c).CurrencyIsoCode;
         System.debug('AJAE: Forecast : '+ fo_new.Id+ ' valor antiguo: ' + fo_old.FO_Seleccionado_Padre__c + ' valor nuevo: '+ fo_new.FO_Seleccionado_Padre__c);
         lst_fo.add(fo_new.FO_Forecast_Padre__c) ; // Añadimos el Forecast Id para luego obtener los objetos a cambiar:
		 double valueAuxCommit =  fo_new.FO_Commit_Pipeline__c;
         double valueAuxUpside =  fo_new.FO_Upside_Pipeline__c;
           System.debug('valueAuxCommit antes de convertir: '+valueAuxCommit+' MOneda padre: '+mp_curr.get(currFO)+' MOneda actual: '+mp_curr.get(fo_new.CurrencyIsoCode));
           if(fo_new.CurrencyIsoCode.equals(currFO)==false){
               System.debug('Transforma!');
               valueAuxCommit=valueAuxCommit*mp_curr.get(currFO)/mp_curr.get(fo_new.CurrencyIsoCode);
               valueAuxUpside=valueAuxUpside*mp_curr.get(currFO)/mp_curr.get(fo_new.CurrencyIsoCode);
           }
           System.debug('valueAuxCommit despues de convertir: '+valueAuxCommit+' MOneda padre: '+mp_curr.get(currFO)+' MOneda actual: '+mp_curr.get(fo_new.CurrencyIsoCode));
         // Bloque que realiza la suma y/o la resta en commit y/o upside según los valores de forecast new y forecast old               
         //RESTAR COMMIT Y UPSIDE
         if (fo_new.FO_Estado__c.equals('Borrador')) {
           System.debug('AJAE: Restar en Commit y Upside para Forecast: '+fo_new.FO_Forecast_Padre__c);
           if(mp_fo_commit.get(fo_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit y upside
             mp_fo_commit.put(fo_new.FO_Forecast_Padre__c, - valueAuxCommit);
             mp_fo_upside.put(fo_new.FO_Forecast_Padre__c, - valueAuxUpside);  
           }else{
             double valor_commit= mp_fo_commit.get(fo_new.FO_Forecast_Padre__c);
             double valor_upside= mp_fo_commit.get(fo_new.FO_Forecast_Padre__c);
             valor_commit=valor_commit - valueAuxCommit;
             valor_upside=valor_commit -valueAuxUpside;
             mp_fo_commit.put(fo_new.FO_Forecast_Padre__c, valor_commit); 
             mp_fo_upside.put(fo_new.FO_Forecast_Padre__c, valor_upside);                
           }
            //JLA_02_Start
            lst_foreFutRest.add(fo_new); 
            //JLA_02_End 
         }    
         //SUMAR COMMIT Y UPSIDE
         if (fo_new.FO_Estado__c.equals('Enviado')) {
           System.debug('AJAE: Sumar en Commit y Upside para Forecast: '+fo_new.FO_Forecast_Padre__c);
           if(mp_fo_commit.get(fo_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit y upside
             mp_fo_commit.put(fo_new.FO_Forecast_Padre__c, valueAuxCommit);
             mp_fo_upside.put(fo_new.FO_Forecast_Padre__c, valueAuxUpside);  
           }else{
             double valor_commit= mp_fo_commit.get(fo_new.FO_Forecast_Padre__c);
             double valor_upside= mp_fo_upside.get(fo_new.FO_Forecast_Padre__c);
             valor_commit=valor_commit + valueAuxCommit;
             valor_upside=valor_commit +valueAuxUpside;
             mp_fo_commit.put(fo_new.FO_Forecast_Padre__c, valor_commit); 
             mp_fo_upside.put(fo_new.FO_Forecast_Padre__c, valor_upside);                
           }
             //JLA_03_Start
             lst_foreFutSum.add(fo_new);
             //JLA_03_End
         }
          

       } // if( fo_new.FO_Seleccionado_Padre__c != fo_old.FO_Seleccionado_Padre__c ){
        lst_ForeFutParent.add(fo_new.FO_Forecast_Padre__c); 
     }// for(Integer i=0;i<news.size();i++){
     
     System.debug('AJAE: Lista de forecast :'+lst_fo);
     System.debug('AJAE: Cambios en commit:'+mp_fo_commit);
     System.debug('AJAE: Cambios en upside:'+mp_fo_upside);
               
     // Obtenermos el mapa de Forecast que hay que actualizar
     Map <Id,FO_Forecast__c> mp_fo=new map <Id,FO_Forecast__c> ([Select Id,FO_Commit_Pipeline__c,FO_Upside_Pipeline__c from FO_Forecast__c where Id in:lst_fo]);
     System.debug('AJAE: Forecast Mapa:'+mp_fo);
        if(mp_fo.values().isEmpty()==false){
            //Recorremos la lista de forecast y vamos actualizando 
     		for (Id idFo : lst_fo){
       			FO_Forecast__c fo=mp_fo.get(idFo);
		       if(fo.FO_Commit_Pipeline__c==null) //Inicializamos valor del forecast si está vacío
        		 fo.FO_Commit_Pipeline__c=0;
		       if(fo.FO_Upside_Pipeline__c==null) //Inicializamos valor del forecast si está vacío
        		 fo.FO_Upside_Pipeline__c=0;
		       if(mp_fo_commit.get(idFo)!=null)
        		 fo.FO_Commit_Pipeline__c+=mp_fo_commit.get(idFo);
       		   if(mp_fo_upside.get(idFo)!=null)
         		 fo.FO_Upside_Pipeline__c+=mp_fo_upside.get(idFo);
		     }//for (Id idFo : lst_fo){
     		System.debug('AJAE: Forecast Mapa actualizado'+ mp_fo);
        
     		upsert(mp_fo.values());
        }
     
	 //JLA_04_Start
	 System.debug('JLA lsitas: Parent: '+lst_ForeFutParent+' sum: '+lst_foreFutSum +' resta: '+lst_foreFutRest);
        if(lst_foreFutSum.isEmpty()==false || lst_foreFutRest.isEmpty()==false){
           	List<FO_Forecast_Futuro__c> lst_fofut = [Select Id,FO_Ajuste_Forecast_Manager__c,FO_Total_Forecast_Ajustado__c,FO_Periodo__c,FO_Forecast_Padre__c,currencyIsoCode from FO_Forecast_Futuro__c where FO_Forecast_Padre__c in :lst_ForeFutParent OR FO_Forecast_Padre__c in :lst_foreFutRest OR FO_Forecast_Padre__c in :lst_foreFutSum];
        	System.debug('JLA escupido por SF: '+lst_fofut);
	        //JLA: {Id_forecast,{Id_periodo,FO_Forecast_Futuro__c}} Mapa para hacer la lógica mase sencilla
    	    Map<String,Map<String,FO_Forecast_Futuro__c>> mp_fotFut = new Map<String,Map<String,FO_Forecast_Futuro__c>>();
        	//JLA:Ordenamos el la lista en el mapa
        	for(FO_Forecast_Futuro__c fofut : lst_fofut){
            	Map<String,FO_Forecast_Futuro__c> mp = mp_fotFut.get(fofut.FO_Forecast_Padre__c);
		            if(mp==null)
        		        mp = new Map<String,FO_Forecast_Futuro__c>();
	            mp.put(fofut.FO_Periodo__c, fofut);
    	        mp_fotFut.put(fofut.FO_Forecast_Padre__c,mp);
        	}
	        System.debug('JLA mpa de los Forecst futuris: '+mp_fotFut);
    	    //JLA: evaluamos las != listas, solo la de suma y de resta, la de padres era auxialiar para sar los Futuros de los padres
        	//Primero la suma
        	for(FO_Forecast__c fo:lst_foreFutSum){
            	Map <String,FO_Forecast_Futuro__c> mp_foOri = mp_fotFut.get(fo.Id);
	            Map <String,FO_Forecast_Futuro__c> mp_foPa = mp_fotFut.get(fo.FO_Forecast_Padre__c);
                System.debug('JLA Padre: '+mp_foPa);
    	        if(mp_foOri!=null && mp_foPa!=null){
        	        for(String per : mp_foOri.keySet()){
            	        FO_Forecast_Futuro__c fotOri = mp_foOri.get(per);
                	    FO_Forecast_Futuro__c fotPa = mp_foPa.get(per);
                        double valueAjuste = fotOri.FO_Ajuste_Forecast_Manager__c;
                        double valueAjustado = fotOri.FO_Total_Forecast_Ajustado__c;
                        System.debug('Antes de sumar ajuste'+valueAjuste+' ajustado: '+valueAjustado);
                        if(fotOri.CurrencyIsoCode.equals(fotPa.CurrencyIsoCode)==false){
                            System.debug('Transforma!');
                            valueAjuste=valueAjuste*mp_curr.get(fotPa.CurrencyIsoCode)/mp_curr.get(fotOri.CurrencyIsoCode);
                            valueAjustado=valueAjustado*mp_curr.get(fotPa.CurrencyIsoCode)/mp_curr.get(fotOri.CurrencyIsoCode);
                        }
                        System.debug('DEspues de sumar ajuste'+valueAjuste+' ajustado: '+valueAjustado);
                    	if(fotPa!=null){
                            if(fotOri.FO_Ajuste_Forecast_Manager__c!=null)
                        		fotPa.FO_Ajuste_Forecast_Manager__c+=valueAjuste;
                            else{
                                fotPa.FO_Ajuste_Forecast_Manager__c=valueAjuste;
                            }
                            if(fotOri.FO_Total_Forecast_Ajustado__c!=null)
	                        	fotPa.FO_Total_Forecast_Ajustado__c+=valueAjustado;
                            else
                                fotPa.FO_Total_Forecast_Ajustado__c=valueAjustado;
    	                }
        	        }
            	}
        	}
	        //Ahora la resta
    	    for(FO_Forecast__c fo:lst_foreFutRest){
        	    Map <String,FO_Forecast_Futuro__c> mp_foOri = mp_fotFut.get(fo.Id);
            	Map <String,FO_Forecast_Futuro__c> mp_foPa = mp_fotFut.get(fo.FO_Forecast_Padre__c);
	            if(mp_fo!=null && mp_foPa!=null){
    	            for(String per : mp_foOri.keySet()){
        	            FO_Forecast_Futuro__c fotOri = mp_foOri.get(per);
            	        FO_Forecast_Futuro__c fotPa = mp_foPa.get(per);
                        double valueAjuste = fotOri.FO_Ajuste_Forecast_Manager__c;
                        double valueAjustado = fotOri.FO_Total_Forecast_Ajustado__c;
                        if(fotOri.CurrencyIsoCode.equals(fotPa.CurrencyIsoCode)==false){
                            valueAjuste=valueAjuste*mp_curr.get(fotPa.CurrencyIsoCode)/mp_curr.get(fotOri.CurrencyIsoCode);
                            valueAjustado=valueAjustado*mp_curr.get(fotPa.CurrencyIsoCode)/mp_curr.get(fotOri.CurrencyIsoCode);
                        }
                        System.debug(fotPa);
                	    if(fotPa!=null){
                    	    fotPa.FO_Ajuste_Forecast_Manager__c=fotPa.FO_Ajuste_Forecast_Manager__c-valueAjuste;
                        	fotPa.FO_Total_Forecast_Ajustado__c=fotPa.FO_Total_Forecast_Ajustado__c-valueAjustado;
                    	}
                	}
	            }
    	    }
        	Set<FO_Forecast_Futuro__c> lst_fotFutFin = new Set<FO_Forecast_Futuro__c>();
	        for(String pa : lst_ForeFutParent){
    	        if(mp_fotFut.get(pa)!=null){
        	        lst_fotFutFin.addAll(mp_fotFut.get(pa).values());
            	}
	        }
    	    System.debug('Para actualizar:'+lst_fotFutFin);
            if(lst_fotFutFin.size()!=0)
        		update(new List<FO_Forecast_Futuro__c>(lst_fotFutFin)); 
        }
     //JLA_04_End
    }//   updateForecastCommitUpside
	/*****************************
	 * @author: Javier Lopez Andradas
	 * @company:gCTIO
	 * @description: Method that retrieves from a NCS if it has overlay or not
	 * 
	 * <date>		<version>		<description>
	 * 09/08/2018	1.0				Initial
	 * ****************************/
    public static boolean checkOverlay(Map<String,List<BI_FVI_NOdos__c>> mp_nd,BI_FVI_Nodos__c nd){
      System.debug('JLA_check->'+nd);
        if(nd.BI_FVI_Overlay__c!=null && nd.BI_FVI_Overlay__c.equals('Overlay')){
            return true;
        }else{
            List<BI_FVI_Nodos__c> lst_nds = mp_nd.get(nd.Id);
            System.debug('JLA_check anulo lista ->'+lst_nds);
            if(lst_nds==null)
                return false;
            else
                for(BI_FVI_Nodos__c aux : lst_nds){
                    if(checkOverlay(mp_nd,aux)==true){
                        return true;
                    }
                }
        }
        return false;
    }
}