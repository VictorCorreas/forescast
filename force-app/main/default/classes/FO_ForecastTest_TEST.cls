@isTest
/******************************************
 * Author: JAvier Lopez Andradas
 * Company: gCTIO
 * Description: Test class for the Forecast development
 * 
 * 
 * 
 * 
 * *****************************************/
public class FO_ForecastTest_TEST {
    /*******************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description: Loads the initial data to do the test
     * 
     * 
     * <date>		<version>		<description>
     * ?????		1.0				Initail
     * 14/06/2018	1.1				Added Oportunidad Test Local_Fuera for coverage purposes
     * 06/08/2018	1.2				Changes for VE Forecast
     * ******************************************/
	@testsetup
    private static void loadTestData(){
        List<Account> lst_acc = new List<Account>();
        //Sustituir en summer por esto los RT Map<String,Schema.RecordTypeInfo> mp = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        List<RecordType> lst_rt = [Select Id,SObjectType,DeveloperName from RecordType where SObjectType IN('Account','BI_FVI_Nodos__c','Opportunity','BI_Objetivo_Comercial__c','BI_FVI_Nodos_Objetivos__c')];Map<String,String> rt_mp = new  Map<String,String>();
        String Id_Prof = [Select Id from Profile where Name='BI_Standard'].ID;
        String Id_Rol = [Select Id from UserRole where DeveloperName='Telefonica_Global'].ID;
       
        User usr_TESTGAM = new User(
        	LastNAme='XXX_TEST_JLA_USER_TEST',
        	username='CALLE_FALSA_123_@SPRINGFIELD.COM',
            profileId=Id_prof,
            UserRoleId=Id_Rol,
            BI_Permisos__c='Ejecutivo de Cliente',
            Email = 'CALLE_FALSA_123_@SPRINGFIELD.COM',
			EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            Localesidkey='en_US',
            Timezonesidkey=Label.BI_TimeZoneLA,
            CommunityNickname = 'CALLE_FALSA_123_',
            Alias='CALLE_FA',
            isActive=true
        	);
        DataBase.SaveResult db_usrGAM =null;
        User me = [Select Id from User where Id = :UserInfo.getUserId() limit 1];
        System.runAs(me){
            db_usrGAM = Database.insert(usr_TESTGAM);
        }
        for(RecordType rt : lst_rt){
            rt_mp.put(rt.SObjectType+rt.DeveloperName,rt.Id);
        }
        Map<Integer,BI_bypass__c> mp_by = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,true,true);                
        
        Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
  			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
		lst_acc.add(cuenta1);

		Account cuenta2 = new Account(
		    Name = 'CLIENTE_TEST',
		    BI_Country__c = Label.BI_Peru,
		    BI_No_Identificador_fiscal__c = '20538273525',
		    CurrencyIsoCode = 'MXN',
		    BI_Segment__c = 'test',
			BI_Subsegment_Regional__c = 'test',
			BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
		);
        lst_acc.add(cuenta2);
        //Aqui tengo los ids de las cuentas
        DataBase.SaveResult[] arr_svrAcc = DataBAse.insert(lst_acc);
        
        List<BI_FVI_Nodos__c> ls_node = new List<BI_FVI_Nodos__c>();
        BI_FVI_Nodos__c nodo_ncs = new BI_FVI_Nodos__c(
	            Name = 'NCS_TEST', 
	            RecordTypeId = rt_mp.get('BI_FVI_Nodos__cBI_FVI_NCS'),
	            BI_FVI_Pais__c = Label.BI_Peru,
                BI_FVI_Activo__c = true
        	);
        ls_node.add(nodo_ncs);
        DataBase.SaveResult[] arr_svrNodeP = DataBase.insert(ls_node); 
        BI_FVI_Nodos__c nodo_ncp = new BI_FVI_Nodos__c(
        	Name='NCP_TEST',
            RecordTypeId=rt_mp.get('BI_FVI_Nodos__cBI_FVI_NCP'),
            BI_FVI_NodoPadre__c=arr_svrNodeP[0].getId(),
            BI_FVI_Pais__c = Label.BI_Peru,
            BI_FVI_Activo__c = true
        );
        System.debug('JLA__TEST_Nodo_NCP: '+nodo_ncp);
        
        ls_node.clear();
        List<BI_FVI_Nodos__c> lst_ndNCP = new List<BI_FVI_Nodos__c>();
        lst_ndNCP.add(nodo_ncp);
       	Database.SaveResult[] arr_svrNode = Database.insert(lst_ndNCP);
        System.debug(arr_svrNode);
        List<BI_FVI_Nodo_Cliente__c> lst_ndc = new List<BI_FVI_Nodo_Cliente__c>();
        for(Database.SaveResult sv: arr_svrAcc){
            BI_FVI_Nodo_Cliente__c ndc = new BI_FVI_Nodo_Cliente__c(
            	BI_FVI_IdCliente__c=sv.getId(),
                BI_Estruct_Saltar_Validacion_Nodo_Activo__c = true,
                BI_FVI_IdNodo__c=arr_svrNode[0].getId()
            );
            lst_ndc.add(ndc);
        }
        insert(lst_ndc);
        List<BI_Periodo__c> lst_per =new List<BI_Periodo__c>();
        Date hoy = Date.today();
        Integer year = hoy.year();
        BI_Periodo__c per_y = new BI_Periodo__c(
        	Name='FY',
            BI_FVI_Activo__c=true,
            FO_Type__c='Año',
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,1,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_y);
        //Hacemos siempre el �ltimo caso del a�o, asi sabemos que siempre va a estar activo
        BI_Periodo__c per_q = new BI_Periodo__C(
        	Name='FYQ4',
            BI_FVI_Activo__c=true,
            
            FO_Type__c='Trimestre',
            FO_Secuencia__c=4,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,10,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_q);
        for(Integer i =10;i<=12;i++){
            Integer day= 31;
            //Novembre
            if(i==11)
                day=30;
            BI_Periodo__c per_M= new BI_Periodo__c(
            	Name='FYQM'+i,
                BI_FVI_Activo__c=true,
                FO_Type__c='Mes',
                FO_Secuencia__c=i-9,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,i,1),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,i, day)
                
            );
            lst_per.add(per_M);
        }
        //AHora van las semanas de diciembre, de 7 en 7 
        for(Integer i=0;i<5;i++){
            BI_Periodo__c per_w = new BI_Periodo__c(
            	Name='FYQM3W'+i,
                FO_Type__c='Semana',
                FO_Secuencia__c=i,
                BI_FVI_Activo__c=true,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,i*7),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, i*7+6)
            );
            lst_per.add(per_w);
        }
        BI_Periodo__c per_w5 = new BI_PEriodo__c(
        	Name='FQM3W5',
            FO_Type__c='Semana',
            BI_FVI_Activo__c=true,
            FO_Secuencia__c=5,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,28),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_w5);
        insert(lst_per);
        List<BI_Periodo__c> lst_perUpd = [Select Id,FO_Type__c,BI_FVI_Fecha_Inicio__c from BI_PEriodo__c];
        String Idy ='';
        String QId='';
        String MId='';
        //Sacamos los Id de los que ser�n jeraquicamente superiores
        for(BI_Periodo__c per: lst_perUpd){
            if(per.FO_Type__c.equals('Año')){
                Idy=per.Id;
            }else if(per.FO_Type__c.equals('Trimestre')){
                QId=per.Id;
            }else if(per.FO_Type__c.equals('Mes')){
                if(per.BI_FVI_Fecha_Inicio__c.month()==12){
                    MId=per.Id;
                }
            }
        }
        //Establecemos la jerarquia y ya que estamos aprvechamos el bucle para crear los Objetivos Peridos
        List<BI_FVI_Nodos_Objetivos__c> lst_noOb = new List<BI_FVI_Nodos_Objetivos__c>();
        List<BI_Objetivo_Comercial__c> mp_obj = new List<BI_Objetivo_Comercial__c>();
        for(BI_PEriodo__c per :lst_perUpd){
            BI_Objetivo_Comercial__c aux = new BI_Objetivo_Comercial__c(
            	BI_Periodo__c=per.Id,
                BI_Objetivo__c=199,
                BI_FVI_Activo__c=true,
                BI_Tipo__c='Cartera',
                FO2_Monocurrency__c=false,
            	BI_OBJ_Campo__c='BI_Net_Annual_Value__c',
                recordTypeId=rt_mp.get('BI_Objetivo_Comercial__cBI_FVI_Objetivos_Nodos')
            );
            mp_obj.add(aux);
        }
        insert(mp_obj);
        mp_obj = [Select Id,BI_Periodo__c from BI_Objetivo_Comercial__c ];
        Map<String,String> mp_perObj = new map<String,String>();
        for(BI_Objetivo_Comercial__c objcm :mp_obj){
            mp_perObj.put(objcm.BI_Periodo__c,objcm.Id);
        }
        for(BI_PEriodo__c per :lst_perUpd){
            BI_FVI_Nodos_Objetivos__c nd1Ob=null,nd2Ob=null;
            if(per.FO_Type__c.equals('Trimestre')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                    
                );
                per.FO_Periodo_Padre__c=Idy;
            }else if(per.FO_Type__c.equals('Mes')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=100,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=100,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                per.FO_Periodo_Padre__c=QId;
            }else if(per.FO_Type__c.equals('Semana')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=20,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=20,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                per.FO_Periodo_Padre__c=Mid;
            }else{
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
            }
            lst_noOb.add(nd1Ob);
            lst_noOb.add(nd2Ob);
        }
        update(lst_perUpd);
        insert(lst_noOb);
        //Ahora toca las oportunidades, tenemos que ver si se puede meter alguna en f1 directamente o no
        //JLA_Changes put the recordTypeId
        String idCiclo = rt_mp.get('OpportunityBI_Ciclo_completo'); 
        List<Opportunity> lst_opp = new List<Opportunity>();
        Opportunity opp1 = new Opportunity(Name = 'Test',
            CloseDate = Date.newInstance(year, 12, 31),
            BI_Requiere_contrato__c = true,
            Generate_Acuerdo_Marco__c = false,
            AccountId = arr_svrAcc[0].getId(),
            StageName = Label.BI_OpportunityStageF1Ganada,
            BI_Ultrarapida__c = true,
            BI_Duracion_del_contrato_Meses__c = 3,
            BI_Recurrente_bruto_mensual__c = 100,
            BI_Ingreso_por_unica_vez__c = 12,
            BI_Recurrente_bruto_mensual_anterior__c = 1,
            BI_Opportunity_Type__c = 'Producto Standard',
            currencyIsoCode='EUR',
            Amount = 23,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            BI_Country__c = Label.BI_Peru,
            RecordTypeId=idCiclo);
            
        lst_opp.add(opp1);
        Opportunity opptyLocal = new Opportunity(Name = 'Oportunidad Test Local',
            AccountId = arr_svrAcc[0].getId(),
            BI_Country__c = Label.BI_Peru,
            BI_Opportunity_Type__c = 'Digital',
            CloseDate =  Date.newInstance(year, 12, 31),
            StageName = 'F5 - Solution Definition',
            BI_Probabilidad_de_exito__c = '25',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            currencyIsoCode='EUR',
            BI_Plazo_estimado_de_provision_dias__c = 5,
            Amount = 5.8,
            RecordTypeId=idCiclo);
		Opportunity opptyLocal_Fuera = new Opportunity(Name = 'Oportunidad Test Local_Fuera',
            AccountId = arr_svrAcc[0].getId(),
            BI_Country__c = Label.BI_Peru,
            BI_Opportunity_Type__c = 'Digital',
            CloseDate =  Date.newInstance(year, 12, 11),
            StageName = Label.BI_F6Preoportunidad,
            BI_Probabilidad_de_exito__c = '25',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            currencyIsoCode='EUR',
            BI_Plazo_estimado_de_provision_dias__c = 5,
            Amount = 5.8,
            RecordTypeId=idCiclo);
        
        lst_opp.add(opptyLocal);
        lst_opp.add(opptyLocal_Fuera);
        
        insert(lst_opp);
        BI_MigrationHelper.disableBypass(mp_by);
    }
    /******************************************
     * Author: Javier López Andradas
     * Company:gCTIO
     * Description: to Test the trigger and controller side of the app, the normal flow of
     * 				the application, includes the detail server-controller coverage, depends of
     * 				the data load for the flow.
     * 
     * <date>		<version>		<description>
     * 11/04/2018	1.1				Modification over FO_ModificaFOIController.updateFOI
     * 21/06/2018	1.2				Modification in Actualiza
     * ***************************************/
    @isTest static void testForecast1(){
        BI_FVI_Nodos__c node = [Select Id from BI_FVI_Nodos__c where Name='NCP_TEST' limit 1];
        BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
        FO_Forecast__c fo = new FO_Forecast__c (
        	FO_Nodo__c=node.Id,
            FO_Periodo__c=per.Id,
            RecordTypeId=Schema.getGlobalDescribe().get('FO_Forecast__c').getDescribe().getRecordTypeInfosByDeveloperName().get('FO_Ejecutivo').getRecordTypeId()
        );
        insert fo;
        FO_Forecast__c foQ = [Select Id,RecordType.DeveloperName  from FO_Forecast__c limit 1];
        //Comprobamos que se ha puesto el RT correcto en el trigger
       	//Metemos al padre antes de todo
       
        System.assert(foQ.RecordType.DeveloperName.equals('FO_Ejecutivo'));
        List<FO_Forecast_Futuro__c> lst_foFut = [Select Id,FO_PEriodo__c,FO_Periodo__r.Name from FO_Forecast_Futuro__c];
        //Comprobamos que se han añadido los Forecast futuros al FO
        System.debug('ForecastFut:'+lst_foFut);
        System.assert(lst_foFut.size()==5);
        //actualiza losFOI
         BI_FVI_Nodos__c nodeP=[Select Id from BI_FVI_Nodos__c where Name='NCS_TEST' limit 1];
        FO_Forecast__c foP = new FO_Forecast__c(
        	FO_Nodo__c=nodeP.Id,
            FO_Periodo__c=per.Id
        );
        System.debug('Empezamos el test strat test del primero JLA_TEST');
        insert foP;
        FO_ActualizaForecast.actualizaFO(foQ.Id);
        
        List<FO_Forecast_Item__c> lst_foi = [Select Id from FO_Forecast_Item__c];
        System.assert(lst_foi.size()==1);
        
        lst_foi.get(0).FO_Tipo_Forecast__c='Commit';
        //Ahora hay que volver a sacar el forecast
        update(lst_foi);
        TEst.startTest();
        FO_ActualizaForecast.actualizaFO(foQ.Id);
        foQ=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c where FO_Nodo__c=:node.Id limit 1];
        System.assert(foQ.FO_Total_Forecast__c==foQ.FO_Total_Pipeline__c);
        
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().year())+'-12-31','Upside',true,true);
        
        foQ=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c limit 1];
        System.assert(foQ.FO_Total_Forecast__c==(foQ.FO_Total_Pipeline__c/2));
        String pa = FO_ModificaFOIController.getParent(foQ.Id);
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().year())+'-12-31','Commit',true,true);
        //No tiene padres
        System.assert(pa==null);
        
        //Enviamos el Forecast del subordinado
        System.assert(FO_DetailRecordPageController.changeStatus(foQ.Id).equals('OK'));
        
        
        FO_Forecast__c foQP=[Select Id,RecordType.DeveloperName,FO_Numero_Nodos__c from FO_Forecast__c where FO_Nodo__c=:nodeP.Id limit 1];
        //enviamos y devolvemos
        
         
        //Comprobamos que el RT es correcto
        System.assert(foQP.RecordType.DeveloperName.equals('FO_Manager'));
        //Deberia tener un unico nodo hijo
        System.assert(foQP.FO_Numero_Nodos__c==1);
        //Actualiza los forecast hijos
        FO_ActualizaForecast.actualizaFO(foQP.Id);
        
        
        foQ = [Select Id,FO_Forecast_Padre__c from FO_Forecast__c where FO_Nodo__c=:node.Id limit 1];
        System.assert(foQ.FO_Forecast_Padre__c.equals(foQP.Id));
        foQP=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c where FO_Nodo__c=:nodeP.Id  limit 1];
        //Nos hemos subido los valores correctos
        FO_ActualizaForecast.actualizaFO(foQP.Id);
        FO_ModificaFOIController.updateFO(foQ.Id,'Borrador');
		FO_ActualizaForecast.actualizaFO(foQP.Id);
        foQ.FO_Estado__c='Enviado';
        update foQ;
        FO_ModificaFOIController.updateFO(foQ.Id,'Borrador');
        System.debug('Finalizamos el Stop_JLA_TEST');
        TEst.stopTest();
        //Actualizamos un fotFut nos da igual cual
        FO_ForecastChange.changeFore(lst_foFut.get(0).Id,Double.valueOf('2000'),true,false);
        //Comprobamos que tenemos un fallo
        System.assert(FO_ForecastChange.changeFore(null,Double.valueOf('2000'),true,false).contains('Error:'));
        //Probamos a eliminar foi
        delete(lst_foi.get(0));
        
    }

    /******************************************
     * Author: Javier López Andradas
     * Company:gCTIO
     * Description: to Test the trigger and controller side of the app, the normal flow of
     * 				the application, includes the detail server-controller coverage, depends of
     * 				the data load for the flow.
     * 
     * <date>		<version>		<description>
     * 11/04/2018	1.1				Modification over FO_ModificaFOIController.updateFOI
     * 04/06/2018	1.2				Changes forversion 1.5 Snapshots
     * 03/08/2018	1.3				Coverage of FO_DetailRecordPageController.getFOF
     * ***************************************/
    @isTest static void testTablaDet(){
        //Ahora probamos la de la tabla de DEtalles
        List<BI_FVI_Nodos__c> lst_nod = FO_ForecastListControllerApex.getNodos(); 
        String Id_nod = '';
        for(BI_FVI_NOdos__c node: lst_nod){
            if(node.name.equals('NCP_TEST'))
                Id_nod=node.ID;
        }
        String perFo = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1].Id;
        System.debug('Nodod: '+Id_nod);
        System.debug('Per: '+perFo);
        Fo_Forecast__C fo = new FO_Forecast__c(
        	FO_PEriodo__c = perFo,
            currencyIsoCode='EUR',
            FO_Nodo__C = Id_nod
        );
        List<BI_Periodo__c> lst_perDet = FO_ForecastListControllerApex.getPeriodos(lst_nod.get(0).Id);

        insert(fo);
        List<BI_Periodo__c> lst_perFO=FO_ForecastListControllerApex.getPeriodosForecast(perFo);
        List<String> lst_auxPer = new List<String>();
        for(BI_Periodo__c perAux : lst_perFO)
            lst_auxPer.add(perAux.Id);
        List<FO_Forecast__c> lst_foDet = FO_ForecastListControllerApex.getForecasts(perFo, Id_nod);
        System.debug('FO: '+lst_foDet.get(0));
        List<FO_Forecast_Futuro__c> lst_foFutDet = FO_ForecastListControllerApex.getAjustes(lst_foDet.get(0).Id);
        List<String> lst_acc = FO_ForecastListControllerApex.getAccs(Id_nod);
        FO_ForecastListControllerApex.getOpps(String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31', lst_acc,lst_foDet.get(0).ID);
        FO_ForecastListControllerApex.getObjetivo(lst_nod.get(0).Id,String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31',new List<String>(),lst_foDet.get(0).Id,false);
        FO_ForecastListControllerApex.getObjetivo(lst_nod.get(0).Id,String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31',new List<String>(),lst_foDet.get(0).Id,true);
        //JLA_V1_3 getFOF(String idFore,boolean manager,String idper) coberaje
        FO_DetailRecordPageController.getFOF(lst_foDet.get(0).Id,false,perFo,lst_foDet.get(0).RecordType.DeveloperName);
    }
    
    /**************************************************
     * Author:Javier Lopez Andradas
     * Company:gCTIO
     * Description: To coverage standAlone methods
     * 
     * 
     * <date>		<version>		<Description>
     * 06/04/2018	1.1				Added today Period
     * 
     * **********************************************/
    @isTest static void testRetrieveForecast(){
        FO_ForecastListControllerApex.getOppStageName();
        FO_ModificaFOIController.getTipo();
        FO_ModificaFOIController.getEstado();
        FO_Crea_Forecast_cls.getNodes();
        BI_Periodo__c per = new BI_Periodo__c(
        Name='TEST',
        BI_FVI_Fecha_Inicio__c=Date.today(),
        BI_FVI_Fecha_Fin__c=Date.today(),
        FO_Type__c='Semana');
        insert per;
        FO_Crea_Forecast_cls.getPer();
        FO_DetailRecordPageController.returnOppsAtr(false);
        FO_DetailRecordPageController.returnOppsAtr(false);
        
    }
    @isTEst static void testForecast2(){
        List<BI_FVI_Nodos__c> node = [Select Id,Name from BI_FVI_Nodos__c ];
        BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
        String IdNodeP ='',IdNode='';
        for(BI_FVI_Nodos__c nod : node){
            if(nod.Name.equals('NCP_TEST')){
                IdNode=nod.Id;
            }else if(nod.Name.equals('NCS_TEST')){
                IdNodeP=nod.Id;
            }
        }
        FO_Forecast__c foP = new FO_Forecast__c(
        	FO_Nodo__c=IdNodeP,
            FO_Periodo__c=per.Id
        );
        DataBase.SaveResult svrFOP = DataBAse.insert(foP);
        FO_Forecast__c fo = new FO_Forecast__c(
        	FO_Nodo__c=IdNode,
            FO_Periodo__c=per.Id
        );
        DataBase.saveResult svrFo = Database.insert(fo);
        FO_ActualizaForecast.actualizaFO(svrFo.getId());
        test.startTest();
        List<FO_Forecast_Item__c>lst_foi = [select Id from FO_Forecast_Item__c];
        FO_ModificaFOIController.getClose(lst_foi.get(0).Id);
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().addYears(1).year())+'-12-31','Upside',true,true);
        
        FO_ModificaFOIController.getState(svrFo.getId());
        //delete(lst_foi.get(0));
        
        FO_ActualizaForecast.actualizaFO(svrFo.getId());
        FO_DetailRecordPageController.changeStatus(svrFo.getId());
        foP.Id=svrFOP.getId();
        foP.FO_Estado__c='Enviado';
        update foP;
        
        FO_DetailRecordPageController.changeStatus(svrFo.getId());
        FO_DetailRecordPageController.changeStatus(foP.Id);
        FO_DetailRecordPageController.checkSubmitted(svrFo.getId());
        FO_DetailRecordPageController.validaSub(svrFo.getId());
        FO_DetailRecordPageController.validaSub(svrFOP.getId());
        FO_DetailRecordPageController.getRT(svrFOP.getId());
        test.stopTest();
            
    }    
    /**********************************************
     * Author:Javier Lopez Andradas
     * Company: gcTIO
     * DEscription: Testing the batch to activate the periods
     * 
     * 
     * ************************************************/
    @isTest static void FO_test_Sch(){
        test.startTest();
        System.schedule('tst_Fore','0 0 0 3 9 ? 2022', new FO_Deactivate_periods_job());
    }
    /**********************************************
     * Author:Javier Lopez Andradas
     * Company: gcTIO
     * DEscription: Testing the ForecastList with all snapshots
     * 
     * <date>		<version>		<Description>
     * ???????		1.0				Initial
     * 18/09/2018	1.1				Added code for coverage FO_RelatedForeSub
     * 18/09/2018	1.2				Changes for support changes in UpdateSnapObj
     * ************************************************/
    @isTest static void FO_test_Snap(){
        List<BI_FVI_Nodos__c> node = [Select Id,Name from BI_FVI_Nodos__c where Name='NCS_TEST'];
        List<BI_Periodo__c> lst_per = [Select Id,Name,FO_Type__C from BI_Periodo__c];    
        //JLA_V1.2_Start
        List<BI_FVI_Nodos_Objetivos__c> lst_obj= [Select BI_FVI_IdPeriodo__c,BI_FVI_Objetivo_comercial__c from BI_FVI_Nodos_Objetivos__c];
        Map<String,String> mp_obj = new Map<String,String>();
        for(BI_FVI_Nodos_Objetivos__c ndObj :lst_obj){
            mp_obj.put(ndObj.BI_FVI_IdPeriodo__c,ndObj.BI_FVI_Objetivo_comercial__c);
        }
        //JLA_V1.2_Ends
        Map<String,Decimal> mp_foreSnapMonth = new Map<String,Decimal>();
        Map<String,Map<String,Decimal>> mp_snapPipe = new Map<String,Map<String,Decimal>>();
        Map<String,Decimal> mp_foreSnapWeek = new Map<String,Decimal>();
        String Idper = '';
        String IdperMonth='';
        for(BI_Periodo__c per:lst_per){
            if(per.Name.equals('FQM3W5')){
                Idper=per.Id;
            }else if(per.Name.equals('FYQM12')){
                	IdperMonth=per.Id;
            }
            
            if(per.FO_Type__C.equals('Semana')){
                mp_foreSnapWeek.put(per.Id,100);
                
            }else{
                Map<String,Decimal> aux = new Map<String,Decimal>();
                for(Integer i=1;i<=2;i++){
                    aux.put('F'+i,100);
                }
                mp_snapPipe.put(per.Id,aux);
                mp_foreSnapMonth.put(per.Id,100);
            }
            
        }
        System.debug('EL periodo del mes '+IdperMonth);
        
        FO_ForecastListControllerApex.getMonthPeriods(idper);
        FO_Forecast__c fore = new FO_Forecast__C(
        FO_Nodo__c=node.get(0).Id,
        FO_PEriodo__c=idper);
        Database.SaveResult svr = Database.insert(fore);
        FO_ForecastListControllerApex.updateSnapObj(mp_foreSnapMonth,mp_obj,svr.getId());
        FO_ForecastListControllerApex.updateActs(JSON.serialize(mp_foreSnapMonth),JSON.serialize(mp_foreSnapWeek),svr.getId());
        FO_ForecastListControllerApex.updatePipeline(System.JSON.serialize(mp_SnapPipe),svr.getId());
        FO_ForecastListControllerApex.getForeInfo(svr.getId());
        FO_ForecastListControllerApex.getActs(svr.getId());
        for(String key:mp_foreSnapMonth.keyset()){
            mp_foreSnapMonth.put(key,50);
            Map<String,Decimal> aux = mp_SnapPipe.get(key);
            for(String key2:aux.keySet()){
                aux.put(key2,50);
            }
            mp_SnapPipe.put(key,aux);
        }
        
        for(String key:mp_foreSnapWeek.keySet()){
            mp_foreSnapWeek.put(key,50);
        }
        FO_ForecastListControllerApex.updateSnapObj(mp_foreSnapMonth,mp_obj,svr.getId());
        FO_ForecastListControllerApex.updateActs(JSON.serialize(mp_foreSnapMonth),JSON.serialize(mp_foreSnapWeek),svr.getId());
        FO_ForecastListControllerApex.updatePipeline(System.JSON.serialize(mp_SnapPipe),svr.getId());
        //CAmbio la oportunidad fuera
        Opportunity op =[Select Id from Opportunity where Name='Oportunidad Test Local_Fuera' limit 1];
        Date hoy = Date.today();
        Integer year = hoy.year();
        op.CloseDate=Date.newInstance(year, 12, 31);
        update(op);
        FO_ActualizaForecast.actualizaFO(svr.getId());
        //JLA_V1.1
        FO_RelatedForeSub_ctr.extractSub(svr.getId());
        FO_RelatedForeSub_ctr.devolverForecast(svr.getId());
        FO_RelatedForeSub_ctr.getFof(svr.getId(),IdperMonth);
    }
    
    /**********************************************
     * Author:Javier Lopez Andradas
     * Company: gcTIO
     * DEscription: Testing the ActulizaFo
     * 
     * 
     * ************************************************/
    @isTest
    public static void cambioNAV(){
         List<BI_FVI_Nodos__c> node = [Select Id,Name from BI_FVI_Nodos__c where Name='NCP_TEST'];
        BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
         FO_Forecast__c fore = new FO_Forecast__C(
        FO_Nodo__c=node.get(0).Id,
        FO_PEriodo__c=per.Id);
        Database.SaveResult svr = DataBase.insert(fore);
        List<FO_Forecast_Item__c> lst_foi = [Select Id from FO_Forecast_Item__c ];
        for(FO_Forecast_Item__c foi :lst_foi){
            foi.FO_Tipo_Forecast__c='Commit';
        }
        update(lst_foi);
        TEst.startTest();
        List<Opportunity> lst_opp = [Select Id from Opportunity];
        for(Opportunity opp:lst_opp){
            opp.BI_Ingreso_por_unica_vez__c=200;
        }
        update(lst_opp);
        FO_ActualizaForecast.actualizaFO(svr.getId());
        TEst.stopTest();
    }
}