@isTest
public class BI_FVIBorrar_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       Aborda
        Description:   Method that manage the coverage of BI_FVIBorrar Coverage
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        23/06/2016                      Jorge Galindo             Initial version
		    03/08/2016						          Jorge Galindo			        added new mandatory BI_Licitacion__c, StageName to Opportunity 
        02/01/2017                      Pedro Párraga             Increase coverage  
        20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c                                                          
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest public static void test_Method_One(){
        BI_TestUtils.throw_exception = false;
        User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.runAs(me){

            Account acc1 = new Account(
                        Name = 'XXX Zytrust 1',
                        BI_Tipo_de_identificador_fiscal__c = 'DNI',
                        BI_No_Identificador_fiscal__c = '20518569391',
                        BI_Country__c= Label.BI_Peru,
                        BillingStreet = 'calle xxx',
                        BillingCity = 'ciudad xxx',
                        BillingState = 'provincia xxx',
                        BillingPostalCode = '00000',
                        BillingCountry = Label.BI_Peru,
                        BI_FVI_Tipo_Planta__c = '',
                        BI_Segment__c = 'test',
                        BI_Subsegment_Regional__c = 'test',
                        BI_Territory__c = 'test'
            );

                  insert acc1;
                
           Contact con = new Contact(LastName = 'test',
                                    AccountId = acc1.Id,
                                    Email = 'test@gmail.com',
                                    HasOptedOutOfEmail = false,
                                    BI_Activo__c = true,
                                    BI_Tipo_de_documento__c = 'NIF',
                                    BI_Country__c = Label.BI_Peru,
                                    BI_Numero_de_documento__c = '0000000'
                        );

              insert con;




            NE__Catalog_Header__c catalogheaderobj = new NE__Catalog_Header__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test WS Zytrust',
                    NE__Active__c = true,
                    NE__Enable_Global_Cart__c = false,
                    NE__Name__c = 'Catalog WS Zytrust',
                    NE__Source_Catalog_Header_Id__c = 'a1R11000000H0tc',
                    NE__Start_Date__c = system.today()-1
                  ); 
                  insert catalogheaderobj;
            
                  NE__Catalog__c catalogobj = new NE__Catalog__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test WS Zytrust x',
                    NE__Active__c = true,
                    NE__Catalog_Header__c = catalogheaderobj.Id,
                    NE__Enable_for_oppty__c = false,
                    NE__Engine_Code__c = 'a1U110000002812',
                    NE__Source_Catalog_Id__c = 'a1U110000002812',
                    NE__Version__c = 1.0,
                    NE__Visible_web__c = false
                  ); 
                  insert catalogobj;

                  NE__Catalog_Category__c catalogCategory = new NE__Catalog_Category__c(
                        Name = 'Movil',
                        NE__CatalogId__c = catalogobj.Id
                  );

                  NE__Product__c prod1 = new NE__Product__c(
                        Name = 'Plan vuela 1',
                        Payback_Family__c = 'Planes'
                  );

                  insert prod1;
                        
                  NE__DocumentationCharacterist__c doc1 = new NE__DocumentationCharacterist__c(Name = 'Tarifas Adicionales: Todo Destino (M-M/M-O/M-F/M-LDI*) *incluye USA y Cánada',
                                                                                              NE__Product__c = prod1.Id,
                                                                                              NE__Value__c = '01'
                                                                                              );

                  insert doc1;
                    
                  NE__Catalog_Item__c catalogItem1 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = prod1.Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );

                  insert catalogItem1;
            
            
            RecordType rt_cc= [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo'];
            
            Opportunity oppCaso1 = new Opportunity(
                Name = 'XXX OppZytrust Caso 1',
                BI_Country__c = 'Peru',
                        AccountId = acc1.Id,
                RecordTypeId = rt_cc.Id,
                BI_Opportunity_Type__c = 'Movil',
                BI_Duracion_del_contrato_Meses__c = 14,
                BI_Plazo_estimado_de_provision_dias__c = 22,
                CloseDate = Date.today() + 15,
                BI_Probabilidad_de_exito__c = '95',
                CurrencyIsoCode = 'MXN',
                BI_Requiere_contrato__c = true,
                BI_FVI_Contacto__c = con.Id,
                BI_Licitacion__c = 'No',
                StageName = 'F6 - Prospecting'
            );

            insert oppCaso1;
                  
                  NE__Order__c order1 = new NE__Order__c(
                        NE__OptyId__c = oppCaso1.Id,
                        NE__OrderStatus__c   =   'Pending',
                        NE__CatalogId__c = catalogobj.Id
                  );

                  insert order1;

                  NE__OrderItem__c orderItem1 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'MNP Port In',
                        NE__Status__c = 'FVI Borrar',
                        NE__Qty__c = 1,
                        NE__BaseOneTimeFee__c =  25,
                        NE__CatalogItem__c = catalogItem1.Id
                  );

                  insert orderItem1;

            try{
              update orderItem1;
            }catch(Exception e){}

        }
    }
}