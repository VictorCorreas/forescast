public with sharing class BI_FVI_SurveyQuestionMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Humberto Nunes
     Company:       NEAborda
     Description:   Methods executed by SurveyQuestion Triggers
     Test Class:    BI_FVI_SurveyQuestionMethods_TEST
    
     History:
     
     <Date>                     <Author>                    <Change Description>
     27/05/2016                 Humberto Nunes				Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Hunberto Nunes			
    Company:       NEAborda
    Description:   Método para mantener solo 1 seleccion de pregunta ISC por Encuesta

    History: 

    <Date>                  	<Author>                	<Change Description>
    27/05/2016                 	Humberto Nunes				Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void OnlyOneCSI(List<Survey_Question__c > news) 
	{
        try 
        {
        	if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
        	
        	Map<Id, Id> mapa_S_SQ = new Map<Id,Id>();
        	
        	for (Survey_Question__c SQ: news) 
        		if (SQ.BI_FVI_CSI_Question__c == true)
        			mapa_S_SQ.put(SQ.Survey__c, SQ.Id);

        	List <Survey_Question__c> SQ2 = [SELECT Id, BI_FVI_CSI_Question__c FROM Survey_Question__c WHERE Survey__c IN: mapa_S_SQ.keySet() AND Id NOT IN: mapa_S_SQ.values() AND BI_FVI_CSI_Question__c = true];
    		for (Survey_Question__c SQ3: SQ2) 
    			SQ3.BI_FVI_CSI_Question__c = false;

    		update SQ2;
        } 
        catch (Exception exc) 
        {
            BI_LogHelper.generate_BILog('BI_FVI_SurveyQuestionMethods.OnlyOneCSI', 'BI_EN', exc, 'Trigger');
        }
    }
	

    public static void ValidateAnswersOnCSIQuestion(List<Survey_Question__c > news) 
    {
        try 
        {
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            for (Survey_Question__c SQ: news)
            { 
                if (SQ.BI_FVI_CSI_Question__c == true)
                {
                    if (!(SQ.Type__c == 'Single Select--Horizontal' || SQ.Type__c == 'Single Select--Vertical')) 
                    {
                        SQ.addError(System.Label.LABS_SF_ERROR_CSI);
                    }
                    else
                    {
                        list<String> listSplit = SQ.Choices__c.split('\r\n'); 
                        for (String S: listSplit)
                            if (!S.isNumeric())
                                SQ.addError(System.Label.LABS_SF_ERROR_CSI);
                    }                    
                } 
            }
        } 
        catch (Exception exc) 
        {
            BI_LogHelper.generate_BILog('BI_FVI_SurveyQuestionMethods.OnlyOneCSI', 'BI_EN', exc, 'Trigger');
        }
    }
}