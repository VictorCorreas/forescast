/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   Test class for the class BI_FVI_NodosShare
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    10/06/2016           Geraldine P. Montes         Initial Version
    12/05/2017              Cristina Rodriguez       Solve Async Future Method Inside Loops. Changed BI_FVI_NodosShare.createNodosShare
    -------------------------------------------------------------------------------*/ 

@isTest
private class BI_FVI_NodosShare_TEST {

	public static list<BI_FVI_Nodos__c> lstNodos = new list<BI_FVI_Nodos__c>();
	public static list<BI_FVI_Nodos__c> lstNodosNAB = new list<BI_FVI_Nodos__c>();
	public static list<BI_FVI_Nodos__c> lstNShare1 = new list<BI_FVI_Nodos__c>();
	public static BI_FVI_Nodos__c objNAG = new BI_FVI_Nodos__c();
	public static BI_FVI_Nodos__c objNAB = new BI_FVI_Nodos__c();
	public static list<Group> lstGroup = new list<Group>();
	public static Group objGroup = new Group();
	public static list<Group> lstGroupNAB = new list<Group>();
	public static Group objGroupNAB = new Group();
	public static String strNABRecordType = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Adquisión Barbecho').getRecordTypeId();
	public static String strNAGRecordType = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Adquisición de Gestión').getRecordTypeId();
    public static BI_FVI_Nodos__Share objNodoShare =  new BI_FVI_Nodos__Share();
    public static list<BI_FVI_Nodos__Share> lstNodoShare =  new list<BI_FVI_Nodos__Share>();
	
	public static void createData() 
	{
		objNAG.name = 'NAG_PRUEBA';
		objNAG.BI_FVI_Pais__c = Label.BI_Peru;
		objNAG.RecordTypeId = strNAGRecordType;

		lstNodos.add(objNAG);		

        objGroup.Name = 'BI_FVI_PER_DIST_ADM_BO';
        objGroup.DoesIncludeBosses = false;
        lstGroup.add(objGroup);

       
	}
	
	@isTest static void createNodosShare_TEST() {
		
		createData();
		BI_TestUtils.throw_exception = false;
		Test.StartTest();
		system.debug('lstNodos::::: ' + lstNodos[0].BI_FVI_Pais__c);
		system.debug('lstGroup::::: ' + lstGroup[0].name);
		insert lstNodos;
		insert lstGroup;
		//START CRM 12/05/2017 Async Future Method Inside Loops
		set<String> setPaises = new set<String>();
		setPaises.add(lstNodos[0].BI_FVI_Pais__c);

		BI_FVI_NodosShare.createNodosShare(setPaises);		
		/*******OLD
		BI_FVI_NodosShare.createNodosShare(lstGroup[0].name, lstNodos[0].BI_FVI_Pais__c);
		*/
		//END CRM 12/05/2017 Async Future Method Inside Loops

		
		Test.StopTest();
	}

	@isTest static void createNodosShareNAB_TEST() {

		createData();
		BI_TestUtils.throw_exception = false;
		
		objNAB.name = 'NAB_PRUEBA';
		objNAB.BI_FVI_Pais__c = Label.BI_Peru;
		objNAB.RecordTypeId = strNABRecordType;
		lstNodosNAB.add(objNAB);
		insert lstNodosNAB;

		objGroupNAB.Name = 'BI_FVI_PER_DIST_ADM_BO';
        objGroupNAB.DoesIncludeBosses = false;
        lstGroupNAB.add(objGroupNAB);
		insert lstGroupNAB;

		//objNodoShare.UserOrGroupId  = lstGroupNAB[0].Id;
  //      objNodoShare.ParentId   = lstNodosNAB[0].Id;
  //      objNodoShare.AccessLevel = 'Edit';
  //      lstNodoShare.add(objNodoShare);
       
        Test.StartTest();
		
		//insert lstNodoShare;
		RecordType NodoRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_NAB'];
		map<string, string> mapgroup = new map<string, string>();
		for(BI_FVI_Nodos__c objNodos :[ SELECT Id,BI_FVI_Pais__c FROM BI_FVI_Nodos__c WHERE RecordTypeId =: NodoRecordType.Id AND BI_FVI_Pais__c = :Label.BI_Peru])
		{

			mapgroup.put(objNodos.BI_FVI_Pais__c, lstGroupNAB[0].name );
			system.debug('mapa::::: ' + mapgroup);
			system.debug('consulta::::: ' + objNodos);
		}

		BI_FVI_NodosShare.createNodosShareNAB(mapgroup);
		Test.StopTest();
	}
}