public with sharing class BI_FVI_DesempenoMethods {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García             
    Company:       NEAborda
    Description:   Método para crear registros en el objeto BI_FVI_Nodo_Objetivo_Desempeno__c

    History: 

    <Date>                  <Author>                <Change Description>
    09/05/2016              Antonio Masferrer       Initial Version
    16/09/2016              Miguel Cabrera          Infinity W4+5
    22/09/2017              Humberto Nunes          Desempeño asociado a NODOS o CLIENTES
    12/02/2018              Humberto Nunes          Desempeños Jerarquicos
    15/03/2018              Humberto Nunes          Se agrego el Tipo de Registro a BI_FVI_Nodo_Objetivo_Desempeno__c
    09/09/2019              Elena Bueno             Se agrega limite a la Query de la l.63

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // FAR 29/09/2016: Use an static map
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    static 
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('BI_O4_Incentive_Details__c', 'BI_Registro_Datos_Desempeno__c', 'BI_FVI_Nodo_Objetivo_Desempeno__c')])
        {
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }
    // END FAR 29/09/2016
    
    public static void generaNodo_Objetivo_Desempeno(List<BI_Registro_Datos_Desempeno__c> tnew , List<BI_Registro_Datos_Desempeno__c> told)
    {       
        try
        {          
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            Set<Id> set_IdCliente = new Set<Id>();
            Set<Id> set_IdNodo = new Set<Id>();

            Date max_date = null;
            Date min_date = null;

            for(BI_Registro_Datos_Desempeno__c rdd : tnew)
            {
                if (rdd.RecordTypeId != MAP_NAME_RT.get('BI_OBJ_Desempeno_Nodos_Jerarquico')) // MODELO NODO
                {
                    if (rdd.BI_FVI_Id_Cliente__c!=null) set_IdCliente.add(rdd.BI_FVI_Id_Cliente__c);
                    if (rdd.BI_FVI_Id_Nodo__c!=null) set_IdNodo.add(rdd.BI_FVI_Id_Nodo__c);  
                }

                if(max_date == null)
                {
                    max_date = rdd.BI_FVI_Fecha__c;
                    min_date = rdd.BI_FVI_Fecha__c;
                }
                else
                {
                    if(rdd.BI_FVI_Fecha__c > max_date) max_date = rdd.BI_FVI_Fecha__c;
                    if(rdd.BI_FVI_Fecha__c < min_date) min_date = rdd.BI_FVI_Fecha__c;
                }
            }   

            List<BI_FVI_Nodos_Objetivos__c> lst_no = [SELECT Id,
                                                             BI_FVI_Id_Nodo__c,
                                                             BI_FVI_Objetivo_Comercial__r.BI_Tipo__c,
                                                             BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c,
                                                             BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c
                                                      FROM BI_FVI_Nodos_Objetivos__c
                                                      WHERE BI_FVI_IdPeriodo__r.BI_FVI_Activo__c = true 
                                                      AND BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >=: min_date
                                                      AND BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <=: max_date
                                                      AND BI_FVI_Objetivo_Comercial__r.BI_FVI_Activo__c = true
                                                      order by CreatedDate desc limit 50000
                                                     ];

            List<BI_FVI_Nodo_Objetivo_Desempeno__c> lst_nod = new List<BI_FVI_Nodo_Objetivo_Desempeno__c>();

            // DESEMPEÑO ASOCIADO A CLIENTE
            if(!set_IdCliente.isEmpty()) 
            {
                List<BI_FVI_Nodo_Cliente__c> lst_nc = [SELECT   BI_FVI_IdCliente__c, 
                                                                BI_FVI_IdNodo__c, 
                                                                BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c, 
                                                                BI_FVI_IdNodo__r.BI_FVI_NodoPadre__r.RecordType.DeveloperName 
                                                       FROM BI_FVI_Nodo_Cliente__c 
                                                       WHERE BI_FVI_IdCliente__c IN: set_IdCliente ];

                for(BI_Registro_Datos_Desempeno__c rdd : tnew)
                {
                    for(BI_FVI_Nodo_Cliente__c nodo_cliente : lst_nc)
                    {                   
                        if(rdd.BI_FVI_Id_Cliente__c == nodo_cliente.BI_FVI_IdCliente__c)
                        {                       
                            for(BI_FVI_Nodos_Objetivos__c nodo_objetivo : lst_no)
                            {
                                // SI ES EL NODO O EL PADRE DEL NODO IGUAL AL NODO DEL NODO OBJETIVO
                                if((nodo_cliente.BI_FVI_IdNodo__c == nodo_objetivo.BI_FVI_Id_Nodo__c || (nodo_cliente.BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c == nodo_objetivo.BI_FVI_Id_Nodo__c && nodo_cliente.BI_FVI_IdNodo__r.BI_FVI_NodoPadre__r.RecordType.DeveloperName == 'BI_FVI_NCP')) && 
                                   nodo_objetivo.BI_FVI_Objetivo_Comercial__r.BI_Tipo__c == rdd.BI_Tipo__c && 
                                   (nodo_objetivo.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <= rdd.BI_FVI_Fecha__c && nodo_objetivo.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >= rdd.BI_FVI_Fecha__c)){
                                    
                                    BI_FVI_Nodo_Objetivo_Desempeno__c nod = new BI_FVI_Nodo_Objetivo_Desempeno__c();
                                    nod.BI_FVI_Nodo_Objetivo__c = nodo_objetivo.Id;
                                    nod.BI_FVI_Registro_Datos_Desempeno__c = rdd.Id;
                                    nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;
                                    nod.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivo_Desempeno');
                                    lst_nod.add(nod);
                                }
                            }
                        }
                    }
                }
            }

            // DESEMPEÑO ASOCIADO A NODO
            if(!set_IdNodo.isEmpty()) 
            {
                for(BI_Registro_Datos_Desempeno__c rdd : tnew)
                {
                    for(BI_FVI_Nodos_Objetivos__c nodo_objetivo : lst_no)
                    {
                        // SI ES EL NODO O EL PADRE DEL NODO IGUAL AL NODO DEL NODO OBJETIVO
                        if(rdd.BI_FVI_Id_Nodo__c == nodo_objetivo.BI_FVI_Id_Nodo__c && 
                           nodo_objetivo.BI_FVI_Objetivo_Comercial__r.BI_Tipo__c == rdd.BI_Tipo__c && 
                           (nodo_objetivo.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <= rdd.BI_FVI_Fecha__c && nodo_objetivo.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >= rdd.BI_FVI_Fecha__c))
                        {
                            
                            BI_FVI_Nodo_Objetivo_Desempeno__c nod = new BI_FVI_Nodo_Objetivo_Desempeno__c();
                            nod.BI_FVI_Nodo_Objetivo__c = nodo_objetivo.Id;
                            nod.BI_FVI_Registro_Datos_Desempeno__c = rdd.Id;
                            nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;
                            nod.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivo_Desempeno');
                            lst_nod.add(nod);
                        }
                    }
                }
            }

            // DESEMPEÑOS JERARQUICOS 
            for(BI_Registro_Datos_Desempeno__c rdd : tnew)
            {
                system.debug('Create_RDD_NCJ rdd.RecordTypeId: ' + rdd.RecordTypeId + ' = ' + MAP_NAME_RT.get('BI_OBJ_Desempeno_Nodos_Jerarquico'));
                if (rdd.RecordTypeId == MAP_NAME_RT.get('BI_OBJ_Desempeno_Nodos_Jerarquico'))
                {
                    BI_FVI_Nodo_Objetivo_Desempeno__c nod = new BI_FVI_Nodo_Objetivo_Desempeno__c();
                    nod.BI_FVI_Nodo_Objetivo__c = rdd.BI_OBJ_NodoObjetivo__c;
                    nod.BI_FVI_Registro_Datos_Desempeno__c = rdd.Id;
                    nod.BI_OBJ_Valor_Moneda__c =  rdd.BI_OBJ_Valor_Moneda__c;
                    nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;
                    nod.CurrencyIsoCode = rdd.CurrencyIsoCode;
                    nod.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_NodoJerarquico_Objetivo_Desempeno');
                    lst_nod.add(nod);
                }
            }

            if(!lst_nod.isEmpty()) insert lst_nod;

        }
        catch(Exception exc)
        {
            BI_LogHelper.generate_BILog('BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno', 'BI_FVI', Exc, 'Trigger');
        }
        
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García             
    Company:       NEAborda
    Description:   Método para actualizar el valor del Nodo Objetivo Desempeño si el desempeño cambia su valor

    History: 

    <Date>                  <Author>                <Change Description>
    09/05/2016              Antonio Masferrer       Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizaValor(List<BI_Registro_Datos_Desempeno__c> tnew , List<BI_Registro_Datos_Desempeno__c> told){
        
        try{
            
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            Set<BI_Registro_Datos_Desempeno__c> set_rdd = new Set<BI_Registro_Datos_Desempeno__c>();
            Set<Id> set_id = new Set<Id>();
            for(Integer i = 0 ; i < tnew.size() ; i++){
                if(tnew[i].BI_FVI_Valor__c != told[i].BI_FVI_Valor__c ){
                    set_rdd.add(tnew[i]);
                    set_id.add(tnew[i].Id);
                }
            }

            List<BI_FVI_Nodo_Objetivo_Desempeno__c> lst_nod = [SELECT Id,BI_FVI_Valor__c,BI_FVI_Registro_Datos_Desempeno__c FROM BI_FVI_Nodo_Objetivo_Desempeno__c WHERE BI_FVI_Registro_Datos_Desempeno__c IN: set_id];

            for(BI_FVI_Nodo_Objetivo_Desempeno__c nod : lst_nod){
                for(BI_Registro_Datos_Desempeno__c rdd : set_rdd){
                    if(nod.BI_FVI_Registro_Datos_Desempeno__c == rdd.Id){
                        nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;
                    }
                }
            }

            update lst_nod;
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_FVI_DesempenoMethods.actualizaValor', 'BI_FVI', Exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera               
    Company:       New Energy Aborda
    Description:   Método que recopila los datos de Registro Datos Desempeño por cada usuario y crea un registro de Incentive Details con las Opp y Revenues 
                    del usuario en un intervalo de tiempo pasado en el Registro de Datos de desempeño.

    History: 

    <Date>                  <Author>                <Change Description>
    16/09/2016              Miguel Cabrera          Initial Version     
    16/06/2017              Alberto Fernández       'F1 - Closed Won' StageName condition added for Opportunities
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
    public static void recopilaDatosDesempenoParaOppYRevenues(List<BI_Registro_Datos_Desempeno__c>news){

        Integer mesIntervalo, anoIntervalo;
        List<Id> idComercial = new List<Id>();
        List<Integer> lstMes = new List<Integer>();
        List<Integer> lstAno = new List<Integer>();
        Map<Id, Date> mapFFI = new Map<Id, Date>();
        Map<Id, Date> mapFII = new Map<Id, Date>();
        Map<Id, Id> mapComercialRegistro = new Map<Id, Id>();
        
        try{
            //De cada BI_Registro_Datos_Desempeno__c con RecordType = TGS nos quedamos con el propietario, que será el "comercial" al que se le hara el cálculo
            //y las fechas para sacar las revenues y opp's en ese intervalo
            for(BI_Registro_Datos_Desempeno__c item : news){
                if(item.RecordTypeId == MAP_NAME_RT.get('BI_O4_TGS')){
                    
                    idComercial.add(item.OwnerId);
                    mapComercialRegistro.put(item.Id, item.OwnerId);
                    lstMes.add(Integer.valueOf(item.BI_O4_Month__c));
                    lstAno.add(Integer.valueOf(item.BI_O4_Year__c));
                    Date fechaFinIntervalo = Date.newInstance(Integer.valueOf(item.BI_O4_Year__c), Integer.valueOf(item.BI_O4_Month__c) + 1, 1 - 1);
                    Date fechaInicioIntervalo = Date.newInstance(Integer.valueOf(item.BI_O4_Year__c), 1, 1);
                    mapFFI.put(item.OwnerId, fechaFinIntervalo);
                    mapFII.put(item.OwnerId, fechaInicioIntervalo);
                }
            }
                    
            //Oportunidades Closed Won asociadas al "comercial" del BI_Registro_Datos_Desempeno__c
            List<Id> lstIdOppComercial = new List<Id>();
            List<Opportunity> lstOppComercial = new List<Opportunity>();
            //sacamos los Id's de las opp del equipo de oportunidades en las que este el comercial
            List<OpportunityTeamMember> lstOtmComercial = [SELECT OpportunityId FROM OpportunityTeamMember WHERE UserId IN: idComercial];       
            if(!lstOtmComercial.isEmpty()){
                for(OpportunityTeamMember otm : lstOtmComercial){
                    
                    lstIdOppComercial.add(otm.OpportunityId);
                }
            }
            //Sacamos las opp's "Closed Won" completas del comercial y si estan dentro del intervalo de fechas del registro de datos de desempeño las guardamos
            if(!lstIdOppComercial.isEmpty()){
                for(Opportunity opp : [SELECT Id, AccountId, Name, BI_Fecha_de_cierre_real__c, BI_O4_Total_Calculated_NAV__c, OwnerId, StageName FROM Opportunity WHERE (StageName = 'Closed Won' OR StageName = 'F1 - Closed Won') AND Id IN: lstIdOppComercial]){
                    if(opp.BI_Fecha_de_cierre_real__c > mapFII.get(opp.OwnerId) && opp.BI_Fecha_de_cierre_real__c <= mapFFI.get(opp.OwnerId)){
                        
                        lstOppComercial.add(opp);
                    }
                    
                }
            }
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------
            //Revenues asociadas al "comercial" del BI_Registro_Datos_Desempeno__c
            List<Id> lstIdAccComercial = new List<Id>();
            List<BI_O4_Revenue__c> lstRevComercial = new List<BI_O4_Revenue__c>();
            //sacamos las cuentas del account team member del comercial
            List<AccountTeamMember> lstAtmComercial = [SELECT AccountId FROM AccountTeamMember WHERE UserId IN: idComercial];
            if(!lstAtmComercial.isEmpty()){
                for(AccountTeamMember atm : lstAtmComercial){
                    
                    lstIdAccComercial.add(atm.AccountId);
                }
            }
            //para esas cuentas, sacamos las revenues asociadas del comercial. Se filtran por fecha segun el intervalo del registro de datos de desempeño y las guardamos
            Integer i = 0;
            if(!lstIdAccComercial.isEmpty() && !lstMes.isEmpty() && !lstAno.isEmpty() && i < lstMes.size() && i < lstAno.size()){
                for(BI_O4_Revenue__c rev : [SELECT Id, BI_O4_Month_number__c, BI_O4_Year__c, BI_O4_Value__c, Name, BI_O4_Account__c, BI_O4_Region_Code__c FROM BI_O4_Revenue__c WHERE BI_O4_Year__c = :lstAno[i] AND BI_O4_Month_number__c <= :lstMes[i] AND BI_O4_Account__c IN: lstIdAccComercial]){
                    
                    lstRevComercial.add(rev);
                    i++;
                }
            }
            
            //Creamos un Incentive Details segun su RecordType, si es Oportunidad o Revenue
            //Oportunidades
            List<BI_O4_Incentive_Details__c> lstIdInsertarOpp = new List<BI_O4_Incentive_Details__c>();
            if(!lstOppComercial.isEmpty()){
                for(BI_Registro_Datos_Desempeno__c rdd : news){
                    for(Opportunity opp : lstOppComercial){
                        if(rdd.OwnerId == mapComercialRegistro.get(rdd.Id)){
                            
                            //recorremos el registro de datos de desempeño y por cada uno, si el propietario coincide con el comercial creamos un Incentive details 
                            //rellenando los campos con los valores de la opp
                            BI_O4_Incentive_Details__c incentiveD = new BI_O4_Incentive_Details__c();
                            incentiveD.RecordTypeId = MAP_NAME_RT.get('BI_O4_Opportunity');
                            incentiveD.BI_O4_Account__c = opp.AccountId;
                            incentiveD.BI_O4_Opportunity__c = opp.Id;
                            incentiveD.BI_O4_User__c = mapComercialRegistro.get(rdd.Id);
                            incentiveD.BI_O4_Registro_Datos_Desempeno__c = rdd.Id;
                            incentiveD.BI_O4_Value_converted__c = opp.BI_O4_Total_Calculated_NAV__c;
                            incentiveD.BI_O4_Year__c = opp.BI_Fecha_de_cierre_real__c.year();
                            incentiveD.BI_O4_Month__c = opp.BI_Fecha_de_cierre_real__c.month();
                            
                            lstIdInsertarOpp.add(incentiveD);
                        }
                    }
                }   
            }

            //Revenues
            List<BI_O4_Incentive_Details__c> lstIdInsertarRv = new List<BI_O4_Incentive_Details__c>();
            if(!lstRevComercial.isEmpty()){
                for(BI_Registro_Datos_Desempeno__c rdd : news){
                    for(BI_O4_Revenue__c rv : lstRevComercial){
                        if(rdd.OwnerId == mapComercialRegistro.get(rdd.Id)){
                            
                            //recorremos el registro de datos de desempeño y por cada uno, si el propietario coincide con el comercial creamos un Incentive details 
                            //rellenando los campos con los valores de la revenue
                            BI_O4_Incentive_Details__c incentiveD = new BI_O4_Incentive_Details__c();
                            incentiveD.RecordTypeId = MAP_NAME_RT.get('BI_O4_Revenue');
                            incentiveD.BI_O4_Account__c = rv.BI_O4_Account__c;
                            incentiveD.BI_O4_Month__c = rv.BI_O4_Month_number__c;
                            incentiveD.BI_O4_Region_Code__c = rv.BI_O4_Region_Code__c;
                            incentiveD.BI_O4_Registro_Datos_Desempeno__c = rdd.Id;
                            incentiveD.BI_O4_Revenue__c = rv.Id;
                            incentiveD.BI_O4_User__c = mapComercialRegistro.get(rdd.Id);
                            incentiveD.BI_O4_Revenue_name__c = rv.Name;
                            incentiveD.BI_O4_Value_converted__c = rv.BI_O4_Value__c;
                            incentiveD.BI_O4_Year__c = rv.BI_O4_Year__c;

                            lstIdInsertarRv.add(incentiveD);
                        }
                    }
                }
            }

            //insertamos los incentive details
            insert lstIdInsertarOpp;
            insert lstIdInsertarRv;
        }
        catch(Exception e){

            BI_LogHelper.generate_BILog('BI_FVI_DesempenoMethods.recopilaDatosDesempenoParaOppYRevenues', 'BI_O4', e, 'Trigger');
        }
    } 
}