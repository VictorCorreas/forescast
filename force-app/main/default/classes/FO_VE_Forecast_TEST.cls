@isTest
/******************************
@author: Javier Lopez Andradas
@company: gCTIO
@Description: Test class to cover all code related with FO_VE

*******************************/
public class FO_VE_Forecast_TEST {
	/*********************
	@author: JAvier Lopez Andradas
	@company: gCTIO
	@description: Method that load the data fro this test

	<date>		<version>		<description>
	17/10/2018	1.0				Initial
	*************************/
	@testsetup
	private static void createData(){
		List<Account> lst_acc = new List<Account>();

		 Map<Integer,BI_bypass__c> mp_by = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,true,true);
		 Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
  			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
		lst_acc.add(cuenta1);

		Account cuenta2 = new Account(
		    Name = 'CLIENTE_TEST',
		    BI_Country__c = Label.BI_Peru,
		    BI_No_Identificador_fiscal__c = '20538273525',
		    CurrencyIsoCode = 'MXN',
		    BI_Segment__c = 'test',
			BI_Subsegment_Regional__c = 'test',
			BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
		);
        lst_acc.add(cuenta2);
        //Aqui tengo los ids de las cuentas
        DataBase.SaveResult[] arr_svrAcc = DataBAse.insert(lst_acc);
        //Sacamos el RT del nodno superior
        String rt_nd = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByDeveloperName().get('BI_FVI_NCS').getRecordTypeId();
        BI_FVI_Nodos__c ncs_sup = new BI_FVI_Nodos__c(BI_FVI_Activo__c = true);
        ncs_sup.Name='NCS_TEST_SUP_VE';
        ncs_sup.BI_FVI_Overlay__c='Superior';
        ncs_sup.BI_Estruct_Subtipo_de_Nodo__c='Venta Especialista';
        ncs_sup.RecordTypeId=rt_nd;
        insert ncs_sup;
        BI_FVI_Nodos__c ncs_over = new BI_FVI_Nodos__c(BI_FVI_Activo__c = true);
        ncs_over.Name='NCS_TEST_VE_OV';
        ncs_over.BI_FVI_Overlay__c='Overlay';
        ncs_over.BI_Estruct_Subtipo_de_Nodo__c='Venta Especialista';
        ncs_over.BI_FVI_NodoPadre__c=ncs_sup.Id;
        ncs_over.RecordTypeId=rt_nd;
        insert ncs_over;
        List<BI_FVI_Nodo_Cliente__c> lst_ndc = new List<BI_FVI_Nodo_Cliente__c>();
        for(Database.SaveResult sv: arr_svrAcc){
            BI_FVI_Nodo_Cliente__c ndc = new BI_FVI_Nodo_Cliente__c(
            	BI_FVI_IdCliente__c=sv.getId(),
                BI_FVI_IdNodo__c=ncs_over.Id
            );
            lst_ndc.add(ndc);
        }
        insert(lst_ndc);
        List<BI_Periodo__c> lst_per =new List<BI_Periodo__c>();
        Date hoy = Date.today();
        Integer year = hoy.year();
        BI_Periodo__c per_y = new BI_Periodo__c(
        	Name='FY',
            BI_FVI_Activo__c=true,
            FO_Type__c='Año',
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,1,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_y);
        //Hacemos siempre el �ltimo caso del a�o, asi sabemos que siempre va a estar activo
        BI_Periodo__c per_q = new BI_Periodo__C(
        	Name='FYQ4',
            BI_FVI_Activo__c=true,
            
            FO_Type__c='Trimestre',
            FO_Secuencia__c=4,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,10,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_q);
        for(Integer i =10;i<=12;i++){
            Integer day= 31;
            //Novembre
            if(i==11)
                day=30;
            BI_Periodo__c per_M= new BI_Periodo__c(
            	Name='FYQM'+i,
                BI_FVI_Activo__c=true,
                FO_Type__c='Mes',
                FO_Secuencia__c=i-9,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,i,1),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,i, day)
                
            );
            lst_per.add(per_M);
        }
        //AHora van las semanas de diciembre, de 7 en 7 
        for(Integer i=0;i<5;i++){
            BI_Periodo__c per_w = new BI_Periodo__c(
            	Name='FYQM3W'+i,
                FO_Type__c='Semana',
                FO_Secuencia__c=i,
                BI_FVI_Activo__c=true,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,i*7),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, i*7+6)
            );
            lst_per.add(per_w);
        }
        BI_Periodo__c per_w5 = new BI_PEriodo__c(
        	Name='FQM3W5',
            FO_Type__c='Semana',
            BI_FVI_Activo__c=true,
            FO_Secuencia__c=5,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,28),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_w5);
        insert(lst_per); 
        List<BI_Periodo__c> lst_perUpd = [Select Id,FO_Type__c,BI_FVI_Fecha_Inicio__c from BI_PEriodo__c];
        String Idy ='';
        String QId='';
        String MId='';
        //Sacamos los Id de los que ser�n jeraquicamente superiores
        for(BI_Periodo__c per: lst_perUpd){
            if(per.FO_Type__c.equals('Año')){
                Idy=per.Id;
            }else if(per.FO_Type__c.equals('Trimestre')){
                QId=per.Id;
            }else if(per.FO_Type__c.equals('Mes')){
                if(per.BI_FVI_Fecha_Inicio__c.month()==12){
                    MId=per.Id;
                }
            }
        }

        //Tenemos que coger el Tipo de registor de esto
        String rt_obj = Schema.SobjectType.BI_Objetivo_Comercial__c.getRecordTypeInfosByDeveloperName().get('BI_FVI_Objetivos_Nodos').getRecordTypeId();
        String rmv = Schema.SObjectType.BI_Objetivo_Comercial__c.fields.BI_Estruct_Rama_Vertical_Global__c.getPicklistValues().get(0).getValue();
        String roseta1 =Schema.SObjectType.BI_Objetivo_Comercial__c.fields.BI_Estruct_Rosseta1__c.getPicklistValues().get(0).getValue();
        BI_Objetivo_Comercial__c obj = new BI_Objetivo_Comercial__c(
        	BI_FVI_Activo__c=true,
            BI_Tipo__c='Cartera',
            FO2_Monocurrency__c=false,
            BI_OBJ_Campo__c='BI_Net_Annual_Value__c',
            RecordTypeId=rt_obj,
            BI_Estruct_Rama_Vertical_Global__c=rmv,
            BI_Estruct_Rosseta1__c=roseta1,
            BI_Periodo__c=Idy
            );
        insert obj;
        List<BI_FVI_Nodos_Objetivos__c> lst_ndObj = new List<BI_FVI_Nodos_Objetivos__c>();
        //Sacamos el RT de nodos Objetivos
        String rt_ndObj = Schema.SobjectType.BI_FVI_Nodos_Objetivos__c.getRecordTypeInfosByDeveloperName().get('BI_OBJ_Nodo_Objetivos_Nodos').getRecordTypeId();
        for(BI_Periodo__c per : lst_perUpd){
        	if(per.FO_Type__c.equals('Semana')){
        		per.FO_Periodo_Padre__c = MId;
        	}else if(per.FO_Type__c.equals('Mes')){
        		per.FO_Periodo_Padre__c=QId;
        	}else if(per.FO_Type__c.equals('Trimestre')){
        		per.FO_Periodo_Padre__c=Idy;
        	}
        	BI_FVI_Nodos_Objetivos__c nd_c = new BI_FVI_Nodos_Objetivos__c(
        		BI_FVI_Id_Nodo__c=ncs_over.Id,
                BI_FVI_IdPeriodo__c=per.Id,
                BI_FVI_Objetivo__c=300,
                BI_FVI_Objetivo_Comercial__c=obj.Id,
                RecordTypeId=rt_ndObj);
        	BI_FVI_Nodos_Objetivos__c nd_sup = new BI_FVI_Nodos_Objetivos__c(
        		BI_FVI_Id_Nodo__c=ncs_sup.Id,
                BI_FVI_IdPeriodo__c=per.Id,
                BI_FVI_Objetivo__c=300,
                BI_FVI_Objetivo_Comercial__c=obj.Id,
                RecordTypeId=rt_ndObj);
        	lst_ndObj.add(nd_c);
        	lst_ndObj.add(nd_sup);
        }
        update lst_perUpd;
        insert(lst_ndObj);
        NE__Product__c pr =  new NE__Product__c(
        	Name='TEST_FO_VE',
        	Familia_MKTG_Nivel_1__c = roseta1,
        	BI_Rama_Vertical_Global__c=rmv,
        	NE__One_Time_Cost__c=1000);
        insert pr;
        //Vamos a por las oportunidades
        List<Opportunity> lst_opp = new List<Opportunity>();
        Opportunity opp1 = new Opportunity(Name = 'Test',
            CloseDate = Date.newInstance(year, 12, 31),
            BI_Requiere_contrato__c = true,
            Generate_Acuerdo_Marco__c = false,
            AccountId = arr_svrAcc[0].getId(),
            StageName = Label.BI_OpportunityStageF1Ganada,
            BI_Ultrarapida__c = true,
            BI_Duracion_del_contrato_Meses__c = 3,
            BI_Recurrente_bruto_mensual__c = 100,
            BI_Ingreso_por_unica_vez__c = 12,
            BI_Recurrente_bruto_mensual_anterior__c = 1,
            BI_Opportunity_Type__c = 'Producto Standard',
            currencyIsoCode='EUR',
            Amount = 23,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            BI_Country__c = Label.BI_Peru);
        lst_opp.add(opp1);
        Opportunity opptyLocal = new Opportunity(Name = 'Oportunidad Test Local',
            AccountId = arr_svrAcc[0].getId(),
            BI_Country__c = Label.BI_Peru,
            BI_Opportunity_Type__c = 'Digital',
            CloseDate =  Date.newInstance(year, 12, 31),
            StageName = Label.BI_F5DefSolucion,
            BI_Probabilidad_de_exito__c = '25',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            currencyIsoCode='EUR',
            BI_Plazo_estimado_de_provision_dias__c = 5,
            Amount = 5.8);
		Opportunity opptyLocal_Fuera = new Opportunity(Name = 'Oportunidad Test Local_Fuera',
            AccountId = arr_svrAcc[0].getId(),
            BI_Country__c = Label.BI_Peru,
            BI_Opportunity_Type__c = 'Digital',
            CloseDate =  Date.newInstance(year, 12, 11),
            StageName = Label.BI_F5DefSolucion,
            BI_Probabilidad_de_exito__c = '25',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_O4_TGS_NBAV_Budget__c=1000,
            BI_Licitacion__c = 'No', //Manuel Medina
            currencyIsoCode='EUR',
            BI_Plazo_estimado_de_provision_dias__c = 5,
            Amount = 5.8);
        
        lst_opp.add(opptyLocal);
        lst_opp.add(opptyLocal_Fuera);
        
        insert(lst_opp);
        //Ahora las ordenes
        List<NE__Order__c> lst_or = new List<NE__Order__c>();
        for(Opportunity opp: lst_opp){

        	NE__Order__c order = new NE__Order__c(
        		NE__OptyId__c=opp.Id,
        		NE__OrderStatus__c='Active');
        	lst_or.add(order);
        }
        insert(lst_or);
        List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();
        for(NE__Order__c ord:lst_or){
        	NE__OrderItem__c oi = new NE__OrderItem__c(
        		NE__prodId__c=pr.Id,
        		NE__OrderId__c=ord.Id,
        		NE__OneTimeFeeOv__c =10000,
        		NE__Qty__c=1);
        	lst_oi.add(oi);
        }
        insert lst_oi;
        BI_MigrationHelper.disableBypass(mp_by);
	}
	/**************************++
	@author : Javier Lopez Andradas
	@company: gCTIO
	@Description: test general del FOrecast de VE

	<date>		<version>		<description>
	2018/10/18	1.0				Initial
	****************************/
	@isTest
	static void test1(){
		List<BI_FVI_Nodos__c> lst_nd = [Select Id,BI_FVI_Overlay__c from BI_FVI_NOdos__C];
		List<BI_Periodo__c> lst_per= [Select Id,BI_FVI_Fecha_Fin__c,BI_FVI_Fecha_Inicio__c,Name from BI_Periodo__C];
		BI_Periodo__c per;
		for(BI_Periodo__c pers : lst_per){
			if(pers.Name.equals('FQM3W5'))
			per=pers;
		}
		String idECC,idMan;
		for(BI_FVI_Nodos__c nd:lst_nd){
			if(nd.BI_FVI_Overlay__c.equals('Superior')){
				idMan=nd.Id;
			}else{
				idECC=nd.Id;
			}
		}
		//Primero creamos el FO del ejecutivo
		String rt_Ecc = Schema.SObjectType.FO_Forecast__c.getRecordTypeInfosByDeveloperName().get('FO_VE_Overlay').getRecordTypeId();
		String rt_Man = Schema.SObjectType.FO_Forecast__c.getRecordTypeInfosByDeveloperName().get('FO_VE_Overlay_Manager').getRecordTypeId();
		Test.startTest();
		FO_FOrecast__c foECC = new FO_FOrecast__C(
			FO_Nodo__c=idEcc,
			RecordTypeId=rt_Ecc,
			FO_Periodo__C=per.Id);
		insert foECC;
		FO_ActualizaForecast.actualizaFO(foECC.Id);
		List<String> lst_acc = FO_ForecastListControllerApex.getAccs(foECC.FO_Nodo__c);
		FO_ForecastListControllerApex.getCIs(lst_acc,foECC.FO_Nodo__c,String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31','EUR');
		FO_Forecast__c fo_Man= new FO_FOrecast__c(
			FO_Nodo__c=idMan,
			RecordTypeId=rt_Man,
			FO_Periodo__C=per.Id);
		insert fo_Man;
		//Creamos los snapshots del FO_ECC
		List<FO_Snapshot_Pipeline__c> lst_pipe = new List<FO_Snapshot_Pipeline__c>();
		List<FO_Snapshot_Actuals__c> lst_act = new List<FO_Snapshot_Actuals__c>();
		for(BI_Periodo__c pers :lst_per){
			for(Integer i=2;i<6;i++){
				FO_Snapshot_Pipeline__c pipe = new FO_Snapshot_Pipeline__c(
				FO_Forecast_Padre__c=foECC.Id,
				FO_Period__c=pers.Id,
				FO_Value__c=1000,
				FO_Stage__c='F'+i
				);
				lst_pipe.add(pipe);
			}
			FO_Snapshot_Actuals__c act = new FO_Snapshot_Actuals__c(
				FO_Forecast_Padre__c=foECC.Id,
				FO_Periodo__c=pers.Id,
				FO_Value__c=1000
				);
			lst_act.add(act);
			
		}
		insert(lst_pipe);
		insert(lst_act);
		FO_ActualizaForecast.actualizaFO(fo_Man.Id);
		FO_ActualizaForecast.actualizaFO(fo_Man.Id);
		TEst.stopTest();

	}
	/********************************
	@author : Javier Lopez Andradas
	@company: gcTIO
	@description: Coverage the standar o multinationals usin the prechanged

	<date>		<version>		<description>
	2018/10/18	1.0				Initial

	*******************************/
	@isTest
	static void testMNCs(){
		List<BI_FVI_Nodos__C> lst_nd = [Select Id,BI_FVI_Overlay__c from BI_FVI_Nodos__c];
		String ecc,man;
		for(BI_FVI_Nodos__c nd :lst_nd){
			nd.FO_Clasificacion__c='Multinationals';
			if(nd.BI_FVI_Overlay__c.equals('Superior')){
				man = nd.Id;
			}else{
				ecc=nd.Id;
			}
		}
		update lst_nd;
		List<Opportunity> lst_opp = [Select Id from Opportunity];
		lst_opp.get(0).BI_O4_Committed__c=true;
		lst_opp.get(1).BI_o4_Upside__c=true;
		lst_opp.get(2).BI_O4_Committed__c=true;
		update lst_opp;
		BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
		FO_Forecast__c fo_ecc=new FO_FOrecast__c(
			FO_NOdo__c=ecc,
			FO_Periodo__c=per.ID);
		Test.startTest();
		insert fo_ecc;
		FO_ActualizaForecast.actualizaFO(fo_ecc.Id);
		List<FO_Forecast_Item__C> lst_foi =[Select Id,FO_Tipo_Forecast__c from FO_Forecast_Item__c];
		for(FO_Forecast_Item__c foi :lst_foi){
			if(foi.FO_Tipo_Forecast__c.equals('Commit')){
				foi.FO_Tipo_Forecast__c='Upside';
			}else{
				foi.FO_Tipo_Forecast__c='Commit';
			}
		}
		update lst_foi;
		//VAmos al manager
		FO_Forecast__c fo_man=new FO_FOrecast__c(
			FO_NOdo__c=man,
			FO_Periodo__c=per.ID);
		
		insert fo_man;
		FO_ActualizaForecast.actualizaFO(fo_man.Id);
		Test.stopTest();
				
	}

}