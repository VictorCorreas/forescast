/*******************************************
 * Author: Javier Lopez Andradas
 * Company: gCTIO
 * Description:  Class to cober the tes
 * 
 * 
 * 
 * <date>		<version>		<description>		   <author>
 * 10/07/2018	1.0				Intial version		 Javier Lopez Andradas
 * 20/08/2018 	1.1       		Added for version
								2.0 of batch class
 * ******************************************/
@isTest
public class BI_FVI_Estr_ActivaNodo_TEST {
	@testSetup
	public static void load(){
		List<Account> lst_acc =new List<Account>();
		Account acc = new Account(
		Name='TESTET',
		BI_Country__c = Label.BI_Peru,
		BI_No_Identificador_fiscal__c = '20431271525',
		CurrencyIsoCode = 'MXN',
		BI_Segment__c = 'test',
		BI_Subsegment_Regional__c = 'test',
		BI_Territory__c = 'test',
		BI_FVI_Tipo_Planta__c = 'No Cliente');
		lst_acc.add(acc);
		Database.SaveResult[] arr_svr = Database.insert(lst_acc);
		List<Opportunity> lst_opp = new List<Opportunity>();
		Date hoy = Date.today();
		Integer year = hoy.year();
		Opportunity opp1 = new Opportunity(Name = 'Test',
			CloseDate = Date.newInstance(year, 12, 31),
			BI_Requiere_contrato__c = true,
			Generate_Acuerdo_Marco__c = false,
			AccountId = arr_svr[0].getId(),
			StageName = Label.BI_OpportunityStageF1Ganada,
			BI_Ultrarapida__c = true,
			BI_Duracion_del_contrato_Meses__c = 3,
			BI_Recurrente_bruto_mensual__c = 100,
			BI_Ingreso_por_unica_vez__c = 12,
			BI_Recurrente_bruto_mensual_anterior__c = 1,
			BI_Opportunity_Type__c = 'Producto Standard',
			currencyIsoCode='EUR',
			Amount = 23,
			BI_O4_TGS_NBAV_Budget__c=1000,
			BI_Licitacion__c = 'No', //Manuel Medina
			BI_Country__c = Label.BI_Peru);
		lst_opp.add(opp1);
		insert(opp1);
		Case cas = new Case(
		AccountId = arr_svr[0].getId(),
		Status='New',
		Type='Change',
		Subject = 'Caso test2');
		insert cas;
		Event evt = new Event();
		
		//evt.AccountId=arr_svr[0].getId();
		evt.ActivityDate=Date.today().addDays(10);
		evt.Subject ='RANDOM12·DASDAWADS';
		evt.startDateTime = Datetime.now();
		evt.EndDateTime = Datetime.now().addDays(10);
		evt.WhatId=arr_svr[0].getId();
		insert evt;
		Task tsk =  new Task();
		//tsk.AccountId = arr_svr[0].getId();
		tsk.Subject = 'RANDOM !"·!K';
		tsk.ActivityDate=Date.today();
		tsk.WhatId= arr_svr[0].getId();
		insert tsk;
		Contact cont = new Contact();
		cont.FirstNAme='TESTETE';
		cont.LastNAme='TESTETE';
		cont.Email='TEST@TEST.com';
		cont.AccountId=arr_svr[0].getId();
		insert cont;
		Profile prof = [Select Id from Profile where Name ='BI_Standard_PER' limit 1];
		User user = new User(alias =   'node123',
						  email =  'node1231231asd1234123asd@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'node1231231asd1234123asdTest',
						  languagelocalekey = 'en_US',
						  localesidkey = 'en_US',
						  ProfileId = prof.Id,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'node1231231asd1234123asd@testorg.com',
						  Pais__c = Label.BI_Argentina,
						  BI_DNI_Per__c = 'AAAAAA');
		
		System.runas(new User(Id=UserInfo.getUserId()))
		{
			insert user;
		}
	}
	/*****************************************
	 * Author : Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: Cover all related to NCP process
	 * <date>		<version>		<description>
	 * 10/07/2018	1.0				Initial
	 * 02/08/2018	1.1				Changed for new PAram in batch
	 *30/10/2018	1.2				Added test to createBuidlEmail
	 * ***************************************/
	@isTest
	public static void testNCP(){
		BI_FVI_Nodos__c node = new BI_FVI_Nodos__c();
		String rt = [Select Id from RecordType where SObjectType='BI_FVI_Nodos__c' AND DeveloperName='BI_FVI_NCP' limit 1].Id;
		String owner = [Select Id from User where username='node1231231asd1234123asd@testorg.com' limit 1].Id;
		node.OwnerId=owner;
		node.BI_FVI_Pais__c='Peru';
		node.RecordTypeId=rt;
		Database.SaveResult svr = Database.insert(node);
		String IdCuenta = [Select Id from Account limit 1 ].Id;
		BI_FVI_Nodo_Cliente__c ndc = new BI_FVI_Nodo_Cliente__c();
		ndc.BI_FVI_IdNodo__c=svr.getId();
		ndc.BI_FVI_IdCliente__c=IdCuenta;
		insert ndc;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Opportunity','Opportunity-','TEST') ;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Case','Case-','TEST') ;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Account','Account-','TEST') ;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Contact','Contact','TEST') ;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Task','Task','TEST') ;
		BI_FVI_Estr_ActivaNodo.changeClosedOpps(svr.getId(),owner,'Select Id from Event','Event','TEST') ;
		
		BI_FVI_Estr_ActivaNodo.retrieveActualOwners(svr.getId());
		BI_FVI_Estr_ActivaNodo.getVDN(svr.getId());
        BI_FVI_Estr_ActivaNodo.activacionNueva(svr.getId());
		//Para la cuenta que vamos a mover 
		List<String> lst_acc= new List<String>();
		lst_acc.add(IdCuenta);
		Map<String,List<String>> mp = new Map<String,List<String>>();
		mp.put('ERROR CATASTROFICO',lst_acc);
		List<String> fields = new List<String>();
		fields.add('Name');
		
		BI_Estruct_BuildEmail.createBody(mp,'Account',fields);

	   
	}
	 /*****************************************
	 * Author : Javier Lopez Andradas
	 * Company: gCTIO
	 * Description: Cover all related to NCS process
	 * <date>		<version>		<description>
	 * 10/07/2018	1.0				Initial
	 * 
	 * ***************************************/
	@isTest
	public static void testNCS(){
		BI_FVI_Nodos__c node = new BI_FVI_Nodos__c();
		String rt = [Select Id from RecordType where SObjectType='BI_FVI_Nodos__c' AND DeveloperName='BI_FVI_NCS' limit 1].Id;
		String owner = [Select Id from User where username='node1231231asd1234123asd@testorg.com' limit 1].Id;
		node.OwnerId=owner;
		node.BI_FVI_Pais__c='Peru';
		node.RecordTypeId=rt;
		Database.SaveResult svr = Database.insert(node);
		String IdCuenta = [Select Id from Account limit 1 ].Id;
		BI_FVI_Nodo_Cliente__c ndc = new BI_FVI_Nodo_Cliente__c();
		ndc.BI_FVI_IdNodo__c=svr.getId();
		ndc.BI_FVI_IdCliente__c=IdCuenta;
		insert ndc;
		BI_FVI_Estr_ActivaNodo.getOppAcc();
		BI_FVI_Estr_ActivaNodo.getCasAcc();
		BI_FVI_Estr_ActivaNodo.getAccAcc();
		BI_FVI_Estr_ActivaNodo.getRoles();
		BI_FVI_Estr_ActivaNodo.activateNode(svr.getId(), owner, 'Edit', 'Edit', 'Edit', 'GAM');
		BI_FVI_Estr_ActivaNodo.deactivateNode(svr.getId(), owner, rt, 'GAM');
		
	}
}