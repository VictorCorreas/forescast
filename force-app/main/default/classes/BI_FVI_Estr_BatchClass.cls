global class BI_FVI_Estr_BatchClass implements Database.Batchable<sObject>,Database.Stateful{
	global final String Query;
	global final String objeto;
	global final String owner;
	global Map<String,List<String>> lst_error;
	global final String nodo;
	global final String nameNodo;
	global final Map<String,String> mp_accTeam;
    global final List<String> propietariosDeCuentas;
    global final List<String> idCuentas;
    global final Boolean isTransfer;
    global List<String> idCuentasExito;
	/*************************
	@author: JAvier Lopez Andradas
	@company:gCTIO
	@description: Initialize the batch

	<date>		<version>		<description>
	??????		1.0				Initial	
	19/11/2018	2.0				Added map to the main object
	17/07/2019  2.1             Agregado nuevo constructor para manejar el envío de mails de acuerdo a si es una transferencia de clientes o una activación de nodo
	****************************/
	public BI_FVI_Estr_BatchClass(String query,String objeto,String owner,String nodo,String nameNodo,Map<String,String> mp_accTeam){
		this.Query=query;
		this.objeto=objeto;
		this.owner=owner;
		lst_error = new Map<String,List<String>>();
		this.nodo=nodo;
		this.nameNodo=nameNodo;
		this.mp_accTeam=mp_accTeam;
		
	}
    
    public BI_FVI_Estr_BatchClass(String query,String objeto,String owner,String nodo,String nameNodo,Map<String,String> mp_accTeam, List<String> propietariosDeCuentas, List<String> idCuentas){
		this.Query=query;
		this.objeto=objeto;
		this.owner=owner;
		lst_error = new Map<String,List<String>>();
		this.nodo=nodo;
		this.nameNodo=nameNodo;
		this.mp_accTeam=mp_accTeam;
        this.propietariosDeCuentas = propietariosDeCuentas;
        this.idCuentas = idCuentas;
		
	}
    
    public BI_FVI_Estr_BatchClass(String query,String objeto,String owner,String nodo,String nameNodo,Map<String,String> mp_accTeam, List<String> propietariosDeCuentas, List<String> idCuentas, Boolean isTransfer){
		this.Query=query;
		this.objeto=objeto;
		this.owner=owner;
		lst_error = new Map<String,List<String>>();
		this.nodo=nodo;
		this.nameNodo=nameNodo;
		this.mp_accTeam=mp_accTeam;
        this.propietariosDeCuentas = propietariosDeCuentas;
        this.idCuentas = idCuentas;
        this.isTransfer = isTransfer;
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}
	/*******************************************
	 * Author: Javier Lopez Andradas
	 * Comapny gCTIO
	 * Description: Main execution of the batch
	 * 
	 * <date>		<version>		<description>                                       <author>
	 * ?????		1.0				Initial                                             Javier Lopez Andradas
	 * 07/08/2018	2.0				New way of process the error in the batch           Javier Lopez Andradas
	 * 26/10/2018   2.1				Added false to update all records that it can       Javier Lopez Andradas
	 * 19/11/2018	2.2				Added method to AccountteamMembers                  Javier Lopez Andradas
	 * 01/04/2019	3.0				Agregada la ejecución del método updateEstadoVDN    Pablo Belzunce
	 * 08/05/2019   2.3				OAJF - Fix null pointer line 137 - arr_svr - when is updating task, Event and Note
	 * *****************************************/
	global void execute(Database.BatchableContext BC,List<sObject> scope){
		Database.SaveResult [] arr_svr = new List<Database.SaveResult>();
		BI_FVI_Nodos__c nd = new BI_FVI_NOdos__c(Id=nodo);
		try{
			if(objeto.startsWith('Opportunity-')){
				List<Opportunity> lst_upd = new List<Opportunity>();
				for(sObject obj :scope){
					Opportunity opp = (Opportunity) obj;
					opp.OwnerId = owner;
					lst_upd.add(opp);
					nd.BI_Estruct_Estado_sincronizacion_opp__c='SINCRONIZANDO';			  
				}
				arr_svr =Database.update(lst_upd,false);	
				System.debug('JLA___BATCH Opp'+arr_svr);
			}else if(objeto.startsWith('Case-')){
				List<Case> lst_cas = new List<Case>();
				for(sObject obj :scope){
					Case cas = (Case) obj;
					cas.OwnerId = owner;
					lst_cas.add(cas);
					nd.BI_Estruct_Estado_sincronizacion_casos__c='SINCRONIZANDO';	
				}
				arr_svr = Database.update(lst_cas,false);	
				System.debug('JLA___BATCH Cas'+arr_svr);
			}else if(objeto.startsWith('Account-')){
				List<Account> lst_acc = new List<Account>();
				for(SObject obj :scope){
					Account acc = (Account) obj;
					acc.ownerId = owner;
					acc.BI_FVI_Nodo__c=nodo;
					lst_acc.add(acc);
					nd.BI_Estruct_Estado_sincronizacion_cuentas__c='SINCRONIZANDO';
				}
				arr_svr =Database.update(lst_acc,false);
			}else if(objeto.equals('Contact')==true){
				List<Contact> lst_upd = new List<Contact> ();
				for(SObject obj : scope){
					Contact cont = (Contact) obj;
					cont.OwnerId=owner;
					lst_upd.add(cont);
				}
				arr_svr =Database.update(lst_upd,false);
			}else if(objeto.equals('Task')==true){
				List<Task> lst_upd = new List<Task> ();
				for(SObject obj : scope){
					Task cont = (Task) obj;
					cont.OwnerId=owner;
					lst_upd.add(cont);
				}
				arr_svr =Database.update(lst_upd,false);
			}else if(objeto.equals('Event')==true){
				List<Event> lst_upd = new List<Event> ();
				for(SObject obj : scope){
					Event cont = (Event) obj;
					cont.OwnerId=owner;
					lst_upd.add(cont);
				}
				arr_svr =Database.update(lst_upd,false);
			}else if(objeto.equals('Note')==true){
				List<Note> lst_upd = new List<Note> ();
				for(SObject obj : scope){
					Note cont = (Note) obj;
					cont.OwnerId=owner;
					lst_upd.add(cont);
				}
				arr_svr =Database.update(lst_upd,false);
			//JLA_2.2
			}else if(objeto.equals('AccountTeam')){
				List<AccountTeamMember> lst_accTe = new List<AccountTeamMember>();
				List<String> lst_acc = new List<String>();
				for(SObject obj:scope){
					BI_FVI_Nodo_Cliente__c ndc = (BI_FVI_Nodo_Cliente__c) obj;
					AccountTeamMember acct = new AccountTeamMember();
					acct.AccountId = ndc.BI_FVI_IdCliente__c;
					acct.UserId=owner;
					acct.TeamMemberRole=mp_accTeam.get('Role');
					lst_accTe.add(acct);
					lst_acc.add(ndc.BI_FVI_idCliente__c);
				}
				arr_svr = Database.insert(lst_accTe,false);
				List<AccountShare> lst_accSh  = [select Id from AccountShare where AccountId in :lst_acc And userorgroupid=:owner ANd RowCause ='Team' ANd LastModifiedByid=:UserInfo.getUserId()];
				for(AccountShare accSh : lst_accSh){
					accSh.OpportunityAccessLevel=mp_accTeam.get('AccOpp');
					if(mp_accTeam.get('AccAcc').equals('All')==false)
						accSh.AccountAccessLevel=mp_accTeam.get('AccAcc');
					else
						accSh.AccountAccessLevel='Read';
					accSh.CaseAccessLevel=mp_accTeam.get('AccCase');
					//accSh.ContactAccessLevel=AccCont;
				}
				arr_svr.addAll(Database.update(lst_accSh,false));
				nd.BI_Estruct_Estado_sincronizacion_cuentas__c='SINCRONIZANDO';
			}
            
            /*svr.getId() solo devuelve el Id si svr.isSuccess == true, de lo contrario devuelve null
			for(Database.saveResult svr : arr_svr){
				if(svr.isSuccess()==false){
                    //Cambio para poder agarrar todos los errores del set
					//addErr(svr.getErrors()[0].getMessage(),svr.getId());
					Database.Error [] arr_err;
                    arr_err = svr.getErrors();
                    for(Database.Error err : arr_err){
                        addErr(err.getMessage(),svr.getId());
                    }
				}
			}*/
            /*
            for(sObject obj : scope){
                Integer tam = scope.size();
                Integer counter = 0;
                for(Database.saveresult svr : arr_svr){
                    if(obj.Id == svr.getId()){
                        break;
                    }else{
                        counter ++;
                        if(counter == tam){
                            Database.Error [] arr_err;
                            arr_err = svr.getErrors();
                            for(Database.Error err : arr_err){
                                addErr(err.getMessage(),obj.Id);
                            }
                        }
                    }
                }
            }
            */
            
            for (Integer i = 0; i < scope.size(); i++) {
                Database.SaveResult svr = arr_svr[i];
                SObject origRecord = scope[i];
                if (!svr.isSuccess()) {
                    Database.Error [] arr_err;
                    arr_err = svr.getErrors();
                    for(Database.Error err : arr_err){
                        addErr(err.getMessage(),origRecord.Id);
                    }
                }else{
                    //Esto es para mandar el nuevo mail con los clientes que se actualizaron correctamente
                    if(objeto.startsWith('Account-')){
                        if(idCuentasExito == null){
                            idCuentasExito = new List<String>();
                        }
                        idCuentasExito.add(svr.getId());
                    }
                } 
            }
			
			update(nd);
            
            updateEstadoVDN(arr_svr, nd, scope, false);

		}catch(Exception e){
		    
		    updateEstadoVDN(arr_svr, nd, scope, true);
		    
			for(sObject obj: scope)
				addErr(e.getMessage() + ' : '+e.getStackTraceString(),obj.Id);
		}
		
	}
	global void finish(Database.BatchableContext BC){
        try{
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                TotalJobItems, CreatedById
                from AsyncApexJob where Id =:BC.getJobId()];
            if(lst_error.isEmpty()==false){
                senEmail(a.CreatedById);
                if(objeto.startsWith('Account-')){
                    emailTraspasoClientes(idCuentasExito, a.CreatedById);
                }
            }else{
                Messaging.SingleEmailMessage single = new Messaging.SingleEmailMessage();
                single.setTargetObjectId(a.CreatedById);
                single.setSubject('Actualizacion de '+objeto+' para el nodo '+nameNodo);
                single.setSaveAsActivity(false);
                if(isTransfer == null || !isTransfer || objeto.startsWith('Account-')){
                    single.setToAddresses(new String[]{owner});
                }
                //single.setToAddresses(new String[]{owner});
                BI_FVI_Nodos__c nd = new BI_FVI_Nodos__c(Id=nodo);
                if(objeto.startsWith('Case-')){
                    nd.BI_Estruct_Estado_sincronizacion_casos__c='FINALIZADO';
                }else if(objeto.startsWith('Opportunity-')){
                    nd.BI_Estruct_Estado_sincronizacion_opp__c='FINALIZADO';
                }else if(objeto.startsWith('Account-')){//Se agrega el if para que el estado de account no se pise con los estados de tasks, events, etc.
                    nd.BI_Estruct_Estado_sincronizacion_cuentas__c='FINALIZADO';
                }else if(objeto.startsWith('AccountTeam')){
                    //Si es AccountTeam y es NCS entonces seteo el estado de las cuentas con su valor correspondiente
                    BI_FVI_Nodos__c objNodo = [select id, RecordType.DeveloperName from BI_FVI_Nodos__c where id = :nodo limit 1];
                    if(objNodo.RecordType.DeveloperName == 'BI_FVI_NCS'){
                        nd.BI_Estruct_Estado_sincronizacion_cuentas__c='FINALIZADO';
                    }
                }
                single.setPlainTextBody('Se han actualizado todos los registros');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ single });
                
                if(objeto.startsWith('Account-')){
                    emailTraspasoClientes(idCuentasExito, a.CreatedById);
                }
                
                update(nd);
                
                BI_LogHelper.generateLog('Actualizacion de '+objeto+' para el nodo '+nameNodo,'Movimiento NCP','Todo Ok', '');
            }
        }catch(Exception e){
			addErr(e.getMessage() + ' : '+e.getStackTraceString(),'Todos los ids');
            String error='';
			error+='Se han producido los siguientes errores: \n';
            BI_LogHelper.generateLog('Catch Actualizacion de '+objeto+' para el nodo '+nameNodo,'Movimiento NCP',error+':::'+lst_error,'');
		}
		
	}
	/***********************************
	@author : Javier Lopez Andradas
	@company gcTIO
	@description: 


	*************************************/
	public void addErr (String err, String id){
		List<String> aux = lst_error.get(err);
		if(aux==null)
			aux = new list<String>();
		aux.add(id);
		lst_error.put(err,aux);
	}
	/**********************************
	@author : Javier Lopez Andradas
	@company: gCTIO
	@description: mandar el mail

	<date>		<version>		<description>
	26/10/2018	1.0				Iniitla			

	*************************************/
	public void senEmail(String mesId){
		Messaging.SingleEmailMessage single = new Messaging.SingleEmailMessage();
	  	single.setTargetObjectId(mesId);
		single.setSubject('Actualizacion de '+objeto+' para el nodo '+nameNodo);
		single.setSaveAsActivity(false);
        if(isTransfer == null || !isTransfer || objeto.startsWith('Account-')){
            single.setToAddresses(new String[]{owner});
        }
		//single.setToAddresses(new String[]{owner});
		BI_FVI_Nodos__c nd = new BI_FVI_Nodos__c(Id=nodo);

		if(objeto.startsWith('Case-')){
			nd.BI_Estruct_Estado_sincronizacion_casos__c='ERROR';
			//List<Case> lst_cas = [Select Id,CaseNumber from Case where Id in:lst_error];
			List<String> lst_fields= new List<String>{'CaseNumber'};
			String bodyHtml = BI_Estruct_BuildEmail.createBody(lst_error,objeto,lst_fields);
			single.setHTMLBody(bodyHtml);
		}else if(objeto.startsWith('Opportunity-')){
			nd.BI_Estruct_Estado_sincronizacion_opp__c='ERROR';
			//List<String> lst_fields= new List<String>{'BI_Id_de_la_oportunidad__c'};
			List<String> lst_fields= new List<String>{'BI_Id_Interno_de_la_Oportunidad__c'};
			String bodyHtml = BI_Estruct_BuildEmail.createBody(lst_error,objeto,lst_fields);
			single.setHTMLBody(bodyHtml);
		}else if(objeto.startsWith('Account-')){
			nd.BI_Estruct_Estado_sincronizacion_cuentas__c='ERROR';
			List<String> lst_fields= new List<String>{'Name'};
			String bodyHtml = BI_Estruct_BuildEmail.createBody(lst_error,objeto,lst_fields);
			single.setHTMLBody(bodyHtml);
		}else if(objeto.startsWith('Contact')){
			List<String> lst_fields= new List<String>{'Name'};
			String bodyHtml = BI_Estruct_BuildEmail.createBody(lst_error,objeto,lst_fields);
			single.setHTMLBody(bodyHtml);
		}else if(objeto.startsWith('AccountTeam')){
            //Comentado ya que no tiene sentido guardar el estado de Error del objeto AccountTeam en el estado de Oportunidades
			//nd.BI_Estruct_Estado_sincronizacion_opp__c='ERROR';
			//Si es AccountTeam y es NCS entonces seteo el estado de las cuentas con su valor correspondiente
			BI_FVI_Nodos__c objNodo = [select id, RecordType.DeveloperName from BI_FVI_Nodos__c where id = :nodo limit 1];
            if(objNodo.RecordType.DeveloperName == 'BI_FVI_NCS'){
                nd.BI_Estruct_Estado_sincronizacion_cuentas__c='ERROR';
            }
		}
		String error='';
		error+='Se han producido los siguientes errores: \n';
		single.setPlainTextBody(error);
		update(nd);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ single });
		BI_LogHelper.generateLog('Actualizacion de '+objeto+' para el nodo '+nameNodo,'Movimiento NCP',error+':::'+lst_error,'');
	}
	
	    /*-------------------------------------------------------------------------------
Autor:        Pablo Belzunce
Compañía:     Certa
Descripción:  Método para setear el estado del vinculo de nodo en Ok / Error según corresponda

Historial:

<Date>                  <Author>                    <Change Description>
01/04/2019              Pablo Belzunce                Versión Inicial    
-------------------------------------------------------------------------------*/
	
	public void updateEstadoVDN (List<Database.SaveResult> arr_svr, BI_FVI_Nodos__c nd, List<sObject> scope, Boolean errorGlobal){
        //Creo un array que contendrá los ids de los registros exitosos y null para aquellos que fueron fallidos, ya que si isSuccess es false, getId() será null
        List<String> arr_ids_svr = new List<String>();
        for(Database.saveResult svr : arr_svr){
            if(svr.getId() != null){
                String idSucc = svr.getId(); //Agregado ya que a la hora de comparar los datos con el .contains() traia problemas con tipo Id
            	arr_ids_svr.add(idSucc);
            }
        }
        //Obtengo los ids de los registros con error y exitosos
        List<String> objectErrorList = new List<String>();
        List<String> objectSuccessList = new List<String>();
        if(!errorGlobal){
            for(sObject obj : scope){
                String idObj = obj.Id; //Agregado ya que a la hora de comparar los datos con el .contains() traia problemas con tipo Id
                if(arr_ids_svr.contains(idObj)){
                    objectSuccessList.add(idObj);
                }else{
                    objectErrorList.add(idObj);
                }
            } 
        }else{
            for(sObject obj : scope){
                objectErrorList.add(obj.Id);
            }
        }
        
        Set<String> accErrorList = new Set<String>();
        Set<String> accSuccessList = new Set<String>();
        List<BI_FVI_Nodo_Cliente__c> vdnErrorList = new List<BI_FVI_Nodo_Cliente__c>();
        List<BI_FVI_Nodo_Cliente__c> vdnSuccessList = new List<BI_FVI_Nodo_Cliente__c>();
        
        List<String> objectList = new List<String>();
        objectList.addAll(objectErrorList);
        objectList.addAll(objectSuccessList);
        
        //Obtengo la lista de ids de cuentas según objeto, para luego conseguir los vdn (voy a tener que hacer un if para saber de qué tipo de objeto es mi lista de ids).
        //Opportunities
        if(objeto.startsWith('Opportunity-')){
            List<Opportunity> oppList = [Select Id, AccountId From Opportunity Where Id in : objectList];
            for(Opportunity opp : oppList){
                String idRegistro = opp.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(opp.AccountId);
                }else{
                    accSuccessList.add(opp.AccountId);
                }
            }
        }
        //Cases
        if(objeto.startsWith('Case-')){
            List<Case> caseList = [Select Id, AccountId From Case Where Id in : objectList];
            for(Case cas : caseList){
                String idRegistro = cas.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(cas.AccountId);
                }else{
                    accSuccessList.add(cas.AccountId);
                }
            }
        }
        //Account (ya tengo directamente los ids de las cuentas)
        if(objeto.startsWith('Account-')){
            accErrorList.addAll(objectErrorList);
            
            accSuccessList.addAll(objectSuccessList);
        }
        //Contact
        if(objeto.startsWith('Contact')){
            List<Contact> contactList = [Select Id, AccountId From Contact Where Id in : objectList];
            for(Contact cont : contactList){
                String idRegistro = cont.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(cont.AccountId);
                }else{
                    accSuccessList.add(cont.AccountId);
                }
            }
        }
        //Task
        if(objeto.startsWith('Task')){
            List<Task> taskList = [Select Id, AccountId From Task Where Id in : objectList];
            for(Task tas : taskList){
                String idRegistro = tas.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(tas.AccountId);
                }else{
                    accSuccessList.add(tas.AccountId);
                }
            }
        }
        //Event
        if(objeto.startsWith('Event')){
            List<Event> eventList = [Select Id, AccountId From Event Where Id in : objectList];
            for(Event eve : eventList){
                String idRegistro = eve.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(eve.AccountId);
                }else{
                    accSuccessList.add(eve.AccountId);
                }
            }
        }
        //Note
        if(objeto.startsWith('Note')){
            List<Note> noteList = [Select Id, ParentId From Note Where Id in : objectList];
            for(Note n : noteList){
                String idRegistro = n.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(n.ParentId);
                }else{
                    accSuccessList.add(n.ParentId);
                }
            }
        }
        //AccountTeam
        if(objeto.equals('AccountTeam')){
            List<AccountTeamMember> accTeamList = [Select Id, AccountId From AccountTeamMember Where Id in : objectList];
            for(AccountTeamMember accTeam : accTeamList){
                String idRegistro = accTeam.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(accTeam.AccountId);
                }else{
                    accSuccessList.add(accTeam.AccountId);
                }
            }
            
            List<AccountShare> accShareList = [Select Id, AccountId From AccountShare Where Id in : objectList];
            for(AccountShare accShare : accShareList){
                String idRegistro = accShare.Id;
                if(objectErrorList.contains(idRegistro)){
                    accErrorList.add(accShare.AccountId);
                }else{
                    accSuccessList.add(accShare.AccountId);
                }
            }
        }
        
        //Obtengo los vinculo de nodo asociados a las listas de cuentas obtenidas a partir de los ids del objeto dado
        vdnErrorList = [Select BI_Estruct_Estado__c From BI_FVI_Nodo_Cliente__c Where BI_FVI_IdCliente__c in : accErrorList AND BI_FVI_IdNodo__c =: nd.Id];
        for(BI_FVI_Nodo_Cliente__c vdnError : vdnErrorList){
            vdnError.BI_Estruct_Estado__c = 'Error';
        }
        update vdnErrorList;
        
        
        vdnSuccessList = [Select BI_Estruct_Estado__c From BI_FVI_Nodo_Cliente__c Where BI_FVI_IdCliente__c in : accSuccessList AND BI_FVI_IdNodo__c =: nd.Id];
        for(BI_FVI_Nodo_Cliente__c vdnSuccess : vdnSuccessList){
            if(vdnSuccess.BI_Estruct_Estado__c != 'Error') vdnSuccess.BI_Estruct_Estado__c = 'Ok';
        }
        update vdnSuccessList;
        
	}
    
    /**********************************
	@author : Pablo Belzunce
	@company: Certa
	@description: Envío de mail de clientes transferidos con éxito

	<date>		<version>		<description>
	22/07/2019	1.0				Inicial			

	*************************************/
    
    public void emailTraspasoClientes (List<String> lstCuentasIds, String currentUser){
        
        Messaging.SingleEmailMessage single = new Messaging.SingleEmailMessage();
        single.setTargetObjectId(currentUser); //Sería el user que ejecuta
        single.setSubject('Transferencia de clientes para el nodo ' + nameNodo);
        single.setSaveAsActivity(false);
        single.setToAddresses(new String[]{owner}); //Sería el EECC, es decir el owner del nodo
        String mensaje='';
        
        if(idCuentasExito == null){//Ninguna cuenta se transfirió con éxito
            
            mensaje+='No se han podido transferir clientes en la gestión: \n';
            
        }else{
            
            List<Account> lst_clientes = [Select Id, Name From Account Where Id in : lstCuentasIds];
            
            String bodyHtml = BI_Estruct_BuildEmail.buildTransferEmail(lst_clientes);
            single.setHTMLBody(bodyHtml);
            mensaje+='Se han transferido los siguientes clientes: \n';
            
        }
        
        single.setPlainTextBody(mensaje);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ single });
        
    }
	
}