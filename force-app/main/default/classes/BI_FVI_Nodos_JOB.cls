/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García
    Company:       NEAborda
    Description:   Clase programada el primero de cada mes para mover a los clientes a los nodos de barbecho en función de las visitas

    History: 

    <Date>                  <Author>                <Change Description>
    21/04/2016              Antonio Masferrer       Initial Version     
    04/04/2017              Humberto Nunes          Se agrego el parametro null (ya que ahora se ejecuta por paises)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class BI_FVI_Nodos_JOB implements Schedulable {

    global void execute(SchedulableContext sc) {
        BI_FVI_NodosMethods.asignaNodosBarbechoSchedule_Countries(null);
    }
}