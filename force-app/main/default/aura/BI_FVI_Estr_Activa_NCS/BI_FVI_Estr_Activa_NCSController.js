({
    loadedRecord : function(cmp,evt,help){
        debugger;
        var changeType = evt.getParams().changeType;
        if(changeType === "LOADED" || changeType === "CHANGED"){
            var isActive  =cmp.get("v.simpleRecord.BI_FVI_Activo__c");
	        if(isActive==true){
    	 	    var resultsToast = $A.get("e.force:showToast");
			    resultsToast.setParams({
    	        	"title": $A.get("$Label.c.BI_FVI_Estr_Warn"),
    	    	  	"message": $A.get("$Label.c.BI_FVI_Estr_Already_Active"),
        	        "type":"warning",
    	            "mode":"sticky"
	        	});
                if(changeType === "LOADED")
            		resultsToast.fire();
            	cmp.find("Submit-but").set("v.label",$A.get("$Label.c.BI_FVI_Estr_Deactivate"));
                
            }else{
                cmp.find("Submit-but").set("v.label",$A.get("$Label.c.BI_FVI_Estr_Activate"));
               
            }
        
        $A.util.removeClass(cmp.find("Submit-but"),"slds-hide"); 
        }else if(changeType==="ERROR"){
            var resultsToast = $A.get("e.force:showToast");
			    resultsToast.setParams({
    	        	"title":"ERROR",
    	    	  	"message": evt.getParams(),
        	        "type":"warning",
    	            "mode":"sticky"
	        	});
            	resultsToast.fire();
        }
    }
})