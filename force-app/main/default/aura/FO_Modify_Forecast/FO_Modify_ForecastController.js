({
	doInit : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        if(recordId==null){
            $A.util.addClass(cmp.find("main-modal"),"slds-hide");
             $A.util.removeClass(cmp.find("error-modal"),"slds-hide");
            
        }
            var valueSelect = cmp.get("v.simpleRecord.FO_Estado__c");
			var action = cmp.get("c.getEstado");
        	action.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                var result = response.getReturnValue();
                var lst =[];
                for(var j = 0;j<result.length;j++){
                    var obj = new Object();
                    obj.label=result[j].label;
                    obj.value=result[j].value;
                    lst.push(obj);
                }
                cmp.set("v.lst_tipo",lst);
            }
        });
        $A.enqueueAction(action);
        var actionPa = cmp.get("c.getParent");
        actionPa.setParams({"recId":cmp.get("v.recordId")});
        actionPa.setCallback(this,function(response){
            cmp.set("v.parent",response.getReturnValue());
        });
        $A.enqueueAction(actionPa);
        
       
        
	},
    save : function(cmp,evt,help){
        var idpa = cmp.get("v.parent");
        if(idpa==null){
            $A.util.addClass(cmp.find("main-modal"),"slds-hide");
            $A.util.removeClass(cmp.find("error-modal"),"slds-hide");
        }else{
            var Id = cmp.get("v.recordId");
	        var tipo = cmp.find("estado").get("v.value");
    	    if(tipo=="NO"){
        	    
        	}
        	else if (tipo=="Enviado"){
            	alert($A.get("$Label.c.FO_err_Subimt_sub"));
        	}else{
	            var action = cmp.get("c.updateFO");
    	    	action.setParams({"recId":Id,"tipo":"Borrador"});
	    	    action.setCallback(this, function(response){
    	    	    var result = response.getReturnValue();
        	    	if(result=="Ok"){
            	    	var navEvt = $A.get("e.force:navigateToSObject");
	        			navEvt.setParams({
    	        			"recordId": cmp.get("v.parent")
	    	    		});	
    	    		navEvt.fire();
        	    	}else{
            	    	alert(result);
            		}
		        });
    		    $A.enqueueAction(action);
        	}
        }
    },
    cancel :function(cmp,evt,help){
        if(cmp.get("v.parent")==null){
             window.history.back();
        }else{
             var navEvt = $A.get("e.force:navigateToSObject");
        	navEvt.setParams({
            	"recordId": cmp.get("v.parent")
        	});
        	navEvt.fire();
        }
       
    },
    cancelBack :function(cmp,evt,help){
        window.history.back();
    }
})