({
	openFutureOppsWindow : function(component, event, helper) {
		 window.open($A.get("$Label.c.FO_Redirect_Opp_30"));
	},
    doInit : function(cmp,evt,help){
        var action =cmp.get("c.returnOppsAtr");
        action.setParams({"atras":false});
        action.setCallback(this,function(response){
            cmp.set("v.name"," ("+response.getReturnValue()+")");
            
        });
        $A.enqueueAction(action);
    }
})