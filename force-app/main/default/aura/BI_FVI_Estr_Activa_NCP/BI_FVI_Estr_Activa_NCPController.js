({
	
    goSure : function(cmp, evt, hel) {
       	$A.util.removeClass(cmp.find("spinnerCharged"),"slds-hide");
        var resultsToast = $A.get("e.force:showToast");
        cmp.find("recordLoader").saveRecord($A.getCallback(function(saveResult){
            debugger;
            console.log(saveResult.state);
            if(saveResult.state==="SUCCESS" ||saveResult.state==="DRAFT" ){
                resultsToast.setParams({
    		       	"title": $A.get("$Label.c.TGS_CWP_SUCC"),
					"message":$A.get("$Label.c.BI_Estruct_record_Upd"),
    	        	"type":"success"
        		});
            }else{
                resultsToast.setParams({
    		       	"title": "ERROR",
                    "message":JSON.stringify(saveResult.error),
    	        	"type":"error"
        		});
            }
            resultsToast.fire();
            $A.util.addClass(cmp.find("spinnerCharged"),"slds-hide");
        }));
        
       
	},
    handleRecordUpdated : function(cmp,evt,hel){
        var eventParams = evt.getParams();
        if(eventParams.changeType==="LOADED"){
            $A.util.removeClass(cmp.find("grid-id"),"slds-hide");
        }
    },
  /* DEprecased  closeModel : function(cmp, evt, hel) {
        $A.util.removeClass(cmp.find("modal-aura"),"slds-fade-in-open");
        $A.util.addClass(cmp.find("backGroundSectionId"),"slds-hide");
        cmp.set("v.modalStyle","");
	},
    submit : function(cmp, evt, hel) {
        debugger;
        $A.util.addClass(cmp.find("div-checks"),"slds-hide");
        $A.util.addClass(cmp.find("grid-id"),"slds-hide");
        $A.util.removeClass(cmp.find("modal-aura"),"slds-fade-in-open");
        cmp.set("v.modalStyle","");
        $A.util.addClass(cmp.find("backGroundSectionId"),"slds-hide");
        $A.util.removeClass(cmp.find("spinnerCharged"),"slds-hide");
        var isActive  =cmp.get("v.simpleRecord.BI_FVI_Activo__c");
        var OpenOpp = cmp.get("v.valOpenOpp");
        var CloseOpp = cmp.get("v.valCloseOpp");
        var OpenCase = cmp.get("v.valOpenCase");
        var CloseCase = cmp.get("v.valCloseCase");
       	var finishOpp = true;
        var finishAcc = true;
        var activate;
        if(isActive==false){
            activate=true;
        }else{
            activate=false;
        }
        var ownerId = cmp.get("v.simpleRecord.OwnerId");
        var updateNode = cmp.get("c.activateNode");
        updateNode.setParams({"idNode":cmp.get("v.recordId"),"active":activate,"mode":"Account-Change owner","ownerId":ownerId});
        updateNode.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                if(response.getReturnValue()=="OK"){
                    //podemos actualizar todo
                    if(activate==true){
                       
	                    if(CloseOpp==true){
    	                    //LLmamos a open 
        	                var action= cmp.get("c.changeClosedOpps");
            	            var query = "Select Id from Opportunity where isClosed=true AND AccountId in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c ='"+cmp.get("v.recordId")+"')";
                	        var mode = "Opportunity-Closed";
                    	    action.setParams({"idNode":cmp.get("v.recordId"),"ownerId":ownerId,"query":query,"mode":mode});
                        	action.setCallback(this,function(response){});
	                        $A.enqueueAction(action); 
    	                }
        	            if(OpenOpp==true){
            	            //LLmamos a open 
                	        var action= cmp.get("c.changeClosedOpps");
                    	    var query = "Select Id from Opportunity where isClosed=false AND AccountId in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c ='"+cmp.get("v.recordId")+"')";
                        	var mode = "Opportunity-Open";
	                        action.setParams({"idNode":cmp.get("v.recordId"),"ownerId":ownerId,"query":query,"mode":mode});
    	                    action.setCallback(this,function(response){});
        	                $A.enqueueAction(action); 
            	        }
                        if(OpenCase==true){
            	            //LLmamos a open 
                	        var action= cmp.get("c.changeClosedOpps");
                    	    var query = "Select Id from Case where isClosed=false AND AccountId in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c ='"+cmp.get("v.recordId")+"')";
                        	var mode = "Case-Open";
	                        action.setParams({"idNode":cmp.get("v.recordId"),"ownerId":ownerId,"query":query,"mode":mode});
    	                    action.setCallback(this,function(response){});
        	                $A.enqueueAction(action); 
            	        }
                        if(CloseCase==true){
            	            //LLmamos a open 
                	        var action= cmp.get("c.changeClosedOpps");
                    	    var query = "Select Id from Case where isClosed=true AND AccountId in (Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c ='"+cmp.get("v.recordId")+"')";
                        	var mode = "Case-Close";
	                        action.setParams({"idNode":cmp.get("v.recordId"),"ownerId":ownerId,"query":query,"mode":mode});
    	                    action.setCallback(this,function(response){});
        	                $A.enqueueAction(action); 
            	        }
                    }
                    if(finishOpp==true && finishAcc==true){
                        var resultsToast = $A.get("e.force:showToast");
                        var title;
                        var message="";
                        if(activate==true){
                            title="Activación realizada con éxito";
                            message="Recibirá un correo cuando los procesos de cambio de Oportunidades y Casos termine";
                        }else{
                            debugger;
                            title="Desactivacion realizada con éxito";
                            message="Por favor avise al usuario propietario del nodo";
                        }
						resultsToast.setParams({
    		        	   	"title": title,
						   	"message":message,
    	        	        "type":"success"
        				});
            	    	resultsToast.fire();
                        $A.get('e.force:refreshView').fire();
                        hel.reload(cmp,evt,hel,activate);
                        $A.util.addClass(cmp.find("spinnerCharged"),"slds-hide");
                        $A.util.removeClass(cmp.find("grid-id"),"slds-hide");
                    }
                }else{
                    var resultsToast = $A.get("e.force:showToast");
		            resultsToast.setParams({
    		            "title": "ERROR",
    			       	"message":response.getReturnValue(),
            	        "type":"error",
                        "mode":"sticky"
        			});
                	resultsToast.fire();
                    $A.util.addClass(cmp.find("spinnerCharged"),"slds-hide");
                    $A.util.removeClass(cmp.find("grid-id"),"slds-hide");
                }
            }
        });
        $A.enqueueAction(updateNode);
	}*/
   
})