({
	/***********************************
	@author:  Javier Lopez Andradas
	@Company: gcTIO
	@description:  process taht make when the component/record is loaded
	<date>		<version>		<descirption>
	?????		1.0				Initial
	29/08/2018	1.1				Added getTypeObj invoke to etrieve the RVG and Rosseta 1 
	11/09/2018	1.2				Added conditine to retreve all families

	****************************************/
	doInit : function(cmp, evt, hel) {
		//Primero  buscamos los periodos 
		var evtPar = evt.getParams();
		if(evtPar.changeType==="LOADED"){
			var finPer = false;
			var finFOF = false;

			var actionRVG = cmp.get("c.getTypeObj");
			actionRVG.setParams({"idNodo":cmp.get("v.simpleRecord.FO_Nodo__c")});
			actionRVG.setCallback(this,function(responseRVG){
				var tabHead = "";
				var resList = responseRVG.getReturnValue();
				if(resList!=null){
					for(var i = 0;i<resList.length;i++){
						var res = resList[i];
						tabHead+="Alcance: ";
						if(res.BI_Estruct_Rama_Vertical_Global__c!=null){
							tabHead+=res.BI_Estruct_Rama_Vertical_Global__c+" | ";
						}
						if(res.BI_Estruct_Rosseta1__c!=null){
							tabHead+=res.BI_Estruct_Rosseta1__c+" | ";
							if(res.BI_Estruct_Rosetta2__c!=null){
								tabHead+=res.BI_Estruct_Rosetta2__c+" | ";
								if(res.BI_Estruct_Rosetta_3__c!=null){
									tabHead+=res.BI_Estruct_Rosetta_3__c+" | ";
									if(res.BI_Estruct_MKTG_Global_Nivel_4__c!=null){
										tabHead+=res.BI_Estruct_MKTG_Global_Nivel_4__c+" | ";
									}
								}
							}
						}
						tabHead = tabHead.substring(0,tabHead.length-3)+"\n";
					}
				}

				cmp.set("v.headTable",tabHead);
			});
			$A.enqueueAction(actionRVG);
			var rMan = cmp.get("v.RetrieveManager");
			if(rMan==false){
				var actionForeType = cmp.get("c.getForecasts");
				actionForeType.setParams({"periodo":cmp.get("v.simpleRecord.FO_Periodo__c"),"nodo":cmp.get("v.simpleRecord.FO_Nodo__c")});
				actionForeType.setCallback(this,function(response){
					var res = response.getReturnValue();
					if(res[0].RecordType.DeveloperName.indexOf("Manager")!=-1 ){
						cmp.set("v.Manager",true);
					}                
				});
				$A.enqueueAction(actionForeType);
			}
			
			var actionPer = cmp.get("c.getPeriodosForecast");
			actionPer.setParams({"idPeriodo":cmp.get("v.simpleRecord.FO_Periodo__c")});
			actionPer.setCallback(this,function(response){
				if(response.getState()==="SUCCESS"){
					var mp_Per = {};
					var mp_PerPa={};
					var keys=[];
					var res = response.getReturnValue();
					var NameSort = [];
					for(var j=0;j<res.length;j++){
						mp_Per[res[j].Id]=res[j];
						var lst_son = mp_PerPa[res[j].FO_Periodo_Padre__c];
						if(lst_son==null)
							lst_son=[];
						lst_son.push(res[j]);
						mp_PerPa[res[j].FO_Periodo_Padre__c]=lst_son;
						if(res[j].FO_Type__c!="Semana"){
							keys.push(res[j].Id);
						}
						
					}
					keys.sort(function(a,b){
					   var fechIniA = new Date(mp_Per[a].BI_FVI_Fecha_Fin__c);
					   var fechIniB = new Date(mp_Per[b].BI_FVI_Fecha_Fin__c);
						if(fechIniA>fechIniB){
							return 1;
						}else if(fechIniA<fechIniB){
							return -1;
						}else{
							var fechFinA = new Date(mp_Per[a].BI_FVI_Fecha_Inicio__c);
							var fechFinB = new Date(mp_Per[b].BI_FVI_Fecha_Inicio__c);
							if(fechFinA>fechFinB){
								return -1;
							}else
								return 1;
						}
					});
					var obj = new Object();
					obj.Name="";
					NameSort.push(obj);
					for(var j = 0;j<keys.length;j++){
						var aux =new Object();
						aux.Name=mp_Per[keys[j]].Name;
						aux.Type=mp_Per[keys[j]].FO_Type__c;
						aux.Id=mp_Per[keys[j]].Id;
						aux.BI_FVI_Fecha_Inicio__c=mp_Per[keys[j]].BI_FVI_Fecha_Inicio__c;
						aux.BI_FVI_Fecha_Fin__c=mp_Per[keys[j]].BI_FVI_Fecha_Fin__c;
						NameSort.push(aux);
					}
					var actionForeAjust = cmp.get("c.getAjustes");
					actionForeAjust.setParams({"IdFore":cmp.get("v.recordId")});
					actionForeAjust.setCallback(this,function(response){
						if(response.getState()==="SUCCESS"){
							var mpFore = {};
							var res = response.getReturnValue();
							for(var j = 0;j<res.length;j++){
								mpFore[res[j].FO_Periodo__c]=res[j];
							}
							var Forecast =  new Object();
							Forecast.ANT=[];
							Forecast.FUTMONTH=[];
							Forecast.FUTQ=[];
							var perActM = mp_Per[mp_Per[cmp.get("v.simpleRecord.FO_Periodo__c")].FO_Periodo_Padre__c];
							var mpForeAux ={};
							debugger;
							for(var j = 0;j<keys.length;j++){
								var idper = keys[j];
								var perEval = mp_Per[idper];
								var auxFore = mpFore[perEval.Id];
								var aux = new Object();
								if(auxFore==null){
									aux.fore = "NAN";
									aux.valueFore="";
									aux.periodo = idper;
									mpForeAux[keys[j]]=aux;
								}else{
									aux.fore = auxFore.Id;
									aux.AjusManager = auxFore.FO_Ajuste_Forecast_Manager__c;
									aux.AjusReal = auxFore.FO_Forecast_Final__c;
									aux.valueFore = auxFore.FO_Total_Forecast_Ajustado__c;
									aux.periodo = idper;
									console.log(cmp.get("v.simpleRecord.FO_Estado__c"));
									aux.Estado =cmp.get("v.simpleRecord.FO_Estado__c");
									aux.FechaFin=perEval.BI_FVI_Fecha_Fin__c;
									mpForeAux[keys[j]]=aux;
								}
								if(new Date (perEval.BI_FVI_Fecha_Fin__c)< new Date(perActM.BI_FVI_Fecha_Fin__c)){
									//Anteriores
									Forecast.ANT.push(aux);
								}else if (perEval.FO_Type__c=="Mes"){
									//FUTMONTH
									Forecast.FUTMONTH.push(aux);
								}else if(perEval.FO_Type__c=="Trimestre" && idper!=perActM.FO_Periodo_Padre__c){
									//FUTQ
									Forecast.FUTQ.push(aux);
								}else if(perEval.FO_Type__c=="Trimestre"){
									//ACTQ
								}else{
									//YEAR
								}
							}
							var perACTQ = mp_Per[perActM.FO_Periodo_Padre__c];
							var lst_sonM = mp_PerPa[perACTQ.Id];
							var ForeACTQ = new Object();
							var auxForeQ = mpFore[perACTQ.Id];
							ForeACTQ.fore = auxFore.Id;
							ForeACTQ.AjusManager = 0;
							ForeACTQ.AjusReal = 0;
							ForeACTQ.valueFore = 0;
							ForeACTQ.periodo = perACTQ.Id;
							ForeACTQ.Estado = cmp.get("v.simpleRecord.FO_Estado__c");
							ForeACTQ.FechaFin=perACTQ.BI_FVI_Fecha_Fin__c;
							debugger;
							for(var j = 0;j<lst_sonM.length;j++){
								if(mpForeAux[lst_sonM[j].Id]==null || mpForeAux[lst_sonM[j].Id].fore=="NAN")
									continue;
								ForeACTQ.AjusManager += Number(mpForeAux[lst_sonM[j].Id].AjusManager);
								ForeACTQ.AjusReal +=Number(mpForeAux[lst_sonM[j].Id].AjusReal);
								ForeACTQ.valueFore += Number(mpForeAux[lst_sonM[j].Id].valueFore);
							}
							mpForeAux[perACTQ.Id]=ForeACTQ;
							Forecast.ACTQ=ForeACTQ;
							var perACTY = mp_Per[perACTQ.FO_Periodo_Padre__c];
							var lst_sonQ = mp_PerPa[perACTY.Id];
							var ForeACTY = new Object();
							var auxForeY = mpFore[perACTY.Id];
							ForeACTY.fore = auxForeY.Id;
							ForeACTY.AjusManager = 0;
							ForeACTY.AjusReal = 0;
							ForeACTY.valueFore = 0;
							ForeACTY.periodo = perACTQ.Id;
							ForeACTY.Estado = cmp.get("v.simpleRecord.FO_Estado__c");
							ForeACTY.FechaFin=perACTY.BI_FVI_Fecha_Fin__c;
							for(var j = 0;j<lst_sonQ.length;j++){
								if(mpForeAux[lst_sonQ[j].Id]==null || mpForeAux[lst_sonQ[j].Id].fore=="NAN")
									continue;
								ForeACTY.AjusManager += Number(mpForeAux[lst_sonQ[j].Id].AjusManager);
								ForeACTY.AjusReal += Number(mpForeAux[lst_sonQ[j].Id].AjusReal);
								ForeACTY.valueFore += Number(mpForeAux[lst_sonQ[j].Id].valueFore);
							}
							mpForeAux[perACTY.Id]=ForeACTY;
							Forecast.Year=ForeACTY;
							
						}
						//cambiarel Forecat por problemas con el renderizado
						cmp.set("v.ForeNotANT",Forecast.ANT);
						cmp.set("v.ForeNotACTM",[]);
						cmp.set("v.ForeNotFUTM",Forecast.FUTMONTH);
						cmp.set("v.ForeNotACTQ",Forecast.ACTQ);
						cmp.set("v.ForeNotFUTQ",Forecast.FUTQ);
						cmp.set("v.ForeNotYear",Forecast.Year);
						cmp.set("v.TotalForeAjustANT",Forecast.ANT);
						cmp.set("v.TotalForeAjustFUTM",Forecast.FUTMONTH);
						cmp.set("v.TotalForeAjustACTQ",Forecast.ACTQ);
						cmp.set("v.TotalForeAjustFUTQ",Forecast.FUTQ);
						cmp.set("v.TotalForeAjustYear",Forecast.Year);
						 //cambiarel Forecat por problemas con el renderizado
						cmp.set("v.NameSort",NameSort);
						cmp.set("v.Forecast",Forecast);
						cmp.set("v.mpForeAux",mpForeAux);
						cmp.set("v.periSon",mp_PerPa);
						cmp.set("v.mp_Per",mp_Per);
						cmp.set("v.RetrieveManager",true);
						if(cmp.get("v.finishSnap")==true)
							hel.trueLoadSnap(cmp,evt,hel);
					   
						
					});
					$A.enqueueAction(actionForeAjust);
				}
			});
			$A.enqueueAction(actionPer);
		}
		var action2 = cmp.get("c.getOppStageName");
        action2.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
            	var mp_Stage = {};
            	var res = response.getReturnValue();
            	for(var stage in res){
            		mp_Stage[stage.substring(0,2)]=res[stage];
            	}
                cmp.set("v.Stages",mp_Stage);
            } 
        });
        $A.enqueueAction(action2); 
		
	},
	changeFore : function(cmp,evt,help){
		
		var idPer = evt.getParam("Id_per");
		var value = evt.getParam("Value_Fore");
		
		var periSon = cmp.get("v.periSon");
		var Forecast = cmp.get("v.Forecast");
		var NameSort =cmp.get("v.NameSort");
		var mpForeAux=cmp.get("v.mpForeAux");
		var manager = cmp.get("v.Manager");
		var mp_Per = cmp.get("v.mp_Per");
		var actMonth = mp_Per[mp_Per[cmp.get("v.simpleRecord.FO_Periodo__c")].FO_Periodo_Padre__c];
		var lst_obj = cmp.get("v.ObjetivoSort");
		var ActualsSort = cmp.get("v.ActualsSort");
		var pipeTot = cmp.get("v.totalPipePer");
		
		if(manager==false){
			mpForeAux[idPer].valueFore = value;    
			mpForeAux[idPer].AjusReal = value;    
		}else{
			mpForeAux[idPer].AjusManager = value;
			mpForeAux[idPer].AjusReal = Number(mpForeAux[idPer].valueFore)+Number(value);
		}
		//Lista de los periodos de los meses hijos del actual Q
		var perACTQ = mp_Per[mp_Per[cmp.get("v.simpleRecord.FO_Periodo__c")].FO_Periodo_Padre__c].FO_Periodo_Padre__c;
		var lst_sonM = periSon[perACTQ];  
		var auxForeQ = mpForeAux[perACTQ];
		auxForeQ.valueFore=0;
		auxForeQ.AjusManager=0;
		auxForeQ.AjusReal=0;
		debugger;
		for(var j = 0;j<lst_sonM.length;j++){
			var per = lst_sonM[j];
			var auxFore = mpForeAux[per.Id];
			if(auxFore!=null && auxFore.fore!="NAN"){
				auxForeQ.valueFore+=Number(auxFore.valueFore);
				auxForeQ.AjusManager+=Number(auxFore.AjusManager);
				auxForeQ.AjusReal+=Number(auxFore.AjusReal);
			}
		}
		mpForeAux[perACTQ]=auxForeQ;
		//Lista de los periodos de los Q hijos del actual Year
		console.log(mp_Per[perACTQ]);
		var perACTYEAR = mp_Per[perACTQ].FO_Periodo_Padre__c;
		var lst_sonQ = periSon[perACTYEAR];
		var auxForeY = mpForeAux[perACTYEAR];
		auxForeY.valueFore=0;
		auxForeY.AjusManager=0;
		auxForeY.AjusReal=0;
		for(var j = 0;j<lst_sonQ.length;j++){
			var per = lst_sonQ[j];
			var auxFore = mpForeAux[per.Id];
			if(auxFore!=null  && auxFore.fore!="NAN"){
				auxForeY.valueFore+=Number(auxFore.valueFore);
				auxForeY.AjusManager+=Number(auxFore.AjusManager);
				auxForeY.AjusReal+=Number(auxFore.AjusReal);
			}
		}
		mpForeAux[perACTYEAR]=auxForeY;
		//AHora vamos a rellenar los del la lista de Forecast
		for(var j = 0;j<Forecast.ANT.length;j++){
			Forecast.ANT[j] = mpForeAux[Forecast.ANT[j].periodo]; 
		}
		for(var j = 0;j<Forecast.FUTMONTH.length;j++){
			Forecast.FUTMONTH[j] = mpForeAux[Forecast.FUTMONTH[j].periodo]; 
		}
		for(var j = 0;j<Forecast.FUTQ.length;j++){
			Forecast.FUTQ[j] = mpForeAux[Forecast.FUTQ[j].periodo]; 
		}
		debugger;
		//Recalculo de los trackers!!
		//lst_exit = actuals+Forecast
		var lst_exit = [];
		//lst_conver = Forecast/Pipeline
		var lst_conver = [];
		//lst_cover = EXIT/Objetivo
		var lst_cover = [];
		for(var i =1;i<NameSort.length;i++){
			var act = ActualsSort[i-1];
			var pipe = pipeTot[i-1];
			var fore = mpForeAux[NameSort[i].Id].AjusReal;
			var obj = lst_obj[i-1];
			var exit = Number(parseFloat(act).toFixed(2))+Number(parseFloat(fore).toFixed(2));
			var conver = "N/A";
			if(new Date(mp_Per[NameSort[i].Id].BI_FVI_Fecha_Fin__c)<new Date(actMonth.BI_FVI_Fecha_Fin__c)){
				conver='';
			}else if(pipe!=0){
				conver = Number(parseFloat(fore).toFixed(2))/Number(parseFloat(pipe).toFixed(2));
			}
				
			var cover = "N/A";
			if(obj!="N/A")
				cover = Number(parseFloat(exit).toFixed(2))/obj;
			lst_exit.push(exit);
			lst_conver.push(conver);
			lst_cover.push(cover);
		}
		//Hay que cambiar esta mierda por problemas en el renderizado
		//
		Forecast.ACTQ =  mpForeAux[perACTQ];
		Forecast.Year=mpForeAux[perACTYEAR];
		cmp.set("v.ForeNotANT",Forecast.ANT);
		//cmp.set("v.ForeNotACTM",[]);
		cmp.set("v.ForeNotFUTM",Forecast.FUTMONTH);
		cmp.set("v.ForeNotACTQ",Forecast.ACTQ);
		cmp.set("v.ForeNotFUTQ",Forecast.FUTQ);
		cmp.set("v.ForeNotYear",Forecast.Year);
		
		cmp.set("v.TotalForeAjustANT",Forecast.ANT);
		cmp.set("v.TotalForeAjustFUTM",Forecast.FUTMONTH);
		cmp.set("v.TotalForeAjustACTQ",Forecast.ACTQ);
		cmp.set("v.TotalForeAjustFUTQ",Forecast.FUTQ);
		cmp.set("v.TotalForeAjustYear",Forecast.Year);
		
		cmp.set("v.exitFore",lst_exit);
		cmp.set("v.conversionForePipe",lst_conver);
		cmp.set("v.coberturaExitFore",lst_cover);
		
		cmp.set("v.Forecast",Forecast);
		cmp.set("v.mpForeAux",mpForeAux);
		
	},
	/********************************
	 * @author: Javier Lopez Andradas
	 * @description: controls the load of snapshots
	 * @company:gcTIO
	 * 
	 * <date>		<version>		<description>
	 * 09/08/2018	1.0				Initial description
	 * 
	 * 
	 * ******************************/
	loadSnapshot : function(cmp,evt,hel){
		var rMan = cmp.get("v.RetrieveManager");
		cmp.set("v.finishSnap",true);
		if(rMan==true){
			 hel.trueLoadSnap(cmp,evt,hel);
		}
	},
	compactExpandRowObj :function(cmp, event, helper) {
		
		//alert("entra en compactExpandRowObj");
		var bool = cmp.get("v.flagObj");
		var tr = cmp.find("tr-weeks");
		debugger;
		if(bool==false) {
			cmp.set("v.flagObj",true);
			
			$A.util.removeClass(tr,"slds-hide");
			$A.util.removeClass(cmp.find("tr-acts"),"slds-hide");
			
			cmp.set("v.actualsSymbol", "[-]");
		}
		else{
			cmp.set("v.flagObj",false); 
			
			$A.util.addClass(tr,"slds-hide");
			$A.util.addClass(cmp.find("tr-acts"),"slds-hide");
			
			cmp.set("v.actualsSymbol", "[+]");
		}
	}

})