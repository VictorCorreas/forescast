({
	trueLoadSnap : function(cmp,evt,hel) {
		var man = cmp.get("v.Manager");
        //ahora tenemos que emepzar a ver los cambios
        //periodos
        var pers = cmp.get("v.NameSort");
        var mp_Per = cmp.get("v.mp_Per");
        var Snap_Pipe = {};
        var Snap_Acts = {};
        var sub  = cmp.get("v.simpleRecord.FO_Estado__c")=="Enviado";
        var finObj=false;
        var finCurr=false;
        var finPipe=false;
        var finAct=false;
        var periods=[];
        var lst_cis = [];
        var lst_def = [];
        //LOs objetivos, tal y como estan montados se van por su lado
		var lastPer = pers[pers.length-1];
        var rObj = cmp.get("c.getObjetivo");
        rObj.setParams({"nodo":cmp.get("v.simpleRecord.FO_Nodo__c"),"fechIni":lastPer.BI_FVI_Fecha_Inicio__c,"fechFin":lastPer.BI_FVI_Fecha_Fin__c,"periodos":null,"IdFore":cmp.get("v.recordId"),"enviado":sub});
        rObj.setCallback(this,function(respObj){
            var mp_obj={};
            var mp_objComer = {};
            var res = respObj.getReturnValue();
            for(var i = 0;i<res.length;i++){
                mp_obj[res[i].idPer]=res[i].value;
                mp_objComer[res[i].idPer]=res[i].idObj;
            }
            var acumTot = 0;
            var acumTri = 0;
            // TO DO lo de los objetivos
            var lst_objFin = [];
            var lst_objFinAcum = [];
          //  lst_objFin.push("");
            for(var i = 1;i<pers.length;i++){
                var per = mp_Per[pers[i].Id];
                if(mp_obj[per.Id]==null){
                    lst_objFin.push("N/A");
                }else{
                    lst_objFin.push(mp_obj[per.Id]);
                }
                
                if(per.FO_Type__c=="Mes"){
                    if(mp_obj[per.Id]==null){
                        lst_objFinAcum.push(acumTri);
                    }else{
                        acumTri+=mp_obj[per.Id];
                        lst_objFinAcum.push(acumTri);    
                    }
                    
                }else if(per.FO_Type__c=="Trimestre"){
                    if(mp_obj[per.Id]==null){
                        lst_objFinAcum.push(acumTot);    
                    }else{
                        acumTot+=mp_obj[per.Id];
                        lst_objFinAcum.push(acumTot);    
                    }
                    
                }else{
                    //Es anual
                    if(mp_obj[per.Id]==null){
                        lst_objFinAcum.push(0);     
                    }else{
                        lst_objFinAcum.push(mp_obj[per.Id]);         
                    }
                    
                }
            }
            cmp.set("v.ObjetivoSort",lst_objFin);
            cmp.set("v.mp_Obj",mp_obj);
            var actionActObj = cmp.get("c.updateSnapObj");
	     	actionActObj.setParams({"lst_obj":mp_obj,"mp_objComer":mp_objComer,"idFore":cmp.get("v.recordId")});
    	    actionActObj.setCallback(this,function(responseActObj){});
        	$A.enqueueAction(actionActObj);
            cmp.set("v.ObjetivoSortAcum",lst_objFinAcum);
            finObj=true;
            if(man==false && sub==false && finCurr==true){
                hel.recursiveCall(cmp,evt,hel,0,lst_def,periods,lst_cis);    
            }else{
                if(finAct==true && finPipe==true){
                    hel.drawTableCurr(cmp,evt,hel);
                }
            }
            
            
        });
        $A.enqueueAction(rObj);
        debugger;
        if(man==false && sub==false){
            //Tenemos que sacar los CIs
            var rAcc = cmp.get("c.getAccs");
            console.log(cmp.get("v.simpleRecord.FO_Nodo__c"));
            var nodo =cmp.get("v.simpleRecord.FO_Nodo__c");
            rAcc.setParams({"IdNodo":cmp.get("v.simpleRecord.FO_Nodo__c")});
            rAcc.setCallback(this,function(response){
                debugger;
                var lst_acc = response.getReturnValue();
                var lst_aux=[];
                //ya tenemos las cuentas, ahora los CIs
                
                var munSpl=0;
                for(var  i = 0; i<lst_acc.length;i++){
                    lst_aux.push(lst_acc[i]);
                    munSpl++;
                    if(munSpl==9999){
                        lst_def.push(lst_aux);
                        lst_aux=[];
                        munSpl=0;
                    }
                }
                if(lst_aux.length!=0){
                    lst_def.push(lst_aux);
                }
                debugger;
                var actionMonth = cmp.get("c.getMonthPeriods");
                actionMonth.setParams({"IdPeriod":cmp.get("v.simpleRecord.FO_Periodo__c")});
                actionMonth.setCallback(this,function(resPeriod){
                    periods = resPeriod.getReturnValue();
               		var lst_cis = [];
                    finCurr=true;
                    if(finObj==true)
                    	hel.recursiveCall(cmp,evt,hel,0,lst_def,periods,lst_cis);    
                });
                $A.enqueueAction(actionMonth);
               
                
            });
            $A.enqueueAction(rAcc);

        }else{
            //sacamos Snapshot
            var actionPipe = cmp.get("c.getPipe");
            
            actionPipe.setParams({"idFore":cmp.get("v.recordId")});
            actionPipe.setCallback(this,function(responPipe){
                var mp_Pipe = {};
                var res = responPipe.getReturnValue();
                for(var i = 0;i<res.length;i++){
                    if(mp_Pipe[res[i].FO_Period__c]==null){
                        var aux = {};
                        for(var j=2;j<=5;j++){
                            aux["F"+j]=0;
                        }
                        mp_Pipe[res[i].FO_Period__c]=aux;
                    }
                    mp_Pipe[res[i].FO_Period__c][res[i].FO_Stage__c]=res[i].FO_Value__c;
                }
                cmp.set("v.mp_Pipe",mp_Pipe);
                finPipe=true;
                if(finAct==true && finObj==true){
                    hel.drawTableCurr(cmp,evt,hel);
                }
            });
            $A.enqueueAction(actionPipe);
            var actionAct = cmp.get("c.getActs");
            actionAct.setParams({"idFore":cmp.get("v.recordId")});
            actionAct.setCallback(this,function(responAct){
                var res = responAct.getReturnValue();
                var mp_actSem = {};
                var mp_act = {};
                for(var i = 0;i<res.length;i++){
                    var act = res[i];
                    if(mp_Per[act.FO_Periodo__c].FO_Type__c=="Semana"){
                        mp_actSem[act.FO_Periodo__c]=act.FO_Value__c;
                    }else{
                        mp_act[act.FO_Periodo__c]=act.FO_Value__c;
                    }
                }
                cmp.set("v.mp_ACT",mp_act);
                cmp.set("v.mp_ACTSEM",mp_actSem);
                finAct=true;
                if(finPipe==true && finObj==true){
                    hel.drawTableCurr(cmp,evt,hel);
                }
            });
            $A.enqueueAction(actionAct);
            
            
        }
        
	},
    recursiveCall : function(cmp,evt,hel,count,lst_def,lst_per,lst_cis){
        debugger;
        if(count==lst_def.length){
            //aqui terminamos
            cmp.set("v.lst_cis",lst_cis);
            hel.drawTableCurr(cmp,evt,hel);
        }else{
            var lst_acc = lst_def[count];
        	var rM = 0;
	        for(var i = 0;i<lst_per.length;i++){
    	        var per = lst_per[i];
        	    var action  = cmp.get("c.getCIs");
            	    action.setParams({"acc":lst_acc,"IdNode":cmp.get("v.simpleRecord.FO_Nodo__c"),"fechIni":per.BI_FVI_Fecha_Inicio__c, "fechFin":per.BI_FVI_Fecha_Fin__c,"foCurr":cmp.get("v.simpleRecord.CurrencyIsoCode")});
                	action.setCallback(this,function(responCI){
                    	var res = responCI.getReturnValue();
	                    if(res!=null){
    	                	for(var j = 0 ;j<res.length;j++)
        	            	lst_cis.push(res[j]);    
            	        }
                	    rM++;
                    	if(rM==11){
                        	var newCount = count+1;
	                        hel.recursiveCall(cmp,evt,hel,newCount,lst_def,lst_per,lst_cis);
    	                }
        	        });
            	$A.enqueueAction(action);
        	}
        }
        
    },
    drawTableCurr : function(cmp,evt,hel){
        var sub  = cmp.get("v.simpleRecord.FO_Estado__c")=="Enviado";
        var man = cmp.get("v.Manager");
        var pers = cmp.get("v.NameSort");
        var mpPer = cmp.get("v.mp_Per");
        var mpPerSon = cmp.get("v.periSon");
        var lst_cis = cmp.get("v.lst_cis");
        var actPer = cmp.get("v.simpleRecord.FO_Periodo__c");
       	var	actMonth = mpPer[mpPer[actPer].FO_Periodo_Padre__c];
        var lst_WeekPeriod = mpPerSon[mpPer[actPer].FO_Periodo_Padre__c];
        var mp_obj = cmp.get("v.mp_Obj");
        var mp_actSemPer={};
        var auxFore = cmp.get("v.mpForeAux");
        var stagesMap = cmp.get("v.Stages");
        //Valores a palo seco
        var lst_act = [];
        var lst_actSUM = [];
        var mp_act={};//para meterlo en los SNapshots
        //Lista de Objetos {nominal:W+seq,pasado:[],act:[],sinScope:[]}
        var lst_actSem=[];
        var mp_actSem = {};
        //PAra el Pipeline
        var lst_pipe = [];
        var mp_pipe ={};
        debugger;
        if(sub==false && man==false){
            //Aqui ya evaluamos la ordenacion de los CIs por estado
            var mp_actSemPer = {};
            for(var i = 0;i<lst_WeekPeriod.length;i++){
                mp_actSemPer[lst_WeekPeriod[i].Id]=0;
            }
            
            for(var j = 0;j<lst_cis.length;j++){
                var oi = lst_cis[j];
                for(var i = 1;i<pers.length;i++){
                    var per = mpPer[pers[i].Id];
                    if(oi.StageName=="F1"){
                      	if(oi.BI_Fecha_de_cierre_real>=per.BI_FVI_Fecha_Inicio__c && oi.BI_Fecha_de_cierre_real<=per.BI_FVI_Fecha_Fin__c){
                            if(mp_act[per.Id]==null){
                                mp_act[per.Id]=Number(parseFloat(oi.BI_Net_annual_value_NAV).toFixed(2));
                            }else{
                                mp_act[per.Id]+=Number(parseFloat(oi.BI_Net_annual_value_NAV).toFixed(2));
                            }	
                    	}
                    }else{
                        console.log(oi.CloseDate+'||| per Ini:'+per.BI_FVI_Fecha_Inicio__c+' per Fin: '+per.BI_FVI_Fecha_Fin__c);
                        if(new Date(oi.CloseDate)>=new Date(per.BI_FVI_Fecha_Inicio__c) && new Date(oi.CloseDate)<=new Date(per.BI_FVI_Fecha_Fin__c)){
                            console.log('Entro');
                            if(mp_pipe[per.Id]==null){
                                var stages = {};
                                for(var k = 2;k<=5;k++){
                                    stages["F"+k]=0;
                                }
                                mp_pipe[per.Id]=stages;
                            }
                            debugger;
                            var NAV_Pipe =oi.BI_Net_annual_value_NAV;
                            mp_pipe[per.Id][oi.StageName]+=Number(parseFloat(oi.BI_Net_annual_value_NAV).toFixed(2));
                        }
                    }
                }
                if(oi.StageName=="F1"){
                    for(var z = 0;z<lst_WeekPeriod.length;z++){
                        var perWeek = lst_WeekPeriod[z];
                        if(oi.BI_Fecha_de_cierre_real>=perWeek.BI_FVI_Fecha_Inicio__c && oi.BI_Fecha_de_cierre_real<=perWeek.BI_FVI_Fecha_Fin__c){
                            mp_actSemPer[perWeek.Id]+=Number(parseFloat(oi.BI_Net_annual_value_NAV).toFixed(2));
                        }
                    }
                }
            }
            var actACts = cmp.get("c.updateActs");
            //JLA_VN_ Serialize to JSON
            actACts.setParams({"mp_actString":JSON.stringify(mp_act),"mp_actSemString":JSON.stringify(mp_actSemPer),"idFore":cmp.get("v.recordId")});
            var actfin =false;
            var pipeFin= false;
            actACts.setCallback(this,function(response2){
                debugger;
                if(response2.getState()==="ERROR"){
                    var err=response2.getErrors();
                    console.log(err);
                }else{
                    actfin=true;
                    if(actfin==true && pipeFin==true){
                        var resultsToast = $A.get("e.force:showToast");
		            	resultsToast.setParams({
	    		            "title": "ÉXITO: ",
    		    		   	"message": "Foto del Forecast actualizada con éxito",
        	    	        "type":"success"
        				});
            			resultsToast.fire(); 
                        var finish =$A.get("e.c:FO_SnapshotFinish");
                        finish.fire();
                    }
                    
                }
            });
            $A.enqueueAction(actACts);
            var actPipe = cmp.get("c.updatePipeline");
            actPipe.setParams({"aux_pipe":JSON.stringify(mp_pipe),"idFore":cmp.get("v.recordId")});
            actPipe.setCallback(this,function(response2){
                    debugger;
                if(response2.getState()==="ERROR"){
                    var err=response2.getError();
                    console.log(err);
                }else{
                    pipeFin=true;
                    if(actfin==true && pipeFin==true){
                        var resultsToast = $A.get("e.force:showToast");
		            	resultsToast.setParams({
	    		            "title": "ÉXITO: ",
    		    		   	"message": "Foto del Forecast actualizada con éxito",
        	    	        "type":"success"
        				});
            			resultsToast.fire(); 
                        var finish =$A.get("e.c:FO_SnapshotFinish");
                        finish.fire();
                    }
                    
                }
            });
            $A.enqueueAction(actPipe);
			cmp.set("v.mp_ACT",mp_act);
            cmp.set("v.mp_ACTSEM",mp_actSemPer);
            cmp.set("v.mp_Pipe",mp_pipe);
            
            //llamar a draw Table
        }else{
            //aqui hemos de sacar de los Snapshots
            mp_pipe=cmp.get("v.mp_Pipe");
            mp_actSemPer=cmp.get("v.mp_ACTSEM");
            mp_act=cmp.get("v.mp_ACT");
            var finish =$A.get("e.c:FO_SnapshotFinish");
            finish.fire();
        }
        // ya tenemos todo ordenado y preparao
        var sumActTot=0;
        var sumActTri=0;
        //Mapa para la generacion de los objetos de los weeks actuals
        //{Seq,Objeto_week}
        //Objeto_week={nom="W"+seq,pasado=[],act=valor,sinScope=[]}
        var mp_drawWeek ={};
        var mp_drawPipe = {};
        debugger;
        for(var i=1;i<6;i++){
        	var objWeek = new Object();
            objWeek.nominal="W"+i;
            objWeek.pasado=[];
            objWeek.act=0;
            objWeek.sinScope=[];
            mp_drawWeek[i]=objWeek;
            if(i!=1){
                var pipeStage = new Object();
                pipeStage.stage=stagesMap["F"+i];
                pipeStage.values=[];
            	mp_drawPipe["F"+i]=pipeStage;
            }
        }
        //Fin generacion mapa
        var pipeTotFin = [];
        //Lista de EXIT = ACTUALS+PIPELINE
        var lst_exit=[];
        //lista conversion Forecast pipeline = Forecast/PIPELINE
        var lst_conver = [];
        //lista conversion exit=> exit/objetivo
        var lst_converExit = [];
        //lista conversion actuals=>actuals/objetivo
        var lst_converAct = [];
        for(var i = 1;i<pers.length;i++){
            var per = pers[i];
            var act = 0;
            if(mp_act!=null && mp_act[per.Id]!=null)
            	act = mp_act[per.Id];
            lst_act.push(act);
            if(per.Type=="Mes"){
                if(mp_act!=null && mp_act[per.Id]!=null)
                	sumActTri+=Number(parseFloat(mp_act[per.Id]).toFixed(2));
                lst_actSUM.push(sumActTri);
            }else if(per.Type=="Trimestre"){
                if(mp_act!=null && mp_act[per.Id]!=null)
                	sumActTot+=Number(parseFloat(mp_act[per.Id]).toFixed(2));
                lst_actSUM.push(sumActTot);
            }else{
                if(mp_act!=null && mp_act[per.Id]!=null)
                	lst_actSUM.push(mp_act[per.Id]);
                else
                	lst_actSUM.push(0);
            }
            //Establecemos los acuals semanales
            if(per.Id==actMonth.Id){
                debugger;
                for(var week in mp_actSemPer){
                    var seq = mpPer[week].FO_Secuencia__c;
                    mp_drawWeek[seq].act=mp_actSemPer[week];
                }
                //Para meses con menos de 5 semanas
                if(lst_WeekPeriod.length<5){
                    mp_drawWeek[5].act="";
                }
            }else if(per.BI_FVI_Fecha_Fin__c<actMonth.BI_FVI_Fecha_Fin__c){
                for(var  j =1;j<6;j++){
                    mp_drawWeek[j].pasado.push("");
                }
            }else{
                for(var  j =1;j<6;j++){
                    mp_drawWeek[j].sinScope.push("");
                }
            }
            //Fin Establecemos los acuals semanales
            //Establecesmos el Pipeline:
            var sumPipe = 0;
            var mp_pipePer = mp_pipe[per.Id];
            if(mp_pipePer==null){
                for(var j =2;j<6;j++){
                    mp_drawPipe["F"+j].values.push(0);
                }
            }else{
                for(var j = 2;j<6;j++){
                    var auxPipe = mp_pipePer["F"+j];
                    if(auxPipe==null){
                        mp_drawPipe["F"+j].values.push(0);
                    }else{
                        mp_drawPipe["F"+j].values.push(auxPipe);
                        sumPipe+=Number(parseFloat(auxPipe).toFixed(2));
                    }
                }
            }
            pipeTotFin.push(sumPipe);
            //Fin establecimiento pipeline
            //START_EXIT
            var exit = "";
           
            //ENDS_EXIT
            //START_Forecast/Pipeline conversion
            debugger;
            if(new Date(mpPer[per.Id].BI_FVI_Fecha_Fin__c)<new Date(actMonth.BI_FVI_Fecha_Fin__c)){
               lst_conver.push("") ;
            }else{
                if(sumPipe==0){
                	lst_conver.push("N/A");
            	}else{
	                if(auxFore[per.Id]==null){
    	                lst_conver.push(0);
        	        }else{
            	        lst_conver.push(Number(parseFloat(auxFore[per.Id].AjusReal).toFixed(2))/Number(parseFloat(sumPipe).toFixed(2)));
                	}
            	}
                exit=Number(parseFloat(act).toFixed(2))+Number(parseFloat(auxFore[per.Id].AjusReal).toFixed(2))
            }
            lst_exit.push(exit);
            //ENDS_Forecast/Pipeline conversion
            //START EXIT/Objetivo && ACTUALS/Objetivo
            //TO DO
            var objPer = mp_obj[per.Id];
            if(objPer==null || objPer==0){
                 lst_converExit.push("N/A");
                 lst_converAct.push("N/A");
            }else{
                lst_converExit.push(exit/Number(parseFloat(objPer).toFixed(2)));
                lst_converAct.push(Number(parseFloat(act).toFixed(2))/Number(parseFloat(objPer).toFixed(2)));
            }
            //ENDS EXIT/Objetivo && ACTUALS/Objetivo
           
            
            
        }
        //Pinemos los semanales en un formato decente
        var actSemFin=[];
        var pipePerStageFin=[];
        for(var j =1;j<6;j++){
            actSemFin.push(mp_drawWeek[j]);
            if(j!=1)
            	pipePerStageFin.push(mp_drawPipe["F"+j]);
        }
        debugger;
        cmp.set("v.ActualsWeekSortAcum",actSemFin);
        cmp.set("v.ActualsSortAcum",lst_actSUM);
        cmp.set("v.ActualsSort",lst_act);
        cmp.set("v.totalPipePer",pipeTotFin);
        cmp.set("v.pipeForStage",pipePerStageFin);
		cmp.set("v.exitFore",lst_exit);
        cmp.set("v.conversionForePipe",lst_conver);
        cmp.set("v.coberturaExitFore",lst_converExit);
        cmp.set("v.coberturaObjetivo",lst_converAct);
        $A.util.removeClass(cmp.find("modal-wait"),"slds-fade-in-open");
        $A.util.addClass(cmp.find("modal-wait"),"slds-hide");
        //$A.util.addClass(cmp.find("modal-wait"),"slds-fade-in-open");
        $A.util.addClass(cmp.find("backGroundSectionId"),"slds-hide");
        $A.util.removeClass(cmp.find("table"),"slds-hide");
        
    }
})