({
	doInit : function(cmp, event, helper) {
        
        var state = cmp.get("v.simpleRecord.FO_Estado_Padre__c");
        var par = cmp.get("v.simpleRecord.FO_Forecast_Padre__c");
        debugger;
        var actionSt = cmp.get("c.getState");
        actionSt.setParams({"recId":par});
        actionSt.setCallback(this,function(responseState){
            debugger;
            console.log(responseState.getReturnValue());
            if(responseState.getReturnValue()=="Enviado"){
                var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Toast_tilte_Error"),
                    "type":"error"
        		});
            	var navEvt = $A.get("e.force:navigateToSObject");
        			navEvt.setParams({
            		"recordId": par
        		});
            	resultsToast.fire();
        		navEvt.fire();
            }else{
                helper.hideSpinner(cmp,event);
		        var valueSelect = cmp.get("v.simpleRecord.FO_Tipo_Forecast__c");
        		console.log(cmp.get("v.simpleRecord.FO_Tipo_Forecast__c"));
				cmp.set("v.selectType",false);
        		cmp.set("v.selectDate",false);	   
		        $A.util.addClass(cmp.find("div-buttons"),"slds-hide");
       			var action2 = cmp.get("c.getClose");
		        action2.setParams({"recId":cmp.get("v.recordId")});
        		action2.setCallback(this,function(response){
		            debugger;
                    //JLA Random error in back to FO Layout
                    if(response.getReturnValue()==null)
                        return;
                    var res = response.getReturnValue();
        		    cmp.find("Closedate").set("v.value",res.FO_CloseDate__c);
                    var fo = res.FO_Forecast_Padre__r.OwnerId;
                    var opp=res.FO_Opportunity__r.OwnerId;
                    if(res.FO_Forecast_Padre__r.OwnerId!=res.FO_Opportunity__r.OwnerId && res.FO_Forecast_Padre__r.RecordType.DeveloperName==="FO_MNC_Ejecutivo"){
                        //aqui hemos de cambiar la aplicacion
                        cmp.set("v.isDiffOwner",true);
                    }
		            var action = cmp.get("c.getTipo");
        			action.setCallback(this,function(response2){
		            	if(response.getState()==="SUCCESS"){
	    		            var result = response2.getReturnValue();
    	        		    var lst =[];
		        	        var aux = new Object()
        		    	    aux.label=$A.get("$Label.c.FO_Not_Slected");
                			aux.value="";
	                		lst.push(aux);
		    	            for(var j = 0;j<result.length;j++){
        			            var obj = new Object();
            			        obj.label=result[j].label;
                	    		obj.value=result[j].value;
		                    	if(result[j].value==response.getReturnValue().FO_Tipo_Forecast__c)
        		                	obj.selected=true;
    	        		        lst.push(obj);
		        	        }
        	    	    	cmp.set("v.lst_tipo",lst);
            			}
                        $A.util.addClass(cmp.find("modal-cont"),"slds-fade-in-open");
                    	$A.util.removeClass(cmp.find("div-buttons"),"slds-hide");  
                        
        			});
        			$A.enqueueAction(action);
        		});
         		$A.enqueueAction(action2); 
            }
        });
        $A.enqueueAction(actionSt);
          
        
	},
    
    save : function(cmp,evt,help){
        help.showSpinner(cmp,evt);
        $A.util.addClass(cmp.find("modal-container"),"slds-hide");
        var Id = cmp.get("v.recordId");
        var closeDate = cmp.find("Closedate").get("v.value");
        var selDate = cmp.get("v.selectDate");
        var selType = cmp.get("v.selectType");
      
        var tipo = cmp.find("tipo").get("v.value");
        var action = cmp.get("c.updateFOI");
        action.setParams({"recId":Id,"CloseDate":closeDate,"tipo":tipo,"selDate":selDate,"selType":selType});
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            help.hideSpinner(cmp,evt);
            $A.util.removeClass(cmp.find("modal-container"),"slds-hide");
            if(result=="Ok"){
                debugger;
                //window.location='one.app#/sObject/'+cmp.get("v.simpleRecord.FO_Forecast_Padre__c")+'/view';
                var navEvt = $A.get("e.force:navigateToSObject");
        		navEvt.setParams({
            		"recordId": cmp.get("v.simpleRecord.FO_Forecast_Padre__c")
        		});	
        	navEvt.fire();
            }else{
                var resultsToast = $A.get("e.force:showToast");
			            resultsToast.setParams({
                			"title": "",
    	       				"message": result,
                            "type":"error"
        				});
                 resultsToast.fire();
            }
        });
        $A.enqueueAction(action);
        
        
    },
    cancel :function(cmp,evt,help){
        //window.location='one.app#/sObject/'+cmp.get("v.simpleRecord.FO_Forecast_Padre__c")+'/view';
        var navEvt = $A.get("e.force:navigateToSObject");
        	navEvt.setParams({
            	"recordId": cmp.get("v.simpleRecord.FO_Forecast_Padre__c")
        	});
        	navEvt.fire();
    },
    changeType : function(cmp,evt){
        var sel = cmp.get("v.selectType");
        if(sel!=true){
           $A.util.removeClass(cmp.find("div-tipo"),"slds-hide");
	        $A.util.addClass(cmp.find("div-date"),"slds-hide");
    	    cmp.set("v.selectType",true);
        	cmp.set("v.selectDate",false);
            cmp.find("but-type").set("v.variant","brand");
            cmp.find("but-date").set("v.variant","neutral");
	        //var but= document.getElementById("but-date");
    	    //but.checked=false;
        }else{
            //var but= document.getElementById("but-type");
    	    //but.checked=true;
        }
        
    },
    changeDate: function(cmp,evt){
        debugger;
        var sel = cmp.get("v.selectDate");
        if(sel == false){
            $A.util.removeClass(cmp.find("div-date"),"slds-hide");
	        $A.util.addClass(cmp.find("div-tipo"),"slds-hide");
    	    cmp.set("v.selectDate",true);
        	cmp.set("v.selectType",false);
	        cmp.find("but-type").set("v.variant","neutral");
            cmp.find("but-date").set("v.variant","brand");
        }else{
            var but=document.getElementById("but-date");
    	    but.checked=true;
        }
        
    }
})