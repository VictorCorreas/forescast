({
	showSpinner : function (cmp, event) {
       $A.util.removeClass(cmp.find("center-ui"),"slds-hide");
      var spinner = cmp.find('spinnerCharged');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire(); 
    },
    hideSpinner : function (cmp, event) {
        $A.util.addClass(cmp.find("center-ui"),"slds-hide");
       var spinner = cmp.find('spinnerCharged');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire(); 
    },
})