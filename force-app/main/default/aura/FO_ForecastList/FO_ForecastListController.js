({
    doInit: function(cmp, event, helper) {
        console.log("Hemos llegado al doInti");
        var idAux = cmp.get("v.recordId");
        var foreFin=false;
        var stageFin=false;
   		cmp.set("v.flagObj",true);
        if(idAux!=null){
           
            console.log("tiene recordId");
            $A.util.addClass(cmp.find("card_Config"),"slds-hide");
            var action = cmp.get("c.getForeInfo");
            action.setParams({"idFore":idAux});
            action.setCallback(this,function(response){
                if(response.getState()==="SUCCESS"){
                    var result =response.getReturnValue();
                    if(result.RecordType.DeveloperName.indexOf("MNC")!=-1){
                        cmp.set("v.isMNC",true);
                    }
                    cmp.set("v.NodoWId",result.FO_Nodo__c);
                    cmp.set("v.PeriodoWId",result.FO_Periodo__c);
                    //JLA_Version_2_Start
                    var actionMonth = cmp.get("c.getMonthPeriods");
                    actionMonth.setParams({"IdPeriod":result.FO_Periodo__c});
                    actionMonth.setCallback(this,function(resPeriod){
                        var periods = resPeriod.getReturnValue();
                       
                        cmp.set("v.Meses",periods);
                        foreFin=true;
                    	if(foreFin==true &&stageFin==true)
                        	helper.recordSelected(cmp,event,helper);
                    });
                    $A.enqueueAction(actionMonth);
                    //JLA_Version_2_Fin
                }
            });
            $A.enqueueAction(action);
        }else{
           var forecast =[];
            var aux = new Object();
            aux.Id="N/A";
            aux.Name="--No elegido--";
            forecast.push(aux);
            console.log("Aqui llego");
            var action1 = cmp.get("c.getNodos");
            console.log("Aqui no llego");
            action1.setCallback(this,function(response){
                if(response.getState()==="SUCCESS"){
                    var result = response.getReturnValue();
                    for(var i = 0;i<result.length;i++){
                        var nodo = new Object();
                        nodo.Id = result[i].Id;
                        nodo.Name=result[i].Name;
                        console.log(nodo.Name);
                        forecast.push(nodo);
                    }
                    console.log("devolvio el callout");
                    console.log(forecast);
                    cmp.set("v.Nodos",forecast);
                } else{
                    console.log("peto el callout");
                    alert("Hubo un error inesperado consiguiendo los nodos a los que se tiene acceso");
                }
            });
            $A.enqueueAction(action1);
        }
        var action2 = cmp.get("c.getOppStageName");
        action2.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                cmp.set("v.Stages",response.getReturnValue());
                stageFin=true;
                 if(foreFin==true &&stageFin==true)
                     helper.recordSelected(cmp,event,helper);
             
            } 
        });
        $A.enqueueAction(action2); 

    },
     showSpinner : function (cmp, event) {
        var spinner = cmp.find('spinnerCharged');        
        $A.util.removeClass(spinner,'slds-hide');  
    },
    hideSpinner : function (cmp, event) {
        var spinner = cmp.find('spinnerCharged');        
        $A.util.addClass(spinner,'slds-hide');
    },
    
    changeNodo : function(cmp,evt,hel){
        var Id = cmp.find("IdNodo").get("v.value");
        console.log("Id");
        $A.util.addClass(cmp.find("table"),"slds-hide");
        var spinner = cmp.find('spinner');    
        $A.util.removeClass(spinner,'slds-hide');  
        if(Id=="N/A"){
            console.log("Hemos puesto N/A")
            $A.util.addClass(cmp.find("PeriodoPick"),"slds-hide");
            $A.util.addClass(cmp.find("fechasPeriodo"),"slds-hide");
            $A.util.addClass(cmp.find("DatosFore"),"slds-hide");
       
            var aux =[];
            var mp_aux={};
            cmp.set("v.Periodos",aux);
            cmp.set("v.InfoPeriodos",mp_aux);
        }else{
            var action = cmp.get("c.getPeriodos");
            action.setParams({"IdNodo":Id});
            console.log("Hacemos la llamda al server Id"+Id);
            action.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                console.log("Exito!!!");
                var result = response.getReturnValue();
                var lst_periodos=[];
                var dummy = new Object();
                dummy.Id="N/A";
                dummy.Name="No Elegido";
                lst_periodos.push(dummy);
                if(result.length==0){
                    $A.util.removeClass(cmp.find("ErrSinFore"),"slds-hide");
                    $A.util.addClass(cmp.find("PeriodoPick"),"slds-hide");
                    var aux =[];
                    cmp.set("v.Periodos",aux);
                }else{
                    $A.util.addClass(cmp.find("ErrSinFore"),"slds-hide");
                    for(var i = 0;i<result.length;i++){
                        var aux = new Object();
                        aux.Id=result[i].Id;
                        aux.Name=result[i].Name;
                        lst_periodos.push(aux);                        
                     }
                        cmp.set("v.Periodos",lst_periodos);
                        $A.util.removeClass(cmp.find("PeriodoPick"),"slds-hide");
                    }
                    console.log("Ahora hacemos visible");
                }else{
                    alert("Hubo un error accediendo a los periodos");
                }                  
            });
            $A.enqueueAction(action);
        }
        
    },
    changePeriodo : function(cmp,evt,helper){
        //AQUI invocamos para buscar el Forecast
        //para lo que buscamos el nodo y el periodo
        var nodo = cmp.find("IdNodo").get("v.value");
        var Idper = cmp.find("Idperio").get("v.value");
        console.log("HOLA"+Idper);
        var finishOpp=false;
        var finishFore=true;
        var finishObje=false;
        var finishAjus=false;
         $A.util.addClass(cmp.find("table"),"slds-hide");
        if(Idper=="N/A"){
            $A.util.addClass(cmp.find("fechasPeriodo"),"slds-hide");
            $A.util.addClass(cmp.find("DatosFore"),"slds-hide");
            $A.util.addClass(cmp.find("table"),"slds-hide");
        }else{
            var periodos={};
            var hijos={};
            var perBuscarNoW=[];
            var actionPer=cmp.get("c.getPeriodosForecast");
            actionPer.setParams({"idPeriodo":Idper});
            actionPer.setCallback(this,function(response){
                console.log("hicimos la de los peridos");
            if(response.getState()==="SUCCESS"){
                console.log("hicimos la de los peridos=SUCCEES");
                var result = response.getReturnValue();                                 
                for(var j = 0;j<result.length;j++){
                    periodos[result[j].Id]=result[j];
                    var lst_son = hijos[result[j].FO_Periodo_Padre__c];
                    if(lst_son==null){
                       lst_son=[]; 
                    }
                    lst_son.push(result[j]);
                    hijos[result[j].FO_Periodo_Padre__c]=lst_son;   
                    if(result[j].FO_Type__c!="Semana"){
                        perBuscarNoW.push(result[j].Id);
                    }
                }
                cmp.set("v.ChosenPer",periodos[Idper]);
                cmp.set("v.InfoPeriodos",periodos);
                cmp.set("v.InfoPeriodosHijos",hijos);
                $A.util.removeClass(cmp.find("fechasPeriodo"),"slds-hide");
                console.log(hijos);
                var periodosBuscar = [];
                periodosBuscar.push(Idper);
                console.log("PEriodos:"+periodos);
                var parent = periodos[Idper].FO_Periodo_Padre__c;
                var fechFin = "";
                var fechIni = "";
                //Esto es caso de tener que ir hacia arriba, ahora tendremos que mirar hacia abajo y ver sus hijos y susesivamente...
                while(parent!=null){
                    var listaHijos = hijos[parent];
                    console.log("Esto son los hijos del padre"+parent+" hijos:"+listaHijos);
                    if(periodos[parent]!=null){
                        fechIni=periodos[parent].BI_FVI_Fecha_Inicio__c;
                        fechFin=periodos[parent].BI_FVI_Fecha_Fin__c;
                        parent=periodos[parent].FO_Periodo_Padre__c;
                    }else{
                        parent=null;
                    }    
                }           
                var actionFo = cmp.get("c.getForecasts");
                actionFo.setParams({"periodo":Idper,"nodo":nodo});
                actionFo.setCallback(this,function(response2){
                    console.log("Forecast");
                    if(cmp.isValid() && response2.getState()==="SUCCESS"){
                        var result = response2.getReturnValue();
                        var mp ={};
                        console.log(result);
                        var fore=result[0];
                        cmp.set("v.ChosenFore",fore);
                        if(fore.RecordType.DeveloperName=="FO_Manager"){
                            cmp.set("v.Manager",true);
                        }else{
                            cmp.set("v.Manager",false);
                        }
                        console.log("@@@@Manager recibido: "+fore.RecordType.DeveloperName+" Estado del bicho: "+cmp.get("v.Manager"));
                        $A.util.removeClass(cmp.find("DatosFore"),"slds-hide");
                        var dateFine = periodos[Idper].BI_FVI_Fecha_Fin__c;
                        
                        var actionForeAjust = cmp.get("c.getAjustes");
                        actionForeAjust.setParams({"IdFore":fore.Id});
                        actionForeAjust.setCallback(this,function(response2){
                            console.log("Ajus:"+fore.Id);
                            if(response2.getState()==="SUCCESS"){
                                var result = response2.getReturnValue();
                                var mp_ajus = {};
                                for(var z=0;z<result.length;z++){
                                    mp_ajus[result[z].FO_Periodo__c]=result[z];
                                    console.log("Ajus["+result[z].FO_Periodo__c+"] :"+result[z]);
                                }
                                console.log("Ajustado:"+mp_ajus);
                                cmp.set("v.ForeAjus",mp_ajus);
                                finishAjus=true;
                                console.log("AJ");
                                debugger;
                                if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                                    //Llamamos al helper para pinta la tabla dinamica.
                                    helper.drawTable(cmp,helper);
                                }
                            }
                        });
                        $A.enqueueAction(actionForeAjust);
                        
                    }else if(response2.getState()==="ERROR"){
                       var errors = response2.getError();
                        if (errors) {                        
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +
                                 errors[0].message);
                            }
                        }
                    }
                });
                var actionOpp = cmp.get("c.getOpps");
                actionOpp.setParams({"fechIni":fechIni,"fechFin":fechFin,"idNodo":nodo});
                actionOpp.setCallback(this,function(response2){
                    console.log("Oppp");
                    if(cmp.isValid() && response2.getState()==="SUCCESS"){
                        cmp.set("v.Opps",response2.getReturnValue());
                        
                        var fechIniPer=periodos[Idper].BI_FVI_Fecha_Inicio__c;
                        var fechFinPer=periodos[Idper].BI_FVI_Fecha_Fin__c;
                        var result=response.getReturnValue();
                        var oppRes = new Object();
                        oppRes.total=0;
                        oppRes.commit=0;
                        oppRes.upside=0;
                        oppRes.non=0;
                        for(var i = 0;i<response.length;i++){
                            if(result[i].CloseDate>=fechIniPer && result[i].CloseDate<=fechFinPer){
                                var op =result[i];
                                oppRes.total+=op.BI_Net_annual_value_NAV__c;
                                if(op.BI_O4_Committed__c==true){
                                    oppRes.commit+=op.BI_Net_annual_value_NAV__c;
                                }else if(op.BI_O4_Upside__c==true){
                                    oppRes.upside+=op.BI_Net_annual_value_NAV__c;
                                }else{
                                    oppRes.non+=op.BI_Net_annual_value_NAV__c;
                                }
                            }
                        }
                        cmp.set("v.ChosenPipe",oppRes);
                        finishOpp=true;
                        debugger;
                        if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                            //Llamamos al helper para pinta la tabla dinamica.
                            helper.drawTable(cmp,helper);
                        }
                    }else if(response2.getState()==="ERROR"){
                        var errors = response2.getError();
                        if(errors){
                            if(errors[0] && errors[0].message){
                                console.log("Error message en Opp:"+errors[0].message);
                            }
                            alert("Hubo un error buscando las Oportunidades para esta configuración, por favor dirigete a tu adminsitrador");
                        }
                    }
                });
                
                var actionObj = cmp.get("c.getObjetivo");
                actionObj.setParams({"nodo":nodo,"fechIni":fechIni,"fechFin":fechFin,"periodos":perBuscarNoW});
                actionObj.setCallback(this,function(response2){
                    console.log("Objetivo");
                     if(cmp.isValid() && response2.getState()==="SUCCESS"){
                        var result = response2.getReturnValue();
                         var mp = {};
                         for(var i = 0;i<result.length;i++){
                             mp[result[i].BI_FVI_IdPeriodo__c]=result[i];
                         }
                         cmp.set("v.Objetivo",mp);
                         finishObje=true;
                         debugger;
                         if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                            //Llamamos al helper para pinta la tabla dinamica.
                            helper.drawTable(cmp,helper);
                         }
                    }else if(response2.getState()==="ERROR"){
                        var errors = response2.getError();
                        if(errors){
                            if(errors[0] && errors[0].message){
                                console.log("Error message en Obj:"+errors[0].message);
                            }
                            alert("Hubo un error buscando los Objetivos para esta configuración, por favor dirigete a tu adminsitrador");
                        }
                    }                 
                });
                
                $A.enqueueAction(actionFo);
                $A.enqueueAction(actionOpp);
                $A.enqueueAction(actionObj);
                
            }                     
            });                                 
            $A.enqueueAction(actionPer);
            
        }
        
       
    },
    changeFore : function(cmp,evt,help){
        debugger;
        var spinner = cmp.find('spinnerCharged');        
        $A.util.removeClass(spinner,'slds-hide');
        var periodos = cmp.get("v.InfoPeriodos");
        var idPer = evt.getParam("Id_per");
        var value = evt.getParam("Value_Fore");
        var mpFore = cmp.get("v.MpFore");
        //var foreNot = cmp.get("v.ForeNot");
        var pipePer = cmp.get("v.totalPipePer");
        var actuals = cmp.get("v.ActualsSort");
        var objetivo = cmp.get("v.ObjetivoSort");
        //Vamos a ver si funciona
        //cmp.set("v.NameSort",nameSort);
        //cmp.set("v.ObjetivoSortAcum",objAcumTabla);
        //cmp.set("v.ActualsSort",actuals);
        //cmp.set("v.ActualsSortAcum",actualsAcum);
        //cmp.set("v.ActualsWeekSortAcum",weeksActualForm);
        //cmp.set("v.OpenForecast",foreOpen);
        //cmp.set("v.CloseForecast",foreClosed);
        //cmp.set("v.TotalFore",totalFore);      
        //cmp.set("v.weeksActualForm",weeksForeForm);
        //cmp.set("v.pipeForStage",pipeForStage);
        //cmp.set("v.TotalForeAjust",foreAjust);
        var nameSort = cmp.get("v.NameSort");
        var objAcumTabla = cmp.get("v.ObjetivoSortAcum");
        var actuals = cmp.get("v.ActualsSort");
        var actualsAcum = cmp.get("v.ActualsSortAcum");
        var weeksActualForm = cmp.get("v.ActualsWeekSortAcum");
        var foreOpen = cmp.get("v.OpenForecast");
        var foreClosed = cmp.get("v.CloseForecast");
        var totalFore = cmp.get("v.TotalFore");
        var weeksForeForm = cmp.get("v.weeksActualForm");
        var pipeForStage = cmp.get("v.pipeForStage");
        var foreAjust = cmp.get("v.TotalForeAjust");
        var conversionForePipe =[];
        var exitFore=[];
        var coberturaExitFore=[];
        var idAux = cmp.get("v.recordId");
        if(idAux==null){
            var idSelect = cmp.find("Idperio").get("v.value");
            var fechIni = periodos[cmp.find("Idperio").get("v.value")].BI_FVI_Fecha_Inicio__c;
            var fechFin = periodos[cmp.find("Idperio").get("v.value")].BI_FVI_Fecha_Fin__c;
        }else{
            var idSelect=cmp.get("v.PeriodoWId");
            var fechIni = periodos[cmp.get("v.PeriodoWId")].BI_FVI_Fecha_Inicio__c;
            var fechFin = periodos[cmp.get("v.PeriodoWId")].BI_FVI_Fecha_Fin__c;
        }
        
        if(cmp.get("v.Manager")==false)
            mpFore[idPer].valueFore=value;
        else
            mpFore[idPer].AjusManager=value;
        var keys = [];
        var periSon = cmp.get("v.InfoPeriodosHijos");
        for(var key in periodos)
            if(periodos[key].FO_Type__c!="Semana")
                keys.push(key);
        for(var pa in periSon){
            var sum = 0;
            var sumAjus=0;

            if(pa!=null && pa!="undefined"){
                debugger;
                if(periodos[pa].FO_Type__c!="Mes" || (periodos[pa].FO_Type__c=="Mes" && cmp.get("v.isMNC")==false)){
                    for(var j = 0;j<periSon[pa].length;j++){
                    if(mpFore[periSon[pa][j].Id]!=null){        
                        sum+=Number(mpFore[periSon[pa][j].Id].valueFore);
                        if(mpFore[periSon[pa][j].Id].AjusManager!=null)
                            sumAjus+=Number(mpFore[periSon[pa][j].Id].AjusManager);
                    }
                    
                }
                if(mpFore[pa]!=null ){
                    mpFore[pa].valueFore=sum;
                    if(pa!=periodos[idSelect].FO_Periodo_Padre__c)
                        mpFore[pa].AjusManager=sumAjus;
                }   
                }
                 
                    
            }
            
           
        }
        keys.sort(function(a,b){
           var fechIniA = periodos[a].BI_FVI_Fecha_Fin__c;
           var fechIniB = periodos[b].BI_FVI_Fecha_Fin__c;
            if(fechIniA>fechIniB){
                return 1;
            }else if(fechIniA<fechIniB){
                return -1;
            }else{
                var fechFinA = periodos[a].BI_FVI_Fecha_Inicio__c;
                var fechFinB = periodos[b].BI_FVI_Fecha_Inicio__c;
                if(fechFinA>fechFinB){
                    return -1;
                }else
                    return 1;
            }
        });
        var foreAjust = new Object();
        foreAjust.ant=[];
        foreAjust.futM=[];
        foreAjust.actQ=[];
        foreAjust.fut=[];
        foreAjust.year=[];
        //fore Normal
        var foreNot = new Object();
        foreNot.ant=[];
        foreNot.actMonth=[];
        foreNot.futMonth=[];
        foreNot.actQ=[];
        foreNot.futQ=[];
        foreNot.year=0;
        for(var j = 0;j<keys.length;j++){
           var per = periodos[keys[j]];
            
            mpFore[per.Id].AjusReal=Number(mpFore[per.Id].valueFore)+Number(mpFore[per.Id].AjusManager);    
                if(per.BI_FVI_Fecha_Fin__c<fechFin){
                    console.log("Fore de los antiguos: "+mpFore[per.Id].valueFore);
                    foreNot.ant.push(mpFore[per.Id]);
                    foreAjust.ant.push(mpFore[per.Id]);
                }else if(per.FO_Type__c=="Mes" && per.BI_FVI_Fecha_Inicio__c<=fechIni){
                    console.log("Fore de los mes actual: "+mpFore[per.Id].valueFore);
                    foreNot.actMonth.push(mpFore[per.Id]);
                    foreAjust.futM.push(mpFore[per.Id]);
                }else if(per.FO_Type__c=="Mes"){
                    console.log("Fore de los meses futuros: "+mpFore[per.Id].valueFore);
                    foreNot.futMonth.push(mpFore[per.Id]);
                    foreAjust.futM.push(mpFore[per.Id]);
                }else if(per.FO_Type__c=="Trimestre" && per.BI_FVI_Fecha_Inicio__c<=fechIni){
                    console.log("Fore de los trimestres actuales: "+mpFore[per.Id].valueFore);                    
                    foreNot.actQ.push(mpFore[per.Id]);
                    foreAjust.actQ.push(mpFore[per.Id]);
                }else if(per.FO_Type__c=="Trimestre"){
                    console.log("Fore de los trimestres futuros: "+mpFore[per.Id].valueFore);
                    foreNot.futQ.push(mpFore[per.Id]);
                    foreAjust.fut.push(mpFore[per.Id]);
                }else{
                    foreNot.year=mpFore[per.Id];
                    foreAjust.year.push(mpFore[per.Id]);
                } 
            if(per.BI_FVI_Fecha_Fin__c<fechFin){
                conversionForePipe.push("");
                exitFore.push("");
                coberturaExitFore.push("");
            }else{
                if(objetivo[j]==0 || objetivo[j]=="N/A"){
                    coberturaExitFore.push("N/A");
                }else{
                    coberturaExitFore.push(((mpFore[per.Id].AjusReal+actuals[j])/objetivo[j]).toFixed(2));
                }
                exitFore.push((mpFore[per.Id].AjusReal+actuals[j]).toFixed(2));
                if(pipePer[j]==0){
                    conversionForePipe.push("N/A");
                }else{
                    conversionForePipe.push((mpFore[per.Id].AjusReal/pipePer[j]).toFixed(2));
                }
            }
        }
       		debugger;
            var spinner = cmp.find('spinnerCharged');        
        	$A.util.removeClass(spinner,'slds-hide');
            cmp.set("v.ForeNot",foreNot);
        //JLA_TEST_SOLUTIONS SUMMER 18
        cmp.set("v.ForeNotANT",foreNot.ant);
        cmp.set("v.ForeNotACTM",foreNot.actMonth);
        cmp.set("v.ForeNotFUTM",foreNot.futMonth);
        cmp.set("v.ForeNotACTQ",foreNot.actQ);
        cmp.set("v.ForeNotFUTQ",foreNot.futQ);
        cmp.set("v.ForeNotYear",foreNot.year);
        
        cmp.set("v.TotalForeAjustANT",foreAjust.ant);
        cmp.set("v.TotalForeAjustFUTM",foreAjust.futM);
        cmp.set("v.TotalForeAjustACTQ",foreAjust.actQ);
        cmp.set("v.TotalForeAjustFUTQ",foreAjust.fut);
        cmp.set("v.TotalForeAjustYear",foreAjust.year);
        //JLA_TEST_SOLUCTINON
        	cmp.set("v.MpFore",mpFore);
        	cmp.set("v.conversionForePipe",conversionForePipe);
        	cmp.set("v.exitFore",exitFore);
        	cmp.set("v.coberturaExitFore",coberturaExitFore);
        	cmp.set("v.TotalForeAjust",foreAjust);
            cmp.set("v.NameSort",nameSort);
        	cmp.set("v.ObjetivoSortAcum",objAcumTabla);
        	cmp.set("v.ActualsSort",actuals);
        	cmp.set("v.ActualsSortAcum",actualsAcum);
        	cmp.set("v.ActualsWeekSortAcum",weeksActualForm);
        	cmp.set("v.OpenForecast",foreOpen);
        	cmp.set("v.CloseForecast",foreClosed);
        	cmp.set("v.TotalFore",totalFore);      
        	cmp.set("v.weeksActualForm",weeksForeForm);
        	cmp.set("v.pipeForStage",pipeForStage);
        	cmp.set("v.TotalForeAjust",foreAjust);
        	var spinner = cmp.find('spinnerCharged');  
            $A.util.addClass(spinner,'slds-hide');
    },
    refresh :function(cmp,evt,helper){
        helper.recordSelected(cmp,evt,helper);
    },
    compactExpandRowObj :function(cmp, event, helper) {
        
        //alert("entra en compactExpandRowObj");
        var bool = cmp.get("v.flagObj");
        var tr = cmp.find("tr-weeks");
        debugger;
        if(bool==false) {
            cmp.set("v.flagObj",true);
            
            $A.util.removeClass(tr,"slds-hide");
            $A.util.removeClass(cmp.find("tr-acts"),"slds-hide");
            
            cmp.set("v.actualsSymbol", "[-]");
        }
        else{
            cmp.set("v.flagObj",false); 
            
            $A.util.addClass(tr,"slds-hide");
            $A.util.addClass(cmp.find("tr-acts"),"slds-hide");
            
            cmp.set("v.actualsSymbol", "[+]");
        }
    },
    
     refrescaElCampo :function(cmp,evt,help){
        debugger;
        var test = cmp.get("v.test");
        test =test*-1;
        cmp.set("v.test",test);
    },
    compactExpandRowAct :function(cmp, event, helper) {

        var bool = cmp.get("v.flagAct");
        debugger;
        var tr = cmp.find("tr-weeks");
        if(bool==false) {
            $A.util.removeClass(tr,"slds-hide");
            $A.util.removeClass(cmp.find("tr-acts"),"slds-hide");
            
            
            
            cmp.set("v.flagAct",true);
            cmp.set("v.actualsSymbol", "[+]");
    }
        else{
            $A.util.addClass(tr,"slds-hide");
            $A.util.addClass(cmp.find("tr-acts"),"slds-hide");
            cmp.set("v.flagAct",false); 
            cmp.set("v.actualsSymbol", "[-]");
        }
    },
    //JLA systemError handler 20180709
    sysErrorHandler : function(cmp,evt,hel){
        console.log("Messagge: "+evt.getParams("message"));
        console.log("Error: "+evt.getParams("error"));
    }
   
})