({
	handleRecordUpdated : function(cmp, event, helper) {
		var eve = event.getParams();
		
		console.log("hemos entrado en el manage de eventos");
		//$A.get("e.force:refreshView").fire();
		$A.util.removeClass(cmp.find("detail"),"slds-hide");
	},
	doNothing :function(cmp,evt){
		var sub = cmp.get("v.submitted");
		if(sub==false){
			cmp.find("but-submit").set("v.disabled",false);
		}
	},
	changeStatusComponent : function(cmp,event,helper){
		var state = cmp.get("v.simpleRecord.FO_Estado__c");
		$A.util.addClass(cmp.find("SaveBut"),"slds-hide");
		$A.util.removeClass(cmp.find("CancelBut"),"slds-hide");
		$A.util.addClass(cmp.find("modal-Status")," slds-fade-in-open");
		 if(state=="Enviado"){
			cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Subi_Submited"));
			$A.util.addClass(cmp.find("mem-grid"),"slds-hide");
		 }else{
			 //JLA_13/04/2018_Start Se cambia para ver que nodos inferiores no han realizado el Forecast 
			 var actionRT = cmp.get("c.getRT");
			 actionRT.setParams({"Id":cmp.get("v.recordId")});
			 actionRT.setCallback(this,function(responseRT){
				 var action;
				 //JLA Avoid for MNCs
				 if(responseRT.getReturnValue()=="FO_Ejecutivo" || responseRT.getReturnValue()=="FO_VE_Overlay" || responseRT.getReturnValue()=="FO_MNC_Ejecutivo" ){    
					 var action = cmp.get("c.getOpps");
					 action.setParams({"Id":cmp.get("v.recordId")});
					 action.setCallback(this,function(response) {
						 var result = response.getReturnValue();
						 var members=[];
						 if(result!=null && result.length>0){
							 var myColumns=[];
							 var objName = new Object();
							 objName.label="Nombre Opp";	
							 objName.fieldName="Name";
							 objName.type="text";
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="Nombre cliente";	
							 objName.fieldName="NameUser";
							 objName.type="text";
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="NAV";	
							 objName.typeAttributes={"minimumFractionDigits":"2"};
							 objName.fieldName="LastUser";
							 objName.type="number";
							 myColumns.push(objName);
							 objName= new Object();
							 objName.label="MONEDA";	
							 objName.fieldName="currencyIso";
							 objName.type="text";

							 
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="Aviso";	
							 objName.fieldName="War";
							 objName.type="text";
							 myColumns.push(objName);
							 var cab=[];
							 cab.push("Nombre de la Oportunidad");
							 cab.push("Nombre del cliente");
							 cab.push("NAV");
							 cab.push("Aviso");
							 cmp.set("v.cab",cab);
							 cmp.set("v.myColumns",myColumns);
							 for(var i = 0;i<result.length;i++){
								 var rec = result[i];
								 var obj = new Object();
								 obj.Name = rec.FO_Opportunity__r.Name
								 obj.NameUser = rec.FO_Cliente__c;
								 obj.LastUser = rec.FO_NAV__c;
								 obj.currencyIso=rec.CurrencyIsoCode;
								 obj.War ="Esta oportunidad no esta incluida en este Forecast";
								 members.push(obj);
							 }
							 cmp.set("v.members",members);
							 cmp.set("v.tabHead",$A.get("$Label.c.FO_FOI_Pend_title"));
							 $A.util.removeClass(cmp.find("mem-grid"),"slds-hide");
							 $A.util.removeClass(cmp.find("div-sec"),"slds-hide");
							 $A.util.addClass(cmp.find("modal-Status"),"slds-modal_large");
					}else{
						 $A.util.addClass(cmp.find("mem-grid"),"slds-hide");
						 $A.util.addClass(cmp.find("div-sec"),"slds-hide");
						 $A.util.removeClass(cmp.find("modal-Status"),"slds-modal_large");
					}
					$A.util.removeClass(cmp.find("OKBut"),"slds-hide");     
					cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Sure_Submit"));
					});
					 $A.enqueueAction(action); 
					 //JLA_V_1_3 vcambios para ver si los FOF se han cambiado o no
					 var actionFOF = cmp.get("c.getFOF");
					 actionFOF.setParams({"idFore":cmp.get("v.recordId"),"manager":false,"idper":cmp.get("v.simpleRecord.FO_Periodo__c"),"recordTypeDev":responseRT.getReturnValue()});
					 actionFOF.setCallback(this,function(responseFOF){
						 if(responseFOF.getState()==="SUCCESS"){
							 
							 var res = responseFOF.getReturnValue();
							 if(res.length!=0){
								 var colsFOF=[];
								 var campo = new Object();
								 campo.label="Periodo";
								 campo.fieldName="Periodo";
								 campo.type="text";
								 colsFOF.push(campo);
								 var memsFOF=[];
								 campo = new Object();
								 campo.label="Aviso";
								 campo.fieldName="war";
								 campo.type="text";
								 colsFOF.push(campo);
								 cmp.set("v.ColumnsFOF",colsFOF);
								 for(var i = 0;i<res.length;i++){
									 var mem = new Object();
									 mem.Periodo=res[i].FO_Periodo__r.Name;
									 mem.war="Valor de Forecast a 0";
									 memsFOF.push(mem);
								 }
								 
								 cmp.set("v.membersFOF",memsFOF);
								 cmp.set("v.tabHeadFOF",$A.get("$Label.c.FO_FOF_Pending"));
								 $A.util.removeClass(cmp.find("mem-grid-fof"),"slds-hide");
								 $A.util.removeClass(cmp.find("div-sec-fof"),"slds-hide");
								 $A.util.addClass(cmp.find("modal-Status"),"slds-modal_large");
							 }else{
								 $A.util.addClass(cmp.find("mem-grid-fof"),"slds-hide");
								 $A.util.addClass(cmp.find("div-sec-fof"),"slds-hide");
							 }
						 }
					 });
					 $A.enqueueAction(actionFOF);
				 }else{
					 
					 var action = cmp.get("c.validaSub");
					 action.setParams({"Id":cmp.get("v.recordId")});
					 action.setCallback(this,function(response) {
						 var result = response.getReturnValue();
						 var members=[];
						 if(result!=null && result.length>0){
							 var myColumns=[];
							 var objName = new Object();
							 objName.label="Nombre nodo";	
							 objName.fieldName="Name";
							 objName.type="text";
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="Nombre usuario";	
							 objName.fieldName="NameUser";
							 objName.type="text";
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="Apellidos usuario";	
							 objName.fieldName="LastUser";
							 objName.type="text";
							 myColumns.push(objName);
							 objName=new Object();
							 objName.label="Aviso";	
							 objName.fieldName="War";
							 objName.type="text";
							 myColumns.push(objName);
							 cmp.set("v.myColumns",myColumns);
							 var cab = [];
							 cab.push("Nombre nodo");
							 cab.push("Nombre usuario");
							 cab.push("Apellidos usuario");
							 cab.push("Aviso");
							 cmp.set("v.cab",cab);
							 for(var i = 0;i<result.length;i++){
								 var spliter=result[i].split('|||');
								 var obj = new Object();
								 obj.Name = spliter[0];
								 obj.NameUser = spliter[1];
								 obj.LastUser = spliter[2];
								 obj.War = spliter[3];
								 members.push(obj);
							 }
							 cmp.set("v.members",members);
							 cmp.set("v.tabHead",$A.get("$Label.c.FO_FO_Pend_title"));
							 $A.util.removeClass(cmp.find("mem-grid"),"slds-hide");
							 $A.util.removeClass(cmp.find("div-sec"),"slds-hide");
							 $A.util.addClass(cmp.find("modal-Status"),"slds-modal_large");
                             if(responseRT.getReturnValue()=="FO_VE_Overlay_Manager"){
                             $A.util.removeClass(cmp.find("OKBut"),"slds-hide");
                                cmp.set("v.buttonDisabled",true); 
                             }
                             
					}else{
						 $A.util.addClass(cmp.find("mem-grid"),"slds-hide");
						 $A.util.removeClass(cmp.find("modal-Status"),"slds-modal_large");
						 $A.util.addClass(cmp.find("div-sec"),"slds-hide");
					}
					$A.util.removeClass(cmp.find("OKBut"),"slds-hide");     
					cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Sure_Submit"));
				});
					//JLA_13/04/2018_End
					$A.enqueueAction(action);
					 //JLA_V1_3_Comprbar FOF a 0
					 var actionFOF = cmp.get("c.getFOF");
					 actionFOF.setParams({"idFore":cmp.get("v.recordId"),"manager":true,"idper":cmp.get("v.simpleRecord.FO_Periodo__c"),"recordTypeDev":responseRT.getReturnValue()});
					 actionFOF.setCallback(this,function(responseFOF){
						 if(responseFOF.getState()==="SUCCESS"){
							 
							 var res = responseFOF.getReturnValue();
							 if(res.length!=0){
								 var colsFOF=[];
								 var campo = new Object();
								 campo.label="Periodo";
								 campo.fieldName="Periodo";
								 campo.type="text";
								 colsFOF.push(campo);
								 campo = new Object();
								 campo.label="Aviso";
								 campo.fieldName="war";
								 campo.type="text";
								 colsFOF.push(campo);
								 var memsFOF=[];
								 for(var i = 0;i<res.length;i++){
									 var mem = new Object();
									 mem.Periodo=res[i].FO_Periodo__r.Name;
									 mem.war="Periodo no ajustado";
									 memsFOF.push(mem);
								 }
								 cmp.set("v.ColumnsFOF",colsFOF);
								 cmp.set("v.membersFOF",memsFOF);
								 cmp.set("v.tabHeadFOF",$A.get("$Label.c.FO_FOF_Ajus"));
								 $A.util.removeClass(cmp.find("mem-grid-fof"),"slds-hide");
								 $A.util.removeClass(cmp.find("div-sec-fof"),"slds-hide");
								 $A.util.addClass(cmp.find("modal-Status"),"slds-modal_large");
							 }else{
								 $A.util.addClass(cmp.find("mem-grid-fof"),"slds-hide");
								 $A.util.addClass(cmp.find("div-sec-fof"),"slds-hide");
							 }
						 }
					 });
					 $A.enqueueAction(actionFOF);
				}  
			 });
			 $A.enqueueAction(actionRT);
			 
		
			 
		 }
			
	},
	save :function (cmp,evt,help){
		if((navigator.userAgent.indexOf("MSIE") != -1 )){
			window.location.href = window.location.href
		}else{
			window.location.reload(true);    
		}
		
	},
	cancel :function (cmp,evt,help){
		cmp.set("v.myColumns",[]);
		cmp.set("v.cab",[]);
		$A.util.addClass(cmp.find("mem-grid"),"slds-hide");
		$A.util.addClass(cmp.find("div-sec"),"slds-hide");
		
		$A.util.removeClass(cmp.find("modal-Status")," slds-fade-in-open");
		
		//$A.get("e.force:closeQuickAction").fire();
	},
	refresh : function(cmp,evt,help){
		
		
	   // $A.get("e.force:refreshView").fire();
		if((navigator.userAgent.indexOf("MSIE") != -1 ) || navigator.userAgent.indexOf('Edge') >= 0){
			
			window.location.href = window.location.href;
			return false;
		}else{
			window.location.reload(true);    
		}
	},
	goForward : function(cmp,evt,help){
		var Id = cmp.get("v.recordId");
		$A.util.addClass(cmp.find("OKBut"),"slds-hide"); 
		$A.util.addClass(cmp.find("CancelBut"),"slds-hide");
			console.log("Hemos entrado");
			var action = cmp.get("c.changeStatus");
			action.setParams({"Id":Id});
			action.setCallback(this,function(response){
			   // $A.get("e.force:closeQuickAction").fire() ;
				if(response.getState()==="SUCCESS"){
				   
					if(response.getReturnValue()=="KO"){
					   // alert($A.get("$Label.c.FO_Err_Super_Submited"));
						$A.util.removeClass(cmp.find("CancelBut"),"slds-hide");
						cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Err_Super_Submited"));
					}else if(response.getReturnValue()=="KO2"){
						 $A.util.removeClass(cmp.find("CancelBut"),"slds-hide");
						cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Subi_Submited"));        
					}else{
						//alert($A.get("$Label.c.FO_Upd_succ"));
						var resultsToast = $A.get("e.force:showToast");
						resultsToast.setParams({
							"title": "",
							"message": $A.get("$Label.c.FO_Upd_succ"),
							"type":"success"
						});
						resultsToast.fire();
						var navEvt = $A.get("e.force:navigateToSObject");
						navEvt.setParams({
							"recordId": Id
						});
						navEvt.fire();

						
						//$A.util.addClass(cmp.find("CancelBut"),"slds-hide");
						//$A.util.removeClass(cmp.find("SaveBut"),"slds-hide");
						//cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Upd_succ"));
						//window.location.reload();
					}
					
				}else{
					 //alert($A.get("$Label.c.FO_Err_Upd_FO")+": "+response.getError()[0].message);
					 cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Err_Upd_FO"));
				}
			});
			$A.enqueueAction(action);
		
		 
	},
	/************************************
	* @author : Javier Lopez Andradas
	* @company: gCTIO
	* @Description: doInit function,when the component is loaded


	* <date>        <version>       <description>   
	* ?????			1.0				Initial
	* 29/08/2018	1.3				delete call to validaOverlay
	***************************************/

	doInit: function(cmp,evt,hel){
		var action = cmp.get("c.checkSubmitted");
		
		action.setParams({"Id":cmp.get("v.recordId")});
		action.setCallback(this,function(response){
			var result = response.getReturnValue();
			if(result=='KO'){
				cmp.set("v.submitted",true);
			}else{
				cmp.set("v.submitted",false);
			}
			
				
		});
		$A.enqueueAction(action);
		/* JLA_ Not needed 
		var actionRT  =cmp.get("c.validaOverlay");
		actionRT.setParams({"idFore":cmp.get("v.recordId")});
		actionRT.setCallback(this,function(response){
			var result =response.getReturnValue();
			if(result== true)
				cmp.find("but-submit").set("v.disabled",false);
		});
		$A.enqueueAction(actionRT);*/
		
	}
	
})