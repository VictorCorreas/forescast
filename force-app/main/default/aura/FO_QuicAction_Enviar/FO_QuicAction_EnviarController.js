({
	changeStatusComponent : function(cmp,event,helper){
        var Id = cmp.get("v.recordId");
        var state = cmp.get("v.simpleRecord.FO_Estado__c");
        console.log("Estado: "+state);
        $A.util.addClass(cmp.find("SaveBut"),"slds-hide");
        if(state=="Enviado"){
            cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Subi_Submited"));
        }else{
            console.log("Hemos entrado");
            var action = cmp.get("c.changeStatus");
            action.setParams({"Id":Id});
            action.setCallback(this,function(response){
               // $A.get("e.force:closeQuickAction").fire() ;
                if(response.getState()==="SUCCESS"){
                    if(response.getReturnValue()=="KO"){
                       // alert($A.get("$Label.c.FO_Err_Super_Submited"));
                        $A.util.removeClass(cmp.find("CancelBut"),"slds-hide");
                        cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Err_Super_Submited"));
                    }else if(response.getReturnValue()=="KO2"){
                         $A.util.removeClass(cmp.find("CancelBut"),"slds-hide");
                     	cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Subi_Submited"));        
                    }else{
                    	//alert($A.get("$Label.c.FO_Upd_succ"));
                    	
                        $A.util.addClass(cmp.find("CancelBut"),"slds-hide");
                    	$A.util.removeClass(cmp.find("SaveBut"),"slds-hide");
                    	cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Upd_succ"));
                    	//window.location.reload();
                    }
                    
                }else{
                   	 //alert($A.get("$Label.c.FO_Err_Upd_FO")+": "+response.getError()[0].message);
                   	 cmp.find("textModal").set("v.value",$A.get("$Label.c.FO_Err_Upd_FO"));
                }
            });
            $A.enqueueAction(action);
        }
         
        
    },
    save :function (cmp,evt,help){
        if((navigator.userAgent.indexOf("MSIE") != -1 )){
            window.location.href = window.location.href
        }else{
        	window.location.reload(true);    
        }
    	
	},
    cancel :function (cmp,evt,help){
        $A.get("e.force:closeQuickAction").fire();
	}
})