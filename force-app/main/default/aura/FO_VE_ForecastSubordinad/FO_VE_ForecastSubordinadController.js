({
	navigateTOForecast : function(cmp, evt, helr) {
        var navToObjeto = $A.get("e.force:navigateToSObject");
        navToObjeto.setParams({"recordId":cmp.get("v.actFore.Id")});
        navToObjeto.fire();
	},
    navigateTONode :function(cmp,evt,help){
        var navToObjeto = $A.get("e.force:navigateToSObject");
        navToObjeto.setParams({"recordId":cmp.get("v.actFore.FO_Nodo__c")});
        navToObjeto.fire();
    },
    doInit: function(cmp,evt,help){
        help.setButton(cmp);    
        help.findForeVE(cmp);
    },
    returnFore:function(cmp,evt,help){
        debugger;
      help.returnForeHelp(cmp);  
    },
})