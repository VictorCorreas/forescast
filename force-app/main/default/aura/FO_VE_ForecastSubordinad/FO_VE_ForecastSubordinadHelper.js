({
	findForeVE : function(cmp) {
		var action=cmp.get("c.getFof");
        action.setParams({"IdFore":cmp.get("v.actFore.Id"),"per":cmp.get("v.actFore.FO_Periodo__r.FO_Periodo_Padre__c")});
        action.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
				$A.util.removeClass(cmp.find("normal"),"slds-hide");
                $A.util.addClass(cmp.find("error"),"slds-hide");       
                cmp.set("v.Fof",response.getReturnValue());
            }else{
                $A.util.removeClass(cmp.find("error"),"slds-hide");
                $A.util.addClass(cmp.find("normal"),"slds-hide");
                var err =response.getError();
                var res = "ERROR: "+err[0].message;
                cmp.set("v.error",res);
            }
        });
       	$A.enqueueAction(action);
	},
    setButton :function(cmp){
      if(cmp.get("v.actFore.FO_Estado__c")==="Borrador" || cmp.get("v.Status_Parent")==="Enviado"){
            cmp.find("return-but").set("v.disabled",true);
            cmp.find("return-but").set("v.title","No se puede devolver este Forecast");
        }else{
            cmp.find("return-but").set("v.disabled",false);
            cmp.find("return-but").set("v.title","Pulsa para devolver este Forecast");
        }          
    },
    returnForeHelp : function(cmp){
        debugger;
        var action = cmp.get("c.devolverForecast");
        action.setParams({"IdFore":cmp.get("v.actFore.Id")});
        action.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                var res = response.getReturnValue();
                var toast = $A.get("e.force:showToast");
                if(res==="OK"){
                    toast.setParams({
                        "title":"Éxito",
                        "message":"Forecast devuelto correctamente",
                        "type":"success"
                    });
                    toast.fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
					navEvt.setParams({
						"recordId": cmp.get("v.Id_Parent")
					});
					navEvt.fire();
                }else{
                    toast.setParams({
                        "title":"ERROR",
                        "message":res,
                        "type":"error"
                    });
                    toast.fire();
                }
                
            }
        });
        $A.enqueueAction(action);
    }
})