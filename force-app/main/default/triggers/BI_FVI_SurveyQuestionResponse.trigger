/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   Trigger on Object SurveyTaker__c  
    Test Class:    BI_FVI_SurveyTakerMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    01/06/2016           	Geraldine P. Montes     Initial Version
    22/07/2016				Alvaro Sevilla			Se adicionó invocacion a la clase BI_FVI_EmailRespuestasEncuesta
    20/07/2017      		Guillermo Muñoz     	Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger BI_FVI_SurveyQuestionResponse on SurveyQuestionResponse__c (before insert, before update, before delete, after insert, after update, after delete, after undelete){
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('SurveyQuestionResponse__c');
    
    if(!isTriggerDisabled){

		if (Trigger.isBefore) {
	    	//call your handler.before method
	    
		} else if (Trigger.isAfter) {
	    	if(Trigger.isInsert)
	    	{
	    		BI_FVI_SurveyQuestionResponseMethods.CreateRegistroDesemp(Trigger.new);
	    		BI_FVI_EmailRespuestasEncuesta.EnviarEmailRespuestasEncuesta(Trigger.new);
	    	}
	    
		}
	}
}