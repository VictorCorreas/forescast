/*******************************************************
 * @author : Javier Lopez Andradas
 * @comapny:gCTIO
 * @description: Triggers that controls the actions over Snapshot of piepelines, born with FO_VE project
 * 
 * <date>		<version>		<description>
 * 09/08/2018	1.0				Initial
 * 
 * *******************************************************/
trigger FO_Snapshot_Pipeline on FO_Snapshot_Pipeline__c (before insert,before update,before delete) {
	
}