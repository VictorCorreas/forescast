/*------------------------------------------------------------
    Author:         Jorge Galindo 
    Company:        New Energy Aborda
    Description:    Trigger to delete OI and child OI Attributes when NE__Status__c = 'FVI Borrar'
    History
    <Date>          <Author>                <Change Description>
    23/06/2016      Jorge Galindo           Creation
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_FVIBorrar on NE__OrderItem__c (after update){

  
  //GMN 20/07/2017
  Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__OrderItem__c');
    
  if(!isTriggerDisabled){

    /*if (trigger.isBefore) // ANTES DE... 
    {
        if (trigger.isInsert)
        {

        }

        if (trigger.isUpdate)
        {

        }             
    }*/

    if (trigger.isAfter){ // DESPUES DE... 
      /*if (trigger.isInsert){

      }*/
      if (trigger.isUpdate){
          BI_FVIBorrarMethods.Borrar(Trigger.new);
      }             
    }         
  }
}