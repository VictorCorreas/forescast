/************************************************************************************
 * Author: JAvier Lopez Andradas
 * Company: gCTIO
 * Description: TRigger for object FO_Forecast__c
 * 
 * 
 * <date>		<version>		<description>
 * 02/04/2018		1.0		Initial
 * 29/05/2018		1.1		Added FO_ForecastMethodsAllView.aactualizaFOGAM
 * 08/06/2018		1.2		Deleted all FO_ForecastMethodsAllView.aactualizaFOGAM
 * ***************************************************************************************/
trigger FO_Forecast on FO_Forecast__c (before insert,after insert,before Update,after Update) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            FO_ForecastMethods.changeRT(trigger.New);
            FO_ForecastMethods.createForecastFutur(trigger.New);
            FO_ForecastMethods.creaForecastItems(trigger.New);
            
        }
        
    }else{
        //Is Before
        if(trigger.isInsert){
            FO_ForecastMethods.validateForecast(trigger.New);
        }
        if(trigger.isUpdate){
            FO_ForecastMethods.updateForecastCommitUpside(trigger.New,trigger.Old);
        }
    }
}