/************************************************************************************
 * Author: JAvier Lopez Andradas
 * Company: gCTIO
 * Description: TRigger for object FO_Forecast_Item__c
 * 
 * 
 * <date>		<version>		<description>
 * 02/04/2018	1.0				Initial
 * 
 * ***************************************************************************************/
trigger FO_ForecastItem  on FO_Forecast_Item__c (before update,after update,after delete,after insert) {
    
  if(trigger.isAfter){
        if(trigger.isUpdate){
           FO_ForecastItemMethods.updateForecastCommitUpside(trigger.New, trigger.Old);
           FO_ForecastItemMethods.updateForecastCloseDate(trigger.New, trigger.Old);
        }
        if(trigger.isDelete){
           FO_ForecastItemMethods.updateForecastCommitUpside(null, trigger.Old);
        } 
        
    }
}