/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   class for sharing object nodos
    Test Class:    BI_FVI_NodosShare_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    09/06/2016              Geraldine P. Montes     Initial Version
    17/06/2016              Alvaro sevilla          Added call to ColaborarNodosNAB method
    07/08/2016              Jorge Galindo           Encapsulate all BI functionality to avoid launching when is not a BI user
    12/08/2016              Humberto Nunes          Se a agregdo la variable Boolean FVI = userTGS.BI_FVI_Is_FVI__c y se ha usaado en el After Insert 
    16/09/2016              Humberto Nunes          Se hizo que la encapsulacion fuese para FVI o BI
    20/07/2017              Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    20/04/2018              Javier López            Added method changeOwnerActivateNode
    05/06/2018              Javier López            Added method FO_Estructura_Nodos_WOSharing.seekHierarchy//Added before insert and before update in trigger call 
    10/09/2018              Daniel Sánchez (everis) Added method createCartera 
    05/02/2019              Pablo de Andrés         Added method VEUserAssign from BI_LEX_FVI_NodosMethods
    15/05/2019              Federico Peña           Se agrega el metodo validacionesNodoActivo (before insert) para no dejar cambiar el propietario ni tipo de nodo si está activo o si alguna vez lo estuvo
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_FVI_Nodos on BI_FVI_Nodos__c (after update, after insert,before update,before insert,before delete) 
{
    // JGL 07/08/2016
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; // HN 12/08/2016
    // END JGL 07/08/2016
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_FVI_Nodos__c');
    List<E2PC_Cartera__c> carteraList = new List<E2PC_Cartera__c>();
    List<E2PC_Cartera__c> carteraListUpdate = new List<E2PC_Cartera__c>();
    if(!isTriggerDisabled){
     
        if(trigger.isAfter)
        {
            if(trigger.isUpdate)
            {
                if(FVI || BI) // HN 12/08/2016 & 16/09/2016
                {// JGL 07/08/2016
                    BI_FVI_NodosMethods.NAGtoNAV(trigger.new, trigger.old);
                    BI_FVI_NodosMethods.ColaborarNodosNAB(trigger.new, trigger.old);  
                    BI_FVI_NodosMethods.changeOwnerAccounts(trigger.new, trigger.old);  
                    BI_FVI_NodosMethods.changeOwnerActivateNode(trigger.new,trigger.old);
                }// END JGL 07/08/2016
               for(BI_FVI_Nodos__c a: trigger.new){
                    carteraList = [select Id, E2PC_Estado_de_PCA__c from E2PC_Cartera__c where E2PC_Nodo__c =: a.Id];
                    for(E2PC_Cartera__c c:carteraList){
                        if(a.BI_FVI_Activo__c==true){
                            c.E2PC_Estado_de_PCA__c = true;
                        }else{
                            c.E2PC_Estado_de_PCA__c = false;
                        }
                        carteraListUpdate.add(c);
                    }
                }
                update carteraListUpdate;
            }
            if (trigger.isInsert)
            {
                if(FVI || BI) // HN 12/08/2016 & 16/09/2016
                {// JGL 07/08/2016
                    BI_FVI_NodosMethods.NodoShare(trigger.new);
                    BI_FVI_NodosMethods.ColaborarNodosNAB(trigger.new, trigger.old);
                    BI_FVI_NodosMethods.createCartera(trigger.new, trigger.old);
                }// END JGL 07/08/2016
            }
        }else{
            //ISBefore
            if(trigger.isUpdate || trigger.isInsert){
                if(FVI || BI){
                    if(trigger.isInsert){
                        BI_LEX_FVI_NodosMethods.VEUserAssign(trigger.new, null);    
                    }
                    else{
                        BI_LEX_FVI_NodosMethods.VEUserAssign(trigger.new, trigger.old);
                        BI_FVI_NodosMethods.validacionesNodoActivo(trigger.new, trigger.oldMap);
                    }
                    
                   FO_Estructura_Nodos_WOSharing.seekHierarchy(trigger.new,trigger.old);
                }
            }
            if(trigger.isDelete){
                if(FVI || BI){
                    BI_LEX_FVI_NodosMethods.VEUserAssign(null, trigger.old);    
                }
            }
        }
        /*else // trigger.isBefore
        {

        }*/
        
    }   
}